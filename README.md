Bienvenue dans le dépôt de code source de [FILAÉ](https://filae.frama.io/) pour la planification, la rédaction et la communication des certificats de ses consultations médico-judiciaires. Ce dépôt a pour objectif de contenir toutes les ressources nécessaires au développement de cette solution logicielle libre.

## Comment contribuer ?

**Le code source n'est pas la seule contribution de valeur**. Avis, problèmes, guides d'utilisation… Entrez dans la danse et appréciez la construction collaborative de biens communs offerte par le logiciel libre.

Informations détaillées et coordonnées dans le [guide de contribution](CONTRIBUTING.md).

## Aspects techniques 🛠️

L'environnement de développement utilise [`docker`](https://www.docker.com/) et son plugin `docker-compose` (v2) intégré pour gérer les différents services nécessaires à sa bonne éxécution.

Le dossier `src` contient une application [_Ruby on Rails_](https://rubyonrails.org/) monolithe.

#### Base de données relationnelle

L'application utilise une base de données [PostgreSQL](https://www.postgresql.org/). Cette base de données renferme toutes les informations intrinsèquement relationnelles, notamment la configuration du mode de fonctionnement du service (domaines d'intervention, motifs de consultation, etc.), la gestion des utilisateurs et des habilitations…

#### Base de données orientée documents

Elle profite également des capacités "orientées documents" de PostgreSQL pour le stockage de documents JSON, version **expérimentale** des certificats et de leurs modèles.

### Tests

L'application utilise [`rspec`](https://github.com/rspec) pour toutes les suites de tests (unitaires, contrôleur, etc.) et plus précisément les [*system tests*](https://guides.rubyonrails.org/testing.html#system-testing) pour se rapprocher au maximum de l'interaction qu'une personne réelle pourrait avoir avec l'application, pilotant le navigateur Firefox au sein de son propre conteneur `docker`.

## Aspects documentaires 📚

Le dossier `doc` a pour vocation de contenir tous les guides nécessaires à l'installation, l'utilisation et le développement de Filaé. C'est un effort permanent qui ne demande pas mieux que d'être partagé 😇

Ainsi, vous y trouverez plus de détails sur comment [déployer](doc/01 Installation), [utiliser](doc/02 Utilisation), [développer](doc/03 Développement) Filaé… Il y même une documentation sur comment [documenter](doc).

À nos claviers !
