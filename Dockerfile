# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
ARG RUBY_VERSION
ARG DEBIAN_VERSION
FROM ruby:$RUBY_VERSION-slim-$DEBIAN_VERSION

ARG DEBIAN_VERSION
ARG DEBIAN_FRONTEND=noninteractive

# Minimal tools
RUN apt update -qq \
  && apt install -yq --no-install-recommends \
    build-essential \
    curl \
    git \
    gnupg2 \
    less \
    libyaml-dev \
    sudo \
    tmux \
    vim \
  && apt clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

# DB
RUN apt update -qq \
  && apt install -yq --no-install-recommends wget \
  && echo "deb http://apt.postgresql.org/pub/repos/apt $DEBIAN_VERSION-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && apt update -qq \
  && apt install -yq --no-install-recommends postgresql-server-dev-13 postgresql-client-13 \
  && apt clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

# LibreOffice
RUN apt update -qq \
  && apt install -yq --no-install-recommends \
    fontconfig libjpeg62-turbo libxrender1 xfonts-75dpi xfonts-base \
    libreoffice-writer \
  && apt clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

# ruby
RUN gem update --system && \
    rm -rf $GEM_HOME/cache/*

# NodeJS and Yarn
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
ARG NODEJS_VERSION
RUN apt update -qq \
  && apt install -yq --no-install-recommends \
    nodejs=$NODEJS_VERSION* \
  && apt clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log
RUN corepack enable

# App and user
ENV APP_DIR=/app/src \
  BUILD_DIR=/app

RUN mkdir -p $BUILD_DIR

ARG USER_NAME
ARG USER_ID
ARG GROUP_ID
ARG GROUP_NAME

RUN if [ "${USER_NAME}" != "root" ] ; then groupadd -o -g ${GROUP_ID} ${GROUP_NAME} \
  && useradd -o -l -u ${USER_ID} -g ${GROUP_NAME} -d ${APP_DIR} ${USER_NAME} \
  && usermod -aG sudo ${USER_NAME} \
  && echo "${USER_NAME} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${USER_NAME} \
  && chown ${USER_NAME}:${GROUP_NAME} ${BUILD_DIR} ; fi

USER ${USER_NAME}

WORKDIR ${BUILD_DIR}
