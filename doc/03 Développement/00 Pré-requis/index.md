# Développement et tests unitaires

Pour le développement et les tests unitaires nous sommes partis sur la solution [Docker](https://www.docker.com/) via [Docker Compose](https://docs.docker.com/compose/). Il est donc nécessaire d'installer Docker au préalable.

Pour lancer la solution, pour une démonstration par exemple :
1. récupérer le code
1. récupérer les images nécessaires et générer les images applicatives : `make build`
1. lancer la solution : `make up`
1. accéder à l'application : http://localhost:3000/
1. se connecter à un compte selon le rôle désiré :
   * les rôles disponibles sont listés dans ce [document](https://framagit.org/filae/src/-/blob/main/src/db/seeds/roles.rb) ;
   * trois comptes pour chaque rôle sont créés au démarrage , avec des identifiants de la forme `title`, `title2`, `title3` et comme mot de passe `Poiuyt123` ;
   * ainsi, vous pouvez par exemple vous connecter avec le couple identifiant/mot de passe `administrator`/`Poiuyt123` ou encore `nurse2`/`Poiuyt123`.

Pour lancer tous les tests les uns à la suite des autres : `make [test]`

Pour lancer les tests isolément :
1. lancer la solution : `make up`
1. se connecter au conteneur : `make connect`
1. lancer le ou les tests voulus, par ex. `bundle exec rspec spec/system/authentication_spec.rb:45`.

Concernant les *system tests*, ils sont joués via capybara/selenium/Firefox. Pour jouer toute la suite de tests la version *headless* est utilisée donc pas de visualisation possible. Si le jeu de tests ne concerne qu'un seul fichier de tests il est possible de visualiser l'avancée des tests en se connectant au conteneur Firefox avec l'URL http://localhost:7900/?autoconnect=1&resize=scale&password=secret.

Quelques outils complémentaires :
- [bundler-audit](https://github.com/rubysec/bundler-audit) (utilisé pour lister les failles de sécurité connues de certaines *gems*) : `make audit`.
