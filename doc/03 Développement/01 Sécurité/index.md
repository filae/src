## Matrice d'autorisations

### Légende

- :white_check_mark: : autorisé
- :gear: : configurable dans les paramètres
- :no_entry_sign: : refusé
- :heavy_minus_sign: : sans objet
- :clipboard: : dont il a la charge
- :syringe: : contenu médical (accessible à tout role sauf Visiteur, Association et Personnel du standard)

### Accès

| En tant que :arrow_right: <br>Je peux :arrow_down: | Administrateur | Sécrétariat | IDE | Médecin | Psychiatre | Psychiatre<br>Expert | Psychologue | Psychologue<br>Expert | Association | Personnel du<br>standard | Visiteur |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| **Authentification** |
| m'authentifier | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :white_check_mark: |
| me déconnecter | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :heavy_minus_sign: |
| réinitialiser mon mot de passe | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :heavy_minus_sign: | :white_check_mark: |
| **Acceuil** |
| afficher les domaines d'intervention | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :no_entry_sign: |
| afficher les types de consultation | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :no_entry_sign: |


### Configuration

| En tant que :arrow_right: <br>Je peux :arrow_down: | Administrateur | Sécrétariat | IDE | Médecin | Psychiatre | Psychiatre<br>Expert | Psychologue | Psychologue<br>Expert | Association | Personnel du<br>standard |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| **Configuration** | **Profil** |
| modifier mon mot de passe | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| **Configuration** | **Utilisateurs** |
| gérer les utilisateurs | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| [dés]activer un utilisateur | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| **Configuration** | **Interventions** |
| gérer les domaines d'intervention | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| gérer les types de consultation | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| gérer les sous-types de consultation | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| gérer les motifs de consultation | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| gérer les plannings | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| **Configuration** | **Services requérants** |
| gérer les secteurs | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| gérer les services requérants | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| **Configuration** | **Modèles de certificats** |
| ajouter un modèle de certificat | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| [dés]activer un modèle de certificat | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| **Configuration** | **Patients** |
| lister les patients | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: |
| modifier les patients | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: |

### Mode flux

| En tant que :arrow_right: <br>Je peux :arrow_down: | Administrateur | Sécrétariat | IDE | Médecin | Psychiatre | Psychiatre<br>Expert | Psychologue | Psychologue<br>Expert | Association | Personnel du<br>standard |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| afficher les consultations/messages actifs | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| ajouter un message/une consultation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :white_check_mark: |
| modifier un message/une consultation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :white_check_mark: |
| assigner un message/une consultation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :white_check_mark: |
| valider un message/une consultation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :white_check_mark: |
| annuler un message/une consultation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :white_check_mark: |
| afficher les archives | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| éditer une archive | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| afficher les fichiers standards | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| ajouter un fichier standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| supprimer un fichier standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| télécharger un fichier standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| afficher les fichiers médicaux | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| ajouter un fichier médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| supprimer un fichier médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| télécharger un fichier médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| afficher les certificats signés | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| ajouter un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| supprimer un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| télécharger un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| [dé]valider un certificat | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| télécharger un certificat validé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| envoyer un certificat validé par mail | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |

### Mode agenda

| En tant que :arrow_right: <br>Je peux :arrow_down: | Administrateur | Sécrétariat | IDE | Médecin | Psychiatre | Psychiatre<br>Expert | Psychologue | Psychologue<br>Expert | Association | Personnel du<br>standard |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| afficher le calendrier | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| afficher les archives | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| éditer une archive | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| affecter un créneau à un ou plusieurs médecins | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| créer un rendez-vous | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: |
| afficher un rendez-vous | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: |
| modifier un rendez-vous | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: |
| indiquer que le patient est arrivé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| prendre en charge une consultation | :gear: | :gear: | :gear: | :gear: | :gear: | :no_entry_sign: | :gear: | :gear: | :gear: | :gear: |
| reporter un rendez-vous | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: |
| annuler un rendez-vous | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :no_entry_sign: |
| lister les fichiers joint standards | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| ajouter un fichier joint standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| télécharger un fichier joint standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: | :gear: |
| supprimer un fichier joint standard | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :clipboard: | :gear: :clipboard: | :gear: :clipboard: | :gear: :clipboard: | :gear: :clipboard: | :gear: :clipboard: | :gear: :clipboard: |
| lister les fichiers joints médicaux | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :syringe: | :gear: :syringe: | :gear: :syringe: | :gear: :syringe: | :gear: :syringe: | :gear: :syringe: | :gear: :syringe: |
| ajouter un fichier joint médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: |
| télécharger un fichier joint médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: |
| supprimer un fichier joint médical | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: |
| lister les certificats signés | :white_check_mark: | :white_check_mark: | :white_check_mark: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: |
| ajouter un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: |
| télécharger un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: |
| télécharger l'aperçu du certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: | :syringe: |
| supprimer un certificat signé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: | :gear: :syringe: :clipboard: |
| [dé]valider un certificat | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: |
| télécharger le certificat validé | :white_check_mark: | :white_check_mark: | :white_check_mark: | :syringe: |:syringe: |:syringe: |:syringe: |:syringe: |:syringe: |:syringe: |
| supprimer le certificat validé | :white_check_mark: | :no_entry_sign: | :no_entry_sign: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: | :gear: :clipboard: :syringe: |
| envoyer le certificat validé par mail | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
| indiquer que le certificat validé a été envoyé (hors Filaé) | :white_check_mark: | :white_check_mark: | :white_check_mark: | :clipboard: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: | :no_entry_sign: |
