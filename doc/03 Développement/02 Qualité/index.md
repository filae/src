## Rattrapage des outrages Rubocop

Rubocop est arrivé tard dans le projet. Nous rattrapons petit à petit les outrages pré-existants ignorés grâce au [fichier généré](https://docs.rubocop.org/rubocop/configuration.html#automatically-generated-configuration) `.rubocop_todo.yml`. Pour ce faire, nous avons décidé de traiter, par ordre de priorité :

1. les outrages les plus fréquents (`make rubocop-top-offenses`) à correction automatique sure (`Safe Correctable`) ;
1. les outrages les plus fréquents à correction automatique non-sure (`Unsafe Correctable`) avec l’appui de notre couverture de code à plus de 95 % ;
1. les derniers outrages seront traités au cas par cas.

### Procédure recommandée

```terminal
$ make rubocop-top-offenses
[…]
717   FactoryBot/ConsistentParenthesesStyle [Safe Correctable]
372   Style/FrozenStringLiteralComment [Unsafe Correctable]
337   Capybara/ClickLinkOrButtonStyle
286   RSpec/ContextWording
283   Style/HashSyntax [Safe Correctable]
173   Layout/LineLength [Safe Correctable]
165   RSpec/NestedGroups
160   RSpec/ExampleLength
147   Capybara/NegationMatcher [Safe Correctable]
139   RSpec/MultipleExpectations
131   Layout/EmptyLinesAroundBlockBody [Safe Correctable]
127   RSpec/NamedSubject
104   RSpec/DescribedClass [Unsafe Correctable]
101   Capybara/CurrentPathExpectation [Safe Correctable]
91    Layout/SpaceInsideHashLiteralBraces [Safe Correctable]
88    Style/SymbolArray [Safe Correctable]
76    Lint/UselessAssignment [Safe Correctable]
73    RSpecRails/InferredSpecType [Unsafe Correctable]
58    Metrics/MethodLength
[…]
```

Dans notre exemple, `FactoryBot/ConsistentParenthesesStyle` est l’outrage le plus fréquent. Il faut donc supprimer tout le dictionnaire `FactoryBot/ConsistentParenthesesStyle` à la racine de `.rubocop_todo.yml` pour les corriger. Une fois que c’est fait, nous pouvons vérifier que nous retrouvons bien les 717 outrages avant de les corriger.

```
$ make rubocop
[…]
374 files inspected, 717 offenses detected, 717 offenses autocorrectable
[…]
$ make rubocop-autocorrect
[…]
374 files inspected, 765 offenses detected, 765 offenses corrected
```

N. B. : ici, on voit que plus d’outrages ont été corrigés que prévu. En effet, certaines autocorrections ont pu entraîner de nouveaux outrages qui ont eux aussi été corrigés si leur correction automatique était sure. Ici, par exemple, l’ajout de parenthèse a engendré des outrages à `Layout/LineLength` : `Line is too long. [121/120]`.

Il est alors temps de vérifier les modifications apportées, avec nos yeux et nos tests.

```
$ git diff
[… un bon paquet de changements à vérifier …]
$ make test
[… un bon paquet de tests à réussir …]
```

Si tout est bon, vous pouvez maintenant passer au `git add`, `git commit`, `git push`, et revue de la MR.
