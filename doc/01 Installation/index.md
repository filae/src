# Déploiement sur serveur

## Pré-requis

La solution Filaé est prévue pour s'exécuter dans un environnement Unix (testée sur Debian dans sa version `stable`, Ubuntu devrait fonctionner aussi sans problème).

Il vous faudra également un serveur Web pour exposer le sous-domaine (testé sur `nginx`, `Apache` devrait fonctionner aussi sans problème). Cela va sans dire, mais toutes ces communications Web doivent être chiffrées via TLS, en utilisant des certificats [`Let's Encrypt`](https://letsencrypt.org/) par exemple.

Filaé stocke ses données dans une base de données relationnelle : [`PostgreSQL`](https://www.postgresql.org/). Filaé a débuté avec la version `9.6`, pour s'exécuter actuellement avec la `13`.

## Application

L'application est développée avec [`Ruby on Rails`](https://rubyonrails.org/), un cadriciel qui produit une application Rack. Il faut ensuite exécuter cette application et l'exposer via votre serveur Web.

Il existe de nombreuses façons d'y arriver, celle ayant déjà fait ses preuves avec Filaé étant le couple [`nginx+Passenger`](https://www.phusionpassenger.com/docs/tutorials/deploy_to_production/installations/oss/ownserver/ruby/standalone/).

Il est également nécesaire d'installer [`nodejs`](https://nodejs.org/en) car l'application utilise quelques bibiliothèque javascript essentielles comme [FullCalendar](https://fullcalendar.io/) ou de manière **expérimentale** [SurveyJS](https://surveyjs.io/).

## Autres dépendances lâches

Filaé dépend aussi d'autres logiciels pour certaines fonctionnalités. Il s'agit notamment de `LibreOffice` et `wkhtmltopdf` pour la conversion des certificats en PDF.

## En résumé

```mermaid
flowchart LR
  subgraph Serveur
    subgraph Serveur Web
        APP
    end
    PostgreSQL[(PostgreSQL)]
    wkhtmltopdf
    LibreOffice
  end
  Navigateur -.-> APP
  APP -.-> PostgreSQL
  APP --> wkhtmltopdf
  APP --> LibreOffice
```

Les lignes pointillées représentent les communications via le réseau, les lignes pleines les exécutions locales.
