# Connexion à mon compte-utilisateur

Filaé est une application Web à accès réglementé. L'administrateur de la plateforme gère les comptes-utilisateurs et leur rôle associé. Pour vous connecter :

1. accédez à la page d'accueil de votre Filaé ;
1. saisissez le couple identifiant/mot de passe transmis, probablement par courriel ;
1. après votre première connexion, changez votre mot de passe dans votre profil.

![Mire de connexion](mire-de-connexion.png)
