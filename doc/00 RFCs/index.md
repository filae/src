# RFCs

## Présentation

Ce dossier contient toutes les *RFC*, cf. [*Request For Comments*](https://fr.wikipedia.org/wiki/Request_for_comments) concernant la solution Filaé.

Les RFC sont utilisées pour réfléchir en détails sur les évolutions **majeures** de Filaé, pour permettre d'avoir une vision la plus objective possible des tenants et aboutisants en vue de pouvoir prendre la meilleure décision possible.

Pour les évolution **mineures**, corrections de :bug: et autres réflexions, ouvrir un ticket prioritairement.

Dans tous les cas, il est recommandé de créer un ticket au préalable.

## Utilisation

Il existe un modèle à la racine de ce dossier, nommé `00-template.md`. Utiliser ce modèle, en changeant le numéro, pour créer une nouvelle RFC.

Chaque RFC est dans un des statuts suivants :
- `brouillon` :pencil: : travail en cours, tout le monde peut l'éditer, idéalement via une *merge request* ;
- `proposée` :hourglass_flowing_sand: : la réflexion est normalement terminée et une décision peut être prise ;
- `acceptée` :white_check_mark: : la mise en place de l'évolution peut avoir lieu en faisant référence à celle-ci ;
- `refusée` :octagonal_sign: : évolution rejetée mais à conserver dans le dossier pour archivage.

Le nom de chaque RFC devra se composer comme suit :
- `xx` : un numéro de RFC, dérivé de la précédente RFC en rajoutant `1` ;
- `ìntitulé` : une brève description de l'évolution attendue ;
- `statut` : cf. ci-dessus.

Par exemple : `42 - compatibilité base de données couchdb - proposée.md`

Le fichier de la RFC en lui-même devra également contenir, en début de fichier, le statut de celle-ci.
