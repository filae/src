# Documentation de *FILAÉ*

> L'application *FILAÉ* a pour objectif de faciliter la circulation du dossier-patient sécurisé en reliant les informations administratives et médicales sur un support informatique.

Parcourez les dossiers de ce répertoire pour retrouver ou ajouter l'information la plus pertinente sur le sujet qui occupe votre esprit.

## Différents manuels pour différents publics

1. [RFCs](00 RFCs) : *Request For Comments* de la solution Filaé ;
1. [Installation](01 Installation) : pour savoir comment déployer Filaé dans votre SI ;
1. [Utilisation](02 Utilisation) : pour en apprendre plus sur l'utilisation de Filaé au quotidien ;
1. [Développement](03 Développement) : pour connaître les dessous de l'architecture technique de Filaé et comment contribuer au code.

## Comment contribuer à la documentation

### Navigation dans les guides

Afin de rendre la navigation la plus cohérente possible, il est préférable de toujours faire des liens vers des dossiers, et non des fichiers. Gitlab présente une interface de lecture à la fois plus riche (boutons _Table des matières_, _Éditer_, etc.) et plus épurée (liste des dossiers/fichiers masquée) pour les fichiers mais…

Le fil d'Ariane ramène toujours à un dossier, et non un fichier. Ainsi, en systématisant les liens vers des dossiers avec `index.md`, Gitlab s'occupe de la présentation des sous-sections (liste des dossiers/* fichiers) et du contenu (`index.md`). Nous sommes sûrs que la navigation via l'interface de Gitlab et via nos liens internes apportent la même expérience.

### Réalisation de schémas

Gitlab intègre [`mermaid`](https://framagit.org/help/user/markdown#mermaid), un langage de descrition pour de nombreux styles de diagrammes : séquences, flux, classes, etc. Le code source est directement saisit au sein du texte de documentation, ce qui le rend facile à trouver et à modifier :

    ### Diagramme de séquence

    ```mermaid
    sequenceDiagram
        participant Victime
        participant Secrétariat

        Victime ->> Secrétariat: Prise de rendez-vous
    ```
