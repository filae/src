# Comment contribuer

Filaé est un logiciel libre et recueille avec intérêt tout avis ou contribution, n'hésitez donc pas à nous contacter.

Ce guide de contribution ne déroge pas à la règle 🙂

## Donner votre avis

Des questions ? Des remarques ? Des choses pas claires ? N'hésitez pas à soit :

1. [Créer un  ticket](https://framagit.org/filae/src/-/issues/new) (_issue_ en anglais) dans ce dépôt, où une discussion publique pourra s'engager ;
1. Nous écrire à **contact(arobase)filae.app**, nous vous répondrons alors directement.

Alors échangeons 💬

## Proposer du code

Il existe deux manières de fournir du code pour Filaé, la première étant préférée :

1. Fourcher (_fork_) le projet sur [Framagit](https://framagit.org/filae/src) et soumettre un [demande de fusion](https://git.framasoft.org/help/gitlab-basics/add-merge-request.md) (_merge request_) ;
2. Envoyer un correctif Git à **contact(arobase)filae.app**.

Dans tous les cas, veuillez respecter la recommandation suivante : **n'oubliez pas les tests**.

Votre correction aura bien plus de chances d'être intégrée à l'outil si ce qu'elle améliore est bien testé. Si vous n'êtes pas à l'aise avec la batterie de tests actuelle, n'hésitez pas à nous en faire part.

Pour savoir comment exécuter les tests, rendez-vous dans la [documentation de développement](doc/03 Développement/00 Pré-requis).
