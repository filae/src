# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
.DEFAULT_GOAL := test

include .env

# Life cycle
.PHONY: build up up-d stop ps restart
build:
	docker compose build --pull \
		--build-arg USER_ID=`id -u` \
		--build-arg USER_NAME=`id -nu` \
		--build-arg GROUP_ID=`id -g` \
		--build-arg GROUP_NAME=`id -ng` \
		--build-arg RUBY_VERSION=$(RUBY_VERSION) \
		--build-arg NODEJS_VERSION=$(NODEJS_VERSION) \
		--build-arg DEBIAN_VERSION=$(DEBIAN_VERSION)

up:
	docker compose up

up-d:
	docker compose up -d

stop:
	docker compose stop

down:
	docker compose down

ps:
	docker compose ps

restart:
	docker compose restart app jobs

# Access
connect:
	docker compose exec app bash -c 'cd src && bash'

# Useful for debugging
attach:
	@echo Attached… Press Ctrl-p Ctrl-q to quit.
	docker attach `docker compose ps | grep "\-app\-" | cut -d ' ' -f 1`

byebug:
	docker compose exec app bash -c 'cd src && bundle exec byebug -R 12345'

# Tests
.PHONY: test-app test-uat test
test-app:
	docker compose exec app bash -c 'cd src && bundle exec rspec spec'

test-uat:
	cd $(CURDIR)/web-uat && xvfb-run ./gwen features -b

test: up-d ps test-app stop

# Audit
.PHONY: audit rubocop rubocop-by-offenses rubocop-by-files rubocop-todo
audit: vulnerabilites check-credits

check-credits: credits
	git diff --exit-code Credits.md

rubocop: up-d
	docker compose exec app bash -c 'cd src && bundle exec rubocop'

rubocop-autocorrect: up-d
	docker compose exec app bash -c 'cd src && bundle exec rubocop -a'

rubocop-top-offenses: up-d
	sed -i 's/inherit_from: .rubocop_todo.yml/#inherit_from: .rubocop_todo.yml/' src/.rubocop.yml
	docker compose exec app bash -c 'cd src && bundle exec rubocop --format offenses' | head -n 20
	sed -i 's/#inherit_from: .rubocop_todo.yml/inherit_from: .rubocop_todo.yml/' src/.rubocop.yml

rubocop-top-files: up-d
	sed -i 's/inherit_from: .rubocop_todo.yml/#inherit_from: .rubocop_todo.yml/' src/.rubocop.yml
	docker compose exec app bash -c 'cd src && bundle exec rubocop --format worst' | head -n 20
	sed -i 's/#inherit_from: .rubocop_todo.yml/inherit_from: .rubocop_todo.yml/' src/.rubocop.yml

rubocop-todo: up-d
	docker compose exec app bash -c 'cd src && bundle exec rubocop --auto-gen-config --no-exclude-limit'

vulnerabilites:
	docker compose exec app bash -c 'cd src && bundle exec bundler-audit --update'

# Updates
.PHONY: update-gems
update-gems:
	docker compose exec app bash -c 'cd src && source .bashrc && BUMMR_HEADLESS=true bundle exec bummr update'

# License
.PHONY: license-fix license-check
license-fix:
	docker run -it --rm -v $(CURDIR):/github/workspace registry.infopiiaf.fr/license-eye:latest header fix

license-check:
	docker run -it --rm -v $(CURDIR):/github/workspace registry.infopiiaf.fr/license-eye:latest header check

credits:
	docker compose exec app bash -c 'cd src && bundle exec rails filae:credits'
