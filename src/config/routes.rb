# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
Rails.application.routes.draw do
  concern :attachable do
    resources :attached_files do
      get "download", on: :member
      get "preview", on: :member
      get "list", on: :collection
    end
  end

  shallow do
    resources :intervention_domains, except: :show do
      resources :consultation_types, except: %i[index show] do
        resources :consultation_subtypes, except: %i[index show]
        resources :consultation_reasons, except: %i[index show]
        resources :appointments, except: :destroy do
          patch :take_charge, on: :member
          get :conclude, on: :member
          get :postpone, on: :member
          resource :certificate_preview, only: %i[show]
          concerns :attachable
          get "archives", on: :collection
          get "reset_filter", on: :collection
        end
      end
      resources :schedules do
        resources :time_slots
      end
      resources :flowing_consultations, except: :destroy do
        collection do
          get "list"
          get "last_updated"
          get "archives"
          get "reset_filter"
        end
        member do
          get "assign"
          get "validate"
          get "cancel"
          get "certificates"
          post "certificate"
        end
        concerns :attachable
      end
      get "appointments"
    end

    resources :calling_areas do
      resources :calling_institutions
    end

    resources :appointments, except: [:index, :destroy] do
      post "certificate", on: :member
      concerns :attachable
      resources :complementary_appointments, only: [:create, :destroy]
    end

    resources :certificates, only: [:edit, :update, :destroy] do
      member do
        patch :toggle_validation
        get :mail_form
        patch :send_by_mail
      end
      concerns :attachable
    end
    resources :certificate_templates do
      patch :toggle_activation, on: :member
      resources :master_documents, only: %i[create destroy] do
        get :download
      end
    end

    resources :calendar_days, only: %i[index show], id: ISO8601_DATE_REGEXP
    resources :consultation_types, only: %i[index create]
    resources :duty_periods, only: %i[edit update]
    resources :medical_files, only: %i[edit update]
    resources :patients, only: %i[index show edit update] do
      get :consultations, on: :member
      resources :duplicates, only: %i[show destroy]
    end
    resources :schedules, only: %i[create]
    resources :time_slots, only: %i[create]
    resources :users do
      patch "activate", on: :member
      get "deactivate", on: :member
    end

    namespace :settings do
      resource :certificate_layout, only: %i[edit update]
    end
  end

  resource :session, only: [:new, :create, :destroy]
  resource :search, only: [] do
    get "calling_institutions"
    get "patients"
  end
  resource :profile, only: [:show, :update]

  root to: "home#show"
end
