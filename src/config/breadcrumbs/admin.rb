# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Admin
crumb :admin do
  link custom_link_name(:long, "Admin", :wrench)
  parent :home
end

## Profile
crumb :profile do
  link custom_link_name(:long, "Mon profil", "user-circle"), profile_path
  parent :admin
end

## Patients
crumb :patients do
  link custom_link_name(:long, "Patients", "hospital-user"), patients_path
  parent :admin
end

## Users
crumb :users do
  link custom_link_name(:long, "Utilisateurs", "users"), users_path
  parent :admin
end

crumb :new_user do
  link custom_link_name(:long, "Nouvel utilisateur", :plus), new_user_path
  parent :users
end

crumb :edit_user do |user|
  link custom_link_name(:long, "Édition de l'utilisateur: #{user.name}", :edit), edit_user_path(user)
  parent :users
end

## Interventions domains
crumb :intervention_domains do
  link "Interventions", intervention_domains_path
  parent :admin
end

crumb :new_intervention_domain do
  link custom_link_name(:long, "Nouveau domaine d'intervention", :plus), new_intervention_domain_path
  parent :intervention_domains
end

crumb :edit_intervention_domain do |intervention_domain|
  link custom_link_name(:long, "Édition de : #{intervention_domain.title}", :edit), edit_intervention_domain_path(intervention_domain)
  parent :intervention_domains
end

crumb :intervention_domain do |intervention_domain|
  link intervention_domain.title
  parent :intervention_domains
end

## Consultation types
crumb :new_consultation_type do |intervention_domain|
  link custom_link_name(:long, "Nouveau type de consultation", :plus), new_intervention_domain_consultation_type_path(intervention_domain)
  parent :intervention_domain, intervention_domain
end

crumb :edit_consultation_type do |consultation_type|
  link custom_link_name(:long, "Édition du type de consultation : #{consultation_type.title}", :edit), edit_consultation_type_path(consultation_type)
  parent :intervention_domain, consultation_type.intervention_domain
end

crumb :consultation_type do |consultation_type|
  link consultation_type.title
  parent :intervention_domain, consultation_type.intervention_domain
end

## Consultation subtypes
crumb :new_consultation_subtype do |consultation_type|
  link custom_link_name(:long, "Nouveau sous-type de consultation", :plus), new_consultation_type_consultation_subtype_path(consultation_type)
  parent :consultation_type, consultation_type
end

crumb :edit_consultation_subtype do |consultation_subtype|
  link custom_link_name(:long, "Édition du sous-type de consultation : #{consultation_subtype.title}", :edit), edit_consultation_subtype_path(consultation_subtype)
  parent :consultation_type, consultation_subtype.consultation_type
end

## Consultation reasons
crumb :new_consultation_reason do |consultation_type|
  link custom_link_name(:long, "Nouveau motif de consultation", :plus), new_consultation_type_consultation_reason_path(consultation_type)
  parent :consultation_type, consultation_type
end

crumb :edit_consultation_reason do |consultation_reason|
  link custom_link_name(:long, "Édition du motif de consultation : #{consultation_reason.title}", :edit), edit_consultation_reason_path(consultation_reason)
  parent :consultation_type, consultation_reason.consultation_type
end

## Schedules
crumb :new_schedule do |intervention_domain|
  link custom_link_name(:long, "Nouveau planning", :plus), new_intervention_domain_schedule_path(intervention_domain)
  parent :intervention_domain, intervention_domain
end

crumb :edit_schedule do |schedule|
  link custom_link_name(:long, "Édition du planning : #{localize_date(schedule.activated_on)}", :edit), edit_schedule_path(schedule)
  parent :intervention_domain, schedule.intervention_domain
end

## Calling insitutions
crumb :calling_institutions do
  link custom_link_name(:long, "Services requérants", "user-secret"), calling_areas_path
  parent :admin
end

crumb :new_calling_area do
  link custom_link_name(:long, "Nouveau secteur", :plus), new_calling_area_path
  parent :calling_institutions
end

crumb :edit_calling_area do |calling_area|
  link custom_link_name(:long, "Édition du secteur : #{calling_area.title}", :edit), edit_calling_area_path(calling_area)
  parent :calling_institutions
end

crumb :calling_area do |calling_area|
  link calling_area.title
  parent :calling_institutions
end

crumb :new_calling_institution do |calling_area|
  link custom_link_name(:long, "Nouveau service requérant", :plus), new_calling_area_calling_institution_path(calling_area)
  parent :calling_area, calling_area
end

crumb :edit_calling_institution do |calling_institution|
  link custom_link_name(:long, "Édition du service requérant : #{calling_institution.title}", :edit), edit_calling_institution_path(calling_institution)
  parent :calling_area, calling_institution.calling_area
end

## Certificate templates
crumb :certificate_templates do
  link custom_link_name(:long, "Modèles de certificat", "file-alt"), certificate_templates_path
  parent :admin
end

crumb :new_certificate_template do
  link custom_link_name(:long, "Nouveau modèle de certificat", :plus), new_certificate_template_path
  parent :certificate_templates
end

crumb :edit_certificate_template do |certificate_template|
  link custom_link_name(:long, "Édition du modèle de certificat : #{certificate_template.title}", :edit), edit_certificate_template_path(certificate_template)
  parent :certificate_templates
end
