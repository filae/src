# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Flowing consultations
crumb :appointments do |consultation_type|
  link custom_link_name(:long, consultation_type.title, :calendar), consultation_type_appointments_path(consultation_type)
  parent :home
end

crumb :new_appointment do |consultation_type|
  link custom_link_name(:long, "Nouveau rendez-vous", :plus), new_consultation_type_appointment_path(consultation_type)
  parent :appointments, consultation_type
end

crumb :edit_appointment do |appointment|
  link custom_link_name(:long, "Édition du rendez-vous : #{appointment.patient&.fullname} - #{localize_slot(appointment)}", :edit), edit_appointment_path(appointment)
  parent :appointments, appointment.consultation_type
end

crumb :edit_certificate do |certificate|
  link custom_link_name(:long, "Certificat", :file)
  parent :appointments, certificate.certificatable.consultation_type
end

crumb :appointment_archives do |consultation_type|
  link custom_link_name(:long, "Archives", :archive)
  parent :appointments, consultation_type
end

crumb :appointments_of_day do |date|
  link custom_link_name(:long, "Consultation du Jour", :columns)
  parent :home
end
