# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Flowing consultations
crumb :flowing_consultations do |intervention_domain|
  link custom_link_name(:long, intervention_domain.title, :list), intervention_domain_flowing_consultations_path(intervention_domain)
  parent :home
end

crumb :new_flowing_consultation do |intervention_domain, is_message|
  title = (is_message ? "Nouveau message" : "Nouvelle consultation")
  link custom_link_name(:long, title, :plus), new_intervention_domain_flowing_consultation_path(intervention_domain)
  parent :flowing_consultations, intervention_domain
end

crumb :edit_flowing_consultation do |flowing_consultation|
  title = (flowing_consultation.simple_message? ? "Édition du message" : "Édition de la consultation")
  title += (flowing_consultation.onml_id.present? ? " : ##{flowing_consultation.onml_id}" : "")
  link custom_link_name(:long, title, :edit), edit_flowing_consultation_path(flowing_consultation)
  parent :flowing_consultations, flowing_consultation.intervention_domain
end

crumb :flowing_consultation_archives do |intervention_domain|
  link custom_link_name(:long, "Archives", :archive)
  parent :flowing_consultations, intervention_domain
end
