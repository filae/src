# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
[
  { "title": "administrator", "medical_confidentiality": true },
  { "title": "physician", "medical_confidentiality": true },
  { "title": "nurse", "medical_confidentiality": true },
  { "title": "secretary", "medical_confidentiality": true },
  { "title": "psychiatrist", "medical_confidentiality": true },
  { "title": "psychiatrist_expert", "medical_confidentiality": true },
  { "title": "psychologist", "medical_confidentiality": true },
  { "title": "psychologist_expert", "medical_confidentiality": true },
  { "title": "switchboard_operator", "medical_confidentiality": false },
  { "title": "association", "medical_confidentiality": false }
].each do |role|
  Role.create!(
    title: role[:title],
    medical_confidentiality: role[:medical_confidentiality]
  )
end
