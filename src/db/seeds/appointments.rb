# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
officers = FactoryBot.create_list(:officer, 5)
consultation_type = ConsultationType.find_by(title: "Consultation médicale");

# Duty periods assignment
pivot_date = Date.current
((pivot_date - 3.days)..(pivot_date + 3.days)).map do |date|
  Schedule.duty_periods_for(consultation_type, date)
end
physicians = User.where(role: Role.find_by_title(:physician))
DutyPeriod.all.each do |duty_period|
  nb_phys = (1..3).to_a.sample
  duty_period.update(assignees: physicians.sample(nb_phys))
end

# Appointments
40.times do
  starts_at = (Time.current + Random.rand(-5..5).days).change(hour: Random.rand(9..16))
  duration = (30..90).step(15).to_a.sample.minutes
  (cancelled_at, cancelled_by) = ([true, false].sample ? [Time.zone.now, Appointment.cancelled_bies.keys.sample] : [nil, nil])

  patient = FactoryBot.build(:patient)

  Appointment.create!(
    consultation_type: consultation_type,
    consultation_reason_id: consultation_type.consultation_reason_ids.sample,
    calling_institution_id: CallingInstitution.ids.sample,
    cancelled_at: cancelled_at,
    cancelled_by: cancelled_by,
    starts_at: starts_at,
    duration: duration,
    patient: patient,
    patient_phone: Faker::PhoneNumber.phone_number,
    officer: officers.sample
  )
end
