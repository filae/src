# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
HOLIDAYS_YEARS ||= (2018..2030)
NationalHoliday.destroy_all
fixed_holidays_specs = {
  "Jour de l'an": [1, 1],
  "Fête du Travail": [5, 1],
  "Victoire 1945": [5, 8],
  "Fête nationale": [7, 14],
  "Assomption": [8, 15],
  "Toussaint": [11, 1],
  "Armistice 1918": [11, 11],
  "Noël": [12, 25]
}
fixed_holidays = fixed_holidays_specs.map do |title, month_day|
  HOLIDAYS_YEARS.map do |year|
    date_components = [year] + month_day
    NationalHoliday.create!(title: title, date: Date.new(*date_components))
  end
end.flatten

moving_holidays_specs = {
  "Lundi de Pâques": [
    Date.new(2018, 4, 2),
    Date.new(2019, 4, 22),
    Date.new(2020, 4, 13),
    Date.new(2021, 4, 5),
    Date.new(2022, 4, 18),
    Date.new(2023, 4, 10),
    Date.new(2024, 4, 1),
    Date.new(2025, 4, 21),
    Date.new(2026, 4, 6),
    Date.new(2027, 3, 29),
    Date.new(2028, 4, 17),
    Date.new(2029, 4, 2),
    Date.new(2030, 4, 22),
  ],
  "Jeudi de l'Ascension": [
    Date.new(2018, 5, 10),
    Date.new(2019, 5, 30),
    Date.new(2020, 5, 21),
    Date.new(2021, 5, 13),
    Date.new(2022, 5, 26),
    Date.new(2023, 5, 18),
    Date.new(2024, 5, 9),
    Date.new(2025, 5, 29),
    Date.new(2026, 5, 14),
    Date.new(2027, 5, 6),
    Date.new(2028, 5, 25),
    Date.new(2029, 5, 10),
    Date.new(2030, 5, 30),
  ],
  "Lundi de Pentecôte": [
    Date.new(2018, 5, 21),
    Date.new(2019, 6, 10),
    Date.new(2020, 6, 1),
    Date.new(2021, 5, 24),
    Date.new(2022, 6, 6),
    Date.new(2023, 5, 28),
    Date.new(2024, 5, 19),
    Date.new(2025, 6, 8),
    Date.new(2026, 5, 24),
    Date.new(2027, 5, 16),
    Date.new(2028, 6, 4),
    Date.new(2029, 5, 20),
    Date.new(2030, 6, 9),
  ]
}
moving_holidays = moving_holidays_specs.map do |title, dates|
  dates.map do |date|
    NationalHoliday.create!(title: title, date: date)
  end
end.flatten

holidays_count = (fixed_holidays + moving_holidays).count
expected_count = HOLIDAYS_YEARS.count * (fixed_holidays_specs.keys + moving_holidays_specs.keys).count
raise "Wrong number of national holidays: #{holidays_count} vs #{expected_count} expected !" unless expected_count == holidays_count
