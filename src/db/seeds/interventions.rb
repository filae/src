# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Consultations
InterventionDomain.create!(
  title: "Consultation",
  modus_operandi: :appointments,
  identifier_prefix: "CONS"
).tap do |domain|
  medical_ct = ConsultationType.create!(
    intervention_domain: domain,
    title: "Consultation médicale",
    role_ids: Role.where(title: %w[administrator physician nurse secretary psychiatrist psychologist association]).ids,
    assigned_role_ids: Role.where(title: %w[administrator physician]).ids,
  ).tap do |ct|
    ConsultationReason.create!(
      consultation_type: ct,
      title: "Violence volontaire",
      code: "VV",
      colour: "#00AFDF",
      appointment_duration: "PT1H"
    )
    ConsultationReason.create!(
      consultation_type: ct,
      title: "Agression sexuelle",
      code: "AS",
      colour: "#00DFAF",
      appointment_duration: "PT45M"
    )
  end
  psy_ct = ConsultationType.create!(
    intervention_domain: domain,
    title: "Psychologue",
    role_ids: Role.where(title: %w[administrator psychologist]).ids,
    assigned_role_ids: Role.where(title: %w[administrator psychologist]).ids
  )
  asso_ct = ConsultationType.create!(
    intervention_domain: domain,
    title: "Association",
    role_ids: Role.where(title: %w[administrator association]).ids,
    assigned_role_ids: Role.where(title: %w[administrator association]).ids
  )
  medical_ct.complementary_consultation_types << [psy_ct, asso_ct]
  ConsultationType.create!(
    intervention_domain: domain,
    title: "Psychiatre",
  )
  ConsultationType.create!(
    intervention_domain: domain,
    title: "Audition/Expertise psy",
  )
  ConsultationType.create!(
    intervention_domain: domain,
    title: "Expertise",
  )
  Schedule.create!(
    intervention_domain: domain,
    activated_on: Date.today - 2.days,
  ).tap do |schedule|
    ConsultationType.where(intervention_domain: domain).each do |consultation_type|
      (-2..2).each do |offset|
        TimeSlot.create!(
          week_day: (Date.today + offset.send(:days)).cwday,
          starts_at: "T09:00",
          ends_at: "T13:00",
          schedule: schedule,
          consultation_type: consultation_type
        )
        TimeSlot.create!(
          week_day: (Date.today + offset.send(:days)).cwday,
          starts_at: "T14:00",
          ends_at: "T18:00",
          schedule: schedule,
          consultation_type: consultation_type
        )
      end
    end
  end
end

# Antenne
InterventionDomain.create!(
  title: "Antenne",
  modus_operandi: :stream,
  identifier_prefix: "AN",
  role_ids: Role.where(title: %w[administrator physician secretary switchboard_operator]).ids
).tap do |domain|
  [
    "GAV",
    "Levée de corps",
    "ITT PDAP commissariat",
    "ITT site extérieur",
    "AS"
  ].each do |title|
    ConsultationType.create!(
      intervention_domain: domain,
      title: title
    )
  end
end

# Sur dossier
InterventionDomain.create!(
  title: "Sur dossier",
  modus_operandi: :stream,
  identifier_prefix: "DOS",
  role_ids: Role.where(title: %w[administrator physician secretary]).ids
).tap do |domain|
  ConsultationType.create!(
    intervention_domain: domain,
    title: "Certificat sur dossier"
  )
end
