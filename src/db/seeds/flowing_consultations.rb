# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
intervention_domain = InterventionDomain.find_by(modus_operandi: "stream");
physician_role = Role.find_by_title(:physician)
physicians = User.where(role: physician_role)
current_year = Date.today.year

# Flowing consultations
20.times do
  called_at =Random.rand(1..24).hours.ago
  assigned_at = [called_at + 5.minutes, nil].sample
  closed_at = called_at + 30.minutes
  cancelled_at = [closed_at, nil].sample
  validated_at = !cancelled_at && assigned_at && [closed_at, nil].sample
  assignee = physicians.sample if assigned_at
  comment = [Faker::Lorem.sentence, nil].sample
  officer = FactoryBot.build :officer

  if [true, false].sample
    patient = FactoryBot.create(:patient, birthdate: nil, birth_year: Random.rand(1900..current_year))
    concerns_minor = (current_year - patient.birth_year) < 18
  else
    patient = FactoryBot.create(:patient)
    concerns_minor = (Date.today - patient.birthdate).days.in_years < 18
  end

  FlowingConsultation.create!(
    intervention_domain: intervention_domain,
    consultation_type_id: intervention_domain.consultation_type_ids.sample,
    calling_institution_id: CallingInstitution.ids.sample,
    called_at: called_at,
    assigned_at: assigned_at,
    validated_at: validated_at,
    cancelled_at: cancelled_at,
    patient: patient,
    officer: officer,
    assignee: assignee,
    comment: comment,
    concerns_minor: concerns_minor
  )
end

# Messages
10.times do
  called_at =Random.rand(1..24).hours.ago
  assigned_at = [called_at + 5.minutes, nil].sample
  closed_at = called_at + 30.minutes
  cancelled_at = [closed_at, nil].sample
  validated_at = !cancelled_at && assigned_at && [closed_at, nil].sample
  assignee = physicians.sample if assigned_at
  comment = Faker::Lorem.sentence
  officer = FactoryBot.build :officer, :lambda_user

  FlowingConsultation.create!(
    intervention_domain: intervention_domain,
    called_at: called_at,
    assigned_at: assigned_at,
    validated_at: validated_at,
    cancelled_at: cancelled_at,
    officer: officer,
    assignee: assignee,
    comment: comment,
  )
end
