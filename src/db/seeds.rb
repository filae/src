# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Mandatory for PostgreSQL
# May not work properly under other RDMS
connection = ActiveRecord::Base.connection
connection.execute("ALTER DATABASE \"#{connection.current_database}\" SET IntervalStyle = iso_8601")
connection.reconnect! # Necessary else previous command not taken into account at first tests iteration

def require_seeds(seeds_files)
  seeds_files.each do |filename|
    puts "Seeding #{filename.humanize}…" unless Rails.env.test?
    require_relative "seeds/#{filename}.rb"
  end
end

# For all environments
require_seeds([
  "roles",
  "national_holidays",
  "certificate_layout"
])

if Rails.env.development? || ENV["FORCE_SEEDS"].present?
  require_seeds([
    "certificate_templates",
    "users",
    "interventions",
    "calling_institutions",
    "flowing_consultations",
    "appointments"
  ])
end
