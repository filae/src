# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CreateMedicalFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_files do |t|
      t.text :address
      t.string :chaperon_name
      t.string :chaperon_quality
      t.string :phone_number
      t.boolean :placed
      t.string :actor_name
      t.text :obs_physician
      t.text :obs_nurse
      t.text :obs_psychologist
      t.text :obs_general
      t.references :appointment

      t.timestamps
    end
  end
end
