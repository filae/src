# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ChangeCalledAtNullBehaviorToFlowingConsultations < ActiveRecord::Migration[7.0]
  def up
    execute("UPDATE flowing_consultations SET called_at = created_at WHERE called_at IS NULL")
    change_column_null :flowing_consultations, :called_at, false
  end

  def down
    change_column_null :flowing_consultations, :called_at, true
  end
end
