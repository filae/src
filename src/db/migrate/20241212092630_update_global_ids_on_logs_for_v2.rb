# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class UpdateGlobalIdsOnLogsForV2 < ActiveRecord::Migration[7.0]
  def up
    execute "UPDATE logs SET who_id = replace(who_id, 'gid://web-api', 'gid://src')"
    execute "UPDATE logs SET what_id = replace(what_id, 'gid://web-api', 'gid://src')"
  end

  def down
    execute "UPDATE logs SET who_id = replace(who_id, 'gid://src', 'gid://web-api')"
    execute "UPDATE logs SET what_id = replace(what_id, 'gid://src', 'gid://web-api')"
  end
end
