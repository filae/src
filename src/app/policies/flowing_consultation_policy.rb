# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultationPolicy < ApplicationPolicy
  # TODO
  def last_updated?
    intervention_domain_allowed? && can_manage?
  end

  def new?
    intervention_domain_allowed? && can_manage?
  end

  def create?
    new?
  end

  def edit?
    intervention_domain_allowed? &&
      (record.archived? ? can_manage? && is_adm_staff? : can_manage?)
  end

  def update?
    edit?
  end

  def assign?
    intervention_domain_allowed? && can_manage?
  end

  def cancel?
    intervention_domain_allowed? && can_manage?
  end

  def validate?
    intervention_domain_allowed? && can_manage?
  end

  def list_certificates?
    list_attached_files?
  end

  def certificate?
    list_attached_files?
  end

  def list_attached_files?
    intervention_domain_allowed? &&
      ( is_adm_staff? || physician_user? )
  end

  class Scope < Scope
    def resolve
      can_manage? ? scope : scope.none
    end

    def can_manage?
      whitelist_roles([
        "administrator",
        "physician",
        "nurse",
        "secretary",
        "switchboard_operator"
      ])
    end
  end

  def permitted_attributes
    return [] unless can_manage?

    [
      :id, :called_at, :comment, :concerns_minor,
      :consultation_type_id, :calling_institution_id,
      :assignee_id, :assigned_at,
      :patient_id,
      :cancelled_at, :cancelled_by,
      :validated_at,
      officer_attributes: [:id, :name, :phone, :email, :lambda_user],
      patient_attributes: [:id, :last_name, :first_name, :birthdate, :gender]
    ]
  end

  private

  def can_manage?
     whitelist_roles([
      "administrator",
      "physician",
      "nurse",
      "secretary",
      "switchboard_operator"
    ])
  end

  def intervention_domain_allowed?
    policy(record.intervention_domain).show?
  end
end
