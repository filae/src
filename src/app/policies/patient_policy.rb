# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class PatientPolicy < ApplicationPolicy
  def index?
    medical_content_access?
  end

  def show?
    medical_content_access? || switchboard_operator_user? # to display on flowing consultation form
  end

  def edit?
    medical_content_access?
  end

  def update?
    medical_content_access?
  end

  def consultations?
    index?
  end

  def permitted_attributes
    allowed_attributes = []
    if medical_content_access?
      allowed_attributes += %i[
        birth_year
        birthdate
        duplicated_patient_id
        first_name
        gender
        last_name
        probably_duplicated_patient_id
      ]
    end
    allowed_attributes
  end

  class Scope < Scope
    def resolve
      if medical_content_access? || switchboard_operator_user? # to create flowing consultations
        scope
      else
        scope.none
      end
    end
  end
end
