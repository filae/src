# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFilePolicy < ApplicationPolicy
  def list?
    has_access_to_attachable?
  end

  def download?
    list?
  end

  def preview?
    list?
  end

  def create?
    list?
  end

  def destroy?
    list?
  end

  private

  def has_access_to_attachable?
    case (attachable = record.attachable)
    when Appointment
      is_adm_staff? || policy(attachable).show?
    when FlowingConsultation
      is_adm_staff? || physician_user?
    when Certificate
      policy(attachable.certificatable).list_certificates?
    end
  end

  def concerns_appointment?
    case (attachable = record.attachable)
    when Appointment
      true
    when Certificate
      attachable.certificatable.is_a?(Appointment)
    end
  end

  def concerns_flowing_consultation?
    case (attachable = record.attachable)
    when FlowingConsultation
      true
    when Certificate
      attachable.certificatable.is_a?(FlowingConsultation)
    end
  end

  def user_in_charge?
    case (attachable = record.attachable)
    when Appointment, FlowingConsultation
      attachable.user_in_charge == user
    when Certificate
      attachable.certificatable.user_in_charge == user
    end
  end
end
