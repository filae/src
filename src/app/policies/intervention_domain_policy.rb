# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class InterventionDomainPolicy < ApplicationPolicy
  def index?
    administrator_user?
  end

  def archives_index?
    record.modus_operandi == "stream" &&
      show? &&
      (is_adm_staff? || physician_user?)
  end

  def show?
    case record.modus_operandi
    when "appointments"
      record.consultation_types.map { |ct| ConsultationTypePolicy.new(user, ct).show? }.any?
    when "stream"
      record.roles.include?(user.role)
    end
  end

  def new?
    administrator_user?
  end

  def create?
    administrator_user?
  end

  def edit?
    administrator_user?
  end

  def update?
    administrator_user?
  end

  def destroy?
    administrator_user?
  end

  class Scope < Scope
    def resolve
      administrator_user? ? scope : scope.none
    end
  end

  def permitted_attributes
    administrator_user? ? [:title, :identifier_prefix, :modus_operandi, role_ids: []] : []
  end
end
