# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module PolicyHelpers
  private

  def connected_user?
    user.connected?
  end

  Role::ALL.each do |role|
    define_method "#{role}_user?" do
      connected_user? && user.send(:has_role?, role)
    end
  end

  def whitelist_roles(roles)
    roles.any? { |role| send "#{role}_user?" }
  end

  def medical_content_access?
    user&.role&.medical_confidentiality?
  end

  def is_adm_staff?
    whitelist_roles([
      "administrator",
      "nurse",
      "secretary"
    ])
  end

  def policy(record)
    Pundit.policy!(user, record)
  end
end

class ApplicationPolicy
  attr_reader :user, :record
  include PolicyHelpers

  def initialize(user, record)
    raise Pundit::InvalidConstructorError, "user must be set" unless user
    @user = user
    @record = record
  end

  class Scope
    attr_reader :user, :scope
    include PolicyHelpers

    def initialize(user, scope)
      raise Pundit::InvalidConstructorError, "user must be set" unless user
      @user = user
      @scope = scope
    end

    def resolve
      scope.none
    end
  end
end
