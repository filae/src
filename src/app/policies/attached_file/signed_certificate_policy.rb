# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFile::SignedCertificatePolicy < AttachedFilePolicy
  def list?
    super && medical_content_access?
  end

  def create?
    list? && (
      (concerns_appointment? && (is_adm_staff? || user_in_charge?)) ||
      (concerns_flowing_consultation? && (is_adm_staff? || (physician_user? && user_in_charge?)))
    )
  end

  def destroy?
    create?
  end
end
