# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AppointmentPolicy < ApplicationPolicy
  def show?
    consultation_type_allowed? && can_manage?
  end

  def new?
    consultation_type_allowed? && can_manage?
  end

  def create?
    new?
  end

  def edit?
    consultation_type_allowed? && permitted_attributes.any?
  end

  def edit_archived?
    consultation_type_allowed? && is_adm_staff?
  end

  def update?
    edit?
  end

  def postpone?
    new?
  end

  def take_charge?
    update? && has_assigned_role?
  end

  def conclude?
    take_charge?
  end

  def list_certificates?
    is_adm_staff? || medical_content_access?
  end

  def certificate?
    update?
  end

  def list_attached_files?
    is_adm_staff? || consultation_type_allowed?
  end

  class Scope < Scope
    def resolve
      can_manage? ? scope : scope.none
    end

    private

    def can_manage?
      whitelist_roles([
        "administrator",
        "association",
        "nurse",
        "physician",
        "psychiatrist",
        "psychologist",
        "psychologist_expert",
        "secretary"
      ])
    end
  end

  def permitted_attributes
    attributes = []
    if can_manage?
      attributes += [
        :id, :duration, :emergency,
        :circumstances, :comment, :official_report,
        :cancelled_at, :cancelled_by,
        :calling_institution_id,
        :consultation_type_id,
        :consultation_subtype_id,
        :consultation_reason_id,
        :officer_id,
        :originating_appointment_id,
        :patient_arrived_at,
        :patient_id,
        :patient_missing,
        :patient_phone,
        :patient_released_at,
        :previous_appointment_id,
        :requisition_received_at,
        :starts_at,
        officer_attributes: [:id, :name, :phone, :email],
        patient_attributes: [:id, :last_name, :first_name, :birthdate, :gender]
      ]
      attributes << :user_in_charge_id if has_assigned_role?
    end
    attributes
  end

  private

  def can_manage?
    whitelist_roles([
      "administrator",
      "association",
      "nurse",
      "physician",
      "psychiatrist",
      "psychologist",
      "psychologist_expert",
      "secretary"
    ])
  end

  def has_assigned_role?
    !record.is_a?(Class) && record.consultation_type&.assigned_roles&.include?(user.role)
  end

  def consultation_type_allowed?
    policy(record.consultation_type).show?
  end
end
