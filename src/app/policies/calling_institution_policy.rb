# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CallingInstitutionPolicy < ApplicationPolicy
  def new?
    administrator_user?
  end

  def create?
    administrator_user?
  end

  def edit?
    administrator_user?
  end

  def update?
    administrator_user?
  end

  def destroy?
    administrator_user? &&
      record.appointments.empty? &&
      record.flowing_consultations.empty?
  end

  class Scope < Scope
    def resolve
      # needed when creating appointment or flowing consultation
      connected_user? ? scope : scope.none
    end
  end

  def permitted_attributes
    administrator_user? ? [:title, :calling_area_id] : []
  end
end
