# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CalendarDaysController < ApplicationController
  before_action :set_consultation_type
  before_action :set_intervention_domain, only: :index
  before_action :set_context_params, only: :index
  before_action :authorize_context

  def index
    skip_policy_scope
    @pivot_date = begin
                    Date.parse((params[:pivot_date].present? ? params[:pivot_date] : session[:pivot_date]))
                  rescue ArgumentError, TypeError => e
                    logger.warn("Error while parsing date: #{e}")
                    Date.current
                  end
    session["pivot_date"] = @pivot_date
    session["include_cancelled"] = ActiveModel::Type::Boolean.new.cast(params[:include_cancelled]) if params[:include_cancelled].present?
    session["assignment_mode"] = ActiveModel::Type::Boolean.new.cast(params[:assignment_mode]) if params[:assignment_mode].present?
    interval = (@consultation_type.present? ? 2 : 1)
    @previous_pivot_date = interval.business_days.before(@pivot_date)
    @next_pivot_date = interval.business_days.after(@pivot_date)
    @days = ((@previous_pivot_date)..(@next_pivot_date)).reject { |date| date.on_weekend? }.map { |date| date.iso8601 }
    @consultation_types = policy_scope(@intervention_domain.consultation_types) if @intervention_domain
  end

  def show
    date = begin
             Date.parse(params[:id])
           rescue ArgumentError, TypeError => e
             logger.warn("Error while parsing date: #{e}")
             Date.current
           end
    last_appointment = Appointment.of_consultation_type(@consultation_type).scheduled_on(date).order(updated_at: :desc).limit(1).first
    if params[:since].present? && last_appointment.present? && Time.zone.iso8601(params[:since]) > last_appointment.updated_at
      head :no_content
    else
      duty_periods = Schedule.duty_periods_for(@consultation_type, date)
      appointments = if session["assignment_mode"]
                       []
                     else
                       all_appointments = Appointment.of_consultation_type(@consultation_type).scheduled_on(date)
                       session["include_cancelled"] ? all_appointments : all_appointments.not_cancelled
                     end
      @calendar_day = Calendars::Day.new(date: date, duty_periods: duty_periods, appointments: appointments, assignment_mode: session["assignment_mode"])
      @title = case params[:title]
               when "day"
                 I18n.l date, format: :calendar_day
               when "consultation_type"
                 @consultation_type.title
               else ""
               end
    end
  end

  private

  def set_consultation_type
    @consultation_type = ConsultationType.find(params["consultation_type_id"]) if params["consultation_type_id"]
  end

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find(params["intervention_domain_id"]) if params["intervention_domain_id"]
  end

  def set_context_params
    @context_params = params.permit(:consultation_type_id, :intervention_domain_id)
  end

  def authorize_context
    context = @consultation_type || @intervention_domain
    authorize context, :show?
  end
end
