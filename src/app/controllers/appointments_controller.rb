# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AppointmentsController < ApplicationController
  before_action :set_consultation_type, only: [:index, :archives, :reset_filter, :new, :create]
  before_action :set_appointment, only: [:show, :edit, :update, :take_charge, :conclude, :certificate]
  before_action :set_locale, only: [:certificate_preview]
  before_action :set_filter, only: [:archives, :reset_filter]

  MAX_DISPLAYABLE_ARCHIVES = 200

  def index
    authorize @consultation_type, :show?
    skip_policy_scope
  end

  def archives
    authorize @consultation_type, :archives_index?
    @appointments = policy_scope(Appointment).of_consultation_type(@consultation_type).
      between(@filter[:start_date], @filter[:end_date]).
      with_emergency(@filter[:emergency]).
      onml_id_like(@filter[:onml_id]).
      official_report_like(@filter[:official_report]).
      patient_last_name_like(@filter[:patient_last_name]).
      of_patient_age_range(@filter[:patient_age_range].try(:split, "-")).
      of_patient_birthdate(@filter[:patient_birthdate]).
      of_patient_gender(@filter[:patient_gender]).
      of_user_in_charge(@filter[:user_in_charge_id]).
      of_calling_institution(@filter[:calling_institution_id]).
      of_consultation_reason(@filter[:consultation_reason_id]).
      with_status(@filter[:status]).
      with_certificate_status(@filter[:certificate_status])

    @appointments_count = @appointments.count
    @too_many_appointments = @appointments_count > MAX_DISPLAYABLE_ARCHIVES
  end

  def reset_filter
    authorize @consultation_type, :archives_index?
    session.delete(:appointments_filter)
    redirect_to archives_consultation_type_appointments_path(@consultation_type)
  end

  def show
  end

  def new
    authorize @consultation_type.appointments.build
    if source_id = params[:source_id].presence
      @appointment = Appointment.find(source_id).duplicate_for_consultation_type(@consultation_type)
    else
      @appointment = @consultation_type.appointments.new(starts_at: params["starts_at"])
      @appointment.build_officer
      @appointment.build_patient
    end
    @pivot_date = @appointment.starts_at.to_date
  end

  def create
    sanitize_params
    @reason = params[:appointment][:previous_appointment_postponed_by]
    @appointment = authorize @consultation_type.appointments.new(permitted_attributes(Appointment))

    if @appointment.save
      postpone_previous_appointment
      log_actions_for [Log::Action.new("created")], @appointment
      redirect_to new_consultation_type_appointment_path(@next_appointment_consultation_type, source_id: @appointment.id) and return if @next_appointment_consultation_type
      redirect_to consultation_type_appointments_path(@consultation_type)
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @consultation_type = @appointment.consultation_type
    @appointment.patient || @appointment.build_patient
    @appointment.officer || @appointment.build_officer
  end

  def update
    sanitize_params
    @appointment.assign_attributes(permitted_attributes(@appointment))

    actions = []
    cancellation_changes = @appointment.changes[:cancelled_at]
    if cancellation_changes
      action_name = "#{action_prefix(cancellation_changes)}cancelled"
      actions << if action_name != "uncancelled"
        Log::Action.new({ action: action_name, source: @appointment.cancelled_by })
      else
        Log::Action.new(action_name)
      end
    end

    if @appointment.save
      log_actions_for actions, @appointment
      flash[:success] = if action_name
        t "flash.action.success", action: t("actions.#{action_name}.subs").upcase_first
      else
        t "flash.update.success"
      end
      redirect_to new_consultation_type_appointment_path(@next_appointment_consultation_type, source_id: @appointment.id) and return if @next_appointment_consultation_type
      redirect_to consultation_type_appointments_path(@appointment.consultation_type) and return if params[:appointment_form]
      render "appointments/_free_patient" if params[:conclusion_form]
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def postpone
    previous_appointment = Appointment.find_by_id(params[:id])
    authorize @appointment = previous_appointment.duplicate_for_postpone
    @reason = params[:reason]
    @appointment.build_officer unless @appointment.officer
    @appointment.build_patient unless @appointment.patient
    @consultation_type = @appointment.consultation_type
    @pivot_date = previous_appointment.starts_at.to_date
    render :new
  end

  def take_charge
    @appointment.update(user_in_charge: current_user)
    certificate = @appointment.certificate || @appointment.create_certificate!
    redirect_to edit_certificate_path(certificate, certificate_template_id: params[:certificate_template_id])
  end

  def conclude
  end

  def certificate
    attached_file = AttachedFile::SignedCertificate.new
    sanitize_filename
    attached_file.file.attach params[:new_certificate][:file]
    @appointment.certificate.attached_files << attached_file
    if @appointment.save
      flash.now[:success] = t "flash.create.success"
    else
      flash.now[:danger] = t "flash.create.failure"
    end
  end

  private

  def set_consultation_type
    @consultation_type = ConsultationType.find(params[:consultation_type_id])
  end

  def set_appointment
    @appointment = Appointment.find(params[:id])
    authorize @appointment
  end

  def sanitize_params
    if (duration = params[:appointment][:duration]).present?
      params[:appointment][:duration] = ActiveSupport::Duration.build(duration.to_i * 60).iso8601
    end
    params[:appointment].delete(:patient_attributes) if params[:appointment][:patient_id].present?
    params[:appointment][:officer_id] = params[:appointment][:officer_attributes][:id] if params[:appointment][:officer_attributes].present? # for postpone
    %i[officer_attributes patient_attributes].each do |key|
      params[:appointment].delete(key) unless params[:appointment][key]&.values&.any?(&:present?)
    end
    [:calling_institution, :patient].each { |p| params[:appointment].delete(p) }
    if next_appointment_consultation_type_id = params[:appointment][:next_appointment_consultation_type_id].presence
      @next_appointment_consultation_type = ConsultationType.find(next_appointment_consultation_type_id)
      params[:appointment].delete(:next_appointment_consultation_type_id)
    end
  end

  def postpone_previous_appointment
    if postponed_by = params[:appointment][:previous_appointment_postponed_by]
      @appointment.previous_appointment.postpone(postponed_by)
      log_action = Log::Action.new({ action: "postponed", source: postponed_by })
      log_actions_for [log_action], @appointment.previous_appointment
    end
  end

  def set_filter
    @filter = params[:appointments_filter] || {}
    @filter[:start_date] ||= (Date.today - 3.days).iso8601
    session[:appointments_filter] = @filter.as_json
  end

  # TODO
  def certificate_preview_as_html
    JsonCertificatesController.render "preview",
      assigns: { appointment: @appointment }
  end

  def sanitize_filename
    extension = params[:new_certificate][:file].original_filename.split(".").last
    params[:new_certificate][:file].original_filename = @appointment.signed_filename(extension)
  end
end
