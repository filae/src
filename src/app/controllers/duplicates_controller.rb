# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class DuplicatesController < ApplicationController
  before_action :set_patients, only: %i[show destroy]

  def show; end

  def destroy
    @patient.duplicated_patient = @probably_duplicated_patient
    @patient.probably_duplicated_patient = nil

    if @patient.save
      Patient.hand_over_consultations!([@patient.id])
      flash[:success] = t "flash.action.success", action: "Fusion"
      redirect_to patients_path(params: { patient: session[:patient_filter] })
    else
      flash.now[:error] = t "flash.action.failure", action: "Fusion"
    end
  end

  private

  def set_patients
    @patient = authorize Patient.find(params[:id]), :edit?
    @probably_duplicated_patient = @patient.probably_duplicated_patient
  end
end
