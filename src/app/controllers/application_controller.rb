# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ApplicationController < ActionController::Base
  include Pundit::Authorization

  rescue_from Pundit::NotAuthorizedError do
    rescue_with_message t("pundit.not_authorized_error")
  end

  rescue_from ActionController::InvalidAuthenticityToken do
    rescue_with_message t("flash.security.failure")
  end

  before_action :verify_user_still_active
  before_action :verify_outdated_session
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  helper_method :current_user

  private

  def current_user
    @current_user ||= User.find_by(id: session[:user_id]) || Visitor.new
  end

  # Logs
  def log_actions_for(actions, item)
    LogJob.perform_later(item, current_user, Time.zone.now.iso8601, actions.map(&:to_json)) unless actions.empty?
  end

  def action_prefix(changes)
    if changes.first.nil?
      ""
    elsif changes.last.nil?
      "un"
    else
      "re"
    end
  end

  def verify_outdated_session
    disconnect_user(reason: "sessions.need_to_reconnect") if (last_signed_in_at = session[:created_at]) && Time.zone.now > Time.zone.parse(last_signed_in_at) + 5.hours
  end

  def verify_user_still_active
    disconnect_user(reason: "sessions.create.authentication.inactive_user") if current_user.inactive?
  end

  def disconnect_user(reason: nil)
    reset_session
    flash[:danger] = t(reason) if reason.present?
    redirect_to new_session_path
  end

  def rescue_with_message(message)
    flash[:danger] = message
    if current_user.connected?
      redirect_back(fallback_location: root_path)
    else
      redirect_to new_session_path
    end
  end
end
