# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class TimeSlotsController < ApplicationController
  before_action :set_schedule, only: [:new, :create]
  before_action :set_time_slot, only: [:edit, :update, :destroy]

  def new
    @time_slot = authorize @schedule.time_slots.build(week_day: params[:week_day], consultation_type_id: params[:consultation_type_id])
  end

  def create
    sanitize_times
    @time_slot = authorize @schedule.time_slots.new(permitted_attributes(TimeSlot))

    if @time_slot.save
      flash[:success] = t "flash.create.success"
      redirect_to edit_schedule_path(@schedule)
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    sanitize_times
    if @time_slot.update(permitted_attributes(@time_slot))
      flash[:success] = t "flash.update.success"
      redirect_to edit_schedule_path(@time_slot.schedule)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if authorize @time_slot.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to edit_schedule_path(@time_slot.schedule)
  end

  private

  def set_schedule
    @schedule = Schedule.find_by(id: params[:schedule_id])
  end

  def set_time_slot
    @time_slot = authorize TimeSlot.find(params[:id])
  end

  def sanitize_times
    params[:time_slot][:starts_at] = "T#{params[:time_slot]['starts_at(4i)']}:#{params[:time_slot]['starts_at(5i)']}"
    params[:time_slot][:ends_at] = "T#{params[:time_slot]['ends_at(4i)']}:#{params[:time_slot]['ends_at(5i)']}"
    (1..5).each do |n|
      params[:time_slot].delete("starts_at(#{n}i)")
      params[:time_slot].delete("ends_at(#{n}i)")
    end
  end
end
