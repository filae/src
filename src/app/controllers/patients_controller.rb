# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class PatientsController < ApplicationController
  before_action :set_patient, only: %i[show edit update consultations]
  before_action :set_patientable, only: %i[update]
  before_action :pagination_params, only: :index
  before_action :filter_params, only: :index

  def index
    @patients = policy_scope(Patient).all
    # Filtering
    @patients = @patients.of_matching_lastname(@last_name_filter) if @last_name_filter.present?
    @patients = @patients.of_matching_firstname(@first_name_filter) if @first_name_filter.present?
    @patients = @patients.born_on_date(@birthdate_filter) if @birthdate_filter.present?
    @patients = @patients.born_on_year(@birth_year_filter) if @birth_year_filter.present?
    @patients = @patients.where.not(probably_duplicated_patient_id: nil) if @probable_duplicates.present?
    # Pagination
    @patients = authorize @patients.fullname_asc.page(@page).per(@size)
    @total_patients ||= Patient.count
  end

  def show
    render :show, layout: false
  end

  def edit
  end

  def update
    if @patient.update(permitted_attributes(@patient))
      if (sanitized_changes = @patient.previous_changes.except(:updated_at)).present?
        log_actions_for [Log::Action.new({ action: :updated, changes: sanitized_changes })], @patient
      end
      @partial = case request.referer
                 when /\/flowing_consultations/, /\/archives/
                   "patients/dropdown"
                 when /\/appointments/, /\/certificates/
                   "appointments/patient"
                 when /\/patients/
                   "patients/patient"
                 end
      flash.now[:success] = t "flash.update.success"
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def consultations
  end

  private

  def set_patient
    @patient = authorize Patient.find(params[:id])
  end

  def set_patientable
    @patientable = if (patientable_id = params[:patient][:patientable_id].presence)
      referer_class.find(patientable_id)
    end
  end

  def pagination_params
    session[:pagination] ||= {}
    size = params[:size].to_i
    @size = (Patient::PAGINATIONS.include?(size) ? size : Patient.default_per_page)
    session[:pagination][:size] = @size
    page = params[:page].to_i
    @page = (Patient.page(page).per(@size).out_of_range? ? 1 : page)
    session[:pagination][:page] = @page
  end

  def filter_params
    @patient_filter = Patient.new

    if params[:patient]
      session[:patient_filter] ||= {}
      @patient_filter = Patient.new(permitted_attributes(Patient))
      @last_name_filter = session[:patient_filter][:last_name] = params[:patient][:last_name]
      @first_name_filter = session[:patient_filter][:first_name] = params[:patient][:first_name]
      @birthdate_filter = session[:patient_filter][:birthdate] = params[:patient][:birthdate]
      @birth_year_filter = session[:patient_filter][:birth_year] = params[:patient][:birth_year]
      @probable_duplicates = session[:patient_filter][:probably_duplicated_patient_id] =
        params[:patient][:probably_duplicated_patient_id]
    end
  end

  def referer_class
    case request.referer
    when /flowing_consultations/
      FlowingConsultation
    when /appointments/, /certificates/
      Appointment
    end
  end
end
