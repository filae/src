# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MasterDocumentsController < ApplicationController
  before_action :set_master_document, only: %i[download create destroy]
  before_action :set_attachable, only: :create
  before_action :set_file_params, only: :create
  before_action :authorize_master_document, only: [:download, :destroy]

  def download
    certificatable = params[:appointment_id].present? && Appointment.find(params[:appointment_id]) ||
      params[:flowing_consultation_id].present? && FlowingConsultation.find(params[:flowing_consultation_id])
    send_data @master_document.file.download, filename: @master_document.filename_for(certificatable), type: @master_document.file.content_type, disposition: "attachment"
  end

  def create
    if @master_document
      authorize @master_document
      if @attachable && @file_params
        attach @file_params
        flash.now[:success] = t "flash.create.success"
      else
        flash.now[:danger] = t "flash.create.failure"
      end
    else
      head :bad_request
    end
  end

  def destroy
    @attachable = @master_document.attachable
    if @master_document.destroy
      flash.now[:success] = t "flash.destroy.success"
    else
      flash.now[:danger] = t "flash.destroy.failure"
    end
  end

  protected

  def fallback_location
    certificate_templates_path
  end

  def set_master_document
    @master_document = if persisted_id = (params[:id] || params[:master_document_id])
      MasterDocument.find(persisted_id)
    else
      MasterDocument.new
    end
  end

  def set_attachable
    @attachable = CertificateTemplate.find(params[:certificate_template_id])
  end

  def set_file_params
    @file_params = (params[:master_document] ? params[:master_document][:file] : nil)
  end

  def attach file_params
    @attachable.master_document&.destroy
    @attachable.build_master_document.file.attach(file_params)
    @attachable.save
    @master_document = @attachable.master_document
  end

  private

  def authorize_master_document
    authorize @master_document
  end
end
