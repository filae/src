# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultationsController < ApplicationController
  before_action :set_intervention_domain, only: [:index, :list, :archives, :reset_filter, :new, :create]
  before_action :set_flowing_consultation, only: [:edit, :update, :certificates, :certificate, :assign, :cancel, :validate]
  before_action :ensure_default_params, only: :archives
  before_action :save_filters_to_session, only: :archives

  MAX_DISPLAYABLE_ARCHIVES = 200

  def index
    authorize @intervention_domain, :show?
    skip_policy_scope
  end

  def list
    authorize @intervention_domain, :show?
    @state = params[:state]
    @title = t("activerecord.attributes.flowing_consultation.state.#{@state.underscore}")
    @title = @title.pluralize if ["validated", "cancelled"].include?(@state)
    base_flowing_consultations = policy_scope(@intervention_domain.flowing_consultations).active.by_calling_areas_and_institutions
    @collapsed, @flowing_consultations = case @state
                 when "to-transmit"
                   [false, base_flowing_consultations.to_transmit]
                 when "in-progress"
                   [false, base_flowing_consultations.in_progress]
                 when "validated"
                   [true, base_flowing_consultations.validated]
                 when "cancelled"
                   [true, base_flowing_consultations.cancelled]
                 end
    @fingerprint = Digest::SHA512.hexdigest(@flowing_consultations.ids.sort.to_s)
    if params[:fingerprint] == @fingerprint
      head :no_content
    else
      render :list
    end
  end

  def archives
    authorize @intervention_domain, :archives_index?
    @flowing_consultations = policy_scope(@intervention_domain.flowing_consultations).
      archived.
      sort_with_instructions(params[:sort]).
      with_status(filter_param(:status)).
      between(filter_param(:action), @start_date_time, @end_date_time)
    if type = ConsultationType.find_by_id(filter_param(:consultation_type_id))
      @flowing_consultations = @flowing_consultations.of_consultation_type(type)
    end
    if calling_institution = CallingInstitution.find_by_id(filter_param(:calling_institution_id))
      @flowing_consultations = @flowing_consultations.of_calling_institution(calling_institution)
    end
    if patient_gender = filter_param(:patient_gender)
      @flowing_consultations = @flowing_consultations.of_patient_gender(patient_gender)
    end
    if (patient_last_name = filter_param(:patient_last_name)).present?
      @flowing_consultations = @flowing_consultations.patient_last_name_like(patient_last_name)
    end
    if (patient_age_range = filter_param(:patient_age_range)).present?
      range_floor, range_ceiling = patient_age_range.split("-")
      @flowing_consultations = @flowing_consultations.of_patient_age_range(range_floor.to_i, range_ceiling.to_i) if range_floor.present? && range_ceiling.present?
    end
    if assignee = User.find_by_id(filter_param(:assignee_id))
      @flowing_consultations = @flowing_consultations.of_assignee(assignee)
    end

    @flowing_consultations_count = @flowing_consultations.count
    @too_many_flowing_consultations = @flowing_consultations_count > MAX_DISPLAYABLE_ARCHIVES
  end

  def reset_filter
    authorize @intervention_domain, :archives_index?
    session.delete(:flowing_consultations_filter)
    redirect_to archives_intervention_domain_flowing_consultations_path(@intervention_domain)
  end

  def new
    @flowing_consultation = authorize @intervention_domain.flowing_consultations.build
    @is_message = params[:message].present?
    @flowing_consultation.build_officer
    @flowing_consultation.build_patient
  end

  def create
    sanitize_params
    @flowing_consultation = authorize @intervention_domain.flowing_consultations.new(permitted_attributes(FlowingConsultation))

    if @flowing_consultation.save
      flash[:success] = t "flash.create.success"
      redirect_to intervention_domain_flowing_consultations_path(@intervention_domain)
    else
      @flowing_consultation.build_patient unless @flowing_consultation.patient
      @flowing_consultation.build_officer unless @flowing_consultation.officer
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize @flowing_consultation
    @is_message = @flowing_consultation.simple_message?
    @flowing_consultation.build_patient unless @flowing_consultation.patient
    @flowing_consultation.build_officer unless @flowing_consultation.officer
  end

  def assign
    authorize @flowing_consultation
    render :assign_form, layout: false
  end

  def cancel
    authorize @flowing_consultation
    render :cancel_form, layout: false
  end

  def validate
    authorize @flowing_consultation
    render :validate_form, layout: false
  end

  def update
    authorize @flowing_consultation
    sanitize_params
    @flowing_consultation.assign_attributes(permitted_attributes(@flowing_consultation))

    assignee_changes = @flowing_consultation.changes[:assignee_id]
    validation_changes = @flowing_consultation.changes[:validated_at]
    cancellation_changes = @flowing_consultation.changes[:cancelled_at]
    actions = []
    actions << Log::Action.new("#{action_prefix(assignee_changes)}assigned") if assignee_changes
    actions << Log::Action.new("#{action_prefix(validation_changes)}validated") if validation_changes
    if cancellation_changes
      action_name = "#{action_prefix(cancellation_changes)}cancelled"
      actions << if action_name == "cancelled"
        Log::Action.new({ action: action_name, source: @flowing_consultation.cancelled_by })
      else
        Log::Action.new(action_name)
      end
    end

    if @flowing_consultation.update(permitted_attributes(FlowingConsultation))
      log_actions_for(actions, @flowing_consultation)
      flash[:success] ||= t "flash.update.success"
      redirect_to (@previous_referer.present? ? @previous_referer : intervention_domain_flowing_consultations_path(@flowing_consultation.intervention_domain))
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def certificates
    authorize @flowing_consultation, :list_certificates?
    @certificate_templates = CertificateTemplate.for_user(current_user).with_master_document.enabled
    @signed_certificates = @flowing_consultation.certificates.signed
    @validated_certificates = @flowing_consultation.certificates.validated
  end

  def certificate
    authorize @flowing_consultation
    attached_file = AttachedFile::SignedCertificate.new
    sanitize_filename
    attached_file.file.attach params[:new_certificate][:file]
    @certificate = Certificate.new(attached_files: [attached_file], certificatable: @flowing_consultation)
    if @certificate.save
      flash.now[:success] = t "flash.create.success"
    else
      flash.now[:danger] = t "flash.create.failure"
    end
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find(params[:intervention_domain_id])
  end

  def set_flowing_consultation
    @flowing_consultation = FlowingConsultation.find(params[:id])
  end

  def sanitize_params
    if params[:simple_message].present?
      params[:flowing_consultation] = params.delete(:simple_message)
      params[:flowing_consultation][:officer_attributes][:lambda_user] = true
    end
    params[:flowing_consultation].delete(:patient_attributes) if params[:flowing_consultation][:patient_id].present?
    params[:flowing_consultation][:validated_at] = nil if params[:flowing_consultation].key?(:assignee_id) && params[:flowing_consultation][:assignee_id].blank?
    %i[patient_attributes officer_attributes].each do |key|
      params[:flowing_consultation].delete(key) unless params[:flowing_consultation][key]&.values&.any?(&:present?)
    end
    [:calling_institution, :patient].each { |p| params[:flowing_consultation].delete(p) }
    @previous_referer = params[:simple_message]&.delete(:previous_referer) || params[:flowing_consultation]&.delete(:previous_referer)
  end

  def ensure_default_params
    if params[:stream_filter].blank?
      stream_filter = session[:flowing_consultations_filter].presence || {
        action: "called_at",
        start_date: (Date.current - 3.days).iso8601
      }

      redirect_to archives_intervention_domain_flowing_consultations_path(@intervention_domain, params: { stream_filter: })
    else
      @start_date_time = if (start_date = filter_param(:start_date).presence)
        params[:stream_filter][:start_time] = params[:stream_filter][:start_time].presence || "00:00"
        [start_date, filter_param(:start_time)].join("T")
      end
      @end_date_time = if (end_date = filter_param(:end_date).presence)
        params[:stream_filter][:end_time] = params[:stream_filter][:end_time].presence || "00:00"
        [end_date, filter_param(:end_time).presence].join("T")
      end
    end
  end

  def save_filters_to_session
    session[:flowing_consultations_filter] ||= {}

    params[:stream_filter].each do |key, value|
      session[:flowing_consultations_filter][key.to_s] = value
    end
  end

  def filter_param(sym)
    params[:stream_filter][sym]
  end

  def sanitize_filename
    extension = params[:new_certificate][:file].original_filename.split(".").last
    params[:new_certificate][:file].original_filename = @flowing_consultation.signed_filename(extension)
  end
end
