# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationTypesController < ApplicationController
  before_action :set_intervention_domain, only: [:new, :create]
  before_action :set_consultation_type, only: [:edit, :update, :destroy]

  def new
    @consultation_type = authorize @intervention_domain.consultation_types.build
  end

  def create
    @consultation_type = @intervention_domain.consultation_types.new(permitted_attributes(ConsultationType))
    authorize @consultation_type

    if @consultation_type.save
      flash[:success] = t "flash.create.success"
      redirect_to intervention_domains_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    if @consultation_type.update(permitted_attributes(@consultation_type))
      flash[:success] = t "flash.update.success"
      redirect_to intervention_domains_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @consultation_type.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to intervention_domains_path
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find_by(id: params[:intervention_domain_id])
  end

  def set_consultation_type
    @consultation_type = ConsultationType.find(params[:id])
    authorize @consultation_type
  end
end
