# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SchedulesController < ApplicationController
  before_action :set_intervention_domain, only: [:new, :create]
  before_action :set_schedule, only: [:edit, :update, :destroy]

  def new
    @schedule = authorize @intervention_domain.schedules.build
  end

  def create
    @schedule = authorize @intervention_domain.schedules.new(permitted_attributes(Schedule))

    if @schedule.save
      flash[:success] = t "flash.create.success"
      redirect_to intervention_domains_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @consultation_types = schedule_consultation_types
  end

  def update
    if @schedule.update(permitted_attributes(@schedule))
      flash[:success] = t "flash.update.success"
      redirect_to intervention_domains_path
    else
      @consultation_types = schedule_consultation_types
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @schedule.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to intervention_domains_path
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find_by(id: params[:intervention_domain_id])
  end

  def set_schedule
    @schedule = authorize Schedule.find(params[:id])
  end

  def schedule_consultation_types
    @schedule.intervention_domain.consultation_types.by_title
  end

end
