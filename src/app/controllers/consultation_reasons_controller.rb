# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationReasonsController < ApplicationController
  before_action :set_consultation_type, only: [:new, :create]
  before_action :set_consultation_reason, only: [:edit, :update, :destroy]

  def new
    @consultation_reason = authorize @consultation_type.consultation_reasons.build
  end

  def create
    @consultation_reason = authorize @consultation_type.consultation_reasons.build(permitted_attributes(ConsultationReason))

    if @consultation_reason.save
      flash[:success] = t "flash.create.success"
      redirect_to intervention_domains_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    if @consultation_reason.update(permitted_attributes(@consultation_reason))
      flash[:success] = t "flash.update.success"
      redirect_to intervention_domains_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @consultation_reason.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to intervention_domains_path
  end

  private

  def set_consultation_type
    @consultation_type = ConsultationType.find(params[:consultation_type_id])
  end

  def set_consultation_reason
    @consultation_reason = authorize ConsultationReason.find(params[:id])
  end
end
