# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class InterventionDomainsController < ApplicationController
  before_action :set_intervention_domain, only: [:edit, :update, :destroy]

  def index
    authorize InterventionDomain
    @intervention_domains = policy_scope(InterventionDomain).includes(:consultation_types).by_title
  end

  def new
    @intervention_domain = authorize InterventionDomain.new
  end

  def create
    @intervention_domain = InterventionDomain.new(permitted_attributes(InterventionDomain))
    authorize @intervention_domain

    if @intervention_domain.save
      flash[:success] = t "flash.create.success"
      redirect_to intervention_domains_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    if @intervention_domain.update(permitted_attributes(@intervention_domain))
      flash[:success] = t "flash.update.success"
      redirect_to intervention_domains_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @intervention_domain.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to intervention_domains_path
  end

  def appointments
    @pivot_date = session[:pivot_date] = Date.today
    @intervention_domain = InterventionDomain.find(params[:intervention_domain_id])
    authorize @intervention_domain, :show?
  end

  private

  def set_intervention_domain
    @intervention_domain = authorize InterventionDomain.find(params[:id])
  end
end
