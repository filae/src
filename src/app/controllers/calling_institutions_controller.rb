# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CallingInstitutionsController < ApplicationController
  before_action :set_calling_area, only: [:new, :create]
  before_action :set_calling_institution, only: [:edit, :update, :destroy]

  def new
    @calling_institution = authorize @calling_area.calling_institutions.build
  end

  def create
    @calling_institution = @calling_area.calling_institutions.new(permitted_attributes(CallingInstitution))
    authorize @calling_institution

    if @calling_institution.save
      flash[:success] = t "flash.create.success"
      redirect_to calling_areas_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    if @calling_institution.update(permitted_attributes(@calling_institution))
      flash[:success] ||= t "flash.update.success"
      redirect_to calling_areas_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @calling_institution.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to calling_areas_path
  end

  private

  def set_calling_area
    @calling_area = CallingArea.find(params[:calling_area_id])
  end

  def set_calling_institution
    @calling_institution = CallingInstitution.find(params[:id])
    authorize @calling_institution
  end

end
