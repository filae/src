# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update, :activate, :deactivate]

  def index
    authorize User
    @users = policy_scope(User).by_deactivation.by_name
  end

  def new
    @user = authorize User.new
  end

  def create
    sanitize_role
    @user = User.new(permitted_attributes(User))
    @user.password = User.generate_secure_password
    authorize @user

    if @user.save
      begin
        PasswordMailer.created(@user).deliver
        flash[:success] = t "flash.create.success"
        redirect_to users_path
      rescue => e
        @user.destroy
        logger.error "***** ERROR: #{e}"
        flash[:danger] = t "flash.create.failure"
        render :new, status: :unprocessable_entity
      end
    else
      flash[:danger] = t "flash.create.failure"
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    sanitize_role
    if @user.update(permitted_attributes(@user))
      if @user.disabled_at.present?
        if @user.disabled_at > Time.zone.now
          flash[:success] = t ".deactivate.success.later", user_name: @user.name, deactivated_on: l(@user.disabled_at)
        else
          flash[:success] = t ".deactivate.success.now", user_name: @user.name
        end
      end
        flash[:success] ||= t "flash.update.success"
        redirect_to users_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def activate
    if @user.update(disabled_at: nil)
      flash[:success] = t ".success", user_name: @user.name
    else
      flash[:danger] = t ".failure", user_name: @user.name
    end
    redirect_to users_path
  end

  def deactivate
  end

  private

  def set_user
    @user = authorize User.find(params[:id])
  end

  def sanitize_role
    role_title = params[:user][:role_id]
    params[:user][:role_id] = Role.find_by_title(role_title).try(:id) || @user.role
  end
end
