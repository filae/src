# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificateTemplatesController < ApplicationController
  before_action :set_certificate_template, only: [:edit, :update, :toggle_activation,  :destroy]

  def index
    session[:show_disabled] = (params[:show_disabled] == "true" ? true : false)
    @certificate_templates = authorize policy_scope(CertificateTemplate).by_title
    @certificate_templates = (session[:show_disabled] ? @certificate_templates.all : @certificate_templates.enabled)
  end

  def new
    @certificate_template = authorize CertificateTemplate.new
  end

  def create
    sanitize_questionnaire
    @certificate_template = authorize CertificateTemplate.new(permitted_attributes(CertificateTemplate))

    if @certificate_template.save
      flash[:success] = t "flash.create.success"
      redirect_to certificate_templates_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    sanitize_questionnaire
    if @certificate_template.update(permitted_attributes(@certificate_template))
      flash[:success] = t "flash.update.success"
      redirect_to certificate_templates_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def toggle_activation
    disabled_at = @certificate_template.disabled_at
    if disabled_at.present?
      disabled_at = nil
      toggle_action = "Activation"
    else
      disabled_at = Time.zone.now
      toggle_action = "Désactivation"
    end
    if @certificate_template.update(disabled_at: disabled_at)
      flash[:success] = t "flash.action.success", action: toggle_action
    else
      flash[:danger] = t "flash.action.failure", action: toggle_action
    end
    redirect_to certificate_templates_path(show_disabled: session[:show_disabled])
  end

  def destroy
    if @certificate_template.destroy
      flash[:success] ||= t "flash.destroy.success"
    else
      flash[:danger] ||= t "flash.destroy.failure"
    end
    redirect_to certificate_templates_path
  end

  private

  def set_certificate_template
    @certificate_template = authorize CertificateTemplate.find(params[:id])
  end

  def sanitize_questionnaire
    if (questionnaire = params[:certificate_template][:questionnaire]).present?
      params[:certificate_template][:questionnaire] = JSON.parse(questionnaire.squish)
    end
  end
end
