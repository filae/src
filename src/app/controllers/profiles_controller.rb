# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ProfilesController < ApplicationController
  def show
    authorize :profile
    @user = current_user
  end

  def update
    @user = authorize User.find(params[:user][:id])
    if @user.update(permitted_attributes(@user))
      flash[:success] = t(".updated_password.success")
      redirect_to profile_path
    else
      render :show, status: :unprocessable_entity
    end
  end
end
