# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Settings::CertificateLayoutsController < ApplicationController
  before_action :set_settings_certificate_layout, only: [:edit, :update]

  def edit
  end

  def update
    if @settings_certificate_layout.update(settings_certificate_layout_params)
      flash[:success] = t "flash.update.success"
      redirect_to certificate_templates_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def set_settings_certificate_layout
    @settings_certificate_layout = authorize Settings::CertificateLayout.first_or_initialize
  end

  def settings_certificate_layout_params
    permitted_attributes(@settings_certificate_layout)
  end

end
