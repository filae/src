# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SessionsController < ApplicationController
  skip_before_action :verify_outdated_session
  before_action { authorize :session }
  before_action :reset_session, except: :new

  def new
    @session = Session.new
  end

  def create
    @session = Session.new(params.require(:session).permit(:identifier, :password, :forgotten_password))
    if @session.valid?
      if @session.forgotten_password?
        if @session.send_new_password
          flash[:success] = t(".forgotten_password.success")
          redirect_to new_session_path and return
        else
          flash.now[:danger] = t(".forgotten_password.failure")
        end
      else
        if user = @session.authenticate
          session[:user_id] = user.id
          session[:created_at] = Time.zone.now.iso8601
          redirect_to root_path and return
        else
          flash.now[:danger] = t(".authentication.failed")
        end
      end
    else
      flash.now[:danger] = t(".authentication.failed")
    end

    render :new, status: :unprocessable_entity
  end

  def destroy
    redirect_to root_path
  end
end
