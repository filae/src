# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFilesController < ApplicationController
  before_action :set_upload, only: %i[download preview destroy]
  before_action :set_attachable, only: %i[index list create]
  before_action :set_file_params, only: :create

  def index
    skip_policy_scope
    if params[:filter] == "certificates"
      authorize @attachable, :list_certificates?
      @certificate_templates = CertificateTemplate.for_user(current_user)
      render :certificates
    else
      authorize @attachable, :list_attached_files?
    end
    @list_url = "list_#{@attachable.class.to_s.underscore}_attached_files_path"
  end

  def list
    @type = params[:type]
    @read_only = params[:read_only]
    @attached_file_class = AttachedFile.new_of_type(@type).class
    authorize @attachable, :list_attached_files?
    @info = @attached_file_class.info
    @post_url = case @attachable
                when Appointment
                  appointment_attached_files_path(@attachable, type: @type)
                when FlowingConsultation
                  flowing_consultation_attached_files_path(@attachable, type: @type)
                end
    @attached_files = @attachable.attached_files.where(type: @attached_file_class.to_s)
  end

  def download
    authorize @upload
    send_data @upload.file.download, filename: @upload.file.filename.to_s, type: @upload.file.content_type
  end

  def preview
    authorize @upload
    file = @upload.file
    if pdf_preview = PdfGenerator.new(file).generate
      send_data pdf_preview.data, filename: file.filename.to_s, type: pdf_preview.content_type
    else
      render status: 500
    end
  end

  def create
    @uploads = []
    if @attachable && @file_params
      @file_params.each do |file_params|
        @uploads << set_upload
        if attach file_params
          set_attached_file_class
          flash.now[:success] = t "flash.create.success"
        else
          flash.now[:danger] = t "flash.create.failure"
        end
      end
    else
      head :bad_request
    end
  end

  def destroy
    authorize @upload
    @type = @upload.type
    set_attached_file_class
    @attachable = @upload.attachable
    if @upload.destroy
      flash.now[:success] = t "flash.destroy.success"
    else
      flash.now[:danger] = t "flash.destroy.failure"
    end
  end

  private

  def set_upload
    @upload = if persisted_id = params[:id]
      AttachedFile.find(persisted_id)
    elsif @type = params[:type]
      AttachedFile.new_of_type(@type)
    end
  end

  def set_attachable
    @attachable = Appointment.find_by_id(params[:appointment_id]) ||
      FlowingConsultation.find_by_id(params[:flowing_consultation_id]) ||
      Certificate.find_by_id(params[:certificate_id])
  end

  def set_attached_file_class
    @attached_file_class = AttachedFile.new_of_type(@type).class
  end

  def set_file_params
    @file_params = (params[:attached_file] ? params[:attached_file][:file] : nil)
  end

  def attach(file_params)
    @upload.file.attach(file_params)
    @attachable.attached_files << @upload
    authorize @upload
  end
end
