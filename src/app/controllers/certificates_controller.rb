# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificatesController < ApplicationController
  before_action :set_certificate, only: [:edit, :update, :toggle_validation, :destroy, :mail_form, :send_by_mail]

  def edit
    @certificate_template = ((param = params[:certificate_template_id]) ? CertificateTemplate.find(param) : nil)
  end

  def update
    @certificate.current_user = current_user
    @certificate.assign_attributes(permitted_attributes(@certificate))
    
    actions = {
      "validated" => @certificate.changes[:validated_at]
    }.map do |action_name, any_changes|
      Log::Action.new(action_prefix(any_changes) + action_name) if any_changes
    end.compact

    if @certificate.save
      log_actions_for actions, @certificate
      flash.now[:success] = t "flash.update.success"
      @appointment = @certificate.certificatable
      render "appointments/update"
    else
      flash.now[:danger] = t "flash.update.failure"
    end
  end

  def destroy
    @certificatable = @certificate.certificatable
    deletions = case @certificatable
                when FlowingConsultation
                  @certificate.destroy
                when Appointment
                  @certificate.attached_files.where(type: "AttachedFile::SignedCertificate").delete_all
                end
    if deletions
      flash.now[:success] = t "flash.destroy.success"
    else
      flash.now[:danger] = t "flash.destroy.failure"
    end
  end

  def mail_form
  end

  def send_by_mail
    if (recipient = params[:mail_form][:recipient]) && recipient.match?(/.*@.*\..*/)
      begin
        CertificateMailer.send_to_officer(recipient, @certificate).deliver_now
        flash.now[:success] = t "flash.send_mail.success"
        @certificate.update(delivered_at: Time.zone.now)
      rescue
        flash.now[:danger] = t "flash.send_mail.failure"
        @certificate.update(delivered_at: nil)
      end
    else
      render status: :unprocessable_entity
    end
  end

  def toggle_validation
    if @certificate.validated_at.present?
      @certificate.validated_at = nil
      change_name = "unvalidate"
    else
      @certificate.validated_at = Time.zone.now
      change_name = "validate"
    end
    actions = {
      "validated" => @certificate.changes[:validated_at]
    }.map do |action_name, any_changes|
      Log::Action.new(action_prefix(any_changes) + action_name) if any_changes
    end.compact

    if @certificate.save
      log_actions_for actions, @certificate
      flash.now[:success] = t "flash.#{change_name}.success"
    else
      flash.now[:danger] = t "flash.#{change_name}.failure"
    end
  end

  private

  def set_certificate
    @certificate = Certificate.find(params[:id])
    authorize @certificate
  end
end
