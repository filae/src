# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ComplementaryAppointmentsController < ApplicationController
  def create
    @appointment = authorize Appointment.find(params[:appointment_id]), :update?
    @consultation_type = ConsultationType.find(params[:consultation_type_id])
    complementary_appointment = @appointment.duplicate_for_consultation_type(@consultation_type)
    if complementary_appointment.save
      @appointment.complementary_appointments << complementary_appointment
      flash.now[:success] = t "flash.create.success"
    else
      flash.now[:danger] = t "flash.create.failure"
    end
    render :update
  end

  def destroy
    complementary_appointment = Appointment.find(params[:id])
    @appointment = authorize complementary_appointment.originating_appointment, :update?
    @consultation_type = complementary_appointment.consultation_type
    if complementary_appointment.update(cancelled_at: Time.zone.now, cancelled_by: :patient)
      flash.now[:success] = t "flash.action.success", action: "Annulation"
    else
      flash.now[:danger] = t "flash.action.failure", action: "Annulation"
    end
    render :update
  end
end
