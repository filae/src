# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificateMailer < ApplicationMailer
  def send_to_officer(recipient, certificate)

    @certificate = certificate

    validated_certificate_file = certificate.validated_attached_file.file
    filename = validated_certificate_file.filename.to_s
    @patient = certificate.certificatable.patient
    @from = custom_from(certificate)

    attachments[filename] = validated_certificate_file.download

    mail from: @from,
      to: recipient,
      subject: "[#{ENV['APP_NAME']}] #{filename}"
  end

  private

  def custom_from(certificate)
    certificate.certificatable.is_a?(Appointment) ? ENV["APPOINTMENTS_MAILER_FROM_ADDRESS"] : ENV["STREAM_MAILER_FROM_ADDRESS"]
  end
end
