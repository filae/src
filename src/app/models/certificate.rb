# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Certificate < ApplicationRecord
  belongs_to :certificatable, polymorphic: true
  belongs_to :validator, class_name: "User", optional: true
  has_many :attached_files, as: :attachable, dependent: :destroy

  before_update :manage_validated_certificates, if: :persisted?

  attr_writer :current_user
  alias_attribute :checksum, :sha512_checksum

  delegate :user_in_charge, to: :certificatable

  scope :validated, -> { where.not(validated_at: nil) }
  scope :signed, -> { where.not(id: validated) }

  def pages
    questionnaire["pages"]
  end

  def data_for_element element, data_set: nil
    if element["type"] == "html"
      data
    else
      (data_set || data)[element["name"]]
    end
  end

  def signed?
    !validated?
  end

  def signed_attached_file
    attached_files.where(type: "AttachedFile::SignedCertificate").last
  end

  def validated?
    validated_attached_file.present?
  end

  def validated_attached_file
    attached_files.where(type: "AttachedFile::ValidatedCertificate").last
  end

  private

  def manage_validated_certificates
    if validated_at_changed?(from: nil)
      self.validator = @current_user
      generate_validated_certificate_pdf
    elsif validated_at_changed?(to: nil)
      AttachedFile::ValidatedCertificate.where(attachable: self).destroy_all
      self.validator = nil
    else
      nil
    end
  end

  def generate_validated_certificate_pdf
    attached_file = AttachedFile::ValidatedCertificate.new

    pdf_data = if data.present? && certificatable.is_a?(Appointment)
      raise "Error: you tried to generate an embedded certificate but this behavior is deprecated for the moment."
    else
      PdfGenerator.new(signed_attached_file.file).generate.data
    end

    blob = ActiveStorage::Blob.create_and_upload!(io: StringIO.new(pdf_data), filename: validated_filename, content_type: "application/pdf")
    attached_file.file.attach blob
    self.attached_files << attached_file
    self.checksum = Digest::SHA512.hexdigest(blob.download)
  end

  def validated_filename
    signed_filename = if signed_attached_file
      signed_attached_file.file.filename.to_s
    else
      certificatable.signed_filename("json")
    end
    [
      "Validé",
      File.basename(signed_filename, ".*"),
      I18n.l(Time.zone.now, format: :filename, locale: :fr)
    ].join(" - ") << ".pdf"
  end
end
