# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFile < ApplicationRecord
  include Attachable

  def self.of_type(type)
    case type
    when "medical_content"
      where(type: "AttachedFile::MedicalContent")
    when "signed_certificate"
      where(type: "AttachedFile::SignedCertificate")
    when "validated-certificate"
      where(type: "AttachedFile::ValidatedCertificate")
    else
      where(type: "AttachedFile::Standard")
    end
  end

  def self.new_of_type(type)
    case type
    when "medical_content"
      AttachedFile::MedicalContent.new
    when "signed_certificate"
      AttachedFile::SignedCertificate.new
    when "validated_certificate"
      AttachedFile::ValidatedCertificate.new
    else
      AttachedFile::Standard.new
    end
  end

  def type
    self.class.to_s.split(":").last.underscore
  end
end
