# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class User < ApplicationRecord
  has_secure_password
  has_many :appointments
  has_many :attached_files, as: :attachable

  belongs_to :role

  validates :identifier, presence: true, uniqueness: { case_sensitive: false }
  validates :email, presence: true
  validates :last_name, presence: true
  validates :password, confirmation: true, length: { minimum: 8 }, format: { with: /\A.*[A-Z]+.*\d+.*|.*\d+.*[A-Z]+.*\z/, message: :strength }, allow_nil: true

  scope :active, ->(disabled_after = Time.zone.now) { where(disabled_at: nil).or(where(disabled_at: disabled_after..)) }
  scope :by_deactivation, -> { order(disabled_at: :desc) }
  scope :by_name, -> { order(:last_name, :first_name) }
  scope :by_role, ->(role) { joins(:role).where(roles: { title: role}) }
  scope :of_matching_name, ->(name) { where("last_name ILIKE ?", "%#{name}%").or(self.where("first_name ILIKE ?", "%#{name}%")).distinct }

  # Activation
  def activation_status
    return "inactive" if inactive?
    return "deactivation-scheduled" if scheduled_for_deactivation?
    return "active" if active?
  end

  def active?
    !inactive?
  end

  def inactive?
    disabled_at&.past?
  end

  def scheduled_for_deactivation?
    disabled_at&.future?
  end

  def has_role?(title)
    self.role == Role.find_by_title(title)
  end

  def connected?
    true
  end

  def name
    "#{last_name} #{first_name}"
  end

  def abridged_name
    "#{first_name[0]}. #{last_name}"
  end

  def certificate_templates
    CertificateTemplate.for_user(self)
  end

  def role_title
    role.localized_title
  end

  def locale
    "fr"
  end

  def self.generate_secure_password
    SecureRandom.hex(10) + "F1L4É"
  end
end
