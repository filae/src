# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MasterDocument < ApplicationRecord
  include Attachable

  def filename_for(certificatable = nil)
    original_filename = file.filename.to_s
    if certificatable
      extension = original_filename.split(".").last
      custom_filename_for(certificatable, extension)
    else
      original_filename
    end
  end

  private

  def custom_filename_for(certificatable, extension)
    time_column = case certificatable
    when Appointment
        :starts_at
    when FlowingConsultation
        :assigned_at
    end

    [
      certificatable.patient&.fullname,
      certificatable.onml_id,
      I18n.l(certificatable.send(time_column), format: :filename)
    ].compact.join(" - ") << ".#{extension}"
  end
end
