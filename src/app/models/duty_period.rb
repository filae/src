# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class DutyPeriod < ApplicationRecord
  belongs_to :time_slot
  has_one :consultation_type, through: :time_slot
  has_many :assigned_roles, through: :consultation_type
  has_many :assignments
  has_many :assignees, through: :assignments, source: :user

  validates :scheduled_on, presence: true, uniqueness: { scope: "time_slot_id" }

  def starts_at
    computed_datetime(scheduled_on, time_slot.starts_at)
  end

  def ends_at
    computed_datetime(scheduled_on, time_slot.ends_at)
  end

  private

  def computed_datetime(date, time)
    time.change(year: date.year, month: date.month, day: date.day).utc
  end
end
