# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Session
  include ActiveModel::Model

  attr_accessor :identifier, :password, :forgotten_password

  validates :identifier, presence: true
  validates :password, presence: true, unless: -> { forgotten_password? }

  def forgotten_password?
    forgotten_password == "1"
  end

  def authenticate
    if user
      if user.inactive?
        errors.add(:identifier, :inactive_user)
      else
        if user.authenticate(password)
          return user
        else
          errors.add(:identifier, :invalid_credentials)
        end
      end
    else
      errors.add(:identifier, :invalid_credentials)
    end

    return nil
  end

  def send_new_password
    if user&.update!(password: User.generate_secure_password)
      PasswordMailer.reset(user).deliver
      return user
    end
  end

  def user
    @user ||= User.find_by(identifier: identifier)
  end

  def persisted?
    false
  end
end
