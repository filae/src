# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationType < ApplicationRecord
  belongs_to :intervention_domain
  has_many :appointments
  has_many :consultation_reasons, dependent: :destroy
  has_many :consultation_subtypes, dependent: :destroy
  has_many :duty_periods, through: :time_slots
  has_many :flowing_consultations
  has_many :time_slots, dependent: :destroy
  # roles
  has_many :consultation_type_authorizations, dependent: :destroy
  has_many :roles, through: :consultation_type_authorizations
  # assignable roles
  has_many :consultation_type_assigned_roles, dependent: :destroy
  has_many :assigned_roles, through: :consultation_type_assigned_roles, source: :role
  # complementary consultation types
  has_many :consultation_type_complementary_consultation_types, dependent: :destroy
  has_many :complementary_consultation_types, through: :consultation_type_complementary_consultation_types, source: :complementary_consultation_type

  validates :title, presence: true, uniqueness: { scope: :intervention_domain_id }

  scope :by_title, ->(direction = :asc) { order(title: direction)  }
  scope :for_user, ->(user) { joins(:roles).where(roles: [user.role]) }

  def assignable_users(duty_period)
    [].tap do |users|
      assigned_roles.each do |role|
        users << User.by_role(role.title).active(duty_period.ends_at)
      end
    end.uniq.compact.flatten.sort_by(&:last_name)
  end

  def available_complementary_consultation_types
    intervention_domain.consultation_types.where.not(id: id).by_title
  end
end
