# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Appointment < ApplicationRecord
  include Loggable

  attribute :duration, :interval
  attr_accessor :next_appointment_consultation_type_id

  belongs_to :officer, optional: true, autosave: true
  accepts_nested_attributes_for :officer
  belongs_to :patient, optional: true, autosave: true
  accepts_nested_attributes_for :patient
  belongs_to :consultation_type
  has_one :intervention_domain, through: :consultation_type
  belongs_to :consultation_reason, optional: true
  belongs_to :calling_institution, optional: true
  belongs_to :consultation_subtype, optional: true
  belongs_to :user_in_charge, class_name: "User", optional: true
  belongs_to :previous_appointment, class_name: "Appointment", optional: true
  has_one :next_appointment, class_name: "Appointment", foreign_key: :previous_appointment_id, dependent: :nullify
  has_one :certificate, as: :certificatable, autosave: true
  has_one :medical_file
  has_many :attached_files, as: :attachable
  has_many :complementary_appointments, class_name: "Appointment", foreign_key: :originating_appointment_id
  belongs_to :originating_appointment, class_name: "Appointment", optional: true

  enum :cancelled_by, { patient: 1, umj: 2, opj: 3}

  scope :of_calling_institution, ->(institution_id) { institution_id.present? ? where(calling_institution_id: institution_id) : current_scope }
  scope :of_consultation_reason, ->(reason_id) { reason_id.present? ? where(consultation_reason_id: reason_id) : current_scope }
  scope :of_consultation_type, ->(type) { where(consultation_type: type) }
  scope :scheduled_on, ->(date) { where(starts_at: date.beginning_of_day..date.end_of_day)}
  scope :onml_id_like, ->(pattern) { pattern.present? ? where("LOWER(onml_id) like ?", "%#{pattern.downcase}%") : current_scope }
  scope :official_report_like, ->(pattern) { pattern.present? ? where("LOWER(official_report) like ?", "%#{pattern.downcase}%") : current_scope }
  scope :of_patient_birthdate, ->(date) { date.present? ? joins(:patient).where(patients: {birthdate: date}) : current_scope }
  scope :of_user_in_charge, ->(user_id) { user_id.present? ? where(user_in_charge_id: user_id) : current_scope }
  scope :patient_last_name_like, ->(pattern) { pattern.present? ? joins(:patient).where("LOWER(patients.last_name) like ?", "#{pattern.downcase}%") : current_scope }
  scope :updated_after, ->(datetime) { datetime.present? ? where("updated_at > ?", datetime) : current_scope }
  scope :with_emergency, ->(emergency) { emergency.present? ? where(emergency: emergency) : current_scope }
  scope :cancelled, -> { where.not(cancelled_at: nil) }
  scope :not_cancelled, -> { where(cancelled_at: nil) }
  scope :with_previous_appointment, -> { where.not(previous_appointment_id: nil) }
  scope :with_certificate_attached_files_of_type, ->(type) { joins(certificate: :attached_files).where(certificate: { attached_files: { type: "AttachedFile::#{type.to_s.upcase_first}Certificate" } })}

  after_save :generate_onml_id
  after_commit :touch_relevant_stuff, on: :update

  validate :duration_is_a_positive_interval
  def duration_is_a_positive_interval
    errors.add(:duration, "n'est pas un entier positif") unless duration.is_a?(ActiveSupport::Duration) && duration > 0
  end

  validates :starts_at, presence: true

  def self.between(floor, ceiling)
    floor_time = (floor.present? ? Date.parse(floor).beginning_of_day : nil)
    ceiling_time = (ceiling.present? ? Date.parse(ceiling).end_of_day : nil)

    if floor_time.present? && ceiling_time.present?
      self.where(starts_at: floor_time..ceiling_time)
    elsif floor_time.present?
      self.where("starts_at >= ?", floor_time)
    elsif ceiling_time.present?
      self.where("starts_at <= ?", ceiling_time)
    else
      all
    end
  end

  def self.of_patient_age_range(range)
    if range.present?
      range_floor = range[0]
      range_ceiling = range[1]
    end

    if range.present? && range_floor.present? && range_ceiling.present?
      birthdate_sql = "extract(year from age(appointments.starts_at, patients.birthdate)) between ? and ?"
      joins(:patient).where.not(patients: { birthdate: nil }).distinct.where("#{birthdate_sql}", range_floor, range_ceiling)
    else
      current_scope
    end
  end

  def self.of_patient_gender(gender)
    gender = "" if gender.nil?
    gender = nil if gender == "unfilled"
    if gender.nil? || gender.present?
      joins(:patient).where(patients: {gender: gender})
    else
      current_scope
    end
  end

  def self.with_certificate_status(status)
    case status
    when "signed"
      with_certificate_attached_files_of_type(:signed).where.not(id: self.with_certificate_attached_files_of_type(:validated))
    when "validated"
      with_certificate_attached_files_of_type(:validated).where(certificate: { delivered_at: nil})
    when "sent"
      with_certificate_attached_files_of_type(:validated).where.not(certificate: { delivered_at: nil})
    else
      current_scope
    end
  end

  def self.with_status(status)
    case status
    when "cancelled"
      cancelled.where.not(id: with_previous_appointment.pluck(:previous_appointment_id))
    when "postponed"
      where(id: with_previous_appointment.pluck(:previous_appointment_id)).cancelled
    when "patient_missing"
      where(patient_missing: true)
    else
      current_scope
    end
  end

  def cancelled?
    cancelled_at.present?
  end

  def concerns_minor?
    patient&.is_minor_on?(starts_at.to_date)
  end

  def ends_at
    @ends_at ||= starts_at + duration
  end

  def duplicate_for_consultation_type(consultation_type)
    duplicated = self.dup
    %i[
      calling_institution
      officer
      patient
      patient_arrived_at
      starts_at
    ].each do |assos|
      duplicated.send("#{assos}=", self.send(assos))
    end
    %i[
      cancelled_at
      cancelled_by
      consultation_reason_id
      consultation_subtype_id
      created_at
      onml_id
      original_patient_id
      patient_missing
      patient_released_at
      updated_at
      user_in_charge_id
    ].each do |assos|
      duplicated.send("#{assos}=", nil)
    end
    duplicated.consultation_type = consultation_type
    duplicated
  end

  def duplicate_for_postpone
    duplicated = self.dup
    %i[
      calling_institution
      consultation_type
      consultation_reason
      consultation_subtype
      officer
      patient
    ].each do |assos|
      duplicated.send("#{assos}=", self.send(assos))
    end
    %i[
      cancelled_at
      cancelled_by
      created_at
      onml_id
      original_patient_id
      patient_arrived_at
      patient_missing
      patient_released_at
      starts_at
      updated_at
      user_in_charge
    ].each do |assos|
      duplicated.send("#{assos}=", nil)
    end
    duplicated.previous_appointment = self
    duplicated
  end

  def postpone(origin)
    self.update({
      cancelled_at: Time.zone.now,
      cancelled_by: origin
    })
    self.attached_files.each do |attached_file|
      duplicate_attached_file = attached_file.dup
      duplicate_attached_file.file.attach(attached_file.file.blob)
      duplicate_attached_file.attachable = next_appointment
      duplicate_attached_file.save!
    end
  end

  def signed_certificate
    certificate.attached_files.where(type: "AttachedFile::SignedCertificate").last
  end

  def validated_certificate
    certificate&.attached_files&.where(type: "AttachedFile::ValidatedCertificate")&.last
  end

  def actual_medical_file
    originating_appointment ? originating_appointment.actual_medical_file : (medical_file || self.create_medical_file)
  end

  def signed_filename(extension)
    [
      [
        patient.fullname,
        onml_id,
        consultation_type.title,
        I18n.l(starts_at, format: :filename, locale: :fr)
      ].join(" - "),
      extension
    ].join(".")
  end

  def last_complementary_appointment_of_consultation_type(consultation_type)
    complementary_appointments.of_consultation_type(consultation_type).last
  end

  def state
    return :postponed if cancelled_at.present? && next_appointment
    return :cancelled if cancelled_at.present?
    return :certificate_delivered if certificate&.delivered_at.present?
    return :certificate_validated if certificate&.validated_at.present?
    return :certificate_completed if validated_certificate.present? || certificate&.questionnaire.present? && certificate&.data.present?
    return :patient_released if patient_released_at.present?
    return :taken_in_charge if user_in_charge.present?
    return :patient_missing if patient_missing
    return :patient_waiting if patient_arrived_at.present?
    return :patient_not_arrived
  end

  def state_icon
    case state
    when :cancelled
      "ban"
    when :certificate_completed
      "file-lines"
    when :certificate_delivered
      "envelope"
    when :certificate_validated
      "square-check"
    when :patient_missing
      "user-slash"
    when :patient_not_arrived
      "user-clock"
    when :patient_waiting
      "house-user"
    when :patient_released
      "user-check"
    when :postponed
      "rotate-right"
    when :taken_in_charge
      "handshake"
    end
  end

  def touch_relevant_stuff
    # for patient journey update
    originating_appointment&.touch
    originating_appointment&.complementary_appointments&.touch_all
  end

  private

  def generate_onml_id
    self.onml_id ||= if (identifier_prefix = intervention_domain.identifier_prefix).present?
      full_prefix = "%s%02i%02i" % [identifier_prefix.gsub(/[^\w]/, ""), starts_at.year % 100, starts_at.month]

      sequence_name = "flowing_consultations_onml_id_#{full_prefix}_seq"
      create_sequence_and_get_val = <<-SQL
      CREATE SEQUENCE IF NOT EXISTS #{sequence_name} OWNED BY flowing_consultations.onml_id;
      SELECT nextval('#{sequence_name}');
      SQL

      rows = self.class.connection.execute(create_sequence_and_get_val)
      nextval = rows.first["nextval"]
      onml_id = full_prefix + nextval.to_s

      self.update_columns(onml_id: onml_id) && onml_id
    end
  end
end
