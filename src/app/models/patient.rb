# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Patient < ApplicationRecord
  include Loggable
  include ActionView::Helpers::DateHelper

  enum :gender, { unknown: 0, woman: 1, man: 2}

  has_many :appointments
  has_many :flowing_consultations
  belongs_to :duplicated_patient, class_name: "Patient", optional: true
  belongs_to :probably_duplicated_patient, class_name: "Patient", optional: true

  default_scope { where(duplicated_patient_id: nil) }
  scope :fullname_asc, ->() { order(Arel.sql("LOWER(last_name) asc, LOWER(first_name) asc")) }
  scope :of_matching_lastname, ->(last_name) { where("last_name ILIKE ?", "#{last_name}%") }
  scope :of_matching_firstname, ->(first_name) { where("first_name ILIKE ?", "#{first_name}%") }
  scope :born_on_date, ->(date) { where(birthdate: date) }
  scope :born_on_year, ->(year) { where(birth_year: year).or(Patient.default_scoped.where("birthdate BETWEEN ? AND ?", Date.new(year.to_i,1,1), Date.new(year.to_i,12,31))) }

  validate :birthdate_within_birth_year
  validate :birthdate_from_string_valid?

  after_commit :touch_relevant_stuff, on: :update

  PAGINATIONS = [25, 50, 75, 100]
  paginates_per PAGINATIONS.min
  max_paginates_per PAGINATIONS.max

  def birthdate_from_string_valid?
    string_birthdate = attributes_before_type_cast["birthdate"].to_s
    string_birthdate.blank? || begin
      Date.parse(string_birthdate)
    rescue Date::Error
      errors.add(:birthdate, :invalid)
    end
  end

  def birthdate_within_birth_year
    if birth_year.present? && birthdate.present? && birth_year != birthdate.year
      errors.add(:birthdate, :must_match_birth_year)
    end
  end

  def aged_at(date)
    if birthdate.present?
      ActiveSupport::Duration.build(date.midday - birthdate.midday)
        .parts
        .slice(:years, :months)
        .map { |unit, val| I18n.t("datetime.distance_in_words.x_#{unit}", count: val) }
        .to_sentence
    elsif birth_year.present?
      oldest = ActiveSupport::Duration.build(date.midday - Date.new(birth_year, 1, 1).midday)
      youngest = ActiveSupport::Duration.build(date.midday - Date.new(birth_year, 12, 31).midday)
      lower_boundary, upper_boundary, unit = if oldest < 3.years
                                               [[youngest.in_months, 0].max, oldest.in_months, "months"]
                                             else
                                               [youngest.in_years, oldest.in_years, "years"]
                                             end
      "#{lower_boundary.floor}-#{I18n.t("datetime.distance_in_words.x_#{unit}", count: upper_boundary.floor)}"
    else
      '<abbr title="Non disponible">ND</abbr>'.html_safe
    end
  end

  def aged_at_now
    aged_at(Date.today)
  end

  def is_minor_on?(date)
    return nil unless date.is_a?(Date) && birthdate.present?
    age = date.year - birthdate.year
    age -= (date.month > birthdate.month || (date.month == birthdate.month && date.day >= birthdate.day) ? 0 : 1)
    age < 18
  end

  def human_birth
    birthdate ? I18n.l(birthdate) : birth_year
  end

  def fullname
    uppercase = last_name.blank? ? nil : last_name.upcase
    lowercase = first_name.blank? ? nil : first_name

    components = [uppercase, lowercase].compact
    if components.any?
      components.join(" ")
    else
      "Identité non-renseignée"
    end
  end

  def phone_numbers
    appointments.pluck(:patient_phone).compact.map { |phones| phones.split(",") }.flatten.map { |phone| phone.strip }.reject { |phone| phone.empty? }.uniq.join(", ")
  end

  def consultations_count
    appointments.count + flowing_consultations.count
  end

  def touch_relevant_stuff
    appointments.touch_all
    flowing_consultations.touch_all
  end

  def self.mark_all_duplicates!
    exact_duplicates(as_array: true).each do |duplicates|
      duplicates.delete((first_one = duplicates.min))
      where(id: duplicates).update_all(duplicated_patient_id: first_one)

      hand_over_consultations!(duplicates)
    end

    probable_duplicates(as_array: true).each do |duplicates|
      duplicates.delete((first_one = duplicates.min))
      where(id: duplicates).update_all(probably_duplicated_patient_id: first_one)
    end
  end

  def self.exact_duplicates as_array: false
    request = probable_duplicates.joins(:appointments).group("REGEXP_REPLACE(patient_phone, '[^[:digit:]]+', '', 'g')")
    as_array ? aggreg_ids(request) : request
  end

  def self.probable_duplicates as_array: false
    request = where.not(gender: nil).
      where.not(last_name: nil).
      where.not(first_name: nil).
      where.not(birthdate: nil).
      group(:gender, "LOWER(TRIM(last_name))", "LOWER(TRIM(first_name))", :birthdate).
      having("COUNT(DISTINCT patients.id) > 1")
    as_array ? aggreg_ids(request) : request
  end

  def self.aggreg_ids grouped
    grouped.pluck(Arel.sql("array_agg(patients.id)"));
  end

  def self.hand_over_consultations! duplicates
    ids_condition = ActiveRecord::Base.sanitize_sql_array(["p.id IN (?)", duplicates])

    hand_over_appointments_sql = <<~SQL
      UPDATE appointments
      SET original_patient_id = patient_id, patient_id = p.duplicated_patient_id
      FROM patients p
      WHERE patient_id = p.id
      AND #{ids_condition};
    SQL
    puts ActiveRecord::Base.connection.exec_query(hand_over_appointments_sql)

    hand_over_flowing_consultations_sql = <<~SQL
      UPDATE flowing_consultations
      SET original_patient_id = patient_id, patient_id = p.duplicated_patient_id
      FROM patients p
      WHERE patient_id = p.id
      AND #{ids_condition};
    SQL
    puts ActiveRecord::Base.connection.exec_query(hand_over_flowing_consultations_sql)
  end

  def self.rollback_all_duplicates!
    give_back_appointments_sql = <<~SQL
      UPDATE appointments
      SET patient_id = original_patient_id, original_patient_id = NULL
      WHERE original_patient_id IS NOT NULL
    SQL
    ActiveRecord::Base.connection.exec_query(give_back_appointments_sql)

    give_back_flowing_consultations_sql = <<~SQL
      UPDATE flowing_consultations
      SET patient_id = original_patient_id, original_patient_id = NULL
      WHERE original_patient_id IS NOT NULL
    SQL
    ActiveRecord::Base.connection.exec_query(give_back_flowing_consultations_sql)

    self.unscope(where: :duplicated_patient_id).update_all(duplicated_patient_id: nil, probably_duplicated_patient_id: nil)
  end
end
