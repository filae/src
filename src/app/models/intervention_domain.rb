# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class InterventionDomain < ApplicationRecord
  has_many :consultation_types, dependent: :destroy
  has_many :schedules, dependent: :destroy
  has_many :domain_authorizations, dependent: :destroy
  has_many :roles, through: :domain_authorizations
  has_many :flowing_consultations

  enum :modus_operandi, {
    appointments: 0,
    stream: 1
  }

  validates :title, presence: true, uniqueness: true
  validates :modus_operandi, presence: true

  scope :stream_ones_for_user, ->(user) { joins(:roles).where(roles: user.role).distinct }
  scope :appointments_ones_for_user, ->(user) { joins(consultation_types: [ :roles ]).where(consultation_types: { roles: user.role}).distinct }
  scope :by_title, ->(direction = :asc) { order(title: direction)  }

  def self.translated_modus_operandi
    self.modus_operandis.map { |k, v| { self.human_attribute_name("modus_operandi.#{k}") => k } }.reduce(:merge)
  end
end
