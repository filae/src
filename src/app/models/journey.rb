# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Journey
  def initialize(appointment)
    @appointment = appointment
  end

  def title
    @title ||= consultation_type.title
  end

  def abbr_title
    @abbr_title ||= title[0].upcase
  end

  def icon_name
    @icon_name ||= consultation_type.icon_name
  end

  def state
    @state ||= @appointment.state
  end

  def valid?
    state != :patient_not_arrived && !@appointment.cancelled?
  end

  def children
    @children ||= @appointment.complementary_appointments.not_cancelled.map { |app| Journey.new(app) }
  end

  def self.full(appointment)
    if (parent = appointment.originating_appointment)
      full(parent)
    else
      new(appointment)
    end
  end

  private

  def consultation_type
    @consultation_type ||= @appointment.consultation_type
  end
end
