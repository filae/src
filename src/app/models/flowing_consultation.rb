# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultation < ApplicationRecord
  Accordion = Struct.new(:id, :collapsed, :pluralize)
  ACCORDIONS = [
    Accordion.new("to-transmit", false, false),
    Accordion.new("in-progress", false, false),
    Accordion.new("validated", true, true),
    Accordion.new("cancelled", true, true)
  ].freeze

  TIPPING_DELAY = 12.hours

  belongs_to :intervention_domain
  belongs_to :officer, optional: true, autosave: true
  accepts_nested_attributes_for :officer
  belongs_to :patient, optional: true, autosave: true
  accepts_nested_attributes_for :patient
  belongs_to :consultation_type, optional: true
  belongs_to :calling_institution, optional: true
  belongs_to :assignee, class_name: "User", optional: true
  has_many :attached_files, as: :attachable
  has_many :certificates, as: :certificatable, autosave: true

  alias user_in_charge assignee
  alias_attribute :starts_at, :assigned_at

  enum :cancelled_by, {
    "custody-ended": 1,
    "sent-to-hospital": 2,
    "brought-before-the-courts": 3,
    "unconcerned": 4,
    "sent-to-morgue": 5,
    "moved-to-appointments": 6,
    "other": 7
  }

  scope :of_intervention_domain, ->(domain) { where(intervention_domain_id: domain.id) }
  scope :of_consultation_type, ->(type) { where(consultation_type_id: type.id) }
  scope :of_calling_institution, ->(institution) { where(calling_institution_id: institution.id) }
  scope :patient_last_name_like, ->(pattern) { joins(:patient).where("LOWER(patients.last_name) like ?", "#{pattern.downcase}%") }
  scope :of_assignee, ->(user) { where(assignee_id: user.id)}
  scope :archived, -> { where("cancelled_at <= ?", FlowingConsultation::TIPPING_DELAY.ago).or(where("validated_at <= ?", FlowingConsultation::TIPPING_DELAY.ago)) }
  scope :active, -> { where.not(id: archived) }
  scope :last_updated, -> { order(updated_at: :desc).limit(1).first }
  scope :validated, -> { where.not(validated_at: nil) }
  scope :cancelled, -> { where.not(cancelled_at: nil) }
  scope :assigned, -> { where.not(assigned_at: nil) }
  scope :to_transmit, -> { active.where.not(id: (validated.pluck(:id) + cancelled.pluck(:id) + assigned.pluck(:id)))}
  scope :in_progress, -> { assigned.where.not(id: (validated.pluck(:id) + cancelled.pluck(:id))) }
  scope :by_calling_areas_and_institutions, -> { left_outer_joins(calling_institution: :calling_area).order("calling_areas.title ASC", "calling_institutions.title ASC") }

  validates :called_at, presence: true
  validates :consultation_type, presence: { unless: :simple_message? }
  validates :assigned_at, presence: { if: -> { assignee_id.present? } }

  after_save :generate_onml_id

  def self.sort_with_instructions(sort_instructions)
    sort_instructions = (sort_instructions.blank? ? "-called_at" : sort_instructions)

    order_instructions = []
    sort_instructions.split(",").each do |instruction|
      matches = instruction.match(/(-)?(\w*)/)
      way = matches[1] ? "DESC" : "ASC"
      order_instructions << "#{ matches[2] } #{ way }" if FlowingConsultation.new.has_attribute?(matches[2])
    end
    order(order_instructions.join(","))
  end

  def self.of_patient_age_range(range_floor, range_ceiling)
    birth_year_sql = "EXTRACT(year FROM flowing_consultations.called_at) - patients.birth_year BETWEEN ? AND ?"
    birth_year_scope = joins(:patient).where(birth_year_sql, range_floor, range_ceiling)
    birthdate_sql = "patients.birthdate > (flowing_consultations.called_at::DATE - INTERVAL '? years') AND patients.birthdate <= (flowing_consultations.called_at::DATE - INTERVAL '? years')"
    birthdate_scope = joins(:patient).where(birthdate_sql, range_ceiling+1, range_floor)
    birth_year_scope.or(birthdate_scope).distinct
  end

  def self.of_patient_gender(gender)
    gender = nil if gender == "unfilled"
    if gender.nil? || gender.present?
      joins(:patient).where(patients: {gender: gender})
    else
      all
    end
  end

  def self.with_status(status)
    case status
    when "cancelled"
      where.not(cancelled_at: nil)
    when "validated"
      where.not(validated_at: nil)
    else
      all
    end
  end

  def self.between(column, floor, ceiling)
    column = column.to_s
    return all if column.empty?
    return none unless %w[called_at cancelled_at validated_at assigned_at].include?(column)

    floor = (floor.present? ? Time.zone.parse(floor).utc.iso8601 : nil)
    ceiling = (ceiling.present? ? Time.zone.parse(ceiling).utc.iso8601 : nil)
    if floor && ceiling
      self.where(column => floor..ceiling)
    elsif floor
      self.where("#{column} >= ?", floor)
    elsif ceiling
      self.where("#{column} <= ?", ceiling)
    else
      none
    end
  end

  def simple_message?
    return officer&.lambda_user
  end

  def archived?
    tipping_date = FlowingConsultation::TIPPING_DELAY.ago
    cancelled_at.try(:<=, tipping_date) || validated_at.try(:<=, tipping_date)
  end

  def active?
    !archived?
  end

  def signed_filename(extension)
    [
      [
        patient&.fullname,
        onml_id,
        consultation_type.title,
        I18n.l(Time.zone.now, format: :filename, locale: :fr)
      ].join(" - "),
      extension
    ].join(".")
  end

  def validated?
    validated_at.present?
  end

  def cancelled?
    cancelled_at.present?
  end

  def assigned?
    assigned_at.present?
  end

  def in_progress?
    active? && assigned? && !validated? && !cancelled?
  end

  def to_transmit?
    active? && !assigned? && !validated? && !cancelled?
  end

  def state
    return :cancelled if cancelled_at.present?
    return :validated if validated_at.present?
    return :in_progress if assigned_at.present?
    return :to_transmit
  end

  def state_icon
    case state
    when :cancelled
      "ban"
    when :in_progress
      "user-doctor"
    when :to_transmit
      "hourglass-empty"
    when :validated
      "check"
    end
  end

  private

  def generate_onml_id
    self.onml_id ||= if (identifier_prefix = intervention_domain.identifier_prefix).present?
      full_prefix = "%s%02i%02i" % [identifier_prefix.gsub(/[^\w]/, ""), called_at.year % 100, called_at.month]

      sequence_name = "flowing_consultations_onml_id_#{full_prefix}_seq"
      create_sequence_and_get_val = <<-SQL
      CREATE SEQUENCE IF NOT EXISTS #{sequence_name} OWNED BY flowing_consultations.onml_id;
      SELECT nextval('#{sequence_name}');
      SQL

      rows = self.class.connection.execute(create_sequence_and_get_val)
      nextval = rows.first["nextval"]
      onml_id = full_prefix + nextval.to_s

      self.update_columns(onml_id: onml_id) && onml_id
    end
  end

end
