# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module AppointmentDuration
  extend ActiveSupport::Concern
 
  included do
    attribute :appointment_duration, :interval
    attr_accessor :appointment_duration_in_minutes

    validate do
      if @appointment_duration_in_minutes.present?
        errors.add(:appointment_duration_in_minutes, :not_an_integer) unless @appointment_duration_in_minutes =~ /\A[+-]?\d+\Z/
        # https://github.com/rails/rails/issues/41270 explains why **
        errors.add(:appointment_duration_in_minutes, :greater_than_or_equal_to, **{ count: 0 }) if @appointment_duration_in_minutes =~ /\A-\d+\Z/
      else
        errors.add(:appointment_duration_in_minutes, :blank) unless appointment_duration_in_minutes.present?
      end
    end

    def appointment_duration_in_minutes=(raw_value)
      @appointment_duration_in_minutes = raw_value
      if raw_value.present?
        if raw_value =~ /\A[+]?\d+\Z/
          write_attribute(:appointment_duration, ActiveSupport::Duration.build(raw_value.to_i*60))
        end
      else
        write_attribute(:appointment_duration, nil)
      end
    end

    def appointment_duration_in_minutes
      (duration = appointment_duration).nil? ? nil : duration.to_i/60
    end
  end
end
