# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class PdfGenerator
  attr_reader :blob
  attr_reader :data

  def initialize(blob)
    raise "'blob' must be an ActiveStorage object" unless blob.is_a?(ActiveStorage::Attached::One)
    @blob = blob
  end

  def generate
    @blob.open do |doc|
      Dir.chdir(Dir.tmpdir) do
        set_pdf_file_path(doc.path)

        if system "libreoffice --headless --convert-to pdf #{doc.path} > /dev/null 2>&1"
          @data = File.read @pdf_file_path
          File.delete(@pdf_file_path)
        else
          return false
        end
      end
    end
    self
  end

  def content_type
    "application/pdf"
  end

  private

  def set_pdf_file_path(path)
    extension = ".pdf"
    @pdf_file_path = (base_path = path.match(/\A(.*)\..*\z/)&.[](1)) ? base_path + extension : path + extension
  end
end
