# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificateTemplate < ApplicationRecord
  has_many :certificate_templates_roles, dependent: :destroy
  has_many :roles, through: :certificate_templates_roles

  has_one :master_document, as: :attachable

  validates :title, presence: true

  scope :enabled, -> { where(disabled_at: nil) }
  scope :by_title, -> { order(title: :asc) }
  scope :for_user, ->(user) { joins(:roles).where(roles: user.role).distinct }
  scope :with_master_document, -> { joins(:master_document) }

  def disabled?
    disabled_at.present?
  end

  def enabled?
    !disabled?
  end

  def embedded?
    questionnaire.present?
  end
end
