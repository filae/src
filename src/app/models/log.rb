# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Log < ApplicationRecord
  validates :what_id, :occured_at, :who_id, :event, presence: true

  scope :for_item, ->(item) { where(what_id: item.to_global_id.to_s)}

  def self.generate(item, user, occured_at, actions)
    if [FlowingConsultation, Appointment].include?(item.class)
      what = item.onml_id
    end

    attributes = {
      what: what,
      what_id: item.to_global_id,
      occured_at: occured_at,
      who: user.name,
      who_id: user.to_global_id,
      event: { actions: actions.map { |action| JSON.parse(action) } }.to_json
    }

    Log.create!(attributes)
  end

  def actions
    [].tap do |log_actions|
      JSON.parse(event)["actions"].each do |action|
        log_actions << Log::Action.new(action, self)
      end
    end
  end

  def actions_label
    JSON.parse(event)["actions"].map { |action| I18n.t("logs.actions.#{action['action']}") }.join(", ")
  end
end
