# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Calendars::Day
  include ActiveModel::Model

  attr_reader :id, :date, :duty_periods, :appointments

  def initialize(date: Date.today, duty_periods: [], appointments: [], assignment_mode: false)
    @id = date.iso8601
    @date = date
    @duty_periods = duty_periods
    @appointments = appointments
    @assignment_mode = assignment_mode
  end

  def resources
    return [Calendars::Resource.default] if @assignment_mode
    @resources ||= [].tap do |result|
      earliest_resource = nil
      sorted_duty_periods.each do |duty_period|
        (duty_period.assignees.presence || 1.times).each do
          available_resource = if earliest_resource&.can_include_duty_period?(duty_period)
            earliest_resource
          else
            (result << Calendars::Resource.number(result.count + 1)).last
          end
          available_resource.append_duty_period(duty_period)

          earliest_resource = result.min { |a, b| a.duty_periods_end_at <=> b.duty_periods_end_at }
        end
      end
    end.presence || [Calendars::Resource.default]
  end

  def assignments_events
    duty_periods.map { |duty_period| Calendars::Event.for_assignment(duty_period) }
  end

  def agenda_events
    if @assignment_mode
      assignments_events
    else
      open_hours_events + appointments_events
    end
  end

  def open_hours_events
    Calendars::Event.all_open_hours(resources)
  end

  def appointments_events
    Calendars::Event.all_appointments(resources_with_appointments)
  end

  def resources_with_appointments
    sorted_appointments.each do |appointment|
      available_resource = resources.find do |resource|
        resource.can_include_appointment?(appointment)
      end || resources.first

      available_resource.append_appointment(appointment)
    end
    resources
  end

  def sorted_duty_periods
    @sorted_duty_periods ||= duty_periods.sort_by do |period|
      # starts_at ASC, ends_at DESC
      [period.starts_at, -period.ends_at.to_i] # to_i because of undefined method `-@' for Time
    end
  end

  def sorted_appointments
    @sorted_appointments ||= appointments.sort_by do |appointment|
      # not_cancelled first, starts_at ASC, ends_at DESC
      [
        appointment.cancelled? ? 1 : -1,
        appointment.starts_at,
        -appointment.ends_at.to_i # to_i because of undefined method `-@' for Time
      ]
    end
  end
end
