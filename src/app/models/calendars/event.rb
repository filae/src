# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Calendars::Event
  include ActiveModel::Model

  attr_accessor :id, :title, :start, :end, :cancelled, :color, :text_color, :background_color, :border_color, :display, :resource_id, :extended_props

  def self.for_assignment duty_period
    new(
      id: duty_period.id,
      title: duty_period.assignees.map(&:name).join(", ").presence || Calendars::Resource.default.title,
      start: duty_period.starts_at,
      end: duty_period.ends_at,
      color: duty_period.assignees.any? ? "mediumseagreen" : "gray",
      extended_props: { type: "assignment" },
      resource_id: Calendars::Resource::DEFAULT
    )
  end

  def self.for_appointment appointment
    new(
      id: appointment.id,
      title: ApplicationController.helpers.event_appointment_title(appointment),
      start: appointment.starts_at,
      end: appointment.starts_at + appointment.duration,
      text_color: (appointment.cancelled? ? "black" : ""),
      background_color: (appointment.cancelled? ? "lightgrey" : (appointment.patient_missing ? "darkgrey" : appointment.consultation_reason&.colour)),
      border_color: (appointment.cancelled? ? "lightgrey" : (appointment.consultation_reason&.colour)),
      extended_props: { type: "appointment" },
    )
  end

  def self.for_open_hours duty_period
    new(
      id: duty_period.id,
      start: duty_period.starts_at,
      end: duty_period.ends_at,
      color: duty_period.assignees.any? ? "mediumseagreen" : "gray",
      display: "background",
      extended_props: { type: "open-hour" },
    )
  end

  def self.all_open_hours resources
    resources.flat_map do |resource|
      resource.duty_periods.map do |period|
        for_open_hours(period).tap do |event|
          event.resource_id = resource.id
        end
      end
    end
  end

  def self.all_appointments resources
    resources.flat_map do |resource|
      resource.appointments.map do |appointment|
        for_appointment(appointment).tap do |event|
          event.resource_id = resource.id unless event.nil?
        end
      end.compact
    end
  end

  def as_json *args
    super(*args).transform_keys! { |key| key.camelize(:lower) }
  end
end
