# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Calendars::Resource
  include ActiveModel::Model
  DEFAULT = "Ø"

  attr_accessor :id, :title, :duty_periods, :appointments

  def self.number index
    new(id: index, title: index.to_s, duty_periods: [], appointments: [])
  end

  def self.default
    new(id: DEFAULT, title: DEFAULT, duty_periods: [], appointments: [])
  end

  def duty_periods_end_at
    duty_periods.map(&:ends_at).max
  end

  def can_include_duty_period? duty_period
    duty_periods_end_at <= duty_period.starts_at
  end

  def append_duty_period duty_period
    duty_periods << duty_period
  end

  def appointments_end_at
    appointments.map(&:ends_at).max
  end

  def can_include_appointment? appointment
    appointments.empty? || appointments_end_at <= appointment.starts_at
  end

  def append_appointment appointment
    appointments << appointment
  end
end
