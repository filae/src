# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module AppointmentEventHelper
  def event_appointment_title(appointment)
    safe_join [
      time_slot(appointment),
      requisition_received(appointment),
      tag.br,
      full_journey_span(appointment),
      emergency_sign(appointment),
      minority_sign(appointment),
      consultation_reason_sign(appointment),
      patient_fullname(appointment),
      state_sign(appointment)
    ]
  end

  def time_slot(appointment)
    start_time = l(appointment.starts_at, format: :only_time)
    end_time = l(appointment.ends_at, format: :only_time)
    "#{start_time} - #{end_time}"
  end

  def requisition_received(appointment)
    tag.span " R", title: "Réquisition reçue" if appointment.requisition_received_at
  end

  def consultation_reason_sign(appointment)
    return unless (reason = appointment.consultation_reason)

    tag.span "[#{reason.code}] ", title: reason.title
  end

  def emergency_sign(appointment)
    return unless appointment.emergency

    tag.span class: "text-danger" do
      fa_icon "exclamation-circle", title: "Urgence"
    end
  end

  def minority_sign(appointment)
    return unless appointment.concerns_minor?

    tag.span class: "#{dom_id(appointment.patient)}_minority" do
      minority_icon_for(appointment)
    end
  end

  def patient_fullname(appointment)
    return unless (patient = appointment.patient)

    tag.span class: "#{dom_id(patient)}_fullname" do
      patient.fullname
    end
  end

  def state_sign(appointment)
    fa_icon appointment.state_icon, title: t("activerecord.attributes.appointment.state.#{appointment.state}")
  end

  # Patient journey
  def full_journey_span(appointment)
    root_journey = Journey.full(appointment)
    return unless root_journey.valid?

    tag.span class: ["PatientJourney", "#{dom_id(appointment)}_patient_journey"] do
      journey_spans(root_journey)
    end
  end

  def journey_spans(journey)
    journey_items = [journey_span(journey)]
    unless journey.children.empty?
      journey_items.unshift "("
      journey_items << " » "
      journey_items << children_journey_spans(journey)
      journey_items << ")"
    end
    safe_join(journey_items)
  end

  def journey_span(journey)
    return unless journey.valid?

    tag.span class: patient_class(journey), title: journey.title do
      if (icon_name = journey.icon_name.presence)
        fa_icon icon_name, narrow: true
      else
        journey.abbr_title
      end
    end
  end

  def children_journey_spans(journey)
    safe_join(journey.children.map do |child_journey|
      journey_spans(child_journey)
    end, " & ").presence
  end

  def patient_class(journey)
    "Patient--#{case journey.state
                when :patient_released, :certificate_validated, :certificate_delivered
                  'out'
                when :patient_waiting
                  'waiting'
                when :taken_in_charge
                  'taken-in-charge'
                else
                  'unavailable'
                end}"
  end
end
