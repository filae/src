# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module ApplicationHelper
  NO_BREADCRUMB = []

  def head_title
    connection_info = [current_user.first_name, "Filaé"].compact.join("@")

    homeless_breadcrumbs = breadcrumbs[1..] || NO_BREADCRUMB
    breadcrumbs_info = strip_tags(homeless_breadcrumbs.reverse.map(&:text).join(" - ")).presence

    [
      connection_info,
      breadcrumbs_info
    ].compact.join(" | ")
  end

  def abbreviated_label(label)
    content_tag :abbr, title: t("labels.#{label}.text") do
      I18n.t "labels.#{label}.abbr"
    end
  end

  def abbr_assignee_names(assignees)
    assignees.present? ? assignees.map(&:abridged_name).to_sentence : "Ø"
  end

  def on_two_lines(text)
    return text if text.blank?

    half_length = text.length / 2
    first_half = text[..half_length]

    if (index_of_new_line = first_half.rindex(/\W/))
      [
        text[..index_of_new_line],
        text[index_of_new_line.succ..]
      ].join("<br />").html_safe
    else
      text
    end
  end

  def attached_file_info(klass)
    content_tag :span, data: { controller: "tooltip", bs_placement: "right", bs_title: klass.info } do
      fa_icon("info-circle")
    end
  end

  def cancel_button(path)
    link_to t("helpers.actions.cancel"), path, class: ["btn", "btn-secondary"]
  end

  def dismiss_modal_button
    button_tag "Annuler", type: "button", class: ["btn", "btn-secondary"], data: { "bs-dismiss": "modal" }
  end

  def custom_add_link(path, title: "Ajouter", format: :short, bs_type: :primary)
    link_to custom_link_name(format, title, "plus"), path, class: ["btn", "btn-#{bs_type}", "mx-2"]
  end

  def custom_appointment_attached_files_label(appointment)
    nb_attached_files = appointment.attached_files.of_type("standard").count + appointment.attached_files.of_type("medical_content").count
    humanize_with_prefixed_count(AttachedFile, nb_attached_files)
  end

  def custom_attached_files_link(item)
    url, nb_attached_files = case item
    when FlowingConsultation
      [flowing_consultation_attached_files_path(item), item.attached_files.count]
    when Appointment
      [appointment_attached_files_path(item),(item.attached_files.count + item&.certificate&.attached_files&.count.to_i) ]
    end
    link_to "#{fa_icon('paperclip', title: 'Fichiers attachés')} #{nb_attached_files}".html_safe, url, class: ["btn", "btn-secondary", "text-nowrap"], data: { turbo_frame: "attached_files" }
  end

  def custom_certificates_link(item)
    link_to "#{fa_icon('certificate', title: 'Certificats')} #{item.certificates.count}".html_safe, certificates_flowing_consultation_path(item), class: ["btn", "btn-outline-warning"], data: { turbo_frame: "certificates" }
  end

  def custom_edit_link(path, title: "Modifier", icon: "pencil-alt", format: :short, bs_type: :warning, **options)
    link_to custom_link_name(format, title, icon), path, class: ["text-#{bs_type}"], **options
  end

  def custom_delete_link(path)
    link_to fa_icon("trash-can", title: "Supprimer"), path, method: :delete, class: ["text-danger"], data: { confirm: "Êtes-vous sûr de vouloir supprimer cet élément ?" }
  end

  def custom_link_name(format, title, icon)
    case format
    when :short
      fa_icon(icon, title: title)
    when :long
      fa_icon(icon) + title
    else
      title
    end
  end

  def display_duration(duration, with_parenthesis: true)
    display_parts = []
    duration.parts.each { |unit, value| display_parts << "#{t "datetime.distance_in_words.x_#{unit}", count: value}" }
    display = display_parts.join(", ")
    with_parenthesis ? "(#{display})" : display
  end

  def display_log(log)
    changes = content_tag :span, fa_icon("info-circle"), data: { controller: "tooltip", "bs-placement": "right", "bs-title": json_escape(JSON.pretty_generate(log.event)) }
    fa_icon("clock") +
      "#{log.actions_label} le #{I18n.l log.occured_at} par #{log.who}".capitalize +
      changes
  end

  def fa_icon(name, style: "solid", narrow: false, title: nil)
    content_tag :i, nil, class: ["fa-#{style}", "fa-#{name}", (narrow && "p-0" || nil)], title: title
  end

  def filae_version
    ENV["FILAE_VERSION"] || "N/A"
  end

  def humanized_gender(item, abbreviated: false)
    gender = (item&.gender ? item.gender : "unknown")
    if abbreviated
      content_tag :abbr, title: I18n.t("gender.#{gender}.normal") do
        I18n.t "gender.#{gender}.abbreviated"
      end
    else
      I18n.t "gender.#{gender}.normal"
    end
  end

  def humanize_with_prefixed_count(clazz, count = 0)
    [count, clazz.model_name.human(count: count)].join(" ")
  end

  def localize_date(date, format: :default)
    I18n.l(date, format: format) if date.present?
  end

  def localize_for_sentence(item)
    case item
    when DutyPeriod
      "#{l item.starts_at, format: :sentence_date} de #{l item.starts_at, format: :sentence_time} à #{l item.ends_at, format: :sentence_time}"
    end
  end

  def localize_slot(item, starts_at: :starts_at, ends_at: :ends_at)
    "#{localize_date(item.send(starts_at), format: :input)} - #{localize_date(item.send(ends_at), format: :only_time)}"
  end

  def minority_icon_for(context)
    context.concerns_minor? ? fa_icon("child", title: "Personne mineure à la date du rendez-vous.") : ""
  end

  def search_label(item)
    case item
    when CallingInstitution
      item.title
    when Patient
      "#{item.fullname} - #{item.birthdate ? l(item.birthdate) : item.birth_year} (#{humanized_gender item})"
    else
      raise "Render me in application_helper#search_label!"
    end if item
  end

  def spinner
    content_tag :div, class: ["spinner-border"] do
      content_tag :span, class: ["visually-hidden"] do
        "Chargement en cours..."
      end
    end
  end

  def todo(text)
    content_tag :span, class: ["text-danger"] do
      fa_icon("triangle-exclamation", title: text)
    end
  end
end
