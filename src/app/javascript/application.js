/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
// Entry point for the build script in your package.json
require("@rails/ujs").start()
import * as bootstrap from "bootstrap"
import "@fortawesome/fontawesome-free/js/all"
import "./controllers"
import "@hotwired/turbo-rails"

window.FontAwesome.config.keepOriginalSource = false

// To allow FontAwesome SVG elements in tooltips
const myDefaultAllowList = bootstrap.Tooltip.Default.allowList
myDefaultAllowList.svg = ["xmlns",,"viewbox", "focusable", "data-fa-i2svg", "data-prefix", "data-icon"]
myDefaultAllowList.path = ["fill", "d"]
