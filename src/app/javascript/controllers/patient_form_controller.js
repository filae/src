/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import moment from 'moment';

const DATE_FORMAT = 'YYYY-MM-DD'

export default class extends Controller {
  static targets = [
    'wholeForm',
    'birthYearInput',
    'birthdateInput',
    'submitButton'
  ]

  birthdateValueChanged() {
    this.configureForm()
  }

  configureForm() {
    const birthdate = this.birthdateInputTarget.value;

    if (birthdate) {
      let newBirthDate = moment(birthdate, DATE_FORMAT, true);
      let newBirthYear = this.birthYearInputTarget.value;

      if (newBirthYear) {
        if (newBirthDate.year() == newBirthYear) {
          this.prepareToSubmit();
        } else {
          this.disableSubmitButton()
          this.showBirthdateError("L'année de la date de naissance doit correspondre à l'année de naissance ci-dessous")
          this.showBirthYearInput()
        }
      } else {
        this.prepareToSubmit()
      }
    } else {
      this.removeBirthdateError()
      this.showBirthYearInput()
    }

    this.submitButtonTarget.toggleAttribute("disabled", !this.wholeFormTarget.checkValidity());
  }

  showBirthYearInput() {
    this.birthYearInputTarget.parentNode.style.display = 'block'
  }

  hideBirthYearInput() {
    this.birthYearInputTarget.parentNode.style.display = 'none'
  }

  enableSubmitButton() {
    this.submitButtonTarget.removeAttribute("disabled");
  }

  disableSubmitButton() {
    this.submitButtonTarget.setAttribute('disabled', 'disabled')
  }

  showBirthdateError(error) {
    this.removeBirthdateError()
    let error_div = document.getElementById('year-mismatch')
    if (!error_div) {
      error_div = document.createElement('div')
      error_div.id = 'year-mismatch'
      error_div.classList.add('invalid-feedback')
      error_div.innerHTML = error
      this.birthdateInputTarget.after(error_div)
    }
    this.birthdateInputTarget.classList.add('is-invalid')
  }

  removeBirthdateError() {
    let error_div = document.getElementById('year-mismatch')
    if (error_div) {
      error_div.remove()
    }
    this.birthdateInputTarget.classList.remove('is-invalid')
  }

  showBirthdateValid() {
    this.birthdateInputTarget.classList.add('is-valid')
  }

  prepareToSubmit() {
    this.hideBirthYearInput()
    this.removeBirthdateError()
    this.showBirthdateValid()
    this.enableSubmitButton()
  }
}
