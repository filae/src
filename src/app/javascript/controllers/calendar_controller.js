/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { Calendar } from "@fullcalendar/core"
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import { get } from "@rails/request.js"
import { Tooltip } from "bootstrap"
import moment from 'moment'
import momentTimezonePlugin from '@fullcalendar/moment-timezone'

let eventMinHeight = 15;

// Connects to data-controller="calendar"
export default class extends Controller {
  static values = {
    date: String,
    resources: Array,
    events: Array,
    newAppointmentUrl: String,
    refreshUrl: String,
    refreshFrameTagName: String,
  }
  static targets = [ 'calendar' ]

  connect() {
    const calendar = new Calendar(this.calendarTarget, {
      plugins: [
        resourceTimeGridPlugin,
        interactionPlugin,
        momentTimezonePlugin,
      ],
      editable: false,
      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      initialView: 'resourceTimeGridDay',
      locale: 'fr',
      timeZone: 'Europe/Paris',
      headerToolbar: false,
      allDaySlot: false,
      nowIndicator: true,
      height: '100%',
      expandRows: true,
      initialDate: this.dateValue,
      slotMinTime: '08:00:00',
      slotMaxTime: '18:00:00',
      resources: this.resourcesValue,
      events: this.eventsValue,
      eventMinHeight,
      eventShortHeight: eventMinHeight,
      displayEventTime: false,
      eventDidMount: (arg) => {
        let event = arg.event
        if (event.extendedProps.type == 'appointment') {
          let element = arg.el

          let start = moment(event.start).format("HH:mm")
          let end = moment(event.end).format("HH:mm")
          let tooltip = `${start} - ${end}`
          element.dataset.testEventTime = `${tooltip}`

          element.querySelectorAll('.fc-event-title')[0].innerHTML = event.title

          new Tooltip(element, {
            title: event.title,
            html: true,
            placement: 'top',
            trigger: 'hover',
            container: 'body'
          })
        }
      },
    })

    calendar.on('eventClick', (element) => {
      let event = element.event
      let eventType = event.extendedProps.type
      switch(eventType) {
        case 'appointment':
          get(`/appointments/${event.id}`, { responseKind: 'turbo-stream' })
          break
        case 'assignment':
          get(`/duty_periods/${event.id}/edit`, { responseKind: 'turbo-stream' })
          break
      }
    })

    calendar.on('dateClick', (info) => {
      if (this.element.closest('form')) {
        this.element.dispatchEvent(new CustomEvent("newStartsAtSelected", {
          detail: {
            message: info.dateStr,
          },
          bubbles: true,
        }))
      } else {
        Turbo.visit(this.newAppointmentUrlValue + '?starts_at=' + info.dateStr)
      }
    })

    calendar.render()
  }
}
