/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import moment from "moment"

const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm'

export default class extends Controller {
  static values = {
    consultationReasonDuration: Number,
    consultationSubtypeDuration: Number,
    duration: Number,
  }

  static targets = [
    'durationHint',
    'durationInput',
    'startsAtInput',
  ]

  consultationReasonDurationValueChanged() {
    this.displayDurationHint()
  }

  consultationSubtypeDurationValueChanged() {
    this.displayDurationHint()
  }

  setDuration(event) {
    let selectNode = event.target
    let optionValue = selectNode.value
    let selectedDuration = Array.from(selectNode.children)
      .filter((node) => node.value == optionValue)[0]
      .dataset
      .duration

    if (/subtype/.test(selectNode.id)) {
      this.consultationSubtypeDurationValue = selectedDuration
    }

    if (/reason/.test(selectNode.id)) {
      this.consultationReasonDurationValue = selectedDuration
    }

    if (this.durationValue == 0) {
      this.durationInputTarget.value = this.consultationSubtypeDurationValue || this.consultationReasonDurationValue
    }
  }

  durationTyped(event) {
    this.durationValue = event.target.value
  }

  setStartsAt(event) {
    let newStartsAt = moment(event.detail.message)
    this.startsAtInputTarget.value = newStartsAt.format(DATETIME_FORMAT)
  }

  displayDurationHint() {
    let durationHint = ''
    let duration = 0

    if (this.consultationReasonDurationValue > 0 || this.consultationSubtypeDurationValue > 0) {
      duration = this.consultationSubtypeDurationValue || this.consultationReasonDurationValue
      durationHint = `Durée recommandée : ${duration} minutes.`
    }

    this.durationHintTarget.innerHTML = durationHint

    if (!this.durationInputTarget.value && this.durationValue == 0) {
      this.durationInputTarget.value = duration
    }
  }
}
