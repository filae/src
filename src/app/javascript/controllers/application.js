/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Application } from "@hotwired/stimulus"

const application = Application.start()

// Configure Stimulus development experience
application.debug = false
window.Stimulus   = application

function handleResponse(event) {
  let response = event.detail.fetchResponse.response
  if (response.redirected && !document.body.dataset.willRedirect) {
    document.body.dataset.willRedirect = true
    let redirectUrl = response.url
    const newSessionRegexp = new RegExp('session/new')
    if (newSessionRegexp.test(redirectUrl) && !document.body.dataset.passwordReset) {
      event.preventDefault()
      Turbo.visit(redirectUrl)
    }
  }
}

document.addEventListener('turbo:before-fetch-response', handleResponse)

export { application }
