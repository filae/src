/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static values = {
    triggers: Array,
  }

  static targets = [ 'downloadLink' ]

  connect() {
    let triggerFrames = []
    this.triggersValue.forEach((trigger) => triggerFrames.push(document.querySelector(`turbo-frame#${trigger}`)))

    this.timerId = setInterval(this.download, 500, triggerFrames, this.downloadLinkTarget);
    setTimeout(() => { clearInterval(this.timerId) }, 5000);
  }

  download(triggerFrames, downloadLink) {
    if (!downloadLink.hasAttribute("complete")) {
      let canDownload = triggerFrames.every((frame) => frame.hasAttribute("complete") )
      if (canDownload) {
        downloadLink.setAttribute("complete", "")
        downloadLink.click()
      }
    }
  }
}
