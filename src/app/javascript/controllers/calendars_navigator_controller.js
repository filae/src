/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import datepicker from 'js-datepicker'
import moment from 'moment'

export default class extends Controller {
  static values = {
    assignmentMode: Boolean,
    baseUrl: String,
    displayCancelled: Boolean,
  }

  static targets = [
    'calendar',
  ]

  connect() {
    this.picker = datepicker('button.datepicker', {
      position: 'br',
      startDay: 1,
      customDays: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
      customMonths: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
      overlayPlaceholder: 'Année',
      noWeekends: true,
      onSelect: instance => {
        this.goToDate(instance.dateSelected)
      },
    })
  }

  toggleAssignmentMode(event) {
    this.assignmentModeValue = !this.assignmentModeValue
    Turbo.visit(this.baseUrlValue + '&assignment_mode=' + this.assignmentModeValue, { frame: 'calendar' })
  }

  toggleDisplayCancelled(event) {
    this.displayCancelledValue = !this.displayCancelledValue
    Turbo.visit(this.baseUrlValue + '&include_cancelled=' + this.displayCancelledValue, { frame: 'calendar' })
  }

  showDatepicker(event) {
   this.picker.show()
  }

  goToDate(date) {
    let new_date = moment(date).format('YYYY-MM-DD')
    Turbo.visit(this.baseUrlValue + `&pivot_date=${new_date}`, { frame: 'calendar' })
  }
}
