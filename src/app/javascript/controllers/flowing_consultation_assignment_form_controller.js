/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"
import moment from 'moment';

const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm'

export default class extends Controller {
  static values = {
    assigneeId: Number,
    assignedAt: String,
  }

  static targets = [
    'assignedAtInput',
    'assignedAtWrapper',
    'submitButton',
  ]

  connect() {
    this.updateAssignmentForm()
  }

  setAssigneeId(event) {
    this.assigneeIdValue = event.target.value
  }

  setAssignedAt(event) {
    this.assignedAtValue = event.target.value
  }

  assigneeIdValueChanged() {
    if (this.assigneeIdValue != 0) {
      if (this.assignedAtValue == '') {
        this.assignedAtValue = moment().format(DATETIME_FORMAT)
      }
    } else {
      this.assignedAtValue = ''
    }
  }

  assignedAtValueChanged() {
    this.updateAssignmentForm()
  }

  updateAssignmentForm() {
    this.assignedAtInputTarget.value = this.assignedAtValue
    if (this.assigneeIdValue != 0) {
      if (this.assignedAtValue == '') {
        this.assignedAtInputTarget.classList.remove('is-invalid')
        this.submitButtonTarget.setAttribute('disabled', 'disabled')
      } else if (moment(this.assignedAtValue, DATETIME_FORMAT, true).isValid()) {
        this.assignedAtInputTarget.classList.remove('is-invalid')
        this.assignedAtInputTarget.classList.add('is-valid')
        this.submitButtonTarget.removeAttribute('disabled')
      } else {
        this.assignedAtInputTarget.classList.add('is-invalid')
        this.submitButtonTarget.setAttribute('disabled', 'disabled')
      }
      this.assignedAtWrapperTarget.removeAttribute('hidden')
    } else {
      this.assignedAtWrapperTarget.setAttribute('hidden', 'hidden')
      this.submitButtonTarget.setAttribute('disabled', 'disabled')
    }
  }
}
