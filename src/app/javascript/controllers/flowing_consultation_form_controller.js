/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"
import moment from 'moment';
import { isValidDatetime, DATETIME_FORMAT } from "../helpers"

export default class extends Controller {
  static values = {
    assigneeId: Number,
    assignedAt: String,
    cancelledAt: String,
    validatedAt: String,
  }

  static targets = [
    'assignedAtInput',
    'assignedAtWrapper',
    'cancelledAtInput',
    'cancelledByInput',
    'cancelledByWrapper',
    'validatedAtInput',
  ]

  // Assignment
  setAssigneeId(event) {
    this.assigneeIdValue = event.target.value
  }

  setAssignedAt(event) {
    this.assignedAtValue = event.target.value
  }

  assigneeIdValueChanged() {
    if (this.assigneeIdValue != 0) {
      if (this.assignedAtValue == '') {
        this.assignedAtValue = moment().format(DATETIME_FORMAT)
      }
    } else {
      this.assignedAtValue = ''
    }
  }

  assignedAtValueChanged() {
    this.hasAssignee = isValidDatetime(this.assignedAtValue)
    this.updateStatusForm()
  }

  updateAssignmentForm() {
    this.assignedAtInputTarget.value = this.assignedAtValue
    if (this.assigneeIdValue != 0) {
      this.assignedAtWrapperTarget.removeAttribute('hidden')
    } else {
      this.assignedAtWrapperTarget.setAttribute('hidden', 'hidden')
    }
  }

  // Cancellation
  setCancelledAtNow() {
    this.cancelledAtValue = moment().format(DATETIME_FORMAT)
  }

  setCancelledAt(event) {
    this.cancelledAtValue = event.target.value
  }

  cancelledAtValueChanged() {
    this.isCancelled = isValidDatetime(this.cancelledAtValue)
    this.updateStatusForm()
  }

  updateCancellationForm() {
    if (this.cancelledAtValue == '') {
      this.cancelledAtInputTarget.value = ''
      this.cancelledByWrapperTarget.setAttribute('hidden', 'hidden')
      this.cancelledByInputTarget.value = ''
    } else if (this.isCancelled) {
      this.cancelledAtInputTarget.value = this.cancelledAtValue
      this.cancelledByWrapperTarget.removeAttribute('hidden')
    } else {
      this.cancelledByWrapperTarget.setAttribute('hidden', 'hidden')
    }
  }

  clearCancellationInput() {
    this.cancelledAtValue = ''
  }

  // Validation
  setValidatedAtNow() {
    this.validatedAtValue = moment().format(DATETIME_FORMAT)
  }

  setValidatedAt(event) {
    this.validatedAtValue = event.target.value
  }

  validatedAtValueChanged() {
    this.isValidated = isValidDatetime(this.validatedAtValue)
    this.updateStatusForm()
  }

  clearValidationInput() {
    this.validatedAtValue = ''
  }

  updateValidationForm() {
    if (this.validatedAtValue == '') {
      this.validatedAtInputTarget.value = ''
    } else if (this.isValidated) {
      this.validatedAtInputTarget.value = this.validatedAtValue
    }
  }

  // Common
  updateStatusForm(){
    this.updateAssignmentForm()
    this.updateValidationForm()
    this.updateCancellationForm()
    if (this.hasAssignee) {
      this.show('validation')
    } else {
      this.hide('validation')
    }
    if (this.isValidated) {
      this.hide('cancellation')
    } else {
      this.show('cancellation')
    }
    if (this.isCancelled) {
      this.hide('validation')
    }
  }

  show(id) {
    document.getElementById(id).removeAttribute('hidden')
  }

  hide(id) {
    document.getElementById(id).setAttribute('hidden', 'hidden')
  }
}
