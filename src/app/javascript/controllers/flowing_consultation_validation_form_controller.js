/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"
import moment from 'moment';

const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm'

export default class extends Controller {
  static values = {
    validatedAt: String,
  }

  static targets = [
    'submitButton',
    'validatedAtInput',
  ]

  connect() {
    this.updateValidationForm()
  }

  // Validation
  setValidatedAtNow() {
    this.validatedAtValue = moment().format(DATETIME_FORMAT)
  }

  setValidatedAt(event) {
    this.validatedAtValue = event.target.value
  }

  validatedAtValueChanged() {
    this.updateValidationForm()
  }

  clearValidationInput() {
    this.validatedAtValue = ''
  }

  updateValidationForm() {
    if (this.validatedAtValue == '') {
      this.validatedAtInputTarget.value = ''
      this.validatedAtInputTarget.classList.add('is-invalid')
      this.validatedAtInputTarget.classList.remove('is-valid')
      this.submitButtonTarget.setAttribute('disabled', 'disabled')
    } else if (moment(this.validatedAtValue, DATETIME_FORMAT, true).isValid()) {
      this.validatedAtInputTarget.value = this.validatedAtValue
      this.validatedAtInputTarget.classList.remove('is-invalid')
      this.validatedAtInputTarget.classList.add('is-valid')
      this.submitButtonTarget.removeAttribute('disabled')
    } else {
      this.validatedAtInputTarget.classList.add('is-invalid')
      this.submitButtonTarget.setAttribute('disabled', 'disabled')
    }
  }
}

