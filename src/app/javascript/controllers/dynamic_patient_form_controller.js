/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"
import moment from 'moment';

const DATE_FORMAT = 'YYYY-MM-DD'

export default class extends Controller {
  static values = {
    patientId: Number,
  }

  static targets = [
    'birthdateInput',
    'existingPatientDiv',
    'existingPatientCard',
    'newPatientDiv',
    'patientFieldset',
    'patientSearchInput',
    'patientIdInput',
    'patientLastNameInput',
  ]

  initialize() {
    this.patientIdValue = this.patientIdInputTarget.value
  }

  // Existing or new patient switch

  clearNewPatientInputs() {
    Array.from(this.newPatientDivTarget.getElementsByTagName('input')).forEach((input) => {
      input.value = ''
      input.classList.remove('is-valid')
      input.classList.remove('is-invalid')
    })
    Array.from(this.newPatientDivTarget.getElementsByTagName('select')).forEach((input) => {
      input.value = ''
      input.classList.remove('is-valid')
      input.classList.remove('is-invalid')
    })
  }

  displayExistingPatient() {
    this.newPatientDivTarget.style.display = 'none'
    this.renderPatient(this.patientIdValue)
  }

  displayNewPatient() {
    this.existingPatientDivTarget.style.display = 'none'
    this.newPatientDivTarget.style.display = 'block'
  }

  patientIdValueChanged() {
    this.updatePatientForm()
  }

  togglePatientId(event) {
    this.patientIdValue = event.target.value
  }

  updatePatientForm() {
    this.patientSearchInputTarget.classList.remove('is-valid')
    this.patientSearchInputTarget.value = ''
    if (this.patientIdValue != 0) {
      this.displayExistingPatient()
      this.clearNewPatientInputs()
    } else {
      this.displayNewPatient()
    }
  }

  updatePatientLastName() {
    this.patientLastNameInputTarget.value = this.patientSearchInputTarget.value
  }

  async renderPatient(id) {
    let response = await get(`/patients/${id}`)
    if (response.ok) {
      let patientCard = await response.text
      this.existingPatientCardTarget.innerHTML = patientCard
      this.existingPatientDivTarget.style.display = 'block'
    }
  }

  unsetPatientId() {
    this.patientIdValue = 0
    this.patientIdInputTarget.value = ''
  }

  // validate birthdate of new patient

  checkBirthdate(event) {
    this.resetPatientLegend()
    this.removePatientAge()

    let birthdate = moment(event.target.value, DATE_FORMAT, true)
    if (birthdate.isValid()) {
      let age = moment().diff(birthdate, 'years')
      this.displayPatientAge(age)
      if (age < 18) {
        this.displayMinorIndicator()
      }
    }
  }

  displayPatientAge(age) {
    let patientAgeDiv = document.getElementById('patient-age')
    if (!patientAgeDiv) {
      patientAgeDiv = document.createElement('div')
      patientAgeDiv.id = 'patient-age'
      patientAgeDiv.classList.add('text-secondary')
    }
    patientAgeDiv.innerHTML += `${age} an(s)`
    this.birthdateInputTarget.after(patientAgeDiv)
  }

  displayMinorIndicator() {
    let legend = this.patientFieldsetTarget.getElementsByTagName('legend')[0]
    legend.innerHTML += ' <i class="fa-solid fa-child"></i>'
  }

  removePatientAge() {
    let minor_div = document.getElementById('patient-age')
    if (minor_div) { minor_div.remove() }
  }

  resetPatientLegend() {
    let legend = this.patientFieldsetTarget.getElementsByTagName('legend')[0]
    legend.innerHTML = 'Patient'
  }
}
