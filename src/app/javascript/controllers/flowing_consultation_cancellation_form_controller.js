/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import moment from 'moment';

const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm'

export default class extends Controller {
  static values = {
    cancelledAt: String,
    cancelledBy: String,
  }

  static targets = [
    'cancelledAtInput',
    'cancelledByInput',
    'cancelledByWrapper',
    'submitButton',
  ]

  connect() {
    this.updateCancellationForm()
  }

  // Cancellation
  setCancelledAtNow() {
    this.cancelledAtValue = moment().format(DATETIME_FORMAT)
  }

  setCancelledAt(event) {
    this.cancelledAtValue = event.target.value
  }

  setCancelledBy(event) {
    this.cancelledByValue = event.target.value
  }

  cancelledAtValueChanged() {
    this.cancelledAtInputTarget.value = this.cancelledAtValue
    this.updateCancellationForm()
  }

  cancelledByValueChanged() {
    this.updateCancellationForm()
  }

  updateCancellationForm() {
    if (this.cancelledAtValue == '') {
      this.submitButtonTarget.setAttribute('disabled', 'disabled')
      this.cancelledAtInputTarget.classList.add('is-invalid')
      this.cancelledByWrapperTarget.setAttribute('hidden', 'hidden')
      this.cancelledByInputTarget.value = ''
    } else if (moment(this.cancelledAtValue, DATETIME_FORMAT, true).isValid()) {
      this.cancelledAtInputTarget.classList.remove('is-invalid')
      this.cancelledAtInputTarget.classList.add('is-valid')
      this.cancelledByWrapperTarget.removeAttribute('hidden')
      if (this.cancelledByValue != '') {
        this.cancelledByInputTarget.classList.remove('is-invalid')
        this.cancelledByInputTarget.classList.add('is-valid')
        this.submitButtonTarget.removeAttribute('disabled')
      } else {
        this.cancelledByInputTarget.classList.remove('is-valid')
        this.cancelledByInputTarget.classList.add('is-invalid')
        this.submitButtonTarget.setAttribute('disabled', 'disabled')
      }
    } else {
      this.submitButtonTarget.setAttribute('disabled', 'disabled')
      this.cancelledAtInputTarget.classList.add('is-invalid')
      this.cancelledByWrapperTarget.setAttribute('hidden', 'hidden')
    }
  }

  clearCancellationInput() {
    this.cancelledAtValue = ''
  }
}

