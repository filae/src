/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2025 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"
import { isTestEnv } from "../helpers"

const REFRESH_INTERVAL = (isTestEnv() ? 2000 : 20000)

export default class extends Controller {
  static values = {
    url: String,
    frameTagName: String,
  }

  connect() {
    this.refreshInterval = setInterval(this.refresh, REFRESH_INTERVAL, this.urlValue, this.frameTagNameValue)
    if (!this.frameTagNameValue) {
      this.refresh(this.urlValue, this.frameTagNameValue);
    }
  }

  refresh(url, frameTagName) {
    if (frameTagName) {
      Turbo.visit(url, { frame: frameTagName })
    }
    else {
      get(url, { responseKind: 'turbo-stream' })
    }
  }

  disconnect() {
    clearInterval(this.refreshInterval)
  }
}
