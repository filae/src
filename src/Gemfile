# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby ENV["RUBY_VERSION"]

gem "rails", "7.1.5.1"

# PostgreSQL
gem "pg"

# Use Active Model has_secure_password
gem "bcrypt"

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", ">= 1.4.4", require: false

# ENV variable management
gem "dotenv-rails"

# Authorisations
gem "pundit"

# Jobs
gem "delayed_job_active_record"
gem "daemons"

# Views
gem "sprockets-rails"
gem "slim-rails"
gem "sass" # for slim embedded engines https://github.com/slim-template/slim#embedded-engines-markdown-
gem "cssbundling-rails"
gem "jsbundling-rails"
gem "simple_form"
gem "rails-i18n", "~> 7.0"
gem "stimulus-rails"
gem "turbo-rails"

# Pagination
gem "kaminari"
gem "kaminari-i18n"

# Tools
gem "business_time"

# Breadcrumb trail
gem "gretel"

# Errors management
gem "exception_notification"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "rspec-rails"
  gem "factory_bot_rails"
  gem "faker"
  gem "bummr", github: "infoPiiaf/bummr"
  gem "rubocop", require: false
  gem "rubocop-capybara", require: false
  gem "rubocop-factory_bot", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "rubocop-rspec_rails", require: false
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem "web-console", ">= 4.1.0"
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem "rack-mini-profiler", "~> 2.0"
  gem "listen", "~> 3.3"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "spring-watcher-listen"
  # Use Puma as the app server
  gem "puma", "~> 6.0"

  gem "bundler-audit", require: false
  gem "foreman"
  gem "brakeman", require: false
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem "capybara", ">= 3.26"
  gem "selenium-webdriver", ">= 4.0.0.rc1"
  gem "shoulda-matchers"
  gem "pundit-matchers"
  gem "timecop"
  gem "fuubar"
  gem "rails-controller-testing"
  gem "simplecov", require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]
