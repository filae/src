# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a simple message regarding assignment" do
  let!(:physician) { create(:user, :physician)}

  context "editing it" do
    scenario "I can [un-]assign it to a physician" do
      # assign
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          select physician.name, from: "Médecin"
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#to-transmit-listing" do
        expect(page).not_to have_content item.onml_id
      end
      within "#in-progress-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Transmis le")
          expect(page).to have_css("li", text: "Non validé")
        end
      end

      # unassign
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          select "", from: "Médecin"
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#in-progress-listing" do
        expect(page).not_to have_content item.onml_id
      end
      within "#to-transmit-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Non transmis")
          expect(page).to have_css("li", text: "Non validé")
        end
      end
    end

    scenario "validation is cleaned if unassigned" do
      # Given
      item.update(validated_at: Time.zone.now)
      # When Then
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          select "", from: "Médecin"
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#to-transmit-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Non transmis")
          expect(page).to have_css("li", text: "Non validé")
        end
      end
    end
  end

  context "using the shortcut" do
    scenario "I can assign it to a physician" do
      visit intervention_domain_flowing_consultations_path(domain)

      within "#to-transmit-listing" do
        within ".FlowingConsultation", text: item.onml_id do
          click_link "Transmettre"
        end
      end

      within "div.modal form" do
        select physician.name, from: "Médecin"
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#to-transmit-listing" do
        expect(page).not_to have_content item.onml_id
      end
      within "#in-progress-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Transmis le")
          expect(page).to have_css("li", text: "Non validé")
        end
      end
    end
  end
end

RSpec.shared_examples "a simple message regarding cancellation" do
  context "editing it" do
    scenario "I can [un-]cancel it" do
      # cancel
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          within "div#cancellation" do
            click_button "Maintenant"
            select "Non concerné", from: "Raison d'annulation"
          end
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#to-transmit-listing" do
        expect(page).not_to have_content item.onml_id
      end

      click_button "Annulées"

      within "#cancelled-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Non transmis")
          expect(page).to have_css("li", text: "Annulé le")
        end
      end

      # uncancel
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          within "div#cancellation" do
            click_button "Effacer"
            expect(page).not_to have_content "Raison d'annulation"
          end
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      click_button "Annulées"

      within "#cancelled-listing" do
        expect(page).not_to have_content item.onml_id
      end
      within "#to-transmit-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Non transmis")
          expect(page).not_to have_css("li", text: "Annulé")
        end
      end
    end
  end

  context "using the shortcut" do
    scenario "I can cancel it" do
      visit intervention_domain_flowing_consultations_path(domain)

      within "#to-transmit-listing" do
        within ".FlowingConsultation", text: item.onml_id do
          click_link "Annuler"
        end
      end

      within "div.modal form" do
        select "Non concerné", from: "Raison d'annulation"
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#to-transmit-listing" do
        expect(page).not_to have_content item.onml_id
      end

      click_button "Annulées"

      within "#cancelled-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Non transmis")
          expect(page).to have_css("li", text: "Annulé le")
        end
      end
    end
  end
end

RSpec.shared_examples "a simple message regarding validation" do
  before {
    item.update(assignee: create(:user, :physician), assigned_at: Time.zone.now)
  }

  context "editing it" do
    scenario "I can [un-]validate it" do
      # validate
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          within "div#validation" do
            click_button "Maintenant"
          end
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#in-progress-listing" do
        expect(page).not_to have_content item.onml_id
      end

      click_button "Validées"

      within "#validated-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Transmis le")
          expect(page).to have_css("li", text: "Validé le")
        end
      end

      # unvalidate
      visit edit_flowing_consultation_path(item)

      within "div.container form" do
        within "fieldset", text: "Statut" do
          within "div#validation" do
            click_button "Effacer"
          end
        end
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      click_button "Validées"

      within "#validated-listing" do
        expect(page).not_to have_content item.onml_id
      end
      within "#in-progress-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Transmis le")
          expect(page).to have_css("li", text: "Non validé")
        end
      end
    end
  end

  context "using the shortcut" do
    scenario "I can validate it" do
      visit intervention_domain_flowing_consultations_path(domain)

      within "#in-progress-listing" do
        within ".FlowingConsultation", text: item.onml_id do
          click_link "Valider"
        end
      end

      within "div.modal form" do
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."

      within "#in-progress-listing" do
        expect(page).not_to have_content item.onml_id
      end

      click_button "Validées"

      within "#validated-listing" do
        expect(page).to have_content item.onml_id
        within ".FlowingConsultation-history" do
          click_button "Historique"

          expect(page).to have_css("li", text: "Reçu le")
          expect(page).to have_css("li", text: "Transmis le")
          expect(page).to have_css("li", text: "Validé le")
        end
      end
    end
  end
end

RSpec.shared_examples "a simple message regarding status management" do
  let!(:physician) { create(:user, :physician)}
  let(:valid_date) { "20/06/2023 11:02" }
  let(:invalid_date) { "20/06/2023 11:0" }

  scenario "some form parts appear as others disappear" do
    visit edit_flowing_consultation_path(item)

    within "div.container form" do
      within "fieldset", text: "Statut" do
        expect(page).to have_css "div#assignment"
        expect(page).not_to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#assignment" do
          select physician.name, from: "Médecin"
        end

        expect(page).to have_css "div#assignment"
        expect(page).to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#assignment" do
          fill_in "Heure de transmission", with: invalid_date
        end

        expect(page).to have_css "div#assignment"
        expect(page).not_to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#assignment" do
          fill_in "Heure de transmission", with: valid_date
        end

        expect(page).to have_css "div#assignment"
        expect(page).to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#validation" do
          click_button "Maintenant"
        end

        expect(page).to have_css "div#assignment"
        expect(page).to have_css "div#validation"
        expect(page).not_to have_css "div#cancellation"

        within "div#validation" do
          click_button "Effacer"
        end

        expect(page).to have_css "div#assignment"
        expect(page).to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#cancellation" do
          click_button "Maintenant"
        end

        expect(page).to have_css "div#assignment"
        expect(page).not_to have_css "div#validation"
        expect(page).to have_css "div#cancellation"

        within "div#cancellation" do
          click_button "Effacer"
        end

        expect(page).to have_css "div#assignment"
        expect(page).to have_css "div#validation"
        expect(page).to have_css "div#cancellation"
      end
    end
  end
end

RSpec.shared_examples "a simple message regarding attached files" do
  let!(:user) { connect_as(:nurse) }

  scenario "I can add a standard and medical ones, download and destroy them" do
    visit intervention_domain_flowing_consultations_path(domain)

    within ".FlowingConsultation", text: item.onml_id do
      within ".FlowingConsultation-attachedFiles" do
        expect(page).to have_content "0"
        click_link "Fichiers attachés"
      end
    end

    # attach one
    within "div.modal" do
      within "#standard_attached_files" do
        expect(page).to have_css("h4", text: "0 fichier standard")
        within "form" do
          attach_file :attached_file_file, fixture_file_path("primum.txt")
        end

        expect(page).to have_css("h4", text: "1 fichier standard")
        expect(page).to have_css("li", text: "primum.txt")
      end
      within "#medical_content_attached_files" do
        expect(page).to have_css("h4", text: "0 fichier médical")
        within "form" do
          attach_file :attached_file_file, fixture_file_path("panem.txt")
        end

        expect(page).to have_css("h4", text: "1 fichier médical")
        expect(page).to have_css("li", text: "panem.txt")
      end
    end

    within ".FlowingConsultation", text: item.onml_id do
      within ".FlowingConsultation-attachedFiles" do
        expect(page).to have_content "2"
      end
    end

    # download them
    within "div.modal" do
      within "#standard_attached_files" do
        within "li", text: "primum.txt" do
          expect(page).to have_link "Télécharger"
        end
      end
      within "#medical_content_attached_files" do
        within "li", text: "panem.txt" do
          expect(page).to have_link "Télécharger"
        end
      end
    end

    # destroy them
    within "div.modal" do
      within "#standard_attached_files" do
        within "li", text: "primum.txt" do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_css("h4", text: "0 fichier standard")
        expect(page).not_to have_content("primum.txt")
      end
    end

    within ".FlowingConsultation", text: item.onml_id do
      within ".FlowingConsultation-attachedFiles" do
        expect(page).to have_content "1"
      end
    end

    within "div.modal" do
      within "#medical_content_attached_files" do
        within "li", text: "panem.txt" do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_css("h4", text: "0 fichier médical")
        expect(page).not_to have_content("panem.txt")
      end
    end

    within ".FlowingConsultation", text: item.onml_id do
      within ".FlowingConsultation-attachedFiles" do
        expect(page).to have_content "0"
      end
    end
  end
end

RSpec.shared_examples "a simple message regarding edition from the archives" do
  let!(:user) { connect_as(:nurse) }

  before do
    item.update(validated_at: (FlowingConsultation::TIPPING_DELAY + 1.hour).ago)
  end

  scenario "I can edit it and I am redirected to the arcchives" do
    visit archives_intervention_domain_flowing_consultations_path(domain)

    within ".FlowingConsultation", text: item.onml_id do
      click_link "Modifier"
    end

    within "div.container form" do
      click_button "Sauvegarder"
    end

    expect(page).to have_css("li.breadcrumb-item", text: "Archives")
  end
end

RSpec.feature "FlowingConsultations", type: :system do
  context "As a connected user who can manage a streaming intervention domain" do
    let!(:user) { connect_as(:switchboard_operator) }
    let!(:domain) { create(:intervention_domain, :stream, roles: [user.role]) }

    context "regarding messages" do
      let(:interlocutor) { build(:officer) }

      scenario "I can create one and edit it" do
        visit root_path

        within "div.container", text: domain.title do
          click_link "Flux"
        end

        # create it
        click_link "Nouveau message"

        within "div.container form" do
          within "fieldset", text: "Rendez-vous" do
            fill_in :simple_message_called_at, with: "09/06/2023 10:59"
            fill_in :simple_message_comment, with: Faker::Lorem.sentence
          end
          within "fieldset", text: "Interlocuteur" do
            fill_in :simple_message_officer_attributes_name, with: interlocutor.name
            fill_in :simple_message_officer_attributes_phone, with: interlocutor.phone
            fill_in :simple_message_officer_attributes_email, with: interlocutor.email
          end
          expect(page).not_to have_css("fieldset", text: "Statut")
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."

        message = FlowingConsultation.last
        within "#to-transmit-listing" do
          within ".FlowingConsultation", text: "Message" do
            expect(page).to have_css(".FlowingConsultation-calledAt", text: "09/06/2023 à 10h59")
            within ".FlowingConsultation-officer", text: interlocutor.name do
              click_button interlocutor.name

              expect(page).to have_css("li", text: interlocutor.phone)
              expect(page).to have_css("li", text: interlocutor.email)
            end
            expect(page).to have_css("td", text: message.comment)
          end
        end

        # edit it
        new_interlocutor = build(:officer)
        within ".FlowingConsultation", text: interlocutor.name do
          click_link "Modifier"
        end

        within "div.container form" do
          within "fieldset", text: "Rendez-vous" do
            fill_in :simple_message_called_at, with: "09/06/2023 11:59"
            fill_in :simple_message_comment, with: Faker::Lorem.sentence
          end
          within "fieldset", text: "Interlocuteur" do
            fill_in :simple_message_officer_attributes_name, with: new_interlocutor.name
            fill_in :simple_message_officer_attributes_phone, with: new_interlocutor.phone
            fill_in :simple_message_officer_attributes_email, with: new_interlocutor.email
          end
          expect(page).to have_css("fieldset", text: "Statut")
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        message.reload
        within "#to-transmit-listing" do
          within ".FlowingConsultation", text: "Message" do
            expect(page).to have_css(".FlowingConsultation-calledAt", text: "09/06/2023 à 11h59")
            expect(page).not_to have_content interlocutor.name
            within ".FlowingConsultation-officer", text: new_interlocutor.name do
              click_button new_interlocutor.name

              expect(page).to have_css("li", text: new_interlocutor.phone)
              expect(page).to have_css("li", text: new_interlocutor.email)
            end
            expect(page).to have_css("td", text: message.comment)
          end
        end
      end

      context "regarding an existing one" do
        let!(:item) { create(:flowing_consultation, :just_a_simple_message, intervention_domain: domain) }

        it_behaves_like "a simple message regarding assignment"
        it_behaves_like "a simple message regarding cancellation"
        it_behaves_like "a simple message regarding validation"
        it_behaves_like "a simple message regarding status management"
        it_behaves_like "a simple message regarding attached files"
        it_behaves_like "a simple message regarding edition from the archives"
      end
    end

    context "regarding flowing consultations" do
      let!(:consultation_type) { create(:consultation_type, intervention_domain: domain) }
      let!(:institution) { create(:calling_institution) }
      let(:officer) { build(:officer) }
      let(:patient) { build(:patient) }
      let(:comment) { Faker::Lorem.sentence }

      context "regarding a new patient" do
        scenario "I can create one" do
          visit root_path

          within "div.container", text: domain.title do
            click_link "Flux"
          end

          click_link "Nouvelle consultation"

          within "div.container form" do
            within "fieldset", text: "Rendez-vous" do
              fill_in :flowing_consultation_called_at, with: "09/06/2023 10:59"
              select consultation_type.title, from: :flowing_consultation_consultation_type_id
              fill_in :flowing_consultation_comment, with: comment
            end
            within "fieldset", text: "OPJ" do
              fill_in :flowing_consultation_calling_institution, with: institution.title[0..2]
              find("li", text: institution.title).click
              fill_in :flowing_consultation_officer_attributes_name, with: officer.name
              fill_in :flowing_consultation_officer_attributes_phone, with: officer.phone
              fill_in :flowing_consultation_officer_attributes_email, with: officer.email
            end
            within "fieldset", text: "Patient" do
              select "Indéterminé", from: :flowing_consultation_patient_attributes_gender
              fill_in :flowing_consultation_patient_attributes_last_name, with: patient.last_name
              fill_in :flowing_consultation_patient_attributes_first_name, with: patient.first_name
              fill_in :flowing_consultation_patient_attributes_birthdate, with: patient.birthdate
              check :flowing_consultation_concerns_minor
            end
            expect(page).not_to have_css("fieldset", text: "Statut")
            click_button "Sauvegarder"
          end

          expect(page).to have_content "Création réussie."

          within "#to-transmit-listing" do
            within ".FlowingConsultation" do
              within ".FlowingConsultation-onmlid" do
                expect(page).to have_content(domain.identifier_prefix)
              end
              within ".FlowingConsultation-consultationType" do
                expect(page).to have_content(consultation_type.title, normalize_ws: true)
              end
              within ".FlowingConsultation-calledAt" do
                expect(page).to have_content("09/06/2023 à 10h59")
              end
              within ".FlowingConsultation-callingInstitution" do
                click_button institution.title

                expect(page).to have_css("li", text: officer.name)
                expect(page).to have_css("li", text: officer.phone)
                expect(page).to have_css("li", text: officer.email)
              end
              within ".FlowingConsultation-patient" do
                click_button patient.fullname

                expect(page).to have_css("li", text: "Indéterminé")
                expect(page).to have_css("li", text: patient.human_birth)
                expect(page).to have_css("li", text: /an|mois/)
              end
              within ".FlowingConsultation-minor" do
                expect(page).to have_selector("title", text: "Personne mineure", visible: false)
              end
              within ".FlowingConsultation-comment" do
                expect(page).to have_content(comment)
              end
              within ".FlowingConsultation-history" do
                click_button "Historique"

                expect(page).to have_css("li", text: "Reçu le")
                expect(page).to have_css("li", text: "Non transmis")
                expect(page).to have_css("li", text: "Non validé")
              end
            end
          end
        end
      end

      context "regarding an existing patient" do
        let!(:patient) { create(:patient) }

        scenario "I can create one" do
          visit root_path

          within "div.container", text: domain.title do
            click_link "Flux"
          end

          click_link "Nouvelle consultation"

          within "div.container form" do
            within "fieldset", text: "Rendez-vous" do
              fill_in :flowing_consultation_called_at, with: "09/06/2023 10:59"
              select consultation_type.title, from: :flowing_consultation_consultation_type_id
              fill_in :flowing_consultation_comment, with: comment
            end
            within "fieldset", text: "OPJ" do
              fill_in :flowing_consultation_calling_institution, with: institution.title[0..2]
              find("li", text: institution.title).click
              fill_in :flowing_consultation_officer_attributes_name, with: officer.name
              fill_in :flowing_consultation_officer_attributes_phone, with: officer.phone
              fill_in :flowing_consultation_officer_attributes_email, with: officer.email
            end
            within "fieldset", text: "Patient" do
              fill_in :flowing_consultation_patient, with: patient.last_name[0..2]
              find("li", text: patient.fullname).click
              expect(page).to have_css("div.card", text: patient.fullname)
              check :flowing_consultation_concerns_minor
            end
            expect(page).not_to have_css("fieldset", text: "Statut")
            click_button "Sauvegarder"
          end

          expect(page).to have_content "Création réussie."

          within "#to-transmit-listing" do
            within ".FlowingConsultation" do
              within ".FlowingConsultation-onmlid" do
                expect(page).to have_content(domain.identifier_prefix)
              end
              within ".FlowingConsultation-consultationType" do
                expect(page).to have_content(consultation_type.title, normalize_ws: true)
              end
              within ".FlowingConsultation-calledAt" do
                expect(page).to have_content("09/06/2023 à 10h59")
              end
              within ".FlowingConsultation-callingInstitution" do
                click_button institution.title

                expect(page).to have_css("li", text: officer.name)
                expect(page).to have_css("li", text: officer.phone)
                expect(page).to have_css("li", text: officer.email)
              end
              within ".FlowingConsultation-patient" do
                click_button patient.fullname

                expect(page).to have_css("li", text: humanized_gender(patient))
                expect(page).to have_css("li", text: patient.human_birth)
                expect(page).to have_css("li", text: "an")
              end
              within ".FlowingConsultation-minor" do
                expect(page).to have_selector("title", text: "Personne mineure", visible: false)
              end
              within ".FlowingConsultation-comment" do
                expect(page).to have_content(comment)
              end
              within ".FlowingConsultation-history" do
                click_button "Historique"

                expect(page).to have_css("li", text: "Reçu le")
                expect(page).to have_css("li", text: "Non transmis")
                expect(page).to have_css("li", text: "Non validé")
              end
            end
          end
        end

        scenario "I can edit one" do
          flowing_consultation = create(:flowing_consultation, intervention_domain: domain, patient: patient, consultation_type: consultation_type)
          new_comment = Faker::Lorem.sentence

          visit intervention_domain_flowing_consultations_path(domain)

          within "#to-transmit-listing" do
            within ".FlowingConsultation", text: patient.fullname do
              click_link "Modifier"
            end
          end

          within "div.container form" do
            within "fieldset", text: "Rendez-vous" do
              fill_in :flowing_consultation_comment, with: new_comment
            end
            within "fieldset", text: "Patient" do
              uncheck :flowing_consultation_concerns_minor
            end
            expect(page).to have_css("fieldset", text: "Statut")
            click_button "Sauvegarder"
          end

          expect(page).to have_content "Mise à jour réussie."

          within "#to-transmit-listing" do
            within ".FlowingConsultation" do
              within ".FlowingConsultation-minor" do
                expect(page).not_to have_content("Personne mineure")
              end
              within ".FlowingConsultation-comment" do
                expect(page).to have_content(new_comment)
              end
            end
          end
        end
      end

      context "regarding an existing one" do
        let!(:item) { create(:flowing_consultation, intervention_domain: domain, consultation_type: consultation_type) }

        it_behaves_like "a simple message regarding assignment"
        it_behaves_like "a simple message regarding cancellation"
        it_behaves_like "a simple message regarding validation"
        it_behaves_like "a simple message regarding status management"
        it_behaves_like "a simple message regarding attached files"
        it_behaves_like "a simple message regarding edition from the archives"

        scenario "I can manage certificates" do
          # Given
          me = connect_as(:physician)
          domain.update(roles: [me.role])
          patient = create(:patient)
          officer = create(:officer)
          item.update(assignee: me, assigned_at: Time.now, patient: patient, officer: officer)
          certificate_template = create(:certificate_template, :with_master_document, roles: [me.role])
          # When
          visit intervention_domain_flowing_consultations_path(domain)

          within ".FlowingConsultation", text: item.onml_id do
            within ".FlowingConsultation-actions" do
              click_link "Certificats"
            end
          end

          # download a template
          within "div.modal" do
            within "#certificate_templates" do
              within "li", text: certificate_template.title do
                click_link "Télécharger"
              end
            end

          # upload a signed certificate
            within "#signed_certificates" do
              within "form" do
                attach_file :new_certificate_file, fixture_file_path("primum.txt")
              end

              within "#signed_certificates_listing" do
                within "li", text: patient.fullname do
          # download the signed certificate
                  expect(page).to have_link "Télécharger"

          # preview the signed certificate
                  expect(page).to have_link "Prévisualiser"

          # validate the signed certificate
                  accept_confirm do
                    click_link "Valider"
                  end
                end

                expect(page).not_to have_css("li", text: patient.fullname)
              end
            end

            within "#validated_certificates" do
              within "#validated_certificates_listing" do
                within "li", text: patient.fullname do
                  expect(page).to have_content "Validé"
          # read the fingerprint of the validated certificate
                  find("span.fingerprint"){ |span| !span["data-bs-title"].empty? }
          # download the validated certificate
                  expect(page).to have_link "Télécharger"

          # preview the validated certificate
                  expect(page).to have_link "Prévisualiser"

          # send the validated certificate by mail
                  click_link "Envoyer le certificat"
                end
              end
            end
          end

          within "form.mail_form" do
            expect(page).to have_field("mail_form_recipient", with: officer.email)
            click_button "Envoyer"
          end

          within "div.modal" do
            within "#validated_certificates" do
              within "#validated_certificates_listing" do
                within "li", text: patient.fullname do
          # unvalidate the validated certitificate
                  accept_confirm do
                    click_link "Dévalider"
                  end
                end

                expect(page).not_to have_css("li", text: patient.fullname)
              end
            end

          # delete the signed certificates
            within "#signed_certificates" do
              within "#signed_certificates_listing" do
                within "li", text: patient.fullname do
                  accept_confirm do
                    click_link "Supprimer"
                  end
                end

                expect(page).not_to have_css("li", text: patient.fullname)
              end
            end
          end
        end
      end
    end

    scenario "dashboard is refreshed automatically when flowing consultations are created or updated by another user" do
      # Given
      physician = create(:user, :physician)
      fc1 = create(:flowing_consultation, intervention_domain: domain)
      fc2 = create(:flowing_consultation, intervention_domain: domain)
      fc3 = create(:flowing_consultation, intervention_domain: domain, assignee: physician, assigned_at: Time.zone.now)
      fc4 = create(:flowing_consultation, intervention_domain: domain, cancelled_at: Time.zone.now - 1.hour)
      # When, Then
      visit intervention_domain_flowing_consultations_path(domain)

      within "#to-transmit-header" do
        expect(page).to have_selector("span.badge", text: "2")
      end
      within "#to-transmit-listing" do
        expect(page).to have_content fc1.onml_id
        expect(page).to have_content fc2.onml_id
      end
      within "#in-progress-header" do
        expect(page).to have_selector("span.badge", text: "1")
      end
      within "#in-progress-listing" do
        expect(page).to have_content fc3.onml_id
      end
      within "#validated-header" do
        expect(page).to have_selector("span.badge", text: "0")
      end
      within "#cancelled-header" do
        expect(page).to have_selector("span.badge", text: "1")
      end
      within "#cancelled-listing", visible: false do
        expect(page).to have_selector(".FlowingConsultation", text: fc4.onml_id, visible: false)
      end

      # Updates by another user
      fc1.update(assignee: physician, assigned_at: Time.zone.now)
      fc2.update(cancelled_at: Time.zone.now)
      fc3.update(validated_at: Time.zone.now)
      fc5 = create(:flowing_consultation, intervention_domain: domain)
      sleep Capybara.default_max_wait_time

      # Check updated
      within "#to-transmit-header" do
        expect(page).to have_selector("span.badge", text: "1")
      end
      within "#to-transmit-listing" do
        expect(page).to have_content fc5.onml_id
      end
      within "#in-progress-header" do
        expect(page).to have_selector("span.badge", text: "1")
      end
      within "#in-progress-listing" do
        expect(page).to have_content fc1.onml_id
      end
      within "#validated-header" do
        expect(page).to have_selector("span.badge", text: "1")
      end
      within "#validated-listing", visible: false do
        expect(page).to have_selector(".FlowingConsultation", text: fc3.onml_id, visible: false)
      end
      within "#cancelled-header" do
        expect(page).to have_selector("span.badge", text: "2")
      end
      within "#cancelled-listing", visible: false do
        expect(page).to have_selector(".FlowingConsultation", text: fc2.onml_id, visible: false)
        expect(page).to have_selector(".FlowingConsultation", text: fc4.onml_id, visible: false)
      end
    end
  end

  context "As a connected user who can edit a patient" do
    let!(:user) { connect_as(:nurse) }
    let!(:domain) { create(:intervention_domain, :stream, roles: [user.role]) }

    context "regarding flowing consultations" do
      let!(:consultation_type) { create(:consultation_type, intervention_domain: domain) }

      context "regarding an existing patient" do
        let!(:patient) { create(:patient) }

        scenario "I can edit the patient" do
          create(:flowing_consultation, intervention_domain: domain, patient: patient, consultation_type: consultation_type)

          visit intervention_domain_flowing_consultations_path(domain)

          within "#to-transmit-listing" do
            within ".FlowingConsultation-patient", text: patient.fullname do
              click_button patient.fullname
              click_link "Modifier le patient"
            end
          end

          within "div.modal form" do
            select "Indéterminé", from: :patient_gender
            fill_in :patient_last_name, with: "Dupont"
            fill_in :patient_first_name, with: "Camille"
            click_button "Sauvegarder"
          end

          within "#to-transmit-listing" do
            expect(page).not_to have_content patient.fullname
            within ".FlowingConsultation", text: "DUPONT Camille" do
              click_button "DUPONT Camille"
              expect(page).to have_css("li", text: "Indéterminé")
            end
          end
        end
      end
    end
  end

  context "As an admin, nurse, secretary or physician" do
    let!(:user) { connect_as(:secretary) }
    let!(:domain) { create(:intervention_domain, :stream, roles: [user.role]) }
    let!(:consultation_type) { create(:consultation_type, intervention_domain: domain) }

    context "Regarding archives" do
      let!(:flowing_consultation) { create(:flowing_consultation, :archived, intervention_domain: domain, consultation_type: consultation_type) }

      scenario "I have access to them" do
        visit intervention_domain_flowing_consultations_path(domain)

        click_link "Archives"

        expect(page).to have_css(".FlowingConsultation", text: flowing_consultation.onml_id)
      end

      scenario "I can filter them" do
        Timecop.freeze(Time.local(2023,9,6,9,30))
        visit archives_intervention_domain_flowing_consultations_path(domain)

        # default values
        within "form#stream_filter" do
          expect(page).to have_field("Statut", with: "")
          expect(page).to have_field("Action horodatée", with: "called_at")
          expect(page).to have_field("Date de début", with: "2023-09-03")
          expect(page).to have_field("Heure de début", with: "00:00")
          expect(page).to have_field("Date de fin", with: "")
          expect(page).to have_field("Heure de fin", with: "")
          expect(page).to have_field("Type de consultation", with: "")
          expect(page).to have_field("Service requérant", with: "")
          expect(page).to have_field("Médecin", with: "")
          expect(page).to have_field("Genre du patient", with: "")
          expect(page).to have_field("Nom du patient", with: "")
          expect(page).to have_field("Âge du patient", with: "")
        end

        Timecop.return

        # filter action
        within "form#stream_filter" do
          select "Validation", from: "Action horodatée"
          fill_in "Date de fin", with: "2023-09-05"
          fill_in "Heure de fin", with: "18:00"
          click_button "Filtrer"
        end

        within "form#stream_filter" do
          expect(page).to have_field("Action horodatée", with: "validated_at")
          expect(page).to have_field("Date de début", with: "2023-09-03")
          expect(page).to have_field("Heure de début", with: "00:00")
          expect(page).to have_field("Date de fin", with: "2023-09-05")
          expect(page).to have_field("Heure de fin", with: "18:00")
        end

        # remove action
        within "form#stream_filter" do
          select "Tous", from: "Action horodatée"

          expect(page).to have_field("Action horodatée", with: "")
          expect(page).not_to have_field "Date de début"
          expect(page).not_to have_field "Heure de début"
          expect(page).not_to have_field "Date de fin"
          expect(page).not_to have_field "Heure de fin"

          click_button "Filtrer"
        end

        within "form#stream_filter" do
          expect(page).to have_field("Action horodatée", with: "")
          expect(page).not_to have_field "Date de début"
          expect(page).not_to have_field "Heure de début"
          expect(page).not_to have_field "Date de fin"
          expect(page).not_to have_field "Heure de fin"
        end

        #reset filter
        Timecop.freeze(Time.local(2023,9,6,9,45))
        within "form#stream_filter" do
          select "Validée", from: "Statut"
          select "Indéterminé", from: "Genre du patient"
          select "40-49", from: "Âge du patient"
          click_button "Filtrer"
        end

        within "form#stream_filter" do
          expect(page).to have_field("Statut", with: "validated")
          expect(page).to have_field("Genre du patient", with: "unknown")
          expect(page).to have_field("Âge du patient", with: "40-49")

          click_link "Effacer"
        end

        within "form#stream_filter" do
          expect(page).to have_field("Statut", with: "")
          expect(page).to have_field("Action horodatée", with: "called_at")
          expect(page).to have_field("Date de début", with: "2023-09-03")
          expect(page).to have_field("Heure de début", with: "00:00")
          expect(page).to have_field("Type de consultation", with: "")
          expect(page).to have_field("Service requérant", with: "")
          expect(page).to have_field("Médecin", with: "")
          expect(page).to have_field("Genre du patient", with: "")
          expect(page).to have_field("Nom du patient", with: "")
          expect(page).to have_field("Âge du patient", with: "")
        end
        Timecop.return
      end
    end
  end
end
