# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Interventions", type: :system do
  context "As a standard connected user" do
    before { connect_as(:nurse) }

    scenario "I cannot access to interventions management" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "ul#menu" do
          expect(page).not_to have_link "Interventions"
        end
      end

      visit intervention_domains_path

      expect(page).to have_content("Accès non-autorisé.")
    end
  end
  context "As an administrator" do
    before { @admin = connect_as(:administrator) }

    scenario "I can manage intervention domains" do
      visit root_path

      # listing
      within "nav" do
        click_button "Paramètres"
        within "ul#menu" do
          click_link "Interventions"
        end
      end

      # add an appointments one
      within "div.container" do
        within "h1", text: "0 domaine d'intervention" do
          click_link "Nouveau domaine d'intervention"
        end
      end

      within "div.container form" do
        fill_in :intervention_domain_title, with: "Consultations"
        fill_in :intervention_domain_identifier_prefix, with: "consult"
        select "Agenda", from: "Mode opératoire"
        expect(page).not_to have_css("#allowed-roles")
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Création réussie."

      # change it to a stream one
      domain = InterventionDomain.last
      within "#intervention-domains h2", text: domain.title do
        click_link "Modifier"
      end

      within "div.container form" do
        fill_in :intervention_domain_title, with: "Antenne"
        fill_in :intervention_domain_identifier_prefix, with: "an"
        select "Flux", from: "Mode opératoire"
        within "#allowed-roles" do
          check "I. D. E."
          check "médecin"
        end
        click_button "Sauvegarder"
      end

      domain.reload
      expect(page).to have_content "Mise à jour réussie."
      within "#intervention-domains h2", text: domain.title do
        expect(page).to have_content "I. D. E., médecin"
      end

      # delete it
      within "#intervention-domains h2", text: domain.title do
        accept_confirm do
          click_link "Supprimer"
        end
      end

      expect(page).to have_content "Suppression réussie."
      expect(page).to have_css("h1", text: "0 domaine d'intervention")
      expect(page).not_to have_content domain.title
    end
  end
end
