# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Users", type: :system do
  context "As a standard connected user" do
    before { connect_as(:nurse) }

    scenario "I cannot access to users management" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "ul#menu" do
          expect(page).not_to have_link "Utilisateurs"
        end
      end

      visit users_path

      expect(page).to have_content("Accès non-autorisé.")
    end
  end

  context "As an administrator" do
    before { @admin = connect_as(:administrator) }

    scenario "I can list all users" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "ul#menu" do
          click_link "Utilisateurs"
        end
      end

      expect(page).to have_css("h1", text: "1 utilisateur")
      expect(page).to have_content(@admin.name)
    end

    scenario "I can create a user with a specific role" do
      visit users_path

      click_link "Nouvel utilisateur"
      within "div.container form" do
        fill_in :user_first_name, with: "Gregory"
        fill_in :user_last_name, with: "House"
        fill_in :user_identifier, with: "greg.house"
        fill_in :user_email, with: "greg.house@filae.tld"
        select "médecin", from: "Rôle"
        click_button "Sauvegarder"
      end

      expect(page).to have_content("Création réussie.")
      within "div#users-physician" do
        expect(page).to have_content("House Gregory")
      end
    end

    scenario "I can edit a user" do
      @user = create(:user, :physician, last_name: "House", first_name: "Gregory")
      
      visit users_path

      within "div#users-physician .card", text: "House Gregory" do
        click_link "Modifier"
      end

      within "div.container form" do
        fill_in :user_first_name, with: "Lisa"
        fill_in :user_last_name, with: "Cuddy"
        select "administrateur", from: "Rôle"
        click_button "Sauvegarder"
      end

      expect(page).to have_content("Mise à jour réussie.")
      expect(page).not_to have_content("House Gregory")
      within "div#users-administrator" do
        expect(page).to have_content("Cuddy Lisa")
      end
    end

    scenario "I can manage the activation of another user" do
      @user = create(:user, :physician, last_name: "House", first_name: "Gregory")
      
      visit users_path

      within "div#users-administrator .card", text: @admin.name do
        expect(page).not_to have_link("Désactiver")
      end

      # Désactivation immédiate
      within "div#users-physician .card", text: "House Gregory" do
        click_link "Désactiver"
      end
      
      within "div.container form" do
        click_button "Sauvegarder"
      end

      expect(page).to have_content("L'utilisateur House Gregory a été désactivé.")
      within "div#users-physician" do
        expect(page).to have_css("div.card.inactive", text: "House Gregory")
      end

      # Réactivation
      within "div#users-physician .card", text: "House Gregory" do
        accept_confirm do
          click_link "Réactiver"
        end
      end
      
      expect(page).to have_content("L'utilisateur House Gregory a été réactivé.")
      within "div#users-physician" do
        expect(page).to have_css("div.card.active", text: "House Gregory")
      end

      # Désactivation planifiée
      within "div#users-physician .card", text: "House Gregory" do
        click_link "Désactiver"
      end
      
      within "div.container form" do
        select  2028, from: :user_disabled_at_1i
        select  "mai", from: :user_disabled_at_2i
        select  24, from: :user_disabled_at_3i
        select  15, from: :user_disabled_at_4i
        select  42, from: :user_disabled_at_5i
        click_button "Sauvegarder"
      end

      expect(page).to have_content("La désactivation de l'utilisateur House Gregory est planifiée pour le 24 mai 2028 à 15h42.")
      within "div#users-physician" do
        expect(page).to have_css("div.card.deactivation-scheduled", text: "House Gregory")
      end

      # Suppression de la désactivation
      within "div#users-physician .card", text: "House Gregory" do
        accept_confirm do
          click_link "Supprimer la désactivation"
        end
      end

      expect(page).to have_content("L'utilisateur House Gregory a été réactivé.")
      within "div#users-physician" do
        expect(page).to have_css("div.card.active", text: "House Gregory")
      end

    end
  end
end
