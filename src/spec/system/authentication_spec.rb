# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Authentication", type: :system do
  context "As a guest" do
    let(:user) { create(:user, identifier: "administrator") }

    context "with good credentials" do
      scenario "I can log in" do
        visit root_path

        within "form#new_session" do
          fill_in :session_identifier, with: user.identifier
          fill_in :session_password, with: "Poiuyt123"
          click_button "Me connecter"
        end

        within "nav" do
          click_button "#{user.first_name} Me déconnecter"
        end

        expect(page).to have_content "Merci de vous authentifier"
      end

      scenario "I am disconnected after 5 hours" do
        visit root_path

        within "form#new_session" do
          fill_in :session_identifier, with: user.identifier
          fill_in :session_password, with: "Poiuyt123"
          click_button "Me connecter"
        end
        signed_in_at = Time.zone.now

        Timecop.travel(signed_in_at + 3.hours)
        visit root_path
        expect(page).to have_content "Me déconnecter"

        Timecop.travel(signed_in_at + 5.hours)
        visit root_path
        expect(page).to have_content "Votre connexion remonte à plus de 5 heures. Merci de vous authentifier à nouveau."
        expect(page).to have_content "Me connecter"
      end

      context "user is disabled while connected" do
        scenario "I am immediately connected" do
          visit root_path

          within "form#new_session" do
            fill_in :session_identifier, with: user.identifier
            fill_in :session_password, with: "Poiuyt123"
            click_button "Me connecter"
          end

          Timecop.travel(Time.zone.now + 5.minutes)
          user.update(disabled_at: Time.zone.now)

          visit root_path
          expect(page).to have_content "Votre compte n'est plus actif, merci de vous rapprocher d'un administrateur."
          expect(page).to have_content "Me connecter"
        end
      end

      context "user is inactive" do
        before { user.update(disabled_at: 5.minutes.ago) }

        scenario "I am warned" do
          visit root_path

          within "form#new_session" do
            fill_in :session_identifier, with: user.identifier
            fill_in :session_password, with: "Poiuyt123"
            click_button "Me connecter"
          end

          expect(page).to have_content "compte désactivé"
          expect(page).to have_content "Me connecter"
        end
      end
    end

    context "with bad credentials" do
      scenario "I cannot log in" do
        visit root_path

        within "form#new_session" do
          fill_in :session_identifier, with: user.identifier
          fill_in :session_password, with: "bad_secret"
          click_button "Me connecter"
        end

        expect(page).to have_content "Connexion impossible, veuillez vérifier vos identifiant et mot de passe."

        within "form#new_session" do
          expect(page).to have_field("session_identifier", with: user.identifier)
        end
      end
    end

    context "forgotten password" do
      scenario "I receive a new password by email" do
        visit root_path

        within "form#new_session" do
          check "Mot de passe oublié"
          fill_in :session_identifier, with: user.identifier
          click_button "En recevoir un nouveau"
        end

        expect(page).to have_content("Un nouveau mot de passe vient de vous être envoyé par courriel.")
        expect(page).to have_content "Me connecter"
        expect(ActionMailer::Base.deliveries.count).to eq 1
      end
    end
  end
end
