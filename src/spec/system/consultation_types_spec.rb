# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "ConsultationTypes", type: :system do
  context "As an administrator" do
    before { connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:other_type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I can manage consultation types" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          click_link "Nouveau type de consultation"
        end

        within "div.container form" do
          fill_in :consultation_type_title, with: "Consultation médicale"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-")
          fill_in :consultation_type_icon_name, with: "stethoscope"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-stethoscope")
          within "fieldset", text: "Autorisations d'accès pour" do
            check "médecin"
            check "I. D. E."
          end
          within "fieldset", text: "Créneaux affectables à" do
            check "médecin"
            check "I. D. E."
          end
          within "fieldset", text: "Types de consultation complémentaires" do
            check other_type.title
          end
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."

        type = ConsultationType.last
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          expect(page).to have_content "Consultation médicale"
          expect(page).to have_content "I. D. E., médecin"
        end

        # edit it
        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_link "Modifier"
        end

        within "div.container form" do
          fill_in :consultation_type_title, with: "Psychologue"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-stethoscope")
          fill_in :consultation_type_icon_name, with: "head-side-virus"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-head-side-virus")
          within "fieldset", text: "Autorisations d'accès pour" do
            uncheck "médecin"
            check "psychologue"
          end
          within "fieldset", text: "Créneaux affectables à" do
            uncheck "médecin"
            check "psychologue"
          end
          within "fieldset", text: "Types de consultation complémentaires" do
            uncheck other_type.title
          end
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        type.reload
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          expect(page).to have_content "Psychologue"
          expect(page).to have_content "I. D. E., psychologue"
        end

        # delete it
        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        within "#intervention-domains" do
          click_button domain.title
          expect(page).not_to have_content type.title
        end
      end
    end

    context "regarding stream intervention domains" do
      let!(:domain) { create(:intervention_domain, :stream) }

      scenario "I can manage consultation types" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          click_link "Nouveau type de consultation"
        end

        within "div.container form" do
          fill_in :consultation_type_title, with: "GAV"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-")
          fill_in :consultation_type_icon_name, with: "handcuffs"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-handcuffs")
          expect(page).not_to have_content "Autorisations d'accès pour"
          expect(page).not_to have_content "Créneaux affectables à"
          expect(page).not_to have_content "Types de consultation complémentaires"
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."

        # edit it
        type = ConsultationType.last
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} li", text: type.title  do
          click_link "Modifier"
        end

        within "div.container form" do
          fill_in :consultation_type_title, with: "Levée de corps"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-handcuffs")
          fill_in :consultation_type_icon_name, with: "skull-crossbones"
          expect(page).to have_css(".ConsultationType__iconName svg.fa-skull-crossbones")
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        # delete it
        type.reload
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} li", text: type.title  do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        within "#intervention-domains" do
          click_button domain.title
          expect(page).not_to have_content type.title
        end
      end
    end
  end
end
