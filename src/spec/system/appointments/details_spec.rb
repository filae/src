# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

def within_today_s_calendar
  within "div.CalendarDay", text: I18n.l(today, format: :calendar_day) do
    yield
  end
end

RSpec.feature "Appointments", type: :system do
  let!(:appointments_domain) { create(:intervention_domain, :appointments) }
  let!(:authorized_consultation_type) { create(:consultation_type, title: "Authorized", intervention_domain: appointments_domain, roles: [authorized_role]) }
  let!(:forbidden_consultation_type) { create(:consultation_type, intervention_domain: appointments_domain, roles: []) }
  let!(:consultation_reason) { create(:consultation_reason, consultation_type: authorized_consultation_type) }

  let(:authorized_role) { Role.find_by(title: :nurse) }

  let!(:now) { 1.business_day.ago.change({ hour: 10, min: 0, sec: 0 }) }
  let!(:today) { now.to_date }

  before { Timecop.freeze(today) }

  after { Timecop.return }

  context "As an authorized user" do
    let!(:user) { connect_as(authorized_role.title) }

    context "Regarding an authorized consultation type" do
      context "regarding an appointment" do
        let(:patient) { create(:patient, birthdate: today - 16.years, gender: :woman) }
        let(:officer) { create(:officer) }
        let(:consultation_reason) { create(:consultation_reason, code: "AS", consultation_type: authorized_consultation_type) }
        let(:consultation_subtype) { create(:consultation_subtype, consultation_type: authorized_consultation_type) }
        let(:calling_institution) { create(:calling_institution) }
        let(:circumstances) { Faker::Lorem.sentence }
        let(:comment) { Faker::Lorem.sentence }
        let!(:appointment) {
          create(:appointment,
          patient: patient,
          officer: officer,
          consultation_type: authorized_consultation_type,
          consultation_reason: consultation_reason,
          consultation_subtype: consultation_subtype,
          calling_institution: calling_institution,
          emergency: true,
          circumstances: circumstances,
          comment: comment,
          starts_at: now.change({ hour: 10, min: 0, sec: 0 }),
          duration: "PT45M")
        }

        before do
          authorized_consultation_type.assigned_roles << user.role
        end

        def display_appointment_details
          visit consultation_type_appointments_path(authorized_consultation_type)

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: patient.fullname).click
            end
          end
        end

        scenario "I can access to its details within modal" do
          display_appointment_details

          within "div.modal" do
            within "div.modal-title" do
              expect(page).to have_css("span.badge", text: appointments_domain.identifier_prefix)
              expect(page).to have_css("h5", text: patient.fullname)
              within "div.patient-details" do
                expect(page).to have_content "Urgence."
                expect(page).to have_content "F"
                expect(page).to have_content localize_date(patient.birthdate)
                expect(page).to have_content "Personne mineure à la date du rendez-vous."
              end
              within "div.appointment-meta" do
                expect(page).to have_content appointment.patient_phone
                expect(page).to have_content consultation_reason.code
                expect(page).to have_content localize_date(appointment.starts_at, format: :input)
                expect(page).to have_content consultation_subtype.title
              end
            end
            within "div.modal-body" do
              within "div.appointment-details" do
                within "dl", text: "Circonstances" do
                  expect(page).to have_content appointment.circumstances
                end
                within "dl", text: "Remarques" do
                  expect(page).to have_content appointment.comment
                end
                within "dl", text: "Service requérant" do
                  expect(page).to have_content calling_institution.title
                end
                within "dl", text: "OPJ" do
                  expect(page).to have_content officer.full_details
                end
              end
            end
          end
        end

        scenario "I can edit the patient" do
          display_appointment_details

          within "div.modal" do
            within "div.modal-header" do
              click_link "Modifier le patient"
            end
          end

          within "div.modal form#edit_patient_#{patient.id}" do
            select "Indéterminé", from: :patient_gender
            fill_in :patient_last_name, with: "Dupont"
            fill_in :patient_first_name, with: "Camille"
            fill_in :patient_birthdate, with: "2012-12-21"
            click_button "Sauvegarder"
          end

          within "div.modal" do
            expect(page).not_to have_content patient.fullname
            expect(page).to have_css("h5", text: "DUPONT Camille")
            expect(page).to have_css("span", text: "21/12/2012")
          end

          within_today_s_calendar do
            within "div.fc" do
              expect(page).not_to have_css("a.fc-event", text: patient.fullname)
              expect(page).to have_css("a.fc-event", text: "DUPONT Camille")
            end
          end
        end

        scenario "I can [un]cancel it" do
          # Given
          reason = "à la demande de l'OPJ"
          phrased_reason = reason.upcase_first
          # When
          display_appointment_details

          within "div.modal" do
            within "#cancellation" do
              within "div.dropdown", text: "Annuler la consultation" do
                click_button "Annuler la consultation"
                click_button phrased_reason
              end

              expect(page).to have_content "Annulé #{reason}"
            end
            within "div.modal-header" do
              click_button "Fermer"
            end

            expect(page).not_to have_content patient.fullname
          end

          within "nav.CalendarDays-nav" do
            check "Afficher les rendez-vous reportés et annulés"
          end

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: patient.fullname).click
            end
          end

          within "div.modal" do
            within "#cancellation" do
              within "div.dropdown", text: "Rétablir la consultation" do
                click_button "Rétablir la consultation"
                click_button "Confirmer le rétablissement"
              end

              expect(page).not_to have_content reason
            end
            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            within "div.fc" do
              expect(page).to have_css("a.fc-event", text: patient.fullname)
            end
          end
        end

        scenario "I can postpone it" do
          # Given
          today_input = I18n.l(today, format: :input)
          # When
          display_appointment_details

          # UMJ needs to postpone
          within "div.modal" do
            within "#workflow" do
              expect(page).to have_css("button.btn-primary", text: "Patient arrivé")
            end
            within "#cancellation" do
              within "div.dropdown", text: "Reporter la consultation" do
                click_button "Reporter la consultation"
                click_link "À la demande de l'UMJ"
              end
            end
          end

          # check next date is empty
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              expect(find_field("Date et heure de début").value).to be_empty
            end
            expect(page).to have_content "RDV précédent : #{today_input} 10:00 - 10:45"
            click_button "Sauvegarder"
          end

          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              expect(page).to have_content "Date et heure de début doit être rempli(e)"
            end
          end

          # fill in next date
          within_today_s_calendar do
            find('td.fc-timegrid-slot-lane[data-time="14:00:00"]').click
          end
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              expect(find_field("Date et heure de début").value).to match "#{today_input} 14:00"
            end
            expect(page).to have_content "RDV précédent : #{today_input} 10:00 - 10:45"
            click_button "Sauvegarder"
          end

          # check new appointment visible and old one unvisible
          within_today_s_calendar do
            within "div.fc" do
              expect(page).to have_selector("a.fc-event", text: patient.fullname, count: 1)
            end
          end

          # show cancelled and old and one visible
          within "nav.CalendarDays-nav" do
            check "Afficher les rendez-vous reportés et annulés"
          end
          within_today_s_calendar do
            within "div.fc" do
              expect(page).to have_selector("a.fc-event", text: patient.fullname, count: 2)
            end
          end

          # click old one and check postpone status
          within_today_s_calendar do
            within "div.fc" do
              all("a.fc-event", text: patient.fullname).first.click
            end
          end

          within "div.modal" do
            within "#cancellation" do
              expect(page).not_to have_css("div.dropdown", text: "Rétablir la consultation")
              expect(page).not_to have_css("div.dropdown", text: "Reporter la consultation")
              expect(page).to have_content "Reporté au #{today_input} 14:00 à la demande de l'UMJ"
            end
            expect(page).not_to have_css("#workflow")
          end
        end

        scenario "I can set if the patient arrived or not" do
          display_appointment_details

          # Patient did not come
          within "div.modal" do
            within "#workflow" do
              expect(page).to have_css("button.btn-primary", text: "Patient arrivé")
              within "div.dropdown", text: "Patient non venu" do
                click_button "Patient non venu"
                click_button "Confirmer la non venue du patient"
              end

              expect(page).to have_css("button.btn-outline-danger", text: "Patient non venu")
              expect(page).not_to have_css("button", text: "Patient arrivé")
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            find("a.fc-event", text: patient.fullname){ |link| link["style"].include?("background-color: darkgrey") }.click
          end

          # Patient finally came
          within "div.modal" do
            within "#workflow" do
              within "div.dropdown", text: "Patient non venu" do
                click_button "Patient non venu"
                click_button "Annuler cette saisie définitivement"
              end

              expect(page).not_to have_css("button", text: "Patient non venu")
              expect(page).to have_css("button.btn-outline-success", text: "Patient arrivé")
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            find("a.fc-event", text: patient.fullname){ |link| link["style"].include?("background-color: #{consultation_reason.colour}") }.click
          end

          # Patient lost
          within "div.modal" do
            within "#workflow" do
              within "div.dropdown", text: "Patient arrivé" do
                click_button "Patient arrivé"
                click_button "Annuler cette saisie définitivement"
              end

              expect(page).to have_css("button.btn-secondary", text: "Patient non venu")
              expect(page).to have_css("button.btn-primary", text: "Patient arrivé")
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            find("a.fc-event", text: patient.fullname){ |link| link["style"].include?("background-color: #{consultation_reason.colour}") }.click
          end

          # Patient found again and ready
          within "div.modal" do
            within "#workflow" do
              within "div.dropdown", text: "Patient arrivé" do
                click_button "Patient arrivé"
                click_button "Confirmer l'arrivée du patient"
              end

              expect(page).not_to have_css("button", text: "Patient non venu")
              expect(page).to have_css("button.btn-outline-success", text: "Patient arrivé")
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            find("a.fc-event", text: patient.fullname){ |link| link["style"].include?("background-color: #{consultation_reason.colour}") }
          end
        end

        scenario "I can set if requisition was received or not" do
          display_appointment_details

          # Requisition was received
          within "div.modal" do
            within "#workflow" do
              click_button "Réquisition reçue"

              expect(page).to have_css("button.btn-outline-success", text: "Réquisition reçue")
            end
          end

          # Well, not for this appointment
          within "div.modal" do
            within "#workflow" do
              within "div.dropdown", text: "Réquisition reçue" do
                click_button "Réquisition reçue"
                click_button "Annuler cette saisie définitivement"
              end

              expect(page).not_to have_css("button.btn-outline-success", text: "Réquisition reçue")
            end
          end
        end

        scenario "I can see the logs regarding actions on it" do
          display_appointment_details

          within "div.modal" do
            # Clean appointment, no log
            within "div.modal-header" do
              within "span#logs" do
                expect(page).to have_selector("button", visible: false)
              end
            end

            # One log when I cancel the appointment
            within "#cancellation" do
              within "div.dropdown", text: "Annuler la consultation" do
                click_button "Annuler la consultation"
                find("li", text: "UMJ").click
              end
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within "nav.CalendarDays-nav" do
            check "Afficher les rendez-vous reportés et annulés"
          end

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: patient.fullname).click
            end
          end

          within "div.modal" do
            within "div.modal-header" do
              within "span#logs" do
                click_button "Historique des actions"
                within "ul" do
                  expect(page).to have_selector("li", count: 1)
                  expect(page).to have_selector("li", text:  "Annulé")
                  find("svg.fa-circle-info").hover
                end
              end
            end
          end
          expect(page).to have_selector("div.tooltip-inner", text: "{\"actions\":")

          within "div.modal" do
            # Two logs when I uncancel it
            within "#cancellation" do
              within "div.dropdown", text: "Rétablir la consultation" do
                click_button "Rétablir la consultation"
                click_button "Confirmer le rétablissement"
              end
            end

            within "div.modal-header" do
              click_button "Fermer"
            end
          end

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: patient.fullname).click
            end
          end

          within "div.modal" do
            within "div.modal-header" do
              within "span#logs" do
                click_button "Historique"
                within "ul" do
                  expect(page).to have_selector("li", count: 2)
                  expect(page).to have_selector("li", text:  "Annulé")
                  expect(page).to have_selector("li", text:  "Rétabli")
                end
              end
            end
          end
        end

        scenario "I can create and cancel complementary appointments" do
          # Given
          another_consultation_type = create(:consultation_type, title: "Other", intervention_domain: appointments_domain, roles: [authorized_role])
          yet_another_consultation_type = create(:consultation_type, title: "Yet Another", intervention_domain: appointments_domain, roles: [authorized_role])
          authorized_consultation_type.complementary_consultation_types << [another_consultation_type, yet_another_consultation_type]
          appointment.update(patient_arrived_at: Time.current)
          # When/Then
          display_appointment_details
          within_today_s_calendar do
            within("a.fc-event", text: patient.fullname) do
              expect(page).to have_css(".PatientJourney", text: "A")
            end
          end

          # create appointment for another_consultation_type
          within "div.modal" do
            within "div.modal-body" do
              within "div#complementary-appointments" do
                within "li#consultation_type_#{another_consultation_type.id} button" do
                  expect(page).to have_css("svg.fa-square")
                  expect(page).to have_content(another_consultation_type.title)
                  expect(page).not_to have_css("svg.fa-square-check")
                end
                within "li#consultation_type_#{yet_another_consultation_type.id} button" do
                  expect(page).to have_css("svg.fa-square")
                  expect(page).to have_content(yet_another_consultation_type.title)
                  expect(page).not_to have_css("svg.fa-square-check")
                end

                click_button another_consultation_type.title
                within "li#consultation_type_#{another_consultation_type.id}" do
                  expect(page).not_to have_css("svg.fa-square")
                  expect(page).to have_content(another_consultation_type.title)
                  expect(page).to have_css("svg.fa-square-check")
                end
              end
            end
          end
          within_today_s_calendar do
            within("a.fc-event", text: patient.fullname) do
              expect(page).to have_css(".PatientJourney", text: "(A » O)")
            end
          end

          # verify appointment created on another_consultation_type calendar
          visit consultation_type_appointments_path(another_consultation_type)

          within_today_s_calendar do
            within "div.fc" do
              expect(page).to have_css("a.fc-event", text: patient.fullname)
            end
          end

          # cancel just created appointment for another_consultation_type
          display_appointment_details

          within "div.modal" do
            within "div.modal-body" do
              within "div#complementary-appointments" do
                click_button another_consultation_type.title
                click_button "Annuler cette consultation complémentaire"
                within "li#consultation_type_#{another_consultation_type.id}" do
                  expect(page).to have_css("svg.fa-square")
                  expect(page).to have_content(another_consultation_type.title)
                  expect(page).not_to have_css("svg.fa-square-check")
                end
              end
            end
          end
          within_today_s_calendar do
            within("a.fc-event", text: patient.fullname) do
              expect(page).to have_css(".PatientJourney", text: "A")
            end
          end

          # verify appointment created on another_consultation_type calendar was cancelled
          visit consultation_type_appointments_path(another_consultation_type)

          within_today_s_calendar do
            within "div.fc" do
              expect(page).not_to have_css("a.fc-event", text: patient.fullname)
            end
          end

          within "nav.CalendarDays-nav" do
            check "Afficher les rendez-vous reportés et annulés"
          end

          within_today_s_calendar do
            within "div.fc" do
              expect(page).to have_css("a.fc-event", text: patient.fullname)
            end
          end
        end

        scenario "I can edit the associated medical file" do
          display_appointment_details

          within "#AppointmentHeader" do
            click_link "Fiche médicale"
          end

          within "turbo-frame#medical_file" do
            expect(page).to have_css("div#AppointmentHeader", text: patient.fullname)

            # edit medical file
            within "#medical_file form" do
              fill_in "Adresse", with: "Chez moi"
              click_button "Observations médicales"
              fill_in "Antécédents", with: "Aucun"
              click_button "Sauvegarder"
            end
          end

          expect(page).to have_content "Mise à jour réussie."

          # take charge
          within "div.modal" do
            within "#workflow" do
              within "div.dropdown", text: "Patient arrivé" do
                click_button "Patient arrivé"
                click_button "Confirmer l'arrivée du patient"
              end
              within "div.dropdown", text: "Prise en charge" do
                click_button "Prise en charge"
                click_link "Confirmer la prise en charge du patient"
              end
            end
          end

          # access medical file
          within "#AppointmentHeader" do
            click_link "Fiche médicale"
          end
          within "#medical_file form" do
            expect(page).to have_field("Adresse", with: "Chez moi")
            click_button "Observations médicales"
            expect(page).to have_field("Antécédents", with: "Aucun")
          end
        end

        context "patient arrived" do
          before { appointment.update(patient_arrived_at: now - 30.minutes )}

          scenario "I can take charge and [un]release the patient" do
            display_appointment_details

            # Take charge
            within "div.modal" do
              within "#workflow" do
                within "div.dropdown", text: "Prise en charge" do
                  click_button "Prise en charge"
                  click_link "Confirmer la prise en charge du patient"
                end
              end
            end

            # Edit patient
            within "#AppointmentHeader" do
              click_link "Modifier le patient"
            end

            within "div.modal form" do
              select "Indéterminé", from: :patient_gender
              fill_in :patient_birthdate, with: "2012-12-21"
              click_button "Sauvegarder"
            end

            within "#AppointmentHeader" do
              expect(page).to have_css("span", text: "21/12/2012")
            end

            # Release patient
            within "div.card", text: "Observations" do
              click_link "La consultation est terminée"
            end

            within "div.modal" do
              within "div.modal-body" do
                expect(page).to have_css("div#complementary-appointments")
                click_button "Patient libéré"
              end

              # Go back to appointment
              within "div.modal-footer" do
                click_button "Revenir à la consultation"
              end
            end

            expect(page).not_to have_css "div.modal"
            expect(page).to have_selector "#AppointmentHeader"

            # Unrelease patient
            within "div.card", text: "Observations" do
              click_link "La consultation est terminée"
            end

            within "div.modal" do
              within "div.modal-body" do
                click_button "Patient libéré"
              end

              # Go to the agenda
              within "div.modal-footer" do
                click_link "Revenir à l'agenda"
              end
            end

            # Go back to the certificate
            display_appointment_details

            within "div.modal" do
              within "#workflow" do
                expect(page).to have_css("button.btn-outline-success", text: "Prise en charge")
              end
              within "#certificate" do
                click_link "Enrichir la consultation"
              end
            end

            # Go back to the agenda
            within "div.card", text: "Observations" do
              click_link "La consultation est terminée"
            end

            within "div.modal" do
              within "div.modal-footer" do
                click_link "Revenir à l'agenda"
              end
            end

            # cancel take charge
            display_appointment_details

            within "div.modal" do
              within "#workflow" do
                click_button "Prise en charge"
                click_button "Annuler cette saisie définitivement"
                expect(page).to have_css("button.btn-primary", text: "Prise en charge")
                expect(page).not_to have_link("Enrichir la consultation")
              end
            end
          end

          context "regarding certificates" do
            context "without any certificate template" do
              scenario "I can manage signed and validated certificates" do
                display_appointment_details

                within "div.modal" do
                  within "#workflow" do
                    within "div.dropdown", text: "Prise en charge" do
                      click_button "Prise en charge"
                      click_link "Confirmer la prise en charge du patient"
                    end
                  end
                end

                # Upload signed certificate
                within "div.card", text: "Observations" do
                  within "#certificate_file" do
                    expect(page).to have_css("h3", text: "Certificat signé")
                    within "form" do
                      attach_file :new_certificate_file, fixture_file_path("primum.txt")
                    end

                    expect(page).to have_css("h3", text: "Certificat signé")
                    expect(page).not_to have_css "form"

                    within "li", text: patient.fullname do
                      # Preview it
                      expect(page).to have_link "Prévisualiser"

                      # Download it
                      expect(page).to have_link "Télécharger"

                      # Validate it
                      accept_confirm do
                        click_link "Valider"
                      end
                    end

                    expect(page).to have_css("h3", text: "Certificat validé")

                    within "li", text: patient.fullname do
                      # Download the validated certificate
                      expect(page).to have_link "Télécharger"

                      # Preview it
                      expect(page).to have_link "Prévisualiser"

                      # read its fingerprint
                      find("span.fingerprint"){ |span| !span["data-bs-title"].empty? }

                      # Not sent by mail here
                      expect(page).not_to have_link "Envoyer"

                      # Unvalidate it
                      accept_confirm do
                        click_link "Dévalider"
                      end
                    end

                    expect(page).to have_css("h3", text: "Certificat signé")

                    within "li", text: patient.fullname do
                      # Delete signed certificate
                      accept_confirm do
                        click_link "Supprimer"
                      end
                    end

                    expect(page).to have_css("h3", text: "Certificat signé")
                    expect(page).to have_css "form"
                  end
                end
              end
            end

            context "with a file as a certificate template" do
              let!(:certificate_template) { create(:certificate_template, :with_master_document, roles: [authorized_role]) }

              scenario "I can manage signed and validated certificates" do
                display_appointment_details

                within "div.modal" do
                  within "#workflow" do
                    within "div.dropdown", text: "Prise en charge" do
                      click_button "Prise en charge"
                      click_link certificate_template.title
                    end
                  end
                end

                within "div.card", text: "Observations" do
                  within "#certificate-template" do
                    expect(page).to have_content "Maquette à télécharger"
                    expect(page).to have_link "Télécharger"
                  end
                end
                # Then same as previous
              end
            end
          end

          context "regarding validated certificate" do
            context "no certificate associated to the appointment" do
              scenario "the button is disabled" do
                display_appointment_details

                within "div.modal" do
                  within "#certificate" do
                    expect(page).not_to have_link("Télécharger le certificat validé")
                    expect(page).to have_button("Certificat absent de Filaé", disabled: true)
                  end
                end
              end
            end

            context "a certificate exists" do
              let!(:certificate) { create(:certificate, certificatable: appointment) }

              context "without validated certificate" do
                scenario "I can indicate it was sent and rollback" do
                  display_appointment_details

                  within "div.modal" do
                    within "#certificate" do
                      expect(page).not_to have_link("Télécharger le certificat validé")
                      # indicate it was sent
                      within "div.dropdown", text: "Certificat non-envoyé" do
                        click_button "Certificat non-envoyé"
                        expect(page).to have_button("Envoyer par courriel (impossible : certificat non validé dans FILAÉ)", disabled: true)
                        click_button "Confirmer l'envoi du certificat hors-FILAÉ"
                      end
                      # oups, I did a mistake
                      within "div.dropdown", text: "Certificat envoyé" do
                        click_button "Certificat envoyé"
                        click_button "Annuler cette saisie définitivement"
                      end
                      expect(page).to have_button("Certificat non-envoyé")
                    end
                  end
                end
              end

              context "with a validated certificate" do
                before { create(:attached_file, :validated, attachable: certificate) }

                scenario "I can download it" do
                  display_appointment_details

                  within "div.modal" do
                    within "#certificate" do
                      expect(page).to have_link("Télécharger le certificat validé")
                    end
                  end
                end

                scenario "I can send it by mail" do
                  display_appointment_details

                  within "div.modal" do
                    within "#certificate" do
                      # send by mail
                      within "div.dropdown", text: "Certificat non-envoyé" do
                        click_button "Certificat non-envoyé"
                        click_link "Envoyer par courriel"
                      end
                    end
                  end

                  within "form.mail_form" do
                    expect(page).to have_field("mail_form_recipient", with: officer.email)
                    click_button "Envoyer"
                  end

                  expect(page).to have_content "Le certificat a été envoyé avec succès."
                  within "div.modal" do
                    within "#certificate" do
                      expect(page).to have_button("Certificat envoyé")
                    end
                  end
                end
              end
            end
          end
        end

        scenario "I can manage standard and medical attached files" do
          display_appointment_details

          within "div.accordion" do
            within "h4.accordion-header" do
              click_button "0 fichier attaché"
            end

            # attach sandard file
            within "#standard_attached_files" do
              expect(page).to have_css("h4", text: "0 fichier standard")
              within "form" do
                attach_file :attached_file_file, fixture_file_path("primum.txt")
              end

              expect(page).to have_css("h4", text: "1 fichier standard")
              expect(page).to have_css("li", text: "primum.txt")
            end
            within "h4.accordion-header" do
              expect(page).to have_content "1 fichier attaché"
            end

            # attach medical file
            within "#medical_content_attached_files" do
              expect(page).to have_css("h4", text: "0 fichier médical")
              within "form" do
                attach_file :attached_file_file, fixture_file_path("panem.txt")
              end

              expect(page).to have_css("h4", text: "1 fichier médical")
              expect(page).to have_css("li", text: "panem.txt")
            end
            within "h4.accordion-header" do
              expect(page).to have_content "2 fichiers attachés"
            end

            # delete one
            within "#standard_attached_files" do
              within "li", text: "primum.txt" do
                accept_confirm do
                  click_link "Supprimer"
                end
              end

              expect(page).to have_css("h4", text: "0 fichier standard")
              expect(page).not_to have_content("primum.txt")
            end
            within "h4.accordion-header" do
              expect(page).to have_content "1 fichier attaché"
            end
          end
        end
      end
    end
  end
end
