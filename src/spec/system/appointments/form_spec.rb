# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

def within_today_s_calendar(&)
  within("div.CalendarDay", text: I18n.l(today, format: :calendar_day), &)
end

RSpec.feature "Appointments", type: :system do
  let!(:appointments_domain) { create(:intervention_domain, :appointments) }
  let!(:authorized_consultation_type) do
    create(:consultation_type, intervention_domain: appointments_domain, roles: [authorized_role])
  end
  let!(:consultation_reason) { create(:consultation_reason, consultation_type: authorized_consultation_type) }

  let(:authorized_role) { Role.find_by(title: :nurse) }

  let!(:now) { 1.business_day.ago.change({ hour: 10, min: 0, sec: 0 }) }
  let!(:today) { now.to_date }

  before { Timecop.freeze(today) }

  after { Timecop.return }

  context "As an authorized user" do
    let!(:user) { connect_as(authorized_role.title) }

    context "Regarding an authorized consultation type" do
      scenario "I can create an appointment" do
        # Given
        calling_institution = create(:calling_institution)
        consultation_reason.update(appointment_duration: "PT45M")

        # When
        visit consultation_type_appointments_path(authorized_consultation_type)

        within "div.CalendarDay", text: I18n.l(Date.today, format: :calendar_day) do
          all("td.fc-timegrid-slot-lane").first.click
        end

        within "form#new_appointment" do
          within "fieldset", text: "Rendez-vous" do
            expect(find_field("Date et heure de début").value).to match I18n.l(today)
            expect(page).not_to have_field "Sous-type de consultation"

            check "Urgence"
            find("div.ss-main").click
          end
        end

        find("div.ss-option", text: consultation_reason.label_for_select).click

        within "form#new_appointment" do
          within "fieldset", text: "Rendez-vous" do
            expect(page).to have_field("Durée", with: consultation_reason.appointment_duration_in_minutes)
            expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_reason.appointment_duration_in_minutes} minutes.")
          end

          within "fieldset", text: "Patient" do
            select "Indéterminé", from: "Genre"
            fill_in "Nom", with: "Dupont"
            fill_in "Prénom", with: "Camille"
            fill_in "Date de naissance", with: (Date.today - 12.years).iso8601
            expect(page).to have_css("svg.fa-child")
            expect(page).to have_css("div#patient-age", text: "12 an(s)")
            fill_in "Téléphone à joindre pour ce rendez-vous", with: "0605040302"
          end

          within "fieldset", text: "OPJ" do
            fill_in "Service requérant", with: calling_institution.title[0..2]
            find("li", text: calling_institution.title).click
            fill_in "Nom", with: "Commissaire MOULIN"
            fill_in "Téléphone", with: "0102030405"
            fill_in "E-Mail", with: "moulin@pj.fr"
            fill_in "Procès verbal", with: "PV12345"
          end

          fill_in "Circonstances", with: "Quelques circonstances."
          fill_in "Remarques", with: "Quelques remarques."

          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          expect(page).to have_selector(".fc-event-main", text: "DUPONT Camille")
        end
      end

      scenario "I can create an appointment without patient and fill it later" do
        # Given
        create(:calling_institution)
        consultation_reason.update(appointment_duration: "PT45M")
        patient = create(:patient)

        # When
        visit consultation_type_appointments_path(authorized_consultation_type)

        within_today_s_calendar do
          all("td.fc-timegrid-slot-lane").first.click
        end

        within "form#new_appointment" do
          within "fieldset", text: "Rendez-vous" do
            fill_in "Durée", with: "45"
          end

          within "fieldset", text: "Patient" do
            # Nothing
          end

          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          find(".fc-event-main", text: "").click
        end

        within "div.modal" do
          within "div.modal-header" do
            click_link "Modifier"
          end
        end

        within "form.edit_appointment" do
          # Change the patient
          within "fieldset", text: "Patient" do
            fill_in "Nom", with: patient.last_name
            fill_in "Prénom", with: patient.first_name
          end

          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          expect(page).to have_css(".fc-event-main", text: patient.fullname)
        end
      end

      scenario "I can create an appointment without officer and fill it later" do
        # Given
        patient = build(:patient)
        officer = build(:officer)

        # When
        visit consultation_type_appointments_path(authorized_consultation_type)

        within_today_s_calendar do
          all("td.fc-timegrid-slot-lane").first.click
        end

        within "form#new_appointment" do
          within "fieldset", text: "Rendez-vous" do
            fill_in "Durée", with: "45"
          end

          within "fieldset", text: "Patient" do
            # To retrieve the good one in the calendar
            fill_in "Nom", with: patient.last_name
            fill_in "Prénom", with: patient.first_name
          end

          within "fieldset", text: "OPJ" do
            # Nothing
          end

          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          find(".fc-event-main", text: patient.fullname).click
        end

        within "div.modal" do
          within "div.modal-header" do
            click_link "Modifier"
          end
        end

        within "form.edit_appointment" do
          # Change the patient
          within "fieldset", text: "OPJ" do
            fill_in "Nom", with: officer.name
            fill_in "Téléphone", with: officer.phone
            fill_in "E-Mail", with: officer.email
          end

          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          find(".fc-event-main", text: patient.fullname).click
        end

        within "dl", text: "OPJ" do
          expect(page).to have_css("dd", text: officer.name)
          expect(page).to have_css("dd", text: officer.phone)
          expect(page).to have_css("dd", text: officer.email)
        end
      end

      context "regarding duration based on consultation reason or consultation subtype" do
        let!(:consultation_subtype) { create(:consultation_subtype, consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "30") }
        let!(:consultation_reason) { create(:consultation_reason, title: "AAAA", consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "45") }
        let!(:other_consultation_reason) { create(:consultation_reason, title: "BBBB", consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "60") }

        before do
          visit consultation_type_appointments_path(authorized_consultation_type)

          within "div.CalendarDay", text: I18n.l(Date.today, format: :calendar_day) do
            all("td.fc-timegrid-slot-lane").first.click
          end
        end

        scenario "uses first consultation reason by default for hint and duration" do
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_reason.appointment_duration_in_minutes} minutes.")
              expect(page).to have_field("Durée", with: consultation_reason.appointment_duration_in_minutes)
            end
          end
        end

        scenario "uses other consultation reason for hint and duration" do
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              find("div.ss-main").click
            end
          end

          find("div.ss-option", text: other_consultation_reason.label_for_select).click

          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              expect(page).to have_css("div.form-text", text: "Durée recommandée : #{other_consultation_reason.appointment_duration_in_minutes} minutes.")
              expect(page).to have_field("Durée", with: other_consultation_reason.appointment_duration_in_minutes)
            end
          end
        end

        scenario "uses consultation subtype for hint and duration" do
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              select consultation_subtype.title, from: "Sous-type de consultation"
              expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_subtype.appointment_duration_in_minutes} minutes.")
              expect(page).to have_field("Durée", with: consultation_subtype.appointment_duration_in_minutes)
            end
          end
        end

        scenario "does not overwrite user-typed duration" do
          within "form#new_appointment" do
            within "fieldset", text: "Rendez-vous" do
              # Given
              fill_in "Durée", with: typed_duration_in_minutes = 42
              # When
              select consultation_subtype.title, from: "Sous-type de consultation"
              # Then
              expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_subtype.appointment_duration_in_minutes} minutes.")
              expect(page).to have_field("Durée", with: typed_duration_in_minutes)
            end
          end
        end
      end

      context "regarding an appointment" do
        let(:patient) { create(:patient, birthdate: today - 16.years, gender: :woman) }
        let(:officer) { create(:officer) }
        let(:consultation_reason) { create(:consultation_reason, code: "AS", consultation_type: authorized_consultation_type) }
        let(:consultation_subtype) { create(:consultation_subtype, consultation_type: authorized_consultation_type) }
        let(:calling_institution) { create(:calling_institution) }
        let(:circumstances) { Faker::Lorem.sentence }
        let(:comment) { Faker::Lorem.sentence }
        let!(:appointment) {
          create(:appointment,
          patient: patient,
          officer: officer,
          consultation_type: authorized_consultation_type,
          consultation_reason: consultation_reason,
          consultation_subtype: consultation_subtype,
          calling_institution: calling_institution,
          emergency: true,
          circumstances: circumstances,
          comment: comment,
          starts_at: now.change({ hour: 10, min: 0, sec: 0 }),
          duration: "PT45M")
        }

        before do
          authorized_consultation_type.assigned_roles << user.role
        end

        def display_appointment_details
          visit consultation_type_appointments_path(authorized_consultation_type)

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: patient.fullname).click
            end
          end
        end

        scenario "I can edit it" do
          # Given
          next_starts_at = (appointment.starts_at + 2.hours).beginning_of_hour
          another_consultation_reason = create(:consultation_reason, consultation_type: authorized_consultation_type)
          another_patient = create(:patient)
          another_calling_institution = create(:calling_institution)
          # When
          display_appointment_details

          within "div.modal" do
            within "div.modal-header" do
              click_link "Modifier"
            end
          end

          # Change starts_at clicking within the calendar
          within_today_s_calendar do
            find("td.fc-timegrid-slot-lane[data-time='#{next_starts_at.strftime('%H:%M:%S')}']").click
          end

          within "form#edit_appointment_#{appointment.id}" do
            within "fieldset", text: "Rendez-vous" do
              expect(find_field("Date et heure de début").value).to eq I18n.l(next_starts_at, format: :input)

              # Change the consultation reason
              expect(page).to have_css("div.ss-single", text: consultation_reason.label_for_select)
              find("div.ss-main").click
            end
          end

          find("div.ss-option", text: another_consultation_reason.label_for_select).click

          within "form#edit_appointment_#{appointment.id}" do
            # Change the patient
            within "fieldset", text: "Patient" do
              expect(page).to have_css("div.card-header", text: patient.fullname)

              click_button "Effacer"
              expect(page).not_to have_css("div.card-header", text: patient.fullname)
              expect(page).to have_field("Nom")

              patient_search = another_patient.last_name[0..2]
              fill_in :appointment_patient, with: patient_search

              expect(page).to have_field("Nom", with: patient_search)

              find("li", text: another_patient.fullname).click

              expect(page).to have_css("div.card-header", text: another_patient.fullname)
            end

            # Change the calling institution
            within "fieldset", text: "OPJ" do
              expect(page).to have_field("Service requérant", with: calling_institution.title)

              click_button "Effacer"
              expect(page).not_to have_field("Service requérant", with: calling_institution.title)

              fill_in :appointment_calling_institution, with: another_calling_institution.title[0..2]
              find("li", text: another_calling_institution.title).click

              expect(page).to have_field("Service requérant", with: another_calling_institution.title)

              fill_in "Nom", with: "Commissaire Moulin"
            end

            click_button "Sauvegarder"
          end

          within_today_s_calendar do
            within "div.fc" do
              find("a.fc-event", text: another_patient.fullname).click
            end
          end

          within "div.modal" do
            within "div.modal-title" do
              expect(page).to have_css("h5", text: another_patient.fullname)
              within "div.appointment-meta" do
                expect(page).to have_css("span.badge", text: another_consultation_reason.code)
                expect(page).to have_content localize_date(next_starts_at, format: :input)
              end
            end
            within "div.modal-body" do
              within "div.appointment-details" do
                within "dl", text: "Service requérant" do
                  expect(page).to have_content another_calling_institution.title
                end
                within "dl", text: "OPJ" do
                  expect(page).to have_content "Commissaire Moulin"
                end
              end
            end
          end
        end

        scenario "I can create a twin appointment" do
          # Given
          another_authorized_consultation_type = create(:consultation_type, intervention_domain: appointments_domain, roles: [authorized_role])
          unauthorized_consultation_type = create(:consultation_type, intervention_domain: appointments_domain)
          # When
          visit edit_appointment_path(appointment)

          within "form#edit_appointment_#{appointment.id}" do
            within "select#appointment_next_appointment_consultation_type_id" do
              expect(page).not_to have_content unauthorized_consultation_type.title
            end
            select another_authorized_consultation_type.title, from: "Créer un autre rendez-vous après sauvegarde"
            click_button "Sauvegarder"
          end

          within "form#new_appointment" do
            within "fieldset", text: "Patient" do
              expect(page).to have_css("div.card-header", text: patient.fullname)
            end
            within "fieldset", text: "OPJ" do
              expect(page).to have_field("Service requérant", with: calling_institution.title)
            end
          end
        end

        context "regarding duration based on consultation reason or consultation subtype" do
          let(:consultation_subtype) { create(:consultation_subtype, consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "30") }
          let(:consultation_reason) { create(:consultation_reason, title: "AAAA", consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "45") }
          let!(:other_consultation_reason) { create(:consultation_reason, title: "BBBB", consultation_type: authorized_consultation_type, appointment_duration_in_minutes: "60") }

          before { appointment.update(duration: "PT15M") }

          context "without a consultation subtype" do
            before do
              appointment.update(consultation_subtype: nil)
              visit edit_appointment_path(appointment)
            end

            scenario "uses the consultation reason for hint and the saved duration by default" do
              within "form#edit_appointment_#{appointment.id}" do
                within "fieldset", text: "Rendez-vous" do
                  expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_reason.appointment_duration_in_minutes} minutes.")
                  expect(page).to have_field("Durée", with: 15)
                end
              end
            end

            scenario "uses other consultation reason for hint and the saved duration" do
              within "form#edit_appointment_#{appointment.id}" do
                within "fieldset", text: "Rendez-vous" do
                  find("div.ss-main").click
                end
              end

              find("div.ss-option", text: other_consultation_reason.label_for_select).click

              within "form#edit_appointment_#{appointment.id}" do
                within "fieldset", text: "Rendez-vous" do
                  expect(page).to have_css("div.form-text", text: "Durée recommandée : #{other_consultation_reason.appointment_duration_in_minutes} minutes.")
                  expect(page).to have_field("Durée", with: 15)
                end
              end
            end

            scenario "uses consultation subtype for hint and the saved duration" do
              within "form#edit_appointment_#{appointment.id}" do
                within "fieldset", text: "Rendez-vous" do
                  select consultation_subtype.title, from: "Sous-type de consultation"
                  expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_subtype.appointment_duration_in_minutes} minutes.")
                  expect(page).to have_field("Durée", with: 15)
                end
              end
            end
          end

          context "with a consultation subtype" do
            before do
              appointment.update(consultation_subtype: consultation_subtype)
              visit edit_appointment_path(appointment)
            end

            scenario "uses the consultation subtype for hint and the saved duration by default" do
              within "form#edit_appointment_#{appointment.id}" do
                within "fieldset", text: "Rendez-vous" do
                  expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_subtype.appointment_duration_in_minutes} minutes.")
                  expect(page).to have_field("Durée", with: 15)
                end
              end
            end

            context "when changing the consultation reason" do
              scenario "uses the consultation subtype for hint and the saved duration" do
                within "form#edit_appointment_#{appointment.id}" do
                  within "fieldset", text: "Rendez-vous" do
                    find("div.ss-main").click
                  end
                end

                find("div.ss-option", text: other_consultation_reason.label_for_select).click

                within "form#edit_appointment_#{appointment.id}" do
                  within "fieldset", text: "Rendez-vous" do
                    expect(page).to have_css("div.form-text", text: "Durée recommandée : #{consultation_subtype.appointment_duration_in_minutes} minutes.")
                    expect(page).to have_field("Durée", with: 15)
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
