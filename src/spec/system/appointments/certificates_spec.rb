# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#

# frozen_string_literal: true

require "rails_helper"

RSpec.feature "Appointments’ certificates", type: :system do
  let(:certificate) { appointment.create_certificate! }

  let!(:appointment) do
    create(:appointment,
           patient:,
           consultation_type:,
           starts_at: now.change({ hour: 10, min: 0, sec: 0 }),
           duration: "PT45M")
  end

  let(:patient) { create(:patient) }

  let!(:consultation_type) do
    create(:consultation_type, title: "Authorized", intervention_domain: appointments_domain, roles: authorized_roles,
                               assigned_roles: assignable_roles)
  end
  let!(:appointments_domain) { create(:intervention_domain, :appointments) }
  let(:authorized_roles) { assignable_roles + acts_as_secretary_roles }
  let(:assignable_roles) { Role.where(title: %i[physician]) }
  let(:acts_as_secretary_roles) { Role.where(title: %i[nurse secretary]) }

  let!(:now) { 1.business_day.ago.change({ hour: 10, min: 0, sec: 0 }) }
  let!(:today) { now.to_date }

  before { Timecop.freeze(today) }

  after { Timecop.return }

  context "with a user in charge" do
    let!(:user) { connect_as(assignable_roles.sample.title) }

    before { appointment.update(user_in_charge: user) }

    scenario "I can conclude the appointment" do
      visit edit_certificate_path(certificate)

      # Release patient
      within "div.card", text: "Observations" do
        click_on "La consultation est terminée"
      end

      within "div.modal" do
        within "div.modal-body" do
          expect(page).to have_css("div#complementary-appointments")
          click_on "Patient libéré"
        end

        # Go to the agenda
        within "div.modal-footer" do
          click_on "Revenir à l'agenda"
        end
      end

      expect(page).to have_css("a.fc-event", text: patient.fullname)
    end
  end

  context "with an non-assignable user" do
    before { connect_as(acts_as_secretary_roles.sample.title) }

    scenario "I can go back to the agenda" do
      visit edit_certificate_path(certificate)

      # Release patient
      within "div.card", text: "Observations" do
        click_on "Retour à l’agenda"
      end

      expect(page).to have_css("a.fc-event", text: patient.fullname)
    end
  end
end
