# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "CertificateTemplates", type: :system do
  context "as a connected user" do
    before { connect_as(:nurse) }

    scenario "I cannot access to the management page of certificate templates" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          expect(page).not_to have_link "Modèles de certificats"
        end
      end
    end
  end

  context "as an administrator" do
    before { connect_as(:administrator) }

    scenario "I can manage certificate templates" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          click_link "Modèles de certificats"
        end
      end

      within "div.container" do
        within "h1", text: "0 modèle" do
          click_link "Nouveau modèle"
        end
      end

      within "div.container form" do
        fill_in :certificate_template_title, with: "AS Femme"
        check "médecin"
        check "I. D. E."
        click_button "Sauvegarder"
      end


      expect(page).to have_content "Création réussie."
      expect(page).to have_css("h1", text: "1 modèle")
      within "h2", text: "AS Femme" do
        expect(page).to have_content "I. D. E., médecin"
      end

      # edit it
      template = CertificateTemplate.last
      within "#certificate-templates h2", text: template.title do
        click_link "Modifier"
      end

      within "div.container form" do
        fill_in :certificate_template_title, with: "AS Homme"
        check "médecin"
        uncheck "I. D. E."
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."
      expect(page).to have_css("h1", text: "1 modèle")
      within "h2", text: "AS Homme" do
        expect(page).to have_content "médecin"
      end

      # delete it
      template.reload
      within "#certificate-templates h2", text: template.title do
        accept_confirm do
          click_link "Supprimer"
        end
      end

      expect(page).to have_content "Suppression réussie."
      expect(page).to have_css("h1", text: "0 modèle")
      expect(page).not_to have_content template.title
    end

    scenario "I can [de]activate a certificate template" do
      template = create(:certificate_template)
      disabled_template = create(:certificate_template, disabled_at: Time.zone.now)

      visit certificate_templates_path

      expect(page).to have_css("h1", text: "1 modèle")
      expect(page).not_to have_css("h2", text: disabled_template.title)

      within "#certificate-templates h2", text: template.title do
        accept_confirm do
          click_link "Désactiver"
        end
      end

      expect(page).to have_content "Désactivation réussi(e)."
      expect(page).to have_css("h1", text: "0 modèle")
      expect(page).not_to have_css("h2", text: template.title)
      expect(page).not_to have_css("h2", text: disabled_template.title)

      click_link "Afficher les modèles désactivés"

      expect(page).to have_css("h1", text: "2 modèles")
      expect(page).to have_css("h2", text: template.title)
      expect(page).to have_css("h2", text: disabled_template.title)

      within "#certificate-templates h2", text: template.title do
        accept_confirm do
          click_link "Activer"
        end
      end

      expect(page).to have_content "Activation réussi(e)."
      expect(page).to have_css("h1", text: "2 modèles")
      expect(page).to have_css("h2", text: template.title)
      expect(page).to have_css("h2", text: disabled_template.title)

      click_link "Masquer les modèles désactivés"

      expect(page).to have_css("h1", text: "1 modèle")
      expect(page).to have_css("h2", text: template.title)
      expect(page).not_to have_css("h2", text: disabled_template.title)
    end


    scenario "I can attach a file template to a certificate template" do
      template = create(:certificate_template)

      visit certificate_templates_path

      # attach one
      within "#certificate-templates" do
        click_button template.title
      end

      within "#certificate-template-#{template.id} form" do
        attach_file :master_document_file, fixture_file_path("primum.txt")
      end

      within "#certificate-templates h2", text: template.title do
        expect(page).to have_css("svg.fa-file-arrow-down")
      end

      within "#certificate-template-#{template.id}" do
        expect(page).to have_content "primum.txt"
        expect(page).to have_css("svg.fa-file-arrow-down")
        expect(page).to have_link "Télécharger"
      end

      # attach another one
      within "#certificate-template-#{template.id} form" do
        attach_file :master_document_file, fixture_file_path("panem.txt")
      end

      within "#certificate-templates h2", text: template.title do
        expect(page).to have_css("svg.fa-file-arrow-down")
      end

      within "#certificate-template-#{template.id}" do
        expect(page).to have_content "panem.txt"
        expect(page).to have_css("svg.fa-file-arrow-down")
      end

      # delete it
      within "#certificate-template-#{template.id}" do
        accept_confirm do
          click_link "Supprimer"
        end
      end

      within "#certificate-templates h2", text: template.title do
        expect(page).not_to have_css("svg.fa-file-arrow-down")
      end

      within "#certificate-template-#{template.id}" do
        expect(page).not_to have_content "panem.txt"
        expect(page).not_to have_css("svg.fa-file-arrow-down")
      end
    end

    scenario "I can manage an embedded model" do
      model = <<EOS
{
 "title": "Survey",
 "logoPosition": "right",
 "pages": [
  {
   "name": "Agression",
   "elements": [
    {
     "type": "boolean",
     "name": "question1",
     "title": "Procès verbal d’audition communiqué ?"
    }
   ],
   "title": "Agression"
  }
 ]
}
EOS
      template = create(:certificate_template, questionnaire: nil)

      # provide one
      visit certificate_templates_path

      within "#certificate-templates h2", text: template.title do
        click_link "Modifier"
      end

      within "div.container form" do
        check "Mode expert"
        fill_in :certificate_template_questionnaire, with: model
        click_button "Sauvegarder"
      end

      within "#certificate-templates" do
        click_button template.title
      end

      within "#certificate-template-#{template.id}" do
        expect(page).to have_content "Agression"
        expect(page).to have_content("Procès verbal d’audition communiqué ?")
      end

      # remove it
      within "#certificate-templates h2", text: template.title do
        click_link "Modifier"
      end

      within "div.container form" do
        check "Mode expert"
        fill_in :certificate_template_questionnaire, with: ""
        click_button "Sauvegarder"
      end

      within "#certificate-templates" do
        click_button template.title
      end

      within "#certificate-template-#{template.id}" do
        expect(page).not_to have_content "Maquette embarquée"
      end
    end

    scenario "I can edit global certificate layout" do
      visit certificate_templates_path

      click_link "Modifier le gabarit général"

      within "div.container form" do
        fill_in :settings_certificate_layout_organisation_chart, with: "Médecin chef : Dr..."
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."
      expect(page).to have_content "Modifier le gabarit général"
    end
  end
end
