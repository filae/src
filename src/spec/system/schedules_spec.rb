# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Schedules", type: :system do
  context "As an administrator" do
    before { @admin = connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:schedule) { build(:schedule, activated_on: "01/06/2023", intervention_domain: domain)}

      scenario "I can manage schedules" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          click_link "Nouvel horaire"
        end

        within "div.container form" do
          fill_in "Date d’activation", with: "2023-06-01"
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}"  do
          expect(page).to have_css("h3", text: "1 horaire")
        end

        # edit it
        within "#intervention-domain-#{domain.id} li", text: "À partir du 01/06/2023"  do
          click_link "Modifier"
        end

        within "div.container form" do
          fill_in "Date d’activation", with: "2023-07-15"
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}"  do
          expect(page).to have_css("h3", text: "1 horaire")
          expect(page).to have_css("li", text: "À partir du 15/07/2023")
        end

        # destroy it
        within "#intervention-domain-#{domain.id} li", text: "À partir du 15/07/2023"  do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}"  do
          expect(page).to have_css("h3", text: "0 horaire")
          expect(page).not_to have_content "À partir du 15/07/2023"
        end
      end
    end

    context "regarding stream intervention domains" do
      let!(:domain) { create(:intervention_domain, :stream) }

      scenario "I cannot manage schedules" do
        visit intervention_domains_path

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          expect(page).not_to have_css("h3", text: "horaire")
          expect(page).not_to have_link "Nouvel horaire"
        end
      end
    end
  end
end
