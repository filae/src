# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "TimeSlots", type: :system do
  context "As an administrator" do
    before { connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I can manage time slots" do
        # Given
        create(:schedule, intervention_domain: domain)

        # When
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} li", text: "À partir du"  do
          click_link "Modifier"
        end

        within "table td#consultation-type-#{type.id}-day-1" do
          click_link "Nouveau créneau"
        end

        within "div.container form" do
          select "08", from: :time_slot_starts_at_4i
          select "45", from: :time_slot_starts_at_5i
          select "17", from: :time_slot_ends_at_4i
          select "30", from: :time_slot_ends_at_5i
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."

        within "table td#consultation-type-#{type.id}-day-1" do
          expect(page).to have_css("li", text: "08:45 > 17:30")
        end

        # edit it
        time_slot = TimeSlot.last
        within "table td#consultation-type-#{type.id}-day-1 li", text: time_slot.display do
          click_link "Modifier"
        end

        within "div.container form" do
          select "09", from: :time_slot_starts_at_4i
          select "15", from: :time_slot_starts_at_5i
          select "18", from: :time_slot_ends_at_4i
          select "45", from: :time_slot_ends_at_5i
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        within "table td#consultation-type-#{type.id}-day-1" do
          expect(page).to have_css("li", text: "09:15 > 18:45")
        end

        # delete it
        time_slot.reload
        within "table td#consultation-type-#{type.id}-day-1 li", text: time_slot.display do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."

        within "table td#consultation-type-#{type.id}-day-1" do
          expect(page).not_to have_content "09:15 > 18:45"
        end
      end
    end
  end
end
