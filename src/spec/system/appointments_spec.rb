# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

def within_today_s_calendar
  within "div.CalendarDay", text: I18n.l(today, format: :calendar_day) do
    yield
  end
end

RSpec.feature "Appointments", type: :system do
  let!(:appointments_domain) { create(:intervention_domain, :appointments) }
  let!(:authorized_consultation_type) { create(:consultation_type, title: "Authorized", intervention_domain: appointments_domain, roles: [authorized_role]) }
  let!(:forbidden_consultation_type) { create(:consultation_type, intervention_domain: appointments_domain, roles: []) }
  let!(:consultation_reason) { create(:consultation_reason, consultation_type: authorized_consultation_type) }

  let(:authorized_role) { Role.find_by(title: :nurse) }

  let!(:now) { 1.business_day.ago.change({ hour: 10, min: 0, sec: 0 }) }
  let!(:today) { now.to_date }

  before { Timecop.freeze(today) }

  after { Timecop.return }

  context "As a guest" do
    scenario "I can not access the agenda" do
      visit consultation_type_appointments_path(authorized_consultation_type)

      expect(page).to have_content "Merci de vous authentifier"
    end
  end

  context "As an authorized user" do
    let!(:user) { connect_as(authorized_role.title) }

    context "Regarding an authorized consultation type" do
      scenario "I can access to the agenda" do
        visit consultation_type_appointments_path(authorized_consultation_type)

        expect(page).to have_selector "div.fc"
      end

      scenario "I can see cancelled appointments" do
        # Given
        appointment = create(:appointment, :with_patient, cancelled_at: now, consultation_type: authorized_consultation_type)
        # When
        visit consultation_type_appointments_path(authorized_consultation_type)

        within_today_s_calendar do
          within "div.fc" do
            expect(page).not_to have_content appointment.patient.fullname
          end
        end

        within "nav.CalendarDays-nav" do
          check "Afficher les rendez-vous reportés et annulés"
        end

        within_today_s_calendar do
          within "div.fc" do
            expect(page).to have_content appointment.patient.fullname
          end
        end
      end

      scenario "I can navigate within dates with a default offset of 2 days" do
        # Given
        Timecop.freeze(2023,9,12)

        visit consultation_type_appointments_path(authorized_consultation_type)

        expect(page).not_to have_css("div.CalendarDay", text: "mer. 06/09")
        expect(page).not_to have_css("div.CalendarDay", text: "jeu. 07/09")
        expect(page).to have_css("div.CalendarDay", text: "ven. 08/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 09/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 10/09")
        expect(page).to have_css("div.CalendarDay", text: "lun. 11/09")
        expect(page).to have_css("div.CalendarDay", text: "mar. 12/09")
        expect(page).to have_css("div.CalendarDay", text: "mer. 13/09")
        expect(page).to have_css("div.CalendarDay", text: "jeu. 14/09")
        expect(page).not_to have_css("div.CalendarDay", text: "ven. 15/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 16/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 17/09")
        expect(page).not_to have_css("div.CalendarDay", text: "lun. 18/09")

        within "nav.CalendarDays-nav" do
          click_link "Précédemment"
        end

        expect(page).to have_css("div.CalendarDay", text: "mer. 06/09")
        expect(page).to have_css("div.CalendarDay", text: "jeu. 07/09")
        expect(page).to have_css("div.CalendarDay", text: "ven. 08/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 09/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 10/09")
        expect(page).to have_css("div.CalendarDay", text: "lun. 11/09")
        expect(page).to have_css("div.CalendarDay", text: "mar. 12/09")
        expect(page).not_to have_css("div.CalendarDay", text: "mer. 13/09")
        expect(page).not_to have_css("div.CalendarDay", text: "jeu. 14/09")
        expect(page).not_to have_css("div.CalendarDay", text: "ven. 15/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 16/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 17/09")
        expect(page).not_to have_css("div.CalendarDay", text: "lun. 18/09")

        within "nav.CalendarDays-nav" do
          click_link "Aujourd'hui"
        end

        expect(page).not_to have_css("div.CalendarDay", text: "mer. 06/09")
        expect(page).not_to have_css("div.CalendarDay", text: "jeu. 07/09")
        expect(page).to have_css("div.CalendarDay", text: "ven. 08/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 09/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 10/09")
        expect(page).to have_css("div.CalendarDay", text: "lun. 11/09")
        expect(page).to have_css("div.CalendarDay", text: "mar. 12/09")
        expect(page).to have_css("div.CalendarDay", text: "mer. 13/09")
        expect(page).to have_css("div.CalendarDay", text: "jeu. 14/09")
        expect(page).not_to have_css("div.CalendarDay", text: "ven. 15/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 16/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 17/09")
        expect(page).not_to have_css("div.CalendarDay", text: "lun. 18/09")

        within "nav.CalendarDays-nav" do
          click_link "À suivre"
        end

        expect(page).not_to have_css("div.CalendarDay", text: "mer. 06/09")
        expect(page).not_to have_css("div.CalendarDay", text: "jeu. 07/09")
        expect(page).not_to have_css("div.CalendarDay", text: "ven. 08/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 09/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 10/09")
        expect(page).not_to have_css("div.CalendarDay", text: "lun. 11/09")
        expect(page).to have_css("div.CalendarDay", text: "mar. 12/09")
        expect(page).to have_css("div.CalendarDay", text: "mer. 13/09")
        expect(page).to have_css("div.CalendarDay", text: "jeu. 14/09")
        expect(page).to have_css("div.CalendarDay", text: "ven. 15/09")
        expect(page).not_to have_css("div.CalendarDay", text: "sam. 16/09")
        expect(page).not_to have_css("div.CalendarDay", text: "dim. 17/09")
        expect(page).to have_css("div.CalendarDay", text: "lun. 18/09")

        if today.workday?
          # Uses javascript for current date, so cannot fake it with Timecop
          # Run only on work days to prevent test failures on week-ens for CI
          Timecop.return

          within "nav.CalendarDays-nav" do
            click_button "Au jour"

            within "div.qs-datepicker" do
              all("div.qs-num", text: Date.today.day).first.click
            end
          end

          expected_date = I18n.l Date.today, format: :calendar_day
          within "div.CalendarDays" do
            expect(page).to have_content expected_date
          end
        end
      end

      describe "with automatic refresh" do
        subject(:i_just_wait) { sleep Capybara.default_max_wait_time }

        let(:dupont) { create(:patient, last_name: "DUPONT") }
        let(:martin) { create(:patient, last_name: "MARTIN") }

        let!(:existing_appointment) do
          create(:appointment,
                 consultation_type: authorized_consultation_type,
                 patient: dupont,
                 starts_at: now.change({ hour: 10, min: 0, sec: 0 }),
                 patient_arrived_at: Time.current)
        end

        before do
          visit consultation_type_appointments_path(authorized_consultation_type)
        end

        describe "when an appointment is created" do
          it "displays nothing first (pre-conditions check)" do
            # Then
            within_today_s_calendar do
              expect(page).not_to have_selector(".fc-event-main", text: "MARTIN")
            end
          end

          it "shows up on next interval" do
            # Given
            create(:appointment, consultation_type: authorized_consultation_type, patient: martin, starts_at: now.change({ hour: 16, min: 0, sec: 0 }))
            # When
            i_just_wait
            # Then
            within_today_s_calendar do
              within '.fc-event[data-test-event-time="16:00 - 17:00"]' do
                expect(page).to have_selector(".fc-event-main", text: "MARTIN")
              end
            end
          end
        end

        describe "when an appointment is updated" do
          it "displays the initial data first (pre-conditions check)" do
            # Then
            within_today_s_calendar do
              within '.fc-event[data-test-event-time="10:00 - 11:00"]' do
                expect(page).to have_selector(".fc-event-main", text: "DUPONT")
                expect(page).to have_selector(".PatientJourney", text: "A")
              end
            end
          end

          it "displays the updated times" do
            # Given
            existing_appointment.update(starts_at: now.change({ hour: 11, min: 0, sec: 0 }))
            # When
            i_just_wait
            # Then
            within_today_s_calendar do
              within '.fc-event[data-test-event-time="11:00 - 12:00"]' do
                expect(page).to have_selector(".fc-event-main", text: "DUPONT")
              end
            end
          end

          it "displays the updated patient name" do
            # Given
            dupont.update(last_name: "DUPONTEL")
            # When
            i_just_wait
            # Then
            within_today_s_calendar do
              within '.fc-event[data-test-event-time="10:00 - 11:00"]' do
                expect(page).to have_selector(".fc-event-main", text: "DUPONTEL")
              end
            end
          end

          it "displays the new complementary consultation" do
            # Given
            another_consultation_type = create(:consultation_type, title: "Other")
            existing_appointment.complementary_appointments << create(:appointment, consultation_type: another_consultation_type, patient_arrived_at: Time.current)
            # When
            i_just_wait
            # Then
            within_today_s_calendar do
              within '.fc-event[data-test-event-time="10:00 - 11:00"]' do
                expect(page).to have_selector(".PatientJourney", text: "(A » O)")
              end
            end
          end
        end
      end

      scenario "I can [un]assign a time slot to an assignable user" do
        # Given
        physician = create(:user, :physician)
        psychologist = create(:user, :psychologist)
        authorized_consultation_type.update(assigned_roles: [Role.find_by_title(:psychologist)])
        schedule = create(:schedule, intervention_domain: appointments_domain, activated_on: today)
        create(:time_slot, consultation_type: authorized_consultation_type, schedule: schedule, week_day: today.cwday, starts_at: now, ends_at: now + 2.hours)
        # When
        visit consultation_type_appointments_path(authorized_consultation_type)

        within_today_s_calendar do
          # wait for open-hour rendered to switch to assignment mode
          expect(page).to have_selector("div.fc-event", visible: true)
        end

        within "nav.CalendarDays-nav" do
          check "Mode Affectation"
        end

        # assign physician
        within_today_s_calendar do
          find("a.fc-event", text: "Ø").click
        end

        within "div.modal" do
          expect(page).not_to have_content physician.name
          check psychologist.name
          click_button "Sauvegarder"
        end

        #unassign physician
        within_today_s_calendar do
          find("a.fc-event", text: psychologist.name).click
        end

        within "div.modal" do
          uncheck psychologist.name
          click_button "Sauvegarder"
        end

        within_today_s_calendar do
          expect(page).to have_css("a.fc-event", text: "Ø")
        end
      end
    end

    context "regarding archives" do
      scenario "I can filter them" do
        # Given
        appointment = create(:appointment, starts_at: now.change({ hour: 10, min: 0, sec: 0 }), consultation_type: authorized_consultation_type)
        old_appointment = create(:appointment, starts_at: now.change({ hour: 10, min: 0, sec: 0 }) - 4.days, consultation_type: authorized_consultation_type)
        default_date_filter = (today - 3.days).iso8601
        custom_date_filter = (today - 5.days).iso8601
        # When
        visit consultation_type_appointments_path(authorized_consultation_type)
        click_link "Archives"

        expect(page).to have_css("tr", text: appointment.onml_id)
        expect(page).not_to have_css("tr", text: old_appointment.onml_id)

        # check default filter
        within "form#appointments_filter" do
          expect(page).to have_field("Urgence", with: "")
          expect(page).to have_field("Code ONML", with: "")
          expect(page).to have_field("Procès verbal", with: "")
          expect(page).to have_field("Date de début", with: default_date_filter)
          expect(page).to have_field("Date de fin", with: "")
          expect(page).to have_field("Motif de consultation", with: "")
          expect(page).to have_field("Service requérant", with: "")
          expect(page).to have_field("Personne en charge", with: "")
          expect(page).to have_field("Nom du patient", with: "")
          expect(page).to have_field("Date de naissance du patient", with: "")
          expect(page).to have_field("Genre du patient", with: "")
          expect(page).to have_field("Âge du patient", with: "")

          fill_in "Date de début", with: custom_date_filter
          click_button "Filtrer"
        end

        expect(page).to have_css("tr", text: appointment.onml_id)
        expect(page).to have_css("tr", text: old_appointment.onml_id)
        # check filter persistent across requests
        within "form#appointments_filter" do
          expect(page).to have_field("Date de début", with: custom_date_filter)
        end

        visit consultation_type_appointments_path(authorized_consultation_type)
        click_link "Archives"
        # check filter persisted in session and reset it
        within "form#appointments_filter" do
          expect(page).to have_field("Date de début", with: custom_date_filter)
          click_link "Effacer"
        end

        within "form#appointments_filter" do
          expect(page).to have_field("Date de début", with: default_date_filter)
        end
      end

      scenario "I can manage standard and medical content attached files and list signed and validated certificate ones" do
        # Given
        appointment = create(:appointment, :with_validated_certificate, consultation_type: authorized_consultation_type)
        # When
        visit archives_consultation_type_appointments_path(authorized_consultation_type)

        within "tr", text: appointment.onml_id do
          within ".Appointment-attachedFiles" do
            expect(page).to have_content "2"
            click_link "Fichiers attachés"
          end
        end

        # attach one
        within "div.modal" do
          within "#signed_certificate_attached_files" do
            expect(page).to have_css("h4", text: "1 certificat signé")
            expect(page).to have_css("li", count: 1)
            expect(page).not_to have_selector "form"
          end
          within "#validated_certificate_attached_files" do
            expect(page).to have_css("h4", text: "1 certificat validé")
            expect(page).to have_css("li", count: 1)
            expect(page).not_to have_selector "form"
          end
          within "#standard_attached_files" do
            expect(page).to have_css("h4", text: "0 fichier standard")
            within "form" do
              attach_file :attached_file_file, fixture_file_path("primum.txt")
            end

            expect(page).to have_css("h4", text: "1 fichier standard")
            expect(page).to have_css("li", text: "primum.txt")
          end
          within "#medical_content_attached_files" do
            expect(page).to have_css("h4", text: "0 fichier médical")
            within "form" do
              attach_file :attached_file_file, fixture_file_path("panem.txt")
            end

            expect(page).to have_css("h4", text: "1 fichier médical")
            expect(page).to have_css("li", text: "panem.txt")
          end
        end

        within "tr", text: appointment.onml_id do
          within ".Appointment-attachedFiles" do
            expect(page).to have_content "4"
          end
        end

        # download them
        within "div.modal" do
          within "#standard_attached_files" do
            within "li", text: "primum.txt" do
              click_link "Télécharger"
            end
          end
          within "#medical_content_attached_files" do
            within "li", text: "panem.txt" do
              click_link "Télécharger"
            end
          end
        end

        # destroy them
        within "div.modal" do
          within "#standard_attached_files" do
            within "li", text: "primum.txt" do
              accept_confirm do
                click_link "Supprimer"
              end
            end

            expect(page).to have_css("h4", text: "0 fichier standard")
            expect(page).not_to have_content("primum.txt")
          end
        end

        within "tr", text: appointment.onml_id do
          within ".Appointment-attachedFiles" do
            expect(page).to have_content "3"
          end
        end

        within "div.modal" do
          within "#medical_content_attached_files" do
            within "li", text: "panem.txt" do
              accept_confirm do
                click_link "Supprimer"
              end
            end

            expect(page).to have_css("h4", text: "0 fichier médical")
            expect(page).not_to have_content("panem.txt")
          end
        end

        within "tr", text: appointment.onml_id do
          within ".Appointment-attachedFiles" do
            expect(page).to have_content "2"
          end
        end
      end
    end

    context "Regarding all authorized consultation types" do
      let!(:another_authorized_consultation_type) { create(:consultation_type, intervention_domain: appointments_domain, roles: [authorized_role]) }

      scenario "I can access to the agenda" do
        visit root_path

        within "div.card", text: appointments_domain.title do
          click_link "Toutes les consultations d’aujourd’hui"
        end

        within "#calendar" do
          expect(page).not_to have_css("div.form-switch", text: "Mode Affectation")
          expect(page).to have_css("div.form-switch", text: "Afficher les rendez-vous reportés et annulés")
          expect(page).to have_css("div.CalendarDay", count: 2)
          expect(page).to have_css("div.CalendarDay", text: authorized_consultation_type.title)
          expect(page).to have_css("div.CalendarDay", text: another_authorized_consultation_type.title)
          expect(page).not_to have_css("div.CalendarDay", text: forbidden_consultation_type.title)

          # Step by 1
          expect(page).to have_link("Aujourd'hui", href: /#{today}/)
          expect(page).to have_link("Précédemment", href: /#{1.business_day.before(today)}/)
          expect(page).to have_link("À suivre", href: /#{1.business_day.after(today)}/)
        end
      end
    end
  end
end
