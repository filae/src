# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "CallingInstitutions", type: :system do
  context "as a connected user" do
    before { connect_as(:nurse) }

    scenario "I cannot access to the management page of calling institiutions" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          expect(page).not_to have_link "Services requérants"
        end
      end
    end
  end

  context "as an administrator" do
    before { connect_as(:administrator) }

    scenario "I can add a calling area and an associated calling institution" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          click_link "Services requérants"
        end
      end

      within "div.container" do
        within "h1", text: "0 secteur" do
          click_link "Nouveau secteur"
        end
      end

      within "div.container form" do
        fill_in :calling_area_title, with: "Nord"
        fill_in :calling_area_colour, with: "#00afdf"
        click_button "Sauvegarder"
      end

      within "div.accordion" do
        click_button "Nord"
      end

      area = CallingArea.last
      within "#calling-area-#{area.id}" do
        click_link "Nouveau service requérant"
      end

      within "div.container form" do
        fill_in :calling_institution_title, with: "Commissariat de police des Mureaux"
        click_button "Sauvegarder"
      end

      within "div.accordion" do
        click_button "Nord"
      end

      within "#calling-area-#{area.id}" do
        expect(page).to have_content("Commissariat de police des Mureaux")
      end
    end

    scenario "I can edit a calling area and an associated calling institution" do
      area = create(:calling_area, title: "Nord")
      institution = create(:calling_institution, title: "Commissariat", calling_area: area)

      visit calling_areas_path

      within "#calling-areas h2", text: area.title do
        click_link "Modifier"
      end

      within "div.container form" do
        fill_in :calling_area_title, with: "Sud"
        click_button "Sauvegarder"
      end

      expect(page).not_to have_content("Nord")
      within "div.accordion" do
        click_button "Sud"
      end

      within "#calling-area-#{area.id} li", text: institution.title do
        click_link "Modifier"
      end

      within "div.container form" do
        fill_in :calling_institution_title, with: "Gendarmerie"
        click_button "Sauvegarder"
      end

      within "div.accordion" do
        click_button "Sud"
      end

      within "#calling-area-#{area.id}" do
        expect(page).to have_content("Gendarmerie")
        expect(page).not_to have_content("Commissariat")
      end
    end

    context "regarding deletion" do
      scenario "I can delete a calling area not associated to any calling institution" do
        area = create(:calling_area)

        visit calling_areas_path

        within "#calling-areas h2", text: area.title do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        expect(page).not_to have_content area.title
      end

      scenario "I cannot delete a calling area associated to at least one calling institution" do
        area = create(:calling_area)
        create(:calling_institution, calling_area: area)

        visit calling_areas_path

        within "#calling-areas h2", text: area.title do
          expect(page).not_to have_link "Supprimer"
        end
      end

      scenario "I can delete a calling institution not associated to any appointment nor flowing consultation" do
        area = create(:calling_area)
        institution = create(:calling_institution, calling_area: area)

        visit calling_areas_path

        within "#calling-areas" do
          click_button area.title
        end

        within "#calling-area-#{area.id}", text: institution.title do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        within "#calling-areas" do
          click_button area.title
        end
        within "#calling-area-#{area.id}" do
          expect(page).not_to have_content institution.title
        end
      end

      scenario "I cannot delete a calling institution associated to at least an appointment or a flowing consultation" do
        area = create(:calling_area)
        institution1 = create(:calling_institution, calling_area: area)
        create(:appointment, calling_institution: institution1)
        institution2 = create(:calling_institution, calling_area: area)
        create(:flowing_consultation, calling_institution: institution2)

        visit calling_areas_path

        within "#calling-areas" do
          click_button area.title
        end

        within "#calling-area-#{area.id}", text: institution1.title do
          expect(page).not_to have_link "Supprimer"
        end

        within "#calling-area-#{area.id}", text: institution2.title do
          expect(page).not_to have_link "Supprimer"
        end
      end
    end
  end
end
