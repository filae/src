# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "ConsultationSubtypes", type: :system do
  context "As an administrator" do
    before { connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }
      let(:subtype) { build(:consultation_subtype, consultation_type: type) }

      scenario "I can manage consultation subtypes" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "0 sous-type de consultation"
          click_link "Nouveau sous-type de consultation"
        end

        within "div.container form" do
          fill_in :consultation_subtype_title, with: subtype.title
          fill_in :consultation_subtype_appointment_duration_in_minutes, with: 60
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "1 sous-type de consultation"
          expect(page).to have_content "#{subtype.title} (1 heure)"
        end

        # edit it
        subtype = ConsultationSubtype.last
        within "#consultation-type-#{type.id} li", text:  subtype.title do
          click_link "Modifier"
        end

        within "div.container form" do
          fill_in :consultation_subtype_appointment_duration_in_minutes, with: 45
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "1 sous-type de consultation"
          expect(page).to have_content "#{subtype.title} (45 minutes)"
        end

        # delete it
        subtype.reload
        within "#consultation-type-#{type.id} li", text:  subtype.title do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "0 sous-type de consultation"
          expect(page).not_to have_content subtype.title
        end
      end
    end

    context "regarding stream intervention domains" do
      let!(:domain) { create(:intervention_domain, :stream) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I cannot manage subtypes" do
        visit intervention_domains_path

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          expect(page).not_to have_button type.title
        end
      end
    end
  end
end
