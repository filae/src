# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Home", type: :system do
  shared_examples "a presenter of Filae version" do |parent_selector|
    scenario "I can see the version of Filaé" do
      filae_version = "main+123456"
      ENV["FILAE_VERSION"] = filae_version

      visit root_path

      within parent_selector do
        find(".filae_version").hover
      end
      expect(page).to have_content(filae_version)
    end
  end

  context "As a guest" do
    it_behaves_like "a presenter of Filae version", ".modal"

    scenario "I can click on home link" do
      visit root_path

      within "nav" do
        expect(page).to have_link("FILAÉ")
      end
    end

    scenario "I cannot see the welcome message on the home page" do
      visit root_path

      expect(page).to have_content "Me connecter"
      expect(page).not_to have_content("Bon")
    end

    scenario "I cannot see the menu" do
      visit root_path

      expect(page).to have_content "Me connecter"
      expect(page).not_to have_content("Paramètres")
    end
  end

  context "As a connected user" do

    let!(:user) { connect_as(:nurse) }
    let!(:appointments_domain) { create(:intervention_domain, :appointments) }
    let!(:stream_domain) { create(:intervention_domain, :stream, roles: [user.role]) }
    let!(:another_stream_domain) { create(:intervention_domain, :stream, roles: []) }
    let!(:ct1) { create(:consultation_type, intervention_domain: appointments_domain, roles: [user.role]) }
    let!(:ct2) { create(:consultation_type, intervention_domain: appointments_domain, roles: [user.role]) }
    let!(:ct3) { create(:consultation_type, intervention_domain: appointments_domain, roles: []) }

    it_behaves_like "a presenter of Filae version", "nav"

    scenario "I can see the intervention domains and consultation types I have access to" do
      visit root_path

      within "div#intervention_domains" do
        within "div.card", text: appointments_domain.title do
          expect(page).to have_link(ct1.title)
          expect(page).to have_link(ct2.title)
          expect(page).not_to have_link(ct3.title)
        end
        within "div.card", text: stream_domain.title do
          expect(page).to have_link("Flux")
        end
        expect(page).not_to have_selector("h2", text: another_stream_domain.title)
      end
    end
  end
end
