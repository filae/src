# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "ConsultationReasons", type: :system do
  context "As an administrator" do
    before { connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }
      let(:reason) { build(:consultation_reason, consultation_type: type) }

      scenario "I can manage consultation reasons" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "0 motif de consultation"
          click_link "Nouveau motif de consultation"
        end

        within "div.container form" do
          fill_in :consultation_reason_title, with: reason.title
          fill_in :consultation_reason_code, with: reason.code
          fill_in :consultation_reason_colour, with: reason.colour
          fill_in :consultation_reason_appointment_duration_in_minutes, with: 60
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Création réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "1 motif de consultation"
          expect(page).to have_content "#{reason.title} (1 heure)"
        end

        # edit it
        reason = ConsultationReason.last
        within "#consultation-type-#{type.id} li", text:  reason.title do
          click_link "Modifier"
        end

        within "div.container form" do
          fill_in :consultation_reason_title, with: "Agression sexuelle"
          fill_in :consultation_reason_code, with: "AS"
          fill_in :consultation_reason_appointment_duration_in_minutes, with: 45
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "1 motif de consultation"
          expect(page).to have_content "AS - Agression sexuelle (45 minutes)"
        end

        # delete it
        reason.reload
        within "#consultation-type-#{type.id} li", text:  reason.title do
          accept_confirm do
            click_link "Supprimer"
          end
        end

        expect(page).to have_content "Suppression réussie."
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "0 motif de consultation"
          expect(page).not_to have_content reason.title
        end
      end
    end

    context "regarding stream intervention domains" do
      let!(:domain) { create(:intervention_domain, :stream) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I cannot manage reasons" do
        visit intervention_domains_path

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          expect(page).not_to have_button type.title
        end
      end
    end
  end
end
