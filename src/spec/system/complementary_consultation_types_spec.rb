# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "ComplementaryConsultationTypes", type: :system do
  context "As an administrator" do
    before { connect_as(:administrator) }

    context "regarding appointments intervention domains" do
      let!(:domain) { create(:intervention_domain, :appointments) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }
      let!(:other_type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I can manage complementary consultation types" do
        visit intervention_domains_path

        # add one
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_link "Modifier"
        end

        within "div.container form" do
          within "fieldset", text: "Types de consultation complémentaires" do
            check other_type.title
          end
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        type.reload
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          within "div.col", text: "Types de consultations complémentaires" do
            expect(page).to have_css("li", text: other_type.title)
          end
        end

        # remove it
        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_link "Modifier"
        end

        within "div.container form" do
          within "fieldset", text: "Types de consultation complémentaires" do
            uncheck other_type.title
          end
          click_button "Sauvegarder"
        end

        expect(page).to have_content "Mise à jour réussie."

        type.reload
        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id} h4", text: type.title  do
          click_button type.title
        end

        within "#consultation-type-#{type.id}" do
          expect(page).to have_content "Types de consultations complémentaires"
          expect(page).not_to have_content other_type.title
        end
      end
    end

    context "regarding stream intervention domains" do
      let!(:domain) { create(:intervention_domain, :stream) }
      let!(:type) { create(:consultation_type, intervention_domain: domain) }

      scenario "I cannot manage complementary consultation types" do
        visit intervention_domains_path

        within "#intervention-domains" do
          click_button domain.title
        end

        within "#intervention-domain-#{domain.id}" do
          expect(page).not_to have_button type.title
        end
      end
    end
  end
end

