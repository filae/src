# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Patients", type: :system do
  context "as a connected user without medical confidentiality access" do
    before { connect_as(:switchboard_operator) }

    scenario "I cannot access to the management of patients" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          expect(page).not_to have_link "Patients"
        end
      end
    end
  end

  context "as a connected user with medical confidentiality access" do
    before { @user = connect_as(:nurse) }

    scenario "I can list paginated patients" do
      30.times { create(:patient) }

      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "#menu" do
          click_link "Patients"
        end
      end

      expect(page).to have_css("h1", text: "25 patients sur 30")
      within "table tbody" do
        expect(all("tr").length).to eq 25
      end
      within "ul.pagination" do
        click_link "2"
      end

      expect(page).to have_css("h1", text: "5 patients sur 30")
      within "table tbody" do
        expect(all("tr").length).to eq 5
      end

      within "#lines-number-selector" do
        click_link "50"
      end

      expect(page).to have_css("h1", text: "30 patients sur 30")
      expect(page).not_to have_css "ul.pagination"
      within "table tbody" do
        expect(all("tr").length).to eq 30
      end
    end

    scenario "I can edit a patient" do
      patient = create(:patient, last_name: "Dupond", first_name: "Camille", birth_year: "2008", birthdate: nil)

      visit patients_path
      within "table tr", text: "DUPOND Camille" do
        click_link "Modifier"
      end

      within "div.modal form" do
        select "Femme", from: :patient_gender
        fill_in :patient_last_name, with: "Dupont"

        # no error message if only birth year
        within "div.patient_birth_year" do
          expect(page).not_to have_css("div#year-mismatch")
        end

        # provide birthdate with NOT matching year
        fill_in :patient_birthdate, with: "2009-06-01"

        within "div.patient_birthdate" do
          expect(page).to have_css("div#year-mismatch")
        end

        # cleanup birthdate input
        #pending 'works in UI...
        #find_field(:patient_birthdate).native.clear
        #fill_in :patient_birthdate, with: ''

        #within 'div.patient_birthdate' do
        #  expect(page).not_to have_css('div#year-mismatch')
        #end

        # provide birthdate with matching year
        fill_in :patient_birthdate, with: "2008-06-01"

        expect(page).not_to have_css("div.patient_birth_year")
        click_button "Sauvegarder"
      end

      expect(page).to have_content "Mise à jour réussie."
      within "table" do
        expect(page).not_to have_css("tr", text: "DUPOND Camille")
        within "tr", text: "DUPONT Camille" do
          expect(page).to have_css("td", text: "01/06/2008")
        end
      end
    end

    scenario "I can de-duplicate a patient" do
      first_patient = create(:patient, last_name: "Dupond", first_name: "Camille").tap do |patient|
        create(:appointment, patient: patient)
        create(:flowing_consultation, patient: patient)
      end
      second_patient = create(:patient, last_name: "Dupond", first_name: "Camille",
                                        probably_duplicated_patient: first_patient).tap do |patient|
        create(:appointment, patient: patient)
        create(:flowing_consultation, patient: patient)
      end

      visit patients_path
      within "##{dom_id(second_patient)}" do
        click_link "Doublon ?"
      end

      within "div.modal form" do
        click_button "Fusionner"
      end

      expect(page).to have_content "Fusion réussi(e)."
      within "table" do
        within "##{dom_id(first_patient)}", text: "DUPOND Camille" do
          expect(page).to have_css("td.text-info", text: "(4)")
        end
        expect(page).not_to have_css("##{dom_id(second_patient)}")
      end
    end

    scenario "I can see the actions logs on a patient" do
      patient = create(:patient, last_name: "Dupond", first_name: "Camille", birthdate: "01/06/2008")
      Log.generate(patient, @user, Time.zone.now, [Log::Action.new("created").to_json])

      visit patients_path

      within "table tr", text: "DUPOND Camille" do
        click_button "Historique des actions"

        within "ul.dropdown-menu" do
          expect(page).to have_css("li", text: "Créé le")
        end
      end
    end

    scenario "I can see appointments and flowing consultations associated to a patient" do
      # Given
      patient = create(:patient)
      appointment = create(:appointment, patient: patient)
      flowing_consultation = create(:flowing_consultation, patient: patient)

      # When
      visit patients_path

      within "table tr", text: patient.fullname do
        click_link "Historique des consultations"
      end

      within "div.modal" do
        within "div.modal-header" do
          expect(page).to have_css("h5", text: "Historique des consultations")
        end

        within "div.modal-body" do
          expect(page).to have_css("div.card", text: patient.fullname)
          within "div", text: "Sur rendez-vous" do
            within "table" do
              expect(page).to have_selector("tr", count: 1)
              expect(page).to have_css("tr", text: appointment.onml_id)
            end
          end
          within "div", text: "En flux" do
            within ".FlowingConsultationsList" do
              expect(page).to have_selector(".FlowingConsultation", count: 1)
              expect(page).to have_css(".FlowingConsultation", text: flowing_consultation.onml_id)
            end
          end
        end
      end
    end

    context "regarding filters" do
      before do
        @patient1 = create(:patient, last_name: "Dupond", first_name: "Camille", birthdate: "01/06/2008")
        @patient2 = create(:patient, last_name: "Durand", first_name: "Estelle", birthdate: nil, birth_year: "2006")
      end

      scenario "I can filter by last name and reset the filter" do
        visit patients_path

        within "form#filter" do
          fill_in :patient_last_name, with: "Durand"
          click_button "Filtrer"
        end

        expect(page).to have_css("h1", text: "1 patient sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 1
          expect(page).to have_content "DURAND Estelle"
          expect(page).not_to have_content "DUPOND"
        end

        within "form#filter" do
          click_link "Effacer"
        end

        expect(page).to have_css("h1", text: "2 patients sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 2
          expect(page).to have_content "DURAND Estelle"
          expect(page).to have_content "DUPOND Camille"
        end
      end

      scenario "I can filter by first name" do
        visit patients_path

        within "form#filter" do
          fill_in :patient_first_name, with: "Est"
          click_button "Filtrer"
        end

        expect(page).to have_css("h1", text: "1 patient sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 1
          expect(page).to have_content "DURAND Estelle"
          expect(page).not_to have_content "DUPOND"
        end
      end

      scenario "I can filter by birthdate" do
        visit patients_path

        within "form#filter" do
          fill_in :patient_birthdate, with: "2008-06-01"
          click_button "Filtrer"
        end

        expect(page).to have_css("h1", text: "1 patient sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 1
          expect(page).to have_content "DUPOND Camille"
          expect(page).not_to have_content "DURAND"
        end
      end

      scenario "I can filter by birth year" do
        visit patients_path

        within "form#filter" do
          fill_in :patient_birth_year, with: "2006"
          click_button "Filtrer"
        end

        expect(page).to have_css("h1", text: "1 patient sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 1
          expect(page).to have_content "DURAND Estelle"
          expect(page).not_to have_content "DUPOND"
        end
      end

      scenario "I can filter by duplication status" do
        @patient2.update(probably_duplicated_patient: @patient1)

        visit patients_path

        within "form#filter" do
          check :patient_probably_duplicated_patient_id
          click_button "Filtrer"
        end

        expect(page).to have_css("h1", text: "1 patient sur 2")
        within "table tbody" do
          expect(all("tr").length).to eq 1
          expect(page).to have_content "DURAND Estelle"
          expect(page).not_to have_content "DUPOND"
        end
      end
    end
  end
end
