# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.feature "Profiles", type: :system do
  context "As a connected user" do
    before { @user = connect_as(:nurse) }

    scenario "I can see my profile" do
      visit root_path

      within "nav" do
        click_button "Paramètres"
        within "ul#menu" do
          click_link "Mon profil"
        end
      end

      within "div.container" do
        expect(page).to have_css("h1", text: "Nurse")
        expect(page).to have_css("p", text: "I. D. E.")
        within "li#email" do
          expect(page).to have_content(@user.email)
        end
        within "li#password" do
          expect(page).to have_css("form")
        end
      end
    end

    scenario "I can change my password" do
      visit profile_path

      within "div.container" do
        within "form" do
          fill_in :user_password, with: "poiuyt"
          fill_in :user_password_confirmation, with: "secret"
          click_button "Changer le mot de passe"
        end

        within "form" do
          expect(page).to have_css("div.user_password", text: "est trop court (au moins 8 caractères)")
          expect(page).to have_css("div.user_password", text: "doit contenir au moins un chiffre et une majuscule")
          expect(page).to have_css("div.user_password_confirmation", text: "ne concorde pas avec")
        end

        # weak password
        within "form" do
          fill_in :user_password, with: "poiuyt123$"
          fill_in :user_password_confirmation, with: "poiuyt123$"
          click_button "Changer le mot de passe"
        end

        within "form" do
          expect(page).to have_css("div.user_password", text: "doit contenir au moins un chiffre et une majuscule")
        end

        # good password
        within "form" do
          fill_in :user_password, with: "Poiuyt123$"
          fill_in :user_password_confirmation, with: "Poiuyt123$"
          click_button "Changer le mot de passe"
        end
      end

      expect(page).to have_content("Mise à jour du mot de passe réussie.")
    end
  end
end
