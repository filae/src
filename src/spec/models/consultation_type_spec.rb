# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe ConsultationType, type: :model do

  subject { build(:consultation_type) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_uniqueness_of(:title).scoped_to(:intervention_domain_id) }

  describe "scopes" do
    it "#by_title returns by title ascending (default)" do
      # Given
      type1 = create(:consultation_type, title: "Expertise")
      type2 = create(:consultation_type, title: "Consulation médicale")
      # When
      result = ConsultationType.by_title
      # Then
      expect(result).to eq [type2, type1]
    end

    it "#by_title(:desc) returns by title descending" do
      # Given
      type1 = create(:consultation_type, title: "Expertise")
      type2 = create(:consultation_type, title: "Consulation médicale")
      # When
      result = ConsultationType.by_title(:desc)
      # Then
      expect(result).to eq [type1, type2]
    end

    it ".for_user returns consultation types allowed for users's role" do
      # Given
      user = create(:user, :nurse)
      type1 = create(:consultation_type, roles: [user.role])
      type2 = create(:consultation_type, roles: [])
      # When
      result = ConsultationType.for_user(user)
      # Then
      expect(result).to eq [type1]
    end
  end

  describe "behaviour" do
    describe "#available_complementary_consultation_types" do
      let!(:domain) { create(:intervention_domain) }
      let!(:type1) { create(:consultation_type, intervention_domain: domain)}
      let!(:type2) { create(:consultation_type, intervention_domain: domain)}

      describe "regarding a new consultation_type" do
        it "returns all the consultation types of the associated intervention domain" do
          # Given
          type = build(:consultation_type, intervention_domain: domain)
          # When
          result = type.available_complementary_consultation_types
          # Then
          expect(result).to match_array [type1, type2]
        end
      end

      describe "regarding an existing consultation type" do
        it "returns all the consultation types of the associated intervention domaini except itself" do
          # Given
          type = create(:consultation_type, intervention_domain: domain)
          # When
          result = type.available_complementary_consultation_types
          # Then
          expect(result).to match_array [type1, type2]
        end
      end
    end

    describe "#assignable_users" do
      let(:duty_period) { create(:duty_period) }

      it "returns users with assigned roles ordered by last_name" do
        # Given
        assignable_user1 = create(:user, :physician, last_name: "Dupont")
        assignable_user2 = create(:user, :physician, last_name: "Dupond")
        create(:user, :psychologist)
        type = create(:consultation_type, assigned_roles: [Role.find_by_title(:physician)])
        # When
        result = type.assignable_users(duty_period)
        # Then
        expect(result).to eq [assignable_user2, assignable_user1]
      end

      it "returns only users active on duty period" do
        # Given
        active_user = create(:user, :physician)
        not_deactivated_yet_user = create(:user, :physician, disabled_at: duty_period.ends_at)
        create(:user, :physician, disabled_at: duty_period.ends_at - 1.minute)
        type = create(:consultation_type, assigned_roles: [Role.find_by_title(:physician)])
        # When
        result = type.assignable_users(duty_period)
        # Then
        expect(result).to contain_exactly(active_user, not_deactivated_yet_user)
      end
    end
  end
end
