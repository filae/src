# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Appointment, type: :model do

  subject { build(:appointment) }

  it { is_expected.to be_valid }
  it { is_expected.to belong_to(:consultation_type) }
  it { is_expected.to allow_values(ActiveSupport::Duration.build(45)).for(:duration) }
  [0, -3].each do |duration|
    it { is_expected.not_to allow_values(ActiveSupport::Duration.build(duration)).for(:duration) }
  end
  it { is_expected.not_to allow_values(nil).for(:duration) }
  it { is_expected.to validate_presence_of(:starts_at) }

  describe "associations" do

    it { is_expected.to have_one(:intervention_domain) }
    it { is_expected.to have_one(:medical_file) }
  end

  it ".of_consultation_type returns only appointments associeated to a given consultation type" do
    # Given
    ct1 = create(:consultation_type)
    ct2 = create(:consultation_type)
    appointment = create(:appointment, consultation_type: ct1)
    create(:appointment, consultation_type: ct2)
    # When
    result = Appointment.of_consultation_type(ct1)
    # Then
    expect(result).to match_array [appointment]
  end

  it ".scheduled_on returns the appointments of a specific day" do
    # Given
    appointment1 = create(:appointment, starts_at: "2018-05-21T00:00Z")
    appointment2 = create(:appointment, starts_at: "2018-05-21T23:59")
    appointment3 = create(:appointment, starts_at: "2018-05-22T00:00Z")
    target_day = Date.parse("2018-05-21")
    # When
    result = Appointment.scheduled_on(target_day)
    # Then
    expect(result).to match_array [appointment1, appointment2]
  end

  describe ".generate_onml_id" do

    it "creates the sequence on first call and assign an onml id" do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: "I")
      subject.starts_at = Time.current.change(year: 2018, month: 05)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq "I18051"
    end

    it "skips if the intenvention domain has no identifier prefix" do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: nil)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to be_nil
    end

    it "skips if onml_id is already set" do
      # Given
      subject.onml_id = ""
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq ""
    end

    it "uses the same sequence on same month" do
      # Given
      month = 5
      same_month = create(:appointment, intervention_domain: create(:intervention_domain, identifier_prefix: "I"), starts_at: Time.current.change(year: 2018, month: month))
      subject.intervention_domain = same_month.intervention_domain
      subject.starts_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(same_month.onml_id).to eq "I18051"
      expect(subject.onml_id).to eq "I18052"
    end

    it "uses another sequence on other month" do
      # Given
      other_month, month = [7, 8]
      other_month = create(:appointment, intervention_domain: create(:intervention_domain, identifier_prefix: "I"), starts_at: Time.current.change(year: 2018, month: other_month))
      subject.intervention_domain = other_month.intervention_domain
      subject.starts_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(other_month.onml_id).to eq "I18071"
      expect(subject.onml_id).to eq "I18081"
    end

    it "uses another sequence on other intervention domain identifier prefix" do
      # Given
      starts_at = Time.current.change(year: 2018, month: 5)
      other_intervention_domain = create(:intervention_domain, identifier_prefix: "O");
      intervention_domain = create(:intervention_domain, identifier_prefix: "I");
      same_identifier_prefix = create(:intervention_domain, identifier_prefix: "I");

      with_other_domain = create(:appointment, intervention_domain: other_intervention_domain, starts_at: starts_at)
      with_same_prefix = create(:appointment, intervention_domain: same_identifier_prefix, starts_at: starts_at)
      subject.intervention_domain = intervention_domain
      subject.starts_at = starts_at
      # When
      subject.save
      # Then
      expect(with_other_domain.onml_id).to eq "O18051"
      expect(with_same_prefix.onml_id).to eq "I18051"
      expect(subject.onml_id).to eq "I18052"
    end
  end

  describe "#postpone" do
    it "cancels appointment and sets origin" do
      # Given
      appointment = create(:appointment)
      # When
      appointment.postpone("patient")
      # Then
      expect(appointment.cancelled_at).not_to be_nil
      expect(appointment.cancelled_by).to eq "patient"
    end

    it "copies former attached files to new appointment" do
      # Given
      appointment = create(:appointment)
      2.times { create(:attached_file, attachable: appointment) }
      next_appointment = create(:appointment, previous_appointment: appointment)
      # When
      appointment.postpone("patient")
      # Then
      expect(next_appointment.attached_files.count).to eq appointment.attached_files.count
      previous_file = appointment.attached_files.first.file
      next_file = next_appointment.attached_files.first.file
      expect(next_file.attachment).not_to be_nil
      expect(next_file.attachment).not_to eq previous_file.attachment
      expect(next_file.blob).to eq previous_file.blob
      expect(next_file.filename).to eq previous_file.filename
    end
  end

  describe "#duplicate_for_consultation_type" do
    let(:appointment) {
      create(:appointment,
        calling_institution: build(:calling_institution),
        consultation_reason: build(:consultation_reason),
        consultation_subtype: build(:consultation_subtype),
        emergency: true,
        officer: build(:officer),
        originating_appointment: build(:appointment),
        patient: build(:patient),
        patient_arrived_at: Time.zone.now,
        requisition_received_at: Time.zone.now)
    }
    let(:complementary_consultation_type) { create(:consultation_type) }

    it "sets the previous appointment of the duplicated one" do
        # Given
        # When
        result = appointment.duplicate_for_consultation_type(complementary_consultation_type)
        # Then
        expect(result.consultation_type).to eq complementary_consultation_type
    end

    %i[
      calling_institution
      officer
      patient
      patient_arrived_at
      starts_at
    ].each do |attr|
      it "copies #{attr} to the new duplicated appointment" do
        # Given
        # When
        result = appointment.duplicate_for_consultation_type(complementary_consultation_type)
        # Then
        expect(result.send(attr)).to eq appointment.send(attr)
      end
    end

    %i[
      cancelled_at
      cancelled_by
      consultation_reason_id
      consultation_subtype_id
      created_at
      onml_id
      original_patient_id
      patient_missing
      patient_released_at
      updated_at
      user_in_charge_id
    ].each do |attr|
      it "nullifies #{attr} for the new duplicated appointment" do
        # Given
        # When
        result = appointment.duplicate_for_consultation_type(complementary_consultation_type)
        # Then
        expect(result.send(attr)).to be_nil
      end
    end
  end

  describe "#duplicate_for_postpone" do
    let(:appointment) {
      create(:appointment,
        calling_institution: build(:calling_institution),
        consultation_reason: build(:consultation_reason),
        consultation_subtype: build(:consultation_subtype),
        emergency: true,
        officer: build(:officer),
        originating_appointment: build(:appointment),
        patient: build(:patient),
        requisition_received_at: Time.zone.now)
    }

    it "sets the previous appointment of the duplicated one" do
        # Given
        # When
        result = appointment.duplicate_for_postpone
        # Then
        expect(result.previous_appointment).to eq appointment
    end

    %i[
      calling_institution
      circumstances
      comment
      consultation_type
      consultation_reason
      consultation_subtype
      duration
      emergency
      officer
      official_report
      originating_appointment
      patient
      patient_phone
      requisition_received_at
    ].each do |attr|
      it "copies #{attr} to the new duplicated appointment" do
        # Given
        # When
        result = appointment.duplicate_for_postpone
        # Then
        expect(result.send(attr)).to eq appointment.send(attr)
      end
    end

    %i[
      cancelled_at
      cancelled_by
      created_at
      onml_id
      original_patient_id
      patient_arrived_at
      patient_missing
      patient_released_at
      starts_at
      updated_at
      user_in_charge
    ].each do |attr|
      it "nullifies #{attr} for the new duplicated appointment" do
        # Given
        # When
        result = appointment.duplicate_for_postpone
        # Then
        expect(result.send(attr)).to be_nil
      end
    end
  end

  describe "#last_complementary_appointment_of_consultation_type" do
    let!(:appointment) { create(:appointment) }
    let!(:consultation_type) { create(:consultation_type) }

    it "returns nil if no complementary appointment" do
      # Given
      # When
      result = appointment.last_complementary_appointment_of_consultation_type(consultation_type)
      # Then
      expect(result).to be_nil
    end

    it "returns nil if no complementary appointment of the given consultation type" do
      # Given
      complementary_appointment = create(:appointment)
      appointment.complementary_appointments << complementary_appointment
      # When
      result = appointment.last_complementary_appointment_of_consultation_type(consultation_type)
      # Then
      expect(result).to be_nil
    end

    it "returns the last id if many complementary appointments of the given consultation type" do
      # Given
      first_complementary_appointment = create(:appointment, consultation_type: consultation_type)
      appointment.complementary_appointments << first_complementary_appointment
      second_complementary_appointment = create(:appointment, consultation_type: consultation_type)
      appointment.complementary_appointments << second_complementary_appointment
      third_complementary_appointment = create(:appointment, consultation_type: consultation_type)
      create(:appointment, complementary_appointments: [third_complementary_appointment])
      # When
      result = appointment.last_complementary_appointment_of_consultation_type(consultation_type)
      # Then
      expect(result).to eq second_complementary_appointment
    end
  end

  describe ".between" do

    let!(:a1) { create(:appointment, starts_at: "13/12/2018 08:00:00")}
    let!(:a2) { create(:appointment, starts_at: "14/12/2018 08:00:00")}
    let!(:a3) { create(:appointment, starts_at: "15/12/2018 08:00:00")}
    let!(:a4) { create(:appointment, starts_at: "18/12/2018 08:00:00")}
    let!(:a5) { create(:appointment, starts_at: "19/12/2018 08:00:00")}

    it "[2018-12-14, nil] returns appointments from 14/12/2018" do
      expect(Appointment.between("2018-12-14", nil)).to match_array [a2, a3, a4, a5]
    end

    it "[nil, 2018-12-18] returns appointments until 18/12/2018 included" do
      expect(Appointment.between(nil, "2018-12-18")).to match_array [a1, a2, a3, a4]
    end

    it "[nil, nil] returns all the appointments" do
      expect(Appointment.between(nil, nil)).to match_array [a1, a2, a3, a4, a5]
    end

    it "[2018-12-14, 2018-12-18] returns all appointments between 14/12/2018 and 18/12/2018 included" do
      expect(Appointment.between("2018-12-14", "2018-12-18")).to match_array [a2, a3, a4]
    end
  end

  it ".onml_id_like returns appointments with ONML id matching %String% SQL patterns" do
    # Given
    a1 = create(:appointment, onml_id: "C181142")
    a2 = create(:appointment, onml_id: "C18121")
    a3 = create(:appointment, onml_id: "C18122")
    # When
    result = Appointment.onml_id_like("c1812")
    # Then
    expect(result).to match_array [a2, a3]
  end

  it ".official_report_like returns appointments with official report matching %String% SQL patterns" do
    # Given
    a1 = create(:appointment, official_report: "PV1345")
    a2 = create(:appointment, official_report: "NPV123")
    a3 = create(:appointment, official_report: "PV1234")
    # When
    result = Appointment.official_report_like("pv123")
    # Then
    expect(result).to match_array [a2, a3]
  end

  it ".patient_last_name_like returns appointments associated with patient names beginning with provided string" do
    # Given
    a1 = create(:appointment, patient: build(:patient, last_name: "dupont"))
    a2 = create(:appointment, patient: build(:patient, last_name: "Durant"))
    a3 = create(:appointment, patient: build(:patient, last_name: "Moldu"))
    # When
    result = Appointment.patient_last_name_like("du")
    # Then
    expect(result).to match_array [a1, a2]
  end

  describe ".of_patient_gender" do
    it "returns appointments of a given patient's gender" do
      # Given
      a1 = create(:appointment, patient: build(:patient, gender: "woman"))
      a2 = create(:appointment, patient: build(:patient, gender: "man"))
      a3 = create(:appointment, patient: build(:patient, gender: nil))
      # When
      result = Appointment.of_patient_gender("woman")
      # Then
      expect(result).to match_array [a1]
    end

    it "returns appointments with no gender for unfilled" do
      # Given
      a1 = create(:appointment, patient: build(:patient, gender: "woman"))
      a2 = create(:appointment, patient: build(:patient, gender: "man"))
      a3 = create(:appointment, patient: build(:patient, gender: nil))
      # When
      result = Appointment.of_patient_gender("unfilled")
      # Then
      expect(result).to eq [a3]
    end

    it "returns nothing if nil provided as gender" do
      # Given
      a1 = create(:appointment, patient: build(:patient, gender: "woman"))
      a2 = create(:appointment, patient: build(:patient, gender: "man"))
      a3 = create(:appointment, patient: build(:patient, gender: nil))
      # When
      result = Appointment.of_patient_gender(nil)
      # Then
      expect(result).to be_nil
    end
  end

  it ".of_patient_age_range returns appointment of a given patient's age range" do
    # Given
    today = Date.today
    date_floor = today - 29.years
    date_ceiling = today - 20.years
    now = Time.zone.now
    a1 = create(:appointment, starts_at: now, patient: build(:patient, birth_year: nil, birthdate: date_floor-1.year))
    a2 = create(:appointment, starts_at: now, patient: build(:patient, birth_year: nil, birthdate: date_floor-1.year+1.day))
    a3 = create(:appointment, starts_at: now, patient: build(:patient, birth_year: nil, birthdate: date_ceiling))
    a4 = create(:appointment, starts_at: now, patient: build(:patient, birth_year: nil, birthdate: date_ceiling+1.day))
    a5 = create(:appointment, starts_at: now, patient: build(:patient, birth_year: nil, birthdate: nil))
    # When
    result = Appointment.of_patient_age_range([20, 29])
    # Then
    expect(result).to be_a ActiveRecord::Relation
    expect(result.to_a).to match_array [a2, a3]
  end

  it ".of_patient_birthdate returns appointments of a given patient's birthdate" do
    # Given
    a1 = create(:appointment, patient: build(:patient, birthdate: Date.new(2000,5,5)))
    a2 = create(:appointment, patient: build(:patient, birthdate: Date.new(2000,5,6)))
    # When
    result = Appointment.of_patient_birthdate("2000-05-05")
    # Then
    expect(result).to match_array [a1]
  end

  it ".of_user_in_charge returns appointments of a given assignee" do
    # Given
    user = create(:user)
    a1 = create(:appointment, user_in_charge: user)
    a2 = create(:appointment)
    # When
    result = Appointment.of_user_in_charge(user.id)
    # Then
    expect(result).to match_array [a1]
  end

  it ".of_calling_institution returns appointments of a given calling institution" do
    # Given
    calling_institution = create(:calling_institution)
    a1 = create(:appointment, calling_institution: calling_institution)
    a2 = create(:appointment)
    # When
    result = Appointment.of_calling_institution(calling_institution.id)
    # Then
    expect(result).to match_array [a1]
  end

  it ".of_consultation_reason returns appointments of a given consultation reason" do
    # Given
    consultation_reason = create(:consultation_reason)
    a1 = create(:appointment, consultation_reason: consultation_reason)
    a2 = create(:appointment)
    # When
    result = Appointment.of_consultation_reason(consultation_reason.id)
    # Then
    expect(result).to match_array [a1]
  end

  it ".updated_after returns appointments updated after a given datetime" do
    # Given
    a1 = create(:appointment)
    a2 = create(:appointment)
    now = Time.zone.now
    a1.update_column(:updated_at, now + 10.minutes)
    # When
    result = Appointment.updated_after(now + 5.minutes)
    # Then
    expect(result).to match_array [a1]
  end

  describe ".with_status" do
    it '"cancelled" returns only cancelled appointments' do
      # Given
      a1 = create(:appointment, cancelled_at: Time.zone.now)
      a2 = create(:appointment, previous_appointment: a1)
      a3 = create(:appointment, cancelled_at: Time.zone.now)
      # When
      result = Appointment.with_status("cancelled")
      # Then
      expect(result).to match_array [a3]
    end

    it '"postponed" returns only appointments which where postponed (the old, not the new one)' do
      # Given
      a1 = create(:appointment, cancelled_at: Time.zone.now)
      a2 = create(:appointment, previous_appointment: a1)
      a3 = create(:appointment, cancelled_at: Time.zone.now)
      # When
      result = Appointment.with_status("postponed")
      # Then
      expect(result).to eq [a1]
    end

    it '"patient_missing" returns only appointments not honored by the patients' do
      # Given
      a1 = create(:appointment, patient_missing: true)
      a2 = create(:appointment)
      # When
      result = Appointment.with_status("patient_missing")
      # Then
      expect(result).to eq [a1]
    end
  end

  describe "regarding attached files of the certificate" do
    let!(:native_certificate_appointment) { create(:appointment, :with_certificate) }
    let!(:signed_certificate_appointment) { create(:appointment, :with_signed_certificate) }
    let!(:validated_certificate_appointment) { create(:appointment, :with_validated_certificate) }

    describe ".with_certificate_attached_files_of_type" do
      it "':signed' returns appointment associated to a certificate with signed attached files" do
        # Given
        # When
        result = Appointment.with_certificate_attached_files_of_type(:signed)
        # Then
        expect(result).to match_array [signed_certificate_appointment, validated_certificate_appointment]
      end

      it "':validated' returns appointment associated to a certificate with validated attached files" do
        # Given
        # When
        result = Appointment.with_certificate_attached_files_of_type(:validated)
        # Then
        expect(result).to eq [validated_certificate_appointment]
      end
    end
  end

  describe ".with_certificate_status" do
    let!(:native_certificate_appointment) { create(:appointment, :with_certificate) }
    let!(:signed_certificate_appointment) { create(:appointment, :with_signed_certificate) }
    let!(:validated_certificate_appointment) { create(:appointment, :with_validated_certificate) }
    let!(:sent_certificate_appointment) { create(:appointment, :with_sent_certificate) }

    it "'signed' returns appointments with certificate with signed attached file but no validated one" do
      # Given
      # When
      result = Appointment.with_certificate_status("signed")
      # Then
      expect(result).to eq [signed_certificate_appointment]
    end

    it "'validated' returns appointments with certificate with validated attached file but not sent" do
      # Given
      # When
      result = Appointment.with_certificate_status("validated")
      # Then
      expect(result).to eq [validated_certificate_appointment]
    end

    it "'sent' returns appointments with certificate with sent validated attached file" do
      # Given
      # When
      result = Appointment.with_certificate_status("sent")
      # Then
      expect(result).to eq [sent_certificate_appointment]
    end
  end

  context "#validated_certificate" do
    context "without any ValidatedCertificate attached to its certificate" do
      it "returns nil" do
        # Given
        appointment = create(:appointment, :with_certificate)
        appointment.certificate.attached_files << AttachedFile::Standard.new
        # When
        result = appointment.validated_certificate
        # Then
        expect(result).to be_nil
      end
    end

    context "with at least one attached file of type :validated_certificate" do
      it "returns the last one" do
        # Given
        appointment = create(:appointment, :with_certificate)
        appointment.certificate.attached_files << AttachedFile::ValidatedCertificate.new
        appointment.certificate.attached_files << AttachedFile::Standard.new
        appointment.certificate.attached_files << (last_one = AttachedFile::ValidatedCertificate.new)
        # When
        result = appointment.validated_certificate
        # Then
        expect(result).to eq last_one
      end
    end
  end

  describe "ends_at" do
    subject { build(:appointment, starts_at: "2023-05-19T09:00:00Z", duration: "PT30M").ends_at }

    it { is_expected.to eq(Time.zone.parse("2023-05-19T09:30:00Z")) }
  end

  describe "#actual_medical_file" do
    describe "with a medical_file" do
      describe "as an originating appointment" do

        it "returns the associated medical file" do
          # Given
          medical_file = create(:medical_file)
          appointment = create(:appointment, medical_file: medical_file)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end

      describe "as a secondary appointment" do

        it "returns the medical file of the originating appointment" do
          # Given
          medical_file = create(:medical_file)
          originating_appointment = create(:appointment, medical_file: medical_file)
          appointment = create(:appointment, originating_appointment: originating_appointment)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end

      describe "as a third appointment" do

        it "returns the medical file of the first originating appointment" do
          # Given
          medical_file = create(:medical_file)
          originating_appointment = create(:appointment, medical_file: medical_file)
          appointment = create(:appointment, originating_appointment: create(:appointment, originating_appointment: originating_appointment))
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end
    end

    describe "without any medical_file" do
      describe "as an originating appointment" do

        it "returns a just created medical file" do
          # Given
          appointment = create(:appointment)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq appointment.reload.medical_file
        end
      end

      describe "as a secondary appointment" do

        it "returns a just created medical file of the originating appointment" do
          # Given
          originating_appointment = create(:appointment)
          appointment = create(:appointment, originating_appointment: originating_appointment)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq originating_appointment.reload.medical_file
        end
      end

      describe "as a third appointment" do

        it "returns a just created medical file of the first originating appointment" do
          # Given
          originating_appointment = create(:appointment)
          appointment = create(:appointment, originating_appointment: create(:appointment, originating_appointment: originating_appointment))
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq originating_appointment.reload.medical_file
        end
      end
    end
  end

  describe "#concerns_minor?" do
    let(:appointment) { create(:appointment) }

    it "returns true if patient is minor regarding the beginning of the appointment" do
      # Given
      patient = create(:patient)
      appointment.update(patient: patient)
      allow(patient).to receive(:is_minor_on?).and_return(true)
      # When
      result = appointment.concerns_minor?
      # Then
      expect(result).to be_truthy
    end

    it "returns false if patient is adult regarding the beginning of the appointment" do
      # Given
      patient = create(:patient)
      appointment.update(patient: patient)
      allow(patient).to receive(:is_minor_on?).and_return(false)
      # When
      result = appointment.concerns_minor?
      # Then
      expect(result).to be_falsy
    end
  end
end
