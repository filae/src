# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe CertificateTemplate, type: :model do
  describe "validations" do
    subject { build(:certificate_template) }

    it { is_expected.to be_valid }
    it { is_expected.to validate_presence_of(:title) }

    it { is_expected.to have_many(:roles)}
  end

  describe "scopes" do
    it ".by_title returns certificate templates ordered by ascending title" do
      # Given
      ct1 = create(:certificate_template, title: "B")
      ct2 = create(:certificate_template, title: "A")
      # When
      result = CertificateTemplate.by_title
      # Then
      expect(result).to eq [ct2, ct1]
    end

    it ".enabled returns only enbaled certificate templates" do
      # Given
      ct1 = create(:certificate_template)
      ct2 = create(:certificate_template, disabled_at: Time.zone.now)
      # When
      result = CertificateTemplate.enabled
      # Then
      expect(result).to match_array [ct1]
    end

    it ".for_user returns only certificates allowed to an user with specific role" do
      # Given
      user = create(:user)
      ct1 = create(:certificate_template, roles: [])
      ct2 = create(:certificate_template, roles: [user.role])
      # When
      result = CertificateTemplate.for_user(user)
      # Then
      expect(result).to match_array [ct2]
    end

    it ".with_master_document returns only certificate templates with an attached master document" do
      # Given
      ct1 = create(:certificate_template)
      ct2 = create(:certificate_template, :with_master_document)
      # When
      result = CertificateTemplate.with_master_document
      # Then
      expect(result).to match_array [ct2]
    end
  end
end
