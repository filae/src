# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Role, type: :model do
  subject { build(:role) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_uniqueness_of(:title).case_insensitive }

  describe "associations" do
    it { is_expected.to have_many(:intervention_domains) }
  end

  describe "behaviour" do
    describe "#role_title" do
      it "returns the translated role in singular by default" do
        # Given
        role = Role.find_by_title("administrator")
        # When
        result = role.localized_title
        # Then
        expect(result).to eq "administrateur"
      end

      it "returns the translated role in plural with count greater than 2" do
        # Given
        role = Role.find_by_title("administrator")
        # When
        result = role.localized_title(2)
        # Then
        expect(result).to eq "administrateurs"
      end
    end
  end
end
