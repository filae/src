# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Log, type: :model do
  subject { build(:log) }
  let!(:now) { Time.zone.now.iso8601 }
  let!(:user) { create(:user) }


  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:what_id) }
  it { is_expected.to validate_presence_of(:occured_at) }
  it { is_expected.to validate_presence_of(:who_id) }
  it { is_expected.to validate_presence_of(:event) }

  describe ".generate generates proper log for" do
    it "flowing consultations" do
      # Given
      intervention_domain = create(:intervention_domain, :stream)
      flowing_consultation = create(:flowing_consultation, intervention_domain: intervention_domain)
      created_action = Log::Action.new("created")
      assigned_action = Log::Action.new("assigned")
      actions2log = [created_action, assigned_action].map(&:to_json)
      expected_event = '{"actions":[{"action":"created"},{"action":"assigned"}]}'
      # When
      log = Log.generate(flowing_consultation, user, now, actions2log)
      # Then
      expect(log.what).to eq flowing_consultation.onml_id
      expect(log.what_id).to eq "gid://src/FlowingConsultation/#{flowing_consultation.id}"
      expect(log.occured_at).to eq now
      expect(log.who).to eq user.name
      expect(log.who_id).to eq "gid://src/User/#{user.id}"
      expect(log.event).to eq expected_event
    end

    it "appointments" do
      # Given
      consultation_type = create(:consultation_type)
      appointment = create(:appointment, consultation_type: consultation_type)
      created_action = Log::Action.new("created")
      postponed_action = Log::Action.new({ action: "postponed", source: "umj"})
      actions2log = [created_action, postponed_action].map(&:to_json)
      expected_event = '{"actions":[{"action":"created"},{"action":"postponed","source":"umj"}]}'
      # When
      log = Log.generate(appointment, user, now, actions2log)
      # Then
      expect(log.what).to eq appointment.onml_id
      expect(log.what_id).to eq "gid://src/Appointment/#{appointment.id}"
      expect(log.occured_at).to eq now
      expect(log.who).to eq user.name
      expect(log.who_id).to eq "gid://src/User/#{user.id}"
      expect(log.event).to eq expected_event
    end
  end

  it ".for_item returns logs of a specific item" do
      # Given
      intervention_domain = create(:intervention_domain, :stream)
      flowing_consultation = create(:flowing_consultation, intervention_domain: intervention_domain)
      consultation_type = create(:consultation_type)
      appointment1 = create(:appointment, consultation_type: consultation_type)
      appointment2 = create(:appointment, consultation_type: consultation_type)
      actions = [Log::Action.new("created").to_json]
      log1 = Log.generate(flowing_consultation, user, now, actions)
      log2 = Log.generate(appointment1, user, now, actions)
      log3 = Log.generate(appointment2, user, now, actions)
      # When
      result = Log.for_item(appointment1)
      # Then
      expect(result).to eq [log2]
  end

  it "#actions_label returns joined and localized action labels" do
    # Given
    appointment = create(:appointment)
    created_action = Log::Action.new("created")
    postponed_action = Log::Action.new({ action: "postponed", source: "umj"})
    actions2log = [created_action, postponed_action].map(&:to_json)
    log = Log.generate(appointment, user, now, actions2log)
    # When
    result = log.actions_label
    # Then
    expect(result).to eq "créé, reporté"
  end
end
