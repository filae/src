# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe User, type: :model do
  subject { build(:user) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:identifier) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_uniqueness_of(:identifier).case_insensitive }
  it { is_expected.to have_secure_password }
  it { is_expected.to validate_confirmation_of(:password) }
  it { is_expected.to validate_length_of(:password).is_at_least(8) }
  it { is_expected.to allow_values("pOiuyt123$").for(:password) }
  it { is_expected.not_to allow_values("poiuyt123$", "poiuytpoiuyt").for(:password) }

  describe "behaviour" do
    describe "activation" do
      before {
        @active_user = create(:user, disabled_at: nil)
        @inactive_user = create(:user, :inactive)
        @scheduled_user = create(:user, :scheduled_for_deactivation)
      }

      it "#inactive? returns true when user has disabled_at value in the past" do
        # Given
        # When
        active_result = @active_user.inactive?
        inactive_result = @inactive_user.inactive?
        scheduled_result = @scheduled_user.inactive?
        # Then
        expect(active_result).to be_falsy
        expect(inactive_result).to be_truthy
        expect(scheduled_result).to be_falsy
      end

      it "#scheduled_for_deactivation? returns true when user has disabled_at value in the future" do
        # Given
        # When
        active_result = @active_user.scheduled_for_deactivation?
        inactive_result = @inactive_user.scheduled_for_deactivation?
        scheduled_result = @scheduled_user.scheduled_for_deactivation?
        # Then
        expect(active_result).to be_falsy
        expect(inactive_result).to be_falsy
        expect(scheduled_result).to be_truthy
      end

      it "#active? returns true when user has disabled_at value in the future or has no disabled_at value" do
        # Given
        # When
        active_result = @active_user.active?
        inactive_result = @inactive_user.active?
        scheduled_result = @scheduled_user.active?
        # Then
        expect(active_result).to be_truthy
        expect(inactive_result).to be_falsy
        expect(scheduled_result).to be_truthy
      end
    end

    describe "#activation_status" do
      it 'returns "active" when the user is active' do
        # Given
        user = create(:user, disabled_at: nil)
        # When
        result = user.activation_status
        # Then
        expect(result).to eq "active"
      end

      it 'returns "inactive" when the user is inactive' do
        # Given
        user = create(:user, :inactive)
        # When
        result = user.activation_status
        # Then
        expect(result).to eq "inactive"
      end

      it 'returns "deactivation-scheduled" when the user is scheduled for deactivation' do
        # Given
        user = create(:user, :scheduled_for_deactivation)
        # When
        result = user.activation_status
        # Then
        expect(result).to eq "deactivation-scheduled"
      end
    end

    describe "#has_secure_password" do

      let(:password) { subject.password }

      it "authenticates on valid credentials" do
        # When
        result = subject.authenticate(password)
        # Then
        expect(result).to eq subject
      end

      it "does not authenticate on buggy credentials" do
        # When
        result = subject.authenticate("buggy" + password)
        # Then
        expect(result).to be false
      end
    end

    it "#name concatenates last and first names" do
      # Given
      user = build(:user, last_name: "Potter", first_name: "Harry")
      # When
      result = user.name
      # Then
      expect(result).to eq "Potter Harry"
    end

    it "#generate_secure_password automatically generates a secured password" do
      # Given
      user = build(:user, password: User.generate_secure_password)
      # When
      result = user.valid?
      # Then
      expect(result).to be_truthy
    end
  end

  describe "scopes" do
    it ".active returns only active users at the moment" do
      # Given
      u1 = create(:user, disabled_at: nil)
      create(:user, disabled_at: Time.zone.now - 1.minute)
      u3 = create(:user, disabled_at: Time.zone.now + 1.minute)
      # When
      result = User.active
      # Then
      expect(result).to contain_exactly(u1, u3)
    end

    it ".active returns only active users at the specified moment" do
      # Given
      u1 = create(:user, disabled_at: nil)
      create(:user, disabled_at: Time.zone.now + 5.minutes)
      u3 = create(:user, disabled_at: Time.zone.now + 15.minutes)
      # When
      result = User.active(Time.zone.now + 10.minutes)
      # Then
      expect(result).to contain_exactly(u1, u3)
    end

    it "#by_name returns users ordered by lastnames then firstnames" do
      # Given
      u1 = create(:user, last_name: "Dupont", first_name: "Hector")
      u2 = create(:user, last_name: "Dupond", first_name: "Hector")
      u3 = create(:user, last_name: "Dupont", first_name: "Arthur")
      # When
      result = User.by_name
      # Then
      expect(result).to eq [u2, u3, u1]
    end

    it "#by_deactivation returns users ordered by desc disabled_at datetime with NULLS first" do
      # Given
      u1 = create(:user, disabled_at: Time.zone.now + 1.hour)
      u2 = create(:user, disabled_at: Time.zone.now - 1.hour)
      u3 = create(:user, disabled_at: nil)
      # When
      result = User.by_deactivation
      # Then
      expect(result).to eq [u3, u1, u2]
    end
  end
end
