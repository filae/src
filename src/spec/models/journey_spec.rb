# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Journey do
  let!(:main_consultation_type) { create(:consultation_type, title: "Consultation", icon_name: "stethoscope") }
  let!(:main_appointment) { create(:appointment, consultation_type: main_consultation_type) }

  let(:journey) { described_class.new(main_appointment) }

  describe "#valid?" do
    it "returns false if patient did not arrived yet" do
      # Given
      # When
      result = journey
      # Then
      expect(result).not_to be_valid
    end

    it "returns false if appointment was cancelled" do
      # Given
      main_appointment.update(patient_arrived_at: 10.minutes.ago, cancelled_at: Time.zone.now)
      # When
      result = journey
      # Then
      expect(result).not_to be_valid
    end
  end

  describe "patient arrived" do
    before { main_appointment.update(patient_arrived_at: Time.zone.now) }

    describe "for a stand-alone appointment" do
      it "is valid and provides the title of the consultation type, its upper cased first letter, its icon name and the appointment's state" do
        # Given
        # When
        result = journey
        # Then
        expect(result).to be_valid
        expect(result.title).to eq "Consultation"
        expect(result.abbr_title).to eq "C"
        expect(result.icon_name).to eq "stethoscope"
        expect(result.state).to eq :patient_waiting
      end
    end

    describe "with nested complementary appointments" do
      before do
        other_consultation_type = create(:consultation_type, title: "Psychologue")
        first_complementary_appointment = create(:appointment, consultation_type: other_consultation_type)
        yet_another_consultation_type = create(:consultation_type, title: "Juriste")
        second_complementary_appointment = create(:appointment, consultation_type: yet_another_consultation_type)
        cancelled_complementary_appointment = create(:appointment, consultation_type: create(:consultation_type), cancelled_at: Time.zone.now)
        main_appointment.complementary_appointments = [first_complementary_appointment,
                                                       second_complementary_appointment,
                                                       cancelled_complementary_appointment]
        some_other_consulation_type = create(:consultation_type, title: "Expert")
        @third_complementary_appointment = create(:appointment, consultation_type: some_other_consulation_type)
        second_complementary_appointment.complementary_appointments = [@third_complementary_appointment]
      end

      it "has nested journey children" do
        # Given
        # When
        result = journey
        # Then
        expect(result.children.length).to eq 2
        expect(result.children.find { |j| j.title == "Psychologue" }).not_to be_nil
        expect(jurist = result.children.find { |j| j.title == "Juriste" }).not_to be_nil
        expect(jurist.children.first.title).to eq "Expert"
      end

      describe ".full" do
        describe "when accessed from complementary appointment" do
          it "returns the root journey" do
            # Given
            # When
            result = Journey.full(@third_complementary_appointment)
            # Then
            expect(result).to be_a Journey
            expect(result.title).to eq "Consultation"
          end
        end
      end
    end
  end
end
