# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe FlowingConsultation, type: :model do
  subject { build(:flowing_consultation) }

  describe "validations" do
    it { is_expected.to be_valid }

    it { is_expected.to validate_presence_of(:called_at) }

    describe "simple messages" do
      subject { build(:flowing_consultation, :just_a_simple_message) }

      it { is_expected.not_to validate_presence_of(:consultation_type) }
    end

    describe "regular consultations" do
      subject { build(:flowing_consultation, :with_officer) }

      it { is_expected.to validate_presence_of(:consultation_type) }

      context "without an assignee" do
        it { is_expected.not_to validate_presence_of(:assigned_at) }
      end

      context "with an assignee" do
        before { subject.assignee_id = "42" }

        it { is_expected.to validate_presence_of(:assigned_at) }
      end
    end
  end

  let!(:now) { Time.zone.now }

  it ".of_intervention_domain returns consultations of types within specific domain" do
    # Given
    concerned_domain = create(:intervention_domain)
    another_domain = create(:intervention_domain)
    concerned_flowing_consultation1 = create(:flowing_consultation, intervention_domain: concerned_domain)
    concerned_flowing_consultation2 = create(:flowing_consultation, intervention_domain: concerned_domain)
    another_flowing_consultation = create(:flowing_consultation, intervention_domain: another_domain)
    # When
    result = FlowingConsultation.of_intervention_domain(concerned_domain).to_a
    # Then
    expect(result).to eq [concerned_flowing_consultation1, concerned_flowing_consultation2]
  end

  describe "state scopes" do
    before do
      @standard_consultation = create(:flowing_consultation, called_at: now - 24.hours)
      @assigned_consultation = create(:flowing_consultation, called_at: now - 24.hours, assigned_at: now - 23.hours)
      @active_cancelled_consultation = create(:flowing_consultation, called_at: now - 24.hours, cancelled_at: now - 11.hours)
      @archived_cancelled_consultation = create(:flowing_consultation, called_at: now - 24.hours, cancelled_at: now - 13.hours)
      @active_validated_consultation = create(:flowing_consultation, called_at: now - 24.hours, validated_at: now - 11.hours)
      @archived_validated_consultation = create(:flowing_consultation, called_at: now - 24.hours, validated_at: now - 13.hours)
      @archived_consultations = [@archived_cancelled_consultation, @archived_validated_consultation]
      @active_consultations = [@standard_consultation, @assigned_consultation, @active_cancelled_consultation, @active_validated_consultation]
    end


    it ".archived returns archives consultations regarding a given delay" do
      # Given
      # When
      result = FlowingConsultation.archived
      # Then
      expect(result).to match_array @archived_consultations
    end

    it ".active returns active consultations regarding a given delay" do
      # Given
      # When
      result = FlowingConsultation.active
      # Then
      expect(result).to match_array @active_consultations
    end

    it ".validated returns validated consultations" do
      # Given
      # When
      result = FlowingConsultation.validated
      # Then
      expect(result).to match_array [@active_validated_consultation, @archived_validated_consultation]
    end

    it ".cancelled returns cancelled consultations" do
      # Given
      # When
      result = FlowingConsultation.cancelled
      # Then
      expect(result).to match_array [@active_cancelled_consultation, @archived_cancelled_consultation]
    end

    it ".assigned returns assigned consultations" do
      # Given
      # When
      result = FlowingConsultation.assigned
      # Then
      expect(result).to match_array [@assigned_consultation]
    end

    it ".to_transmit returns not treated consultations" do
      # Given
      # When
      result = FlowingConsultation.to_transmit
      # Then
      expect(result).to match_array [@standard_consultation]
    end
  end

  describe ".sort_with_instructions" do

    let!(:consultation1) { create(:flowing_consultation, called_at: now - 2.hours) }
    let!(:consultation2) { create(:flowing_consultation, called_at: now - 1.hour, cancelled_at: now - 20.minutes) }
    let!(:consultation3) { create(:flowing_consultation, called_at: now - 1.hour, cancelled_at: now - 10.minutes) }

    it "'called_at' returns consultations by ascending called_at attribute" do
      # Given
      # When
      result = FlowingConsultation.sort_with_instructions("called_at")
      # Then
      expect(result.first).to eq consultation1
      expect(result.last(2)).to match_array [consultation2, consultation3]
    end

    it "'-called_at' returns consultations by descending called_at attribute" do
      # Given
      # When
      result = FlowingConsultation.sort_with_instructions("-called_at")
      # Then
      expect(result.first(2)).to match_array [consultation2, consultation3]
      expect(result.last).to eq consultation1
    end

    it "defaults to '-called_at' sorting" do
      # Given
      # When
      desc_called_at_result = FlowingConsultation.sort_with_instructions("-called_at")
      result_for_nil = FlowingConsultation.sort_with_instructions(nil)
      result_for_empty_string = FlowingConsultation.sort_with_instructions("")
      # Then
      expect(result_for_nil).to eq desc_called_at_result
      expect(result_for_empty_string).to eq desc_called_at_result
    end

    it "'called_at,-cancelled_at' returns consultations by ascending called_at attribute and descending cancelled_at attribute" do
      # Given
      # When
      result = FlowingConsultation.sort_with_instructions("called_at,-cancelled_at")
      # Then
      expect(result).to eq [consultation1, consultation3, consultation2]
    end

    it "'nil' does not change the scope" do
      # Given
      by_descending_id = FlowingConsultation.order(id: :desc)
      # When
      result = by_descending_id.sort_with_instructions(nil)
      # Then
      expect(result).to eq [consultation3, consultation2, consultation1]
    end
  end

  describe ".generate_onml_id" do

    it "creates the sequence on first call and assign an onml id" do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: "I")
      subject.called_at = Time.current.change(year: 2018, month: 05)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq "I18051"
    end

    it "skips if the intenvention domain has no identifier prefix" do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: nil)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to be_nil
    end

    it "skips if onml_id is already set" do
      # Given
      subject.onml_id = ""
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq ""
    end

    it "uses the same sequence on same month" do
      # Given
      month = 5
      same_month = create(:flowing_consultation, intervention_domain: create(:intervention_domain, identifier_prefix: "I"), called_at: Time.current.change(year: 2018, month: month))
      subject.intervention_domain = same_month.intervention_domain
      subject.called_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(same_month.onml_id).to eq "I18051"
      expect(subject.onml_id).to eq "I18052"
    end

    it "uses another sequence on other month" do
      # Given
      other_month, month = [7, 8]
      other_month = create(:flowing_consultation, intervention_domain: create(:intervention_domain, identifier_prefix: "I"), called_at: Time.current.change(year: 2018, month: other_month))
      subject.intervention_domain = other_month.intervention_domain
      subject.called_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(other_month.onml_id).to eq "I18071"
      expect(subject.onml_id).to eq "I18081"
    end

    it "uses another sequence on other intervention domain identifier prefix" do
      # Given
      called_at = Time.current.change(year: 2018, month: 5)
      other_intervention_domain = create(:intervention_domain, identifier_prefix: "O");
      intervention_domain = create(:intervention_domain, identifier_prefix: "I");
      same_identifier_prefix = create(:intervention_domain, identifier_prefix: "I");

      with_other_domain = create(:flowing_consultation, intervention_domain: other_intervention_domain, called_at: called_at)
      with_same_prefix = create(:flowing_consultation, intervention_domain: same_identifier_prefix, called_at: called_at)
      subject.intervention_domain = intervention_domain
      subject.called_at = called_at
      # When
      subject.save
      # Then
      expect(with_other_domain.onml_id).to eq "O18051"
      expect(with_same_prefix.onml_id).to eq "I18051"
      expect(subject.onml_id).to eq "I18052"
    end
  end

  describe ".of_consultation_type" do
    it "returns flowing consultations of a given consultation type" do
      # Given
      consultation_type = create(:consultation_type)
      fc1 = create(:flowing_consultation, consultation_type: consultation_type)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.of_consultation_type(consultation_type)
      # Then
      expect(result).to eq [fc1]
    end
  end

  describe ".of_calling_institution" do
    it "returns flowing consultations of a given calling institution" do
      # Given
      calling_institution = create(:calling_institution)
      fc1 = create(:flowing_consultation, calling_institution: calling_institution)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.of_calling_institution(calling_institution)
      # Then
      expect(result).to eq [fc1]
    end
  end

  describe ".of_patient_gender" do
    it "returns flowing consultations of a given patient's gender" do
      # Given
      fc1 = create(:flowing_consultation, patient: build(:patient, gender: "woman"))
      fc2 = create(:flowing_consultation, patient: build(:patient, gender: "man"))
      fc3 = create(:flowing_consultation, patient: build(:patient, gender: nil))
      # When
      result = FlowingConsultation.of_patient_gender("woman")
      # Then
      expect(result).to eq [fc1]
    end

    it "returns flowing consultations with no gender for nil" do
      # Given
      fc1 = create(:flowing_consultation, patient: build(:patient, gender: "woman"))
      fc2 = create(:flowing_consultation, patient: build(:patient, gender: "man"))
      fc3 = create(:flowing_consultation, patient: build(:patient, gender: nil))
      # When
      result = FlowingConsultation.of_patient_gender(nil)
      # Then
      expect(result).to eq [fc3]
    end

    it "returns all flowing consultations with empty gender" do
      # Given
      fc1 = create(:flowing_consultation, patient: build(:patient, gender: "woman"))
      fc2 = create(:flowing_consultation, patient: build(:patient, gender: "man"))
      fc3 = create(:flowing_consultation, patient: build(:patient, gender: nil))
      # When
      result = FlowingConsultation.of_patient_gender("")
      # Then
      expect(result.to_a).to match_array [fc1, fc2, fc3]
    end
  end

  describe ".patient_last_name_like" do
    it "returns flowing consultations of a given patient's lastname beginning with provided string" do
      # Given
      fc1 = create(:flowing_consultation, patient: build(:patient, last_name: "durand "))
      fc2 = create(:flowing_consultation, patient: build(:patient, last_name: "Dupont"))
      fc3 = create(:flowing_consultation, patient: build(:patient, last_name: "Moldu"))
      # When
      result = FlowingConsultation.patient_last_name_like("du")
      # Then
      expect(result).to eq [fc1, fc2]
    end
  end

  describe ".of_assignee" do
    it "returns flowing consultations of a given assignee" do
      # Given
      user = create(:user)
      fc1 = create(:flowing_consultation, assignee: user, assigned_at: Time.zone.now)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.of_assignee(user)
      # Then
      expect(result).to eq [fc1]
    end
  end

  describe ".of_patient_age_range" do
    it "returns flowing consultations of a given patient's age range" do
      # Given
      called_at = "2023-08-21T15:02"
      fc1 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: nil, birth_year: "1993"))
      fc2 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: nil, birth_year: "1994"))
      fc3 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: nil, birth_year: "2003"))
      fc4 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: nil, birth_year: "2004"))
      fc5 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: "1993-08-21"))
      fc6 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: "1993-08-22"))
      fc7 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: "2003-08-21"))
      fc8 = create(:flowing_consultation, called_at: called_at, patient: build(:patient, birthdate: "2003-08-22"))
      # When
      result = FlowingConsultation.of_patient_age_range(20, 29)
      # Then
      expect(result).to be_a ActiveRecord::Relation
      expect(result.to_a).to match_array([fc2, fc3, fc6, fc7])
    end
  end

  describe ".with_status" do
    it "'cancelled' returns cancelled flowing consultations" do
      # Given
      fc1 = create(:flowing_consultation, cancelled_at: Time.zone.now)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.with_status("cancelled")
      # Then
      expect(result).to eq [fc1]
    end

    it "'validated' returns validated flowing consultations" do
      # Given
      fc1 = create(:flowing_consultation, validated_at: Time.zone.now)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.with_status("validated")
      # Then
      expect(result).to eq [fc1]
    end

    it "unknown status returns all flowing consultations" do
      # Given
      fc1 = create(:flowing_consultation, validated_at: Time.zone.now)
      fc2 = create(:flowing_consultation)
      # When
      result = FlowingConsultation.with_status("foo")
      # Then
      expect(result).to match_array [fc1, fc2]
    end
  end

  describe "between" do

    %w[called_at validated_at cancelled_at assigned_at].each do |column|

      unless column == "called_at"
        it "[#{column}, nil, nil] returns nothing" do
          # Given
          fc1 = create(:flowing_consultation)
          fc1.update_attribute(column, nil)
          # When
          result = FlowingConsultation.between(column, nil, nil)
          # Then
          expect(result).to be_empty
        end
      end

      it "[#{column}, 2018-01-01T00:00Z, nil] returns all where #{column} is after 01/01/2018 at 00:00" do
        # Given
        fc1 = create(:flowing_consultation, column => "2017-12-31T00:00Z")
        fc2 = create(:flowing_consultation, column => "2018-02-01T00:00Z")
        # When
        result = FlowingConsultation.between(column, "2018-01-01T00:00Z", nil)
        # Then
        expect(result).to eq [fc2]
      end

      it "[#{column}, nil, 2018-09-01T00:00Z] returns all where #{column} is before 01/09/2018 at 00:00" do
        # Given
        fc1 = create(:flowing_consultation, column => "2018-02-01T00:00Z")
        fc2 = create(:flowing_consultation, column => "2018-09-10T00:00Z")
        # When
        result = FlowingConsultation.between(column, nil, "2018-09-01T00:00Z")
        # Then
        expect(result).to eq [fc1]
      end

      it "[#{column}, 2018-01-01T00:00Z, 2018-09-01T00:00Z] returns all where #{column} is between 01/01/2018 at 00:00 and 01/09/2018 at 00:00" do
        # Given
        fc1 = create(:flowing_consultation, column => "2017-12-31T00:00Z")
        fc2 = create(:flowing_consultation, column => "2018-02-01T00:00Z")
        fc3 = create(:flowing_consultation, column => "2018-09-10T00:00Z")
        # When
        result = FlowingConsultation.between(column, "2018-01-01T00:00Z", "2018-09-01T00:00Z")
        # Then
        expect(result).to eq [fc2]
      end
    end

    it "returns nothing if wrong column" do
      # Given
      fc = create(:flowing_consultation)
      # When
      result = FlowingConsultation.between(:destroyed_at, "2018-01-01T00:00Z", "2018-09-01T00:00Z")
      # Then
      expect(result).to be_empty
    end

    it "returns everything if empty column provided" do
      # Given
      fc = create(:flowing_consultation)
      # When
      result = FlowingConsultation.between("", "2018-01-01T00:00Z", "2018-09-01T00:00Z")
      # Then
      expect(result).to eq [fc]
    end
  end

  describe ".by_calling_areas_and_institutions" do
    it "returns flowing consultations by ascending calling area title then calling institution title" do
      # Given
      south = create(:calling_area, title: "Sud")
      north = create(:calling_area, title: "Nord")
      paris = create(:calling_institution, title: "Paris", calling_area: north)
      lille = create(:calling_institution, title: "Lille", calling_area: north)
      toulouse = create(:calling_institution, title: "Toulouse", calling_area: south)
      marseille = create(:calling_institution, title: "Marseille", calling_area: south)
      fc_nulle_part = create(:flowing_consultation)
      fc_toulouse = create(:flowing_consultation, calling_institution: toulouse)
      fc_marseille = create(:flowing_consultation, calling_institution: marseille)
      fc_paris = create(:flowing_consultation, calling_institution: paris)
      fc_lille = create(:flowing_consultation, calling_institution: lille)
      # When
      result = FlowingConsultation.by_calling_areas_and_institutions
      # Then
      expect(result).to eq [fc_lille, fc_paris, fc_marseille, fc_toulouse, fc_nulle_part]
    end
  end

  describe "#simple_message?" do

    describe "simple messages" do
      subject { build(:flowing_consultation, :just_a_simple_message) }

      it { is_expected.to be_a_simple_message }
    end

    describe "regular consultations" do
      subject { build(:flowing_consultation, :with_officer) }

      it { is_expected.not_to be_a_simple_message }
    end
  end

  describe "#archived?" do

    let(:flowing_consultation) { create(:flowing_consultation) }

    describe "regarding cancellation date" do

      it "returns true if older than 12 hours" do
        # Given
        flowing_consultation.cancelled_at = Time.zone.now - (12.hours+1.second)
        # When
        result = flowing_consultation.archived?
        # Then
        expect(result).to be_truthy
      end

      it "returns false if younger than 12 hours" do
        # Given
        flowing_consultation.cancelled_at = Time.zone.now - (12.hours-1.second)
        # When
        result = flowing_consultation.archived?
        # Then
        expect(result).to be_falsy
      end
    end

    describe "regarding validation date" do

      it "returns true if older than 12 hours" do
        # Given
        flowing_consultation.validated_at = Time.zone.now - (12.hours+1.second)
        # When
        result = flowing_consultation.archived?
        # Then
        expect(result).to be_truthy
      end

      it "returns false if younger than 12 hours" do
        # Given
        flowing_consultation.validated_at = Time.zone.now - (12.hours-1.second)
        # When
        result = flowing_consultation.archived?
        # Then
        expect(result).to be_falsy
      end
    end
  end

  it "#signed_filename returns formated filename with current datetime and extension" do
    # Given
    Timecop.freeze(Time.local(2019, 12, 11, 10, 24))
    consultation_type = create(:consultation_type, title: "GAV")
    patient = create(:patient, last_name: "Dupont", first_name: "Camille")
    flowing_consultation = create(:flowing_consultation, onml_id: "A1912042", patient: patient, consultation_type: consultation_type)
    # When
    result = flowing_consultation.signed_filename("doc")
    # Then
    expect(result).to eq "DUPONT Camille - A1912042 - GAV - 11122019-1024.doc"
    Timecop.return
  end
end
