# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe InterventionDomain, type: :model do

  subject { build(:intervention_domain) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_uniqueness_of(:title) }
  it { is_expected.to define_enum_for(:modus_operandi) }

  describe "associations" do
    it { is_expected.to have_many(:consultation_types) }
    it { is_expected.to have_many(:schedules) }
    it { is_expected.to have_many(:roles) }
  end

  describe "scopes" do
    let(:user) { create(:user) }

    it "#stream_ones_for_user returns stream intervention domains available to a specific user" do
      # Given
      domain1 = create(:intervention_domain, :stream, roles: [user.role])
      domain2 = create(:intervention_domain, :stream)
      # When
      result = InterventionDomain.stream_ones_for_user(user)
      # Then
      expect(result).to eq [domain1]
    end

    it "#appointments_ones_for_user returns appointments intervention domains with at least one consultation type available to a specific user" do
      # Given
      domain1 = create(:intervention_domain, :appointments)
      create(:consultation_type, intervention_domain: domain1, roles: [user.role])
      create(:consultation_type, intervention_domain: domain1)
      domain2 = create(:intervention_domain, :appointments)
      create(:consultation_type, intervention_domain: domain2)
      # When
      result = InterventionDomain.appointments_ones_for_user(user)
      # Then
      expect(result).to eq [domain1]
    end

    it "#by_title returns by title ascending (default)" do
      # Given
      domain1 = create(:intervention_domain, title: "Consultations")
      domain2 = create(:intervention_domain, title: "Antenne")
      # When
      result = InterventionDomain.by_title
      # Then
      expect(result).to eq [domain2, domain1]
    end

    it "#by_title(:desc) returns by title descending" do
      # Given
      domain1 = create(:intervention_domain, title: "Consultations")
      domain2 = create(:intervention_domain, title: "Antenne")
      # When
      result = InterventionDomain.by_title(:desc)
      # Then
      expect(result).to eq [domain1, domain2]
    end
  end
end
