# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Calendars::Day, type: :model do
  subject { described_class.new(date: date, duty_periods: duty_periods, appointments: appointments) }

  let(:date) { Date.current }
  let(:duty_periods) { [] }
  let(:appointments) { [] }

  describe "#assignments_events" do
    subject { super().assignments_events }

    describe "when there is no duty periods" do
      it { is_expected.to be_empty }
    end

    describe "when there are duty periods" do
      let(:duty_periods) { [some_duty_period, other_duty_period] }
      let(:some_duty_period) { create(:duty_period) }
      let(:other_duty_period) { create(:duty_period) }

      it { is_expected.to have_attributes(length: 2) }
    end
  end

  describe "#sorted_duty_periods" do
    subject { super().sorted_duty_periods }

    describe "when there is no duty periods" do
      it { is_expected.to be_empty }
    end

    describe "when there are duty periods" do
      describe "with different start time" do
        let(:morning_slot) { create(:time_slot, week_day: date.cwday, starts_at: "09:00", ends_at: "13:00") }
        let(:afternoon_slot) { create(:time_slot, week_day: date.cwday, starts_at: "14:00", ends_at: "18:00") }
        let(:earliest_duty_period) { create(:duty_period, scheduled_on: date, time_slot: morning_slot) }
        let(:latest_duty_period) { create(:duty_period, scheduled_on: date, time_slot: afternoon_slot) }
        let(:duty_periods) { [latest_duty_period, earliest_duty_period] }

        it { is_expected.to have_attributes(length: 2) }

        it "sorts the earliest first" do
          expect(subject.first).to eq earliest_duty_period
        end
      end

      describe "with same start time" do
        let(:short_slot) { create(:time_slot, week_day: date.cwday, starts_at: "09:00", ends_at: "12:00") }
        let(:long_slot) { create(:time_slot, week_day: date.cwday, starts_at: short_slot.starts_at, ends_at: short_slot.ends_at + 1.hour) }
        let(:shortest_duty_period) { create(:duty_period, scheduled_on: date, time_slot: short_slot) }
        let(:longest_duty_period) { create(:duty_period, scheduled_on: date, time_slot: long_slot) }
        let(:duty_periods) { [shortest_duty_period, longest_duty_period] }

        it { is_expected.to have_attributes(length: 2) }

        it "sorts the longest first" do
          expect(subject.first).to eq longest_duty_period
        end
      end
    end
  end

  describe "#resources" do
    subject { super().resources }

    describe "when there is no duty periods" do
      it { is_expected.to have_attributes(length: 1) }

      it "returns the default resource" do
        expect(subject.first.title).to eq Calendars::Resource::DEFAULT
      end
    end

    describe "when there are duty periods" do
      describe "not overlapping" do
        let(:morning_slot) { create(:time_slot, week_day: date.cwday, starts_at: "09:00", ends_at: "13:00") }
        let(:afternoon_slot) { create(:time_slot, week_day: date.cwday, starts_at: "14:00", ends_at: "18:00") }
        let(:some_duty_period) { create(:duty_period, scheduled_on: date, time_slot: morning_slot) }
        let(:other_duty_period) { create(:duty_period, scheduled_on: date, time_slot: afternoon_slot) }
        let(:duty_periods) { [some_duty_period, other_duty_period] }

        it { is_expected.to have_attributes(length: 1) }

        it "returns indexed titles" do
          expect(subject.map(&:title)).to eq ["1"]
        end
      end

      describe "with overlap" do
        let(:earliest_slot) { create(:time_slot, week_day: date.cwday, starts_at: "09:00", ends_at: "12:00") }
        let(:later_slot) { create(:time_slot, week_day: date.cwday, starts_at: "10:00", ends_at: "13:00") }
        let(:some_duty_period) { create(:duty_period, scheduled_on: date, time_slot: earliest_slot) }
        let(:other_duty_period) { create(:duty_period, scheduled_on: date, time_slot: later_slot) }
        let(:duty_periods) { [some_duty_period, other_duty_period] }

        it { is_expected.to have_attributes(length: 2) }

        it "returns indexed titles" do
          expect(subject.map(&:title)).to eq ["1", "2"]
        end
      end

      describe "with successive slots" do
        let(:earliest_slot) { create(:time_slot, week_day: date.cwday, starts_at: "09:00", ends_at: "12:00") }
        let(:later_slot) { create(:time_slot, week_day: date.cwday, starts_at: "12:00", ends_at: "15:00") }
        let(:some_duty_period) { create(:duty_period, scheduled_on: date, time_slot: earliest_slot) }
        let(:other_duty_period) { create(:duty_period, scheduled_on: date, time_slot: later_slot) }
        let(:duty_periods) { [some_duty_period, other_duty_period] }

        it { is_expected.to have_attributes(length: 1) }

        it "returns indexed titles" do
          expect(subject.map(&:title)).to eq ["1"]
        end
      end

      describe "with assignees" do
        let(:some_slot) { create(:time_slot, week_day: date.cwday) }
        let(:duo_duty_period) { create(:duty_period, scheduled_on: date, time_slot: some_slot, assignees: create_list(:user, 2)) }
        let(:duty_periods) { [duo_duty_period] }

        it { is_expected.to have_attributes(length: 2) }

        it "returns duplicated duty periods" do
          expect(subject.map(&:duty_periods)).to match_array [[duo_duty_period], [duo_duty_period]]
        end
      end
    end
  end

  describe "#sorted_appointments" do
    subject { super().sorted_appointments }

    describe "when there is no appointments" do
      it { is_expected.to be_empty }
    end

    describe "when there are appointments" do
      describe "with different start time" do
        let(:earliest_appointment) { create(:appointment) }
        let(:latest_appointment) { create(:appointment, starts_at: earliest_appointment.starts_at + 1.hour) }
        let(:appointments) { [latest_appointment, earliest_appointment] }

        it { is_expected.to have_attributes(length: 2) }

        it "sorts the earliest first" do
          expect(subject.first).to eq earliest_appointment
        end
      end

      describe "with same start time" do
        let(:shortest_appointment) { create(:appointment) }
        let(:longest_appointment) { create(:appointment, starts_at: shortest_appointment.starts_at, duration: shortest_appointment.duration + 30.minutes) }
        let(:appointments) { [shortest_appointment, longest_appointment] }

        it { is_expected.to have_attributes(length: 2) }

        it "sorts the longest first" do
          expect(subject.first).to eq longest_appointment
        end
      end
    end
  end

  describe "#resources_with_appointments" do
    subject { super().resources_with_appointments }

    describe "when there is no appointment" do
      it { is_expected.to have_attributes(length: 1) }

      it "returns the resource without appointment" do
        expect(subject.first.appointments).to be_empty
      end
    end

    describe "when there are appointments" do
      let(:appointments) { [earliest_appointment, overlapping_appointment, later_appointment, latest_appointment] }
      let(:earliest_appointment) { create(:appointment, starts_at: date.at_noon - 1.hours, duration: "PT1H30M") }
      let(:overlapping_appointment) { create(:appointment, starts_at: date.at_noon, duration: "PT2H") }
      let(:later_appointment) { create(:appointment, starts_at: date.at_noon + 30.minutes, duration: "PT1H") }
      let(:latest_appointment) { create(:appointment, starts_at: later_appointment.ends_at) }

      describe "when there is not enough resources" do
        it { is_expected.to have_attributes(length: 1) }

        it "returns the resource with overlapping appointments" do
          expect(subject.first.appointments).to eq(appointments)
        end
      end

      describe "when there are enough resources" do
        let(:duty_periods) { [create(:duty_period, assignees: create_list(:user, 2))] }

        it { is_expected.to have_attributes(length: 2) }

        it "returns resources without overlapping appointments" do
          expect(subject.first.appointments).to eq([earliest_appointment, later_appointment, latest_appointment])
          expect(subject.second.appointments).to eq([overlapping_appointment])
        end
      end

      describe "when the second ressource ends before the first" do
        let(:later_appointment) { create(:appointment, starts_at: date.at_noon + 30.minutes, duration: "PT2H") }

        let(:duty_periods) { [create(:duty_period, assignees: create_list(:user, 2))] }

        it { is_expected.to have_attributes(length: 2) }

        it "returns all non-overlapping appointments in the first resource" do
          expect(subject.first.appointments).to eq([earliest_appointment, later_appointment, latest_appointment])
          expect(subject.second.appointments).to eq([overlapping_appointment])
        end
      end
    end

    describe "when there are cancelled appointments" do
      let(:appointments) { [cancelled_appointment, overlapping_appointment, after_the_cancelled_one] }
      let(:cancelled_appointment) { create(:appointment, :cancelled, starts_at: date.at_noon - 1.hours, duration: "PT1H30M") }
      let(:overlapping_appointment) { create(:appointment, starts_at: cancelled_appointment.starts_at + 30.minutes, duration: "PT2H") }
      let(:after_the_cancelled_one) { create(:appointment, starts_at: cancelled_appointment.ends_at) }

      let(:duty_periods) { [create(:duty_period, assignees: create_list(:user, 2))] }

      it { is_expected.to have_attributes(length: 2) }

      it "returns resources with cancelled appointments last even if earliest" do
        expect(subject.first.appointments).to eq([overlapping_appointment, cancelled_appointment])
        expect(subject.second.appointments).to eq([after_the_cancelled_one])
      end
    end
  end
end
