# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"
require Rails.root.join "spec/concerns/appointment_duration.rb"

RSpec.describe ConsultationReason, type: :model do
  subject { build(:consultation_reason) }

  it { is_expected.to be_valid }
  %i[code title colour].each do |attr|
    it { is_expected.to validate_presence_of(attr) }
  end

  it_behaves_like "a model with an appointment duration"

  describe "behavior" do
    it "#label_for_select returns the title followed by the code in parenthesis" do
      # Given
      consultation_reason = create(:consultation_reason, title: "Violences volontaires", code: "VV")
      # When
      result = consultation_reason.label_for_select
      # Then
      expect(result).to eq "Violences volontaires (VV)"
    end
  end

  describe "scopes" do
    it ".by_title returns consultation reasons ordered by ascending title" do
      # Given
      violences_volontaires = create(:consultation_reason, title: "Violences volontaires")
      agression_sexuelle = create(:consultation_reason, title: "Agression sexuelle")
      # When
      result = described_class.by_title
      # Then
      expect(result).to eq [agression_sexuelle, violences_volontaires]
    end
  end
end
