# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"
require Rails.root.join "spec/concerns/loggable.rb"

RSpec.describe Patient, type: :model do
  describe "validations" do
    subject { build(:patient)}

    it { is_expected.to be_valid }

    it "is valid if only a birth year is provided" do
      # Given
      patient = build(:patient, birthdate: nil, birth_year: "1936")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_truthy
    end

    it "is valid if only a valid birthdate is provided" do
      # Given
      patient = build(:patient, birthdate: "16/06/1936")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_truthy
    end

    it "is NOT valid if only an invalid birthdate is provided" do
      # Given
      patient = build(:patient, birthdate: "32/06/1936")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_falsy
    end

    it "is NOT valid if only a birth_year is provided instead of a birthdate" do
      # Given
      patient = build(:patient, birthdate: "1936")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_falsy
    end

    it "is valid if both 16/06/1936 and 1936 are provided" do
      # Given
      patient = build(:patient, birthdate: "16/06/1936", birth_year: "1936")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_truthy
    end

    it "is NOT valid if both 16/06/1936 and 1937 are provided" do
      # Given
      patient = build(:patient, birthdate: "16/06/1936", birth_year: "1937")
      # When
      result = patient.valid?
      # Then
      expect(result).to be_falsy
    end


  end

  describe "#fullname" do

    it "returns upcased last name followed by first name" do
      # Given
      patient = create(:patient, last_name: "House", first_name: "Gregory")
      # When
      result = patient.fullname
      # Then
      expect(result).to eq "HOUSE Gregory"
    end

    it "returns only the first name when there is no last name provided" do
      # Given
      patient = create(:patient, last_name: nil, first_name: "Lio")
      # When
      result = patient.fullname
      # Then
      expect(result).to eq "Lio"
    end

    it "returns a placeholder when last and first name are blank" do
      # Given
      patient = create(:patient, last_name: nil, first_name: nil)
      # When
      result = patient.fullname
      # Then
      expect(result).to eq "Identité non-renseignée"
    end

  end

  describe "#phone_numbers" do
    it "returns correctly formated patient's phone numbers" do
      # Given
      patient = create(:patient)
      app1 = create(:appointment, patient: patient, patient_phone: "0102030405")
      app2 = create(:appointment, patient: patient, patient_phone: nil)
      app3 = create(:appointment, patient: patient, patient_phone: "0706050403 (mère) ,   0102030405 ,  ")
      # When
      result = patient.phone_numbers
      # Then
      expect(result).to match(/0102030405/)
      expect(result).to match(/0706050403 \(mère\)/)
    end
  end

  describe "#aged_at" do
    describe "when on 06/06/2020" do
      let(:date) { Date.new(2020,6,6) }

      it "returns an age in years for possibly older than 36 months old" do
        # Given
        patient = create(:patient, birthdate: nil, birth_year: 2017)
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "2-3 ans"
      end

      it "returns a age in month for a patient born less than three years ago" do
        # Given
        patient = create(:patient, birthdate: nil, birth_year: 2018)
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "17-29 mois"
      end

      it "returns an age starting at 0 for a patient born less than a year ago" do
        # Given
        patient = create(:patient, birthdate: nil, birth_year: 2020)
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "0-5 mois"
      end

      it 'returns "20 ans et 5 mois" for a patient born on 01/01/2000' do
        # Given
        patient = create(:patient, birthdate: "01/01/2000")
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "20 ans et 5 mois"
      end

      it 'returns "1 an" for a patient born on 05/06/2019' do
        # Given
        patient = create(:patient, birthdate: "05/06/2019")
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "1 an"
      end

      it 'returns "11 mois" for a patient born on 07/06/2019' do
        # Given
        patient = create(:patient, birthdate: "07/06/2019")
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "11 mois"
      end

      it 'returns "5 mois" for a patient born on 01/01/2020' do
        # Given
        patient = create(:patient, birthdate: "01/01/2020")
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to eq "5 mois"
      end

      it 'returns "Non disponible" for a patient without any birthdate nor birth year' do
        # Given
        patient = create(:patient, birthdate: nil, birth_year: nil)
        # When
        result = patient.aged_at(date)
        # Then
        expect(result).to match "Non disponible"
      end
    end
  end

  describe "#is_minor_on? checks if patient is minor at a specfic date" do
    it "returns true if age is less than 18" do
      # Given
      today = Date.today
      patient = create(:patient, birthdate: today - 18.years + 1.day)
      # When
      result = patient.is_minor_on?(today)
      # Then
      expect(result).to be_truthy
    end

    it "returns false if age is equal or greater than 18" do
      # Given
      today = Date.today
      patient = create(:patient, birthdate: today - 18.years)
      # When
      result = patient.is_minor_on?(today)
      # Then
      expect(result).to be_falsy
    end
  end

  describe "duplications" do
    let!(:unique_patient) { create(:patient, :with_appointment) }
    let!(:appointment_patient) { create(:patient, :with_appointment) }
    let!(:flowing_consultation_patient) { create(:patient, :with_flowing_consultation) }

    def create_duplicate patient, *traits
      create(:patient, *traits,
        gender: patient.gender,
        last_name: patient.last_name,
        first_name: patient.first_name,
        birthdate: patient.birthdate,
      ).tap do |created|
        if (first_appointment = patient.appointments.first) && (created_appointment = created.appointments.first)
          created_appointment.update(patient_phone: first_appointment.patient_phone)
        end
      end
    end

    describe "self.probable_duplicates" do
      subject { described_class.probable_duplicates(as_array: true) }

      it "returns only fully-qualitfied duplicates" do
        # Given
        duplicate = create_duplicate(appointment_patient)
        other_gender = create_duplicate(appointment_patient).tap do |created|
          created.update(
            gender: (%w[unknown woman man] - [created.gender]).sample,
          )
        end
        other_last_name = create_duplicate(appointment_patient).tap do |created|
          created.update(
            last_name: created.last_name + " Jr",
          )
        end
        other_first_name = create_duplicate(appointment_patient).tap do |created|
          created.update(
            first_name: created.first_name + " W.",
          )
        end
        other_birthdate = create_duplicate(appointment_patient).tap do |created|
          created.update(
            birthdate: created.birthdate + 1.day,
          )
        end
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id]
        )
      end

      it 'returns "case insensitive" duplicates' do
        # Given
        duplicate = create_duplicate(appointment_patient)
        duplicate_upcased = create_duplicate(appointment_patient).tap do |created|
          created.update(
            last_name: created.last_name.upcase,
            first_name: created.first_name.upcase
          )
        end
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id, duplicate_upcased.id]
        )
      end

      it 'returns "with surrounding spaces" duplicates' do
        # Given
        duplicate = create_duplicate(appointment_patient)
        duplicate_spaced = create_duplicate(appointment_patient).tap do |created|
          created.update(
            last_name: " #{created.last_name} ",
            first_name: " #{created.first_name} "
          )
        end
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id, duplicate_spaced.id]
        )
      end

      it "does not return duplicates on missing gender" do
        # Given
        appointment_patient.update(gender: nil)
        duplicate = create_duplicate(appointment_patient)
        # Then
        expect(subject).to be_empty
      end

      it "does not return duplicates on missing last name" do
        # Given
        appointment_patient.update(last_name: nil)
        duplicate = create_duplicate(appointment_patient)
        # Then
        expect(subject).to be_empty
      end

      it "does not return duplicates on missing first name" do
        # Given
        appointment_patient.update(first_name: nil)
        duplicate = create_duplicate(appointment_patient)
        # Then
        expect(subject).to be_empty
      end

      it "does not return duplicates on missing birthdate" do
        # Given
        appointment_patient.update(birthdate: nil)
        duplicate = create_duplicate(appointment_patient)
        # Then
        expect(subject).to be_empty
      end

    end

    describe "self.exact_duplicates" do
      subject { described_class.exact_duplicates(as_array: true) }

      it 'returns "exact match" duplicates' do
        # Given
        duplicate = create_duplicate(appointment_patient, :with_appointment)
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id]
        )
      end

      it "does not returns duplicates with different phone number" do
        # Given
        duplicate = create_duplicate(appointment_patient, :with_appointment)
        with_different_phone = create_duplicate(appointment_patient, :with_appointment).tap do |created|
          first_phone_number = appointment_patient.appointments.first.patient_phone
          created.appointments.first.update(patient_phone: first_phone_number + "42")
        end
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id]
        )
      end

      it 'returns "phone number format insensitive" duplicates' do
        # Given
        duplicate = create_duplicate(appointment_patient, :with_appointment)
        duplicate_noisy_phone = create_duplicate(appointment_patient, :with_appointment).tap do |created|
          first_phone_number = appointment_patient.appointments.first.patient_phone
          created.appointments.first.update(patient_phone: first_phone_number + " .-/")
        end
        # Then
        expect(subject.length).to eq(1)
        expect(subject.first).to match_array(
          [appointment_patient.id, duplicate.id, duplicate_noisy_phone.id]
        )
      end

      it "does not return exact same patient as duplicates" do
        # Given
        phone_number = appointment_patient.appointments.first.patient_phone
        other_appointment = create(:appointment, patient: appointment_patient, patient_phone: phone_number)
        # Then
        expect(subject).to be_empty
      end

    end

    describe "self.hand_over_consultations!" do
      subject { described_class.hand_over_consultations!(@duplicates) }

      it "update all associations" do
        # Given
        @first_appointment = appointment_patient.appointments.first
        patient_with_appointment = create(:patient, :with_appointment).tap do |created|
          @second_appointment = created.appointments.first
          created.update(duplicated_patient: appointment_patient)
        end
        patient_with_flowing_consultation = create(:patient, :with_flowing_consultation).tap do |created|
          @flowing_consultation = created.flowing_consultations.first
          created.update(duplicated_patient: appointment_patient)
        end

        appointment_with_duplicate_patient = create(:appointment, patient: patient_with_flowing_consultation)
        flowing_consultation_with_duplicate_patient = create(:flowing_consultation, patient: patient_with_appointment)
        # When
        @duplicates = [patient_with_appointment.id, patient_with_flowing_consultation.id] and subject
        [
          appointment_patient,
          patient_with_appointment,
          patient_with_flowing_consultation,
          @first_appointment,
          @second_appointment,
          @flowing_consultation,
          appointment_with_duplicate_patient,
          flowing_consultation_with_duplicate_patient
        ].each(&:reload)
        # Then
        expect(appointment_patient.appointments).to match_array([@first_appointment, @second_appointment, appointment_with_duplicate_patient])
        expect(appointment_patient.flowing_consultations).to match_array([@flowing_consultation, flowing_consultation_with_duplicate_patient])
        expect(@first_appointment.original_patient_id).to be_nil
        expect(@second_appointment.original_patient_id).to eq(patient_with_appointment.id)
        expect(appointment_with_duplicate_patient.original_patient_id).to eq(patient_with_flowing_consultation.id)
        expect(@flowing_consultation.original_patient_id).to eq(patient_with_flowing_consultation.id)
        expect(flowing_consultation_with_duplicate_patient.original_patient_id).to eq(patient_with_appointment.id)
      end

    end

    describe "self.mark_all_duplicates!" do
      subject { described_class.mark_all_duplicates! }

      it "associates duplicates' appointments to first patient" do
        # Given
        exact_duplicate = create_duplicate(appointment_patient, :with_appointment)
        probable_duplicate = create_duplicate(appointment_patient, :with_flowing_consultation)
        # When
        subject
        [
          appointment_patient,
          exact_duplicate,
          probable_duplicate,
        ].each(&:reload)
        # Then
        expect(appointment_patient.duplicated_patient_id).to be_nil
        expect(appointment_patient.appointments.count).to eq(2)
        expect(exact_duplicate.duplicated_patient_id).to eq(appointment_patient.id)
        expect(probable_duplicate.probably_duplicated_patient_id).to eq(appointment_patient.id)
      end

    end

    describe "self.rollback_all_duplicates!" do
      subject { described_class.rollback_all_duplicates! }

      it "rollbacks all duplicates" do
        # Given
        exact_duplicate = create_duplicate(appointment_patient).tap do |created|
          created.update(duplicated_patient: appointment_patient)
        end
        appointment = create(:appointment, patient: appointment_patient, original_patient_id: exact_duplicate.id)
        flowing_consultation = create(:flowing_consultation, patient: appointment_patient, original_patient_id: exact_duplicate.id)

        appointment_patient.reload
        expect(appointment_patient.appointments.count).to eq(2)
        expect(appointment_patient.flowing_consultations.count).to eq(1)

        probable_duplicate = create_duplicate(appointment_patient).tap do |created|
          created.update(probably_duplicated_patient: appointment_patient)
        end
        # When
        subject
        [
          appointment_patient,
          exact_duplicate,
          probable_duplicate,
        ].each(&:reload)
        # Then
        expect(appointment_patient.duplicated_patient_id).to be_nil
        expect(appointment_patient.appointments.count).to eq(1)
        expect(appointment_patient.flowing_consultations.count).to eq(0)

        expect(exact_duplicate.appointments.count).to eq(1)
        expect(exact_duplicate.flowing_consultations.count).to eq(1)
        expect(exact_duplicate.duplicated_patient_id).to be_nil

        expect(probable_duplicate.probably_duplicated_patient_id).to be_nil
      end
    end

  end


  describe "scopes" do
    describe "default_scope" do
      let!(:unique_patient) { create(:patient) }
      let!(:duplicated_patient) { create(:patient) }

      it "does not return duplicates" do
        # Given
        duplicate = create(:patient, duplicated_patient: duplicated_patient)
        # When
        result = Patient.all
        # Then
        expect(result).to match_array([unique_patient, duplicated_patient])
      end
    end

    it "fullname_asc returns patient ordered by fullname" do
      # Given
      p1 = create(:patient, last_name: "DUPONT", first_name: "Camille")
      p2 = create(:patient, last_name: "DUPOND", first_name: "Camille")
      p3 = create(:patient, last_name: "dupond", first_name: "alphonse")
      p4 = create(:patient, last_name: "Dupont", first_name: "Alphonse")
      # When
      result = Patient.fullname_asc
      # Then
      expect(result).to eq [p3,p2,p4,p1]
    end

    it "of_matching_lastname returns patients with lastname beginning with a matching string" do
      # Given
      p1 = create(:patient, last_name: "DUPONT")
      p2 = create(:patient, last_name: "dupond")
      p3 = create(:patient, last_name: "Moldu")
      # When
      dupont = Patient.of_matching_lastname("dupont")
      du = Patient.of_matching_lastname("DU")
      # Then
      expect(dupont).to eq [p1]
      expect(du).to eq [p1,p2]
    end

    it "of_matching_firstname returns patients with firstname beginning with a matching string" do
      # Given
      p1 = create(:patient, first_name: "CAMILLE")
      p2 = create(:patient, first_name: "Caroline")
      p3 = create(:patient, first_name: "Monica")
      # When
      caro = Patient.of_matching_firstname("CARO")
      ca = Patient.of_matching_firstname("ca")
      # Then
      expect(caro).to eq [p2]
      expect(ca).to eq [p1,p2]
    end

    it "born_on_date returns patients born at a specific date" do
      # Given
      p1 = create(:patient, birthdate: "17/04/1967")
      p2 = create(:patient, birthdate: "17/04/1968")
      # When
      result = Patient.born_on_date("17/04/1967")
      # Then
      expect(result).to eq [p1]
    end

    it "born_on_year returns patients born within specific year" do
      # Given
      p1 = create(:patient, birthdate: nil, birth_year: 1967)
      p2 = create(:patient, birthdate: nil, birth_year: 1968)
      p3 = create(:patient, birthdate: "17/04/1967")
      # When
      result = Patient.born_on_year("1967")
      # Then
      expect(result).to eq [p1,p3]
    end

    it_behaves_like "a loggable model"
  end
end
