# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe MasterDocument, type: :model do

  let(:certificate_template) { create(:certificate_template) }
  subject { build(:master_document, attachable: certificate_template) }

  it { is_expected.to be_valid }

  describe "behavior" do
    describe "#filename_for" do
      it "returns the original filename for certificate templates management" do
        # Given
        # When
        result = subject.filename_for(nil)
        # Then
        expect(result).to eq subject.file.filename.to_s
      end

      it "returns a custom filename for a specific appointment" do
        # Given
        patient = build(:patient, last_name: "Dupont", first_name: "Camille")
        appointment = create(:appointment, patient: patient, starts_at: "27/06/2024 14:36", onml_id: "CON12345")
        # When
        result = subject.filename_for(appointment)
        # Then
        expect(result).to eq "DUPONT Camille - CON12345 - 27062024-1436.txt"
      end

      it "returns a custom filename for a specific flowing consultation" do
        # Given
        patient = build(:patient, last_name: "Dupont", first_name: "Camille")
        flowing_consultation = create(:flowing_consultation, patient: patient, assigned_at: "27/06/2024 14:36", onml_id: "AN12345")
        # When
        result = subject.filename_for(flowing_consultation)
        # Then
        expect(result).to eq "DUPONT Camille - AN12345 - 27062024-1436.txt"
      end
    end
  end
end
