# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe CallingInstitutionPolicy do
  subject { described_class.new(user, calling_institution) }
  let(:resolved_scope) { described_class::Scope.new(user, CallingInstitution.all).resolve }

  let(:calling_institution) { create(:calling_institution) }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_all_actions }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end

    it "cannot retrieve any from the database" do
      expect(resolved_scope).to be_empty
    end
  end

  context "as an administrator" do
    let(:user) { create(:user, :administrator) }

    it { is_expected.to permit_actions(:new, :create, :edit, :update) }

    context "for an institution associated to no appointment nor flowing consultation" do
      it { is_expected.to permit_action(:destroy) }
    end

    context "for an institution associated to at least one appointment" do
      let!(:appointment) { create(:appointment, calling_institution: calling_institution) }

      it { is_expected.to forbid_action(:destroy) }
    end

    context "for an institution associated to at least one flowing consultation" do
      let!(:flowing_consultation) { create(:flowing_consultation, calling_institution: calling_institution) }

      it { is_expected.to forbid_action(:destroy) }
    end

    it "permits all attributes" do
      expect(subject.permitted_attributes).to match_array [:title, :calling_area_id]
    end

    it "includes the calling institution in resolved scope" do
      expect(resolved_scope).to include(calling_institution)
    end
  end

  Role::ALL.excluding(:administrator).each do |role|
    context "as a #{role}" do
      let(:user) { create(:user, role) }

      it { is_expected.to forbid_all_actions }

      it "permits no attribute" do
        expect(subject.permitted_attributes).to be_empty
      end

      it "includes the calling institution in resolved scope" do
        expect(resolved_scope).to include(calling_institution)
      end
    end
  end
end
