# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a user with medical confidentiality regarding patients" do |role|
  it { is_expected.to permit_actions(:index, :show , :edit, :update, :consultations) }

  it "permits all attributes" do
    expected = %i[
      birth_year
      birthdate
      duplicated_patient_id
      first_name
      gender
      last_name
      probably_duplicated_patient_id
    ]
    expect(subject.permitted_attributes).to match_array expected
  end

  it "includes the patient in resolved scope" do
    expect(resolved_scope).to include(patient)
  end
end

RSpec.shared_examples "a visitor regarding patients" do |role|
  it { is_expected.to forbid_all_actions }

  it "permits no attribute" do
    expect(subject.permitted_attributes).to be_empty
  end

  it "cannot retrieve any from the database" do
    expect(resolved_scope).to be_empty
  end
end

describe PatientPolicy do
  subject { described_class.new(user, patient) }
  let(:resolved_scope) { described_class::Scope.new(user, Patient.all).resolve }

  let(:patient) { create(:patient) }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it_behaves_like "a visitor regarding patients"
  end

  Role::ALL.excluding(:association, :switchboard_operator).each do |role|
    context "as a #{role}" do
      let(:user) { create(:user, role) }

      it_behaves_like "a user with medical confidentiality regarding patients"
    end
  end

  context "as an association" do
    let(:user) { create(:user, :association) }

    it_behaves_like "a visitor regarding patients"
  end

  context "as a switchboard operator" do
    let(:user) { create(:user, :switchboard_operator) }

    it { is_expected.to permit_only_actions(:show) }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end

    it "includes the patient in resolved scope" do
      expect(resolved_scope).to include(patient)
    end
  end
end
