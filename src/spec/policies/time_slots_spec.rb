# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a visitor regarding time slots" do |role|
  it { is_expected.to forbid_all_actions }

  it "permits no attribute" do
    expect(subject.permitted_attributes).to be_empty
  end
end

describe TimeSlotPolicy do
  subject { described_class.new(user, time_slot) }

  let(:time_slot) { create(:time_slot) }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it_behaves_like "a visitor regarding time slots"
  end

  context "as an administrator" do
    let(:user) { create(:user, :administrator) }

    it { is_expected.to permit_actions(:new, :create, :edit, :update, :destroy) }

    it "permits all attributes" do
      expect(subject.permitted_attributes).to match_array [:week_day, :starts_at, :ends_at, :consultation_type_id]
    end
  end

  Role::ALL.excluding(:administrator).each do |role|
    context "as a #{role}" do
      let(:user) { create(:user, role) }

      it_behaves_like "a visitor regarding time slots"
    end
  end
end
