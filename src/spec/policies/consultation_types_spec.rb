# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe ConsultationTypePolicy do
  subject { described_class.new(user, consultation_type) }

  let(:consultation_type) { create(:consultation_type) }

  let(:resolved_scope) { described_class::Scope.new(user, ConsultationType.all).resolve }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_all_actions }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end

    it "cannot retrieve any from the database" do
      expect(resolved_scope).to be_empty
    end
  end

  describe "Management" do
    context "as an administrator" do
      let(:user) { create(:user, :administrator) }

      it { is_expected.to permit_actions(:new, :create, :edit, :update) }

      context "for a type associated to no appointment nor flowing consultation" do
        it { is_expected.to permit_action(:destroy) }
      end

      context "for a type associated to at leats one appointment" do
        before { create(:appointment, consultation_type: consultation_type) }

        it { is_expected.to forbid_action(:destroy) }
      end

      context "for a type associated to at leats one flowing consultation" do
        before { create(:flowing_consultation, consultation_type: consultation_type) }

        it { is_expected.to forbid_action(:destroy) }
      end

      it "permits main attributes" do
        expect(subject.permitted_attributes).to match_array [:title, :icon_name, :intervention_domain_id, role_ids: [], assigned_role_ids: [], complementary_consultation_type_ids: []]
      end

      context "for a type she does not have access to" do
        it { is_expected.to forbid_actions(:show, :archives_index) }

        it "does not include the consultation type in resolved scope" do
          expect(resolved_scope).not_to include(consultation_type)
        end
      end

      context "for a type she has access to" do
        let(:consultation_type) { create(:consultation_type, roles: [user.role]) }

        it { is_expected.to permit_actions(:show, :archives_index) }

        it "includes the consultation type in resolved scope" do
          expect(resolved_scope).to include(consultation_type)
        end
      end
    end

    Role::ALL.excluding(:administrator).each do |role|
      context "as a #{role}" do
        let(:user) { create(:user, role) }

        it { is_expected.to forbid_all_actions }

        it "permits no attribute" do
          expect(subject.permitted_attributes).to be_empty
        end

        context "for a type she does not have access to" do
          it { is_expected.to forbid_actions(:show, :archives_index) }

          it "does not include the consultation type in resolved scope" do
            expect(resolved_scope).not_to include(consultation_type)
          end
        end

        context "for a type she has access to" do
          let(:consultation_type) { create(:consultation_type, roles: [user.role]) }

          it { is_expected.to permit_actions(:show, :archives_index) }

          it "includes the consultation type in resolved scope" do
            expect(resolved_scope).to include(consultation_type)
          end
        end
      end
    end
  end
end
