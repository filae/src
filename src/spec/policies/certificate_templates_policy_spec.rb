# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a visitor regarding certificate templates" do |role|
  it { is_expected.to forbid_all_actions }

  it "permits no attribute" do
    expect(subject.permitted_attributes).to be_empty
  end

  it "cannot retrieve any from the database" do
    expect(resolved_scope).to be_empty
  end
end

describe CertificateTemplatePolicy do
  subject { described_class.new(user, certificate_template) }
  let(:certificate_template) { create(:certificate_template) }
  let(:resolved_scope) { described_class::Scope.new(user, CertificateTemplate.all).resolve }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it_behaves_like "a visitor regarding certificate templates"
  end

  context "as an administrator" do
    let(:user) { create(:user, :administrator) }

    it { is_expected.to permit_actions(:index, :new, :create, :edit, :update, :destroy, :toggle_activation) }

    it "permits main attributes" do
      expect(subject.permitted_attributes).to match_array [:title, :disabled_at, :questionnaire, role_ids: [], questionnaire: {}]
    end

    it "includes the certificate template in resolved scope" do
      expect(resolved_scope).to include(certificate_template)
    end
  end

  Role::ALL.excluding(:administrator).each do |role|
    context "as a #{role}" do
      let(:user) { create(:user, role) }

      it_behaves_like "a visitor regarding certificate templates"
    end
  end
end
