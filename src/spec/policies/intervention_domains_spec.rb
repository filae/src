# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe InterventionDomainPolicy do
  subject { described_class.new(user, intervention_domain) }
  let(:resolved_scope) { described_class::Scope.new(user, InterventionDomain.all).resolve }

  let(:intervention_domain) { create(:intervention_domain) }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_all_actions }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end

    it "cannot retrieve any from the database" do
      expect(resolved_scope).to be_empty
    end
  end

  describe "Management" do
    context "as an administrator" do
      let(:user) { create(:user, :administrator) }

      it { is_expected.to permit_actions(:index, :new, :create, :edit, :update, :destroy) }

      it "permits main attributes" do
        expect(subject.permitted_attributes).to match_array [:title, :identifier_prefix, :modus_operandi, role_ids: []]
      end

      it "includes the domain in resolved scope" do
        expect(resolved_scope).to include(intervention_domain)
      end
    end

    Role::ALL.excluding(:administrator).each do |role|
      context "as a #{role}" do
        let(:user) { create(:user, role) }

        it { is_expected.to forbid_actions(:index, :new, :create, :edit, :update, :destroy) }

        it "permits no attribute" do
          expect(subject.permitted_attributes).to be_empty
        end

        it "cannot retrieve any from the database" do
          expect(resolved_scope).to be_empty
        end
      end
    end
  end

  describe "Display items" do
    context "regarding stream intervention domain" do
      Role::ALL.each do |role|
        context "as a #{role}" do
          let(:user) { create(:user, role) }

          context "regarding a domain she does not have access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream) }

            it { is_expected.to forbid_action(:show) }
          end

          context "regarding a domain she has access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream, roles: [user.role]) }

            it { is_expected.to permit_action(:show) }
          end
        end
      end
    end

    context "regarding appointments intervention domain" do
      Role::ALL.each do |role|
        context "as a #{role}" do
          let(:user) { create(:user, role) }

          context "regarding a domain with consultation types she does not have access to" do
            let(:intervention_domain) { create(:intervention_domain, :appointments) }
            before { create(:consultation_type, intervention_domain: intervention_domain) }

            it { is_expected.to forbid_action(:show) }
          end

          context "regarding a domain with consultation types she has access to" do
            let(:intervention_domain) { create(:intervention_domain, :appointments) }
            before { create(:consultation_type, intervention_domain: intervention_domain, roles: [user.role]) }

            it { is_expected.to permit_action(:show) }
          end
        end
      end
    end
  end


  describe "List archives" do
    context "regarding stream intervention domain" do
      [:administrator, :nurse, :secretary, :physician].each do |role|
        context "as a #{role}" do
          let(:user) { create(:user, role) }

          context "regarding a domain with consultation types she does not have access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream) }

            it { is_expected.to forbid_action(:archives_index) }
          end

          context "regarding a domain with consultation types she has access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream, roles: [user.role]) }

            it { is_expected.to permit_action(:archives_index) }
          end
        end
      end

      Role::ALL.excluding(:administrator, :nurse, :secretary, :physician).each do |role|
        context "as a #{role}" do
          let(:user) { create(:user, role) }

          context "regarding a domain with consultation types she does not have access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream) }

            it { is_expected.to forbid_action(:archives_index) }
          end

          context "regarding a domain with consultation types she has access to" do
            let(:intervention_domain) { create(:intervention_domain, :stream, roles: [user.role]) }

            it { is_expected.to forbid_action(:archives_index) }
          end
        end
      end
    end

    context "regarding appointments intervention domain" do
      Role::ALL.each do |role|
        context "as a #{role}" do
          let(:user) { create(:user, role) }

          context "regarding a domain she does not have access to" do
            let(:intervention_domain) { create(:intervention_domain, :appointments) }
            before { create(:consultation_type, intervention_domain: intervention_domain) }

            it { is_expected.to forbid_action(:archives_index) }
          end

          context "regarding a domain she has access to" do
            let(:intervention_domain) { create(:intervention_domain, :appointments, roles: [user.role]) }
            before { create(:consultation_type, intervention_domain: intervention_domain, roles: [user.role]) }

            it { is_expected.to forbid_action(:archives_index) }
          end
        end
      end
    end
  end
end
