# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe FlowingConsultationPolicy do
  ALL_ACTIONS = %i[new create edit update assign cancel validate list_certificates certificate list_attached_files]

  subject { described_class.new(user, flowing_consultation) }

  let(:flowing_consultation) { create(:flowing_consultation) }

  describe "Actions" do
    context "regarding an intervention domain she does not has access to" do
      context "as a visitor" do
        let(:user) { Visitor.new }

        it { is_expected.to forbid_all_actions }
      end

      Role::ALL.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to forbid_all_actions }
        end
      end
    end

    context "regarding an intervention domain she has access to" do
      let(:intervention_domain) { create(:intervention_domain, roles: [user.role], modus_operandi: :stream) }

      context "regarding a current flowing consultation" do
        let(:flowing_consultation) { create(:flowing_consultation, intervention_domain: intervention_domain) }
        ALLOWED_ACTIONS = {
          administrator: ALL_ACTIONS,
          association: [],
          nurse: ALL_ACTIONS,
          physician: ALL_ACTIONS,
          psychiatrist: [],
          psychiatrist_expert: [],
          psychologist: [],
          psychologist_expert: [],
          secretary: ALL_ACTIONS,
          switchboard_operator: %i[new create edit update assign validate cancel]
        }

        ALL_ACTIONS.each do |action|
          describe "##{action}?" do
            Role::ALL.each do |role|
              context "as a(n) #{role}" do
                let(:user) { create(:user, role) }

                if ALLOWED_ACTIONS[role].include?(action)
                  it { is_expected.to permit_action(action)}
                else
                  it { is_expected.to forbid_action(action)}
                end
              end
            end
          end
        end
      end

      context "regarding an archived flowing consultation" do
        let(:flowing_consultation) do
          create(:flowing_consultation, :archived, intervention_domain: intervention_domain)
        end

        %i[administrator nurse secretary].each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_action(:edit) }
          end
        end

        Role::ALL.excluding(%i[administrator nurse secretary]).each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_action(:edit) }
          end
        end
      end
    end
  end

  describe "Scope" do
    let(:resolved_scope) { described_class::Scope.new(user, FlowingConsultation.all).resolve }

    context "as a visitor" do
      let(:user) { Visitor.new }

      it "cannot retrieve any from the database" do
        expect(resolved_scope).to be_empty
      end
    end

    %i[administrator nurse secretary physician switchboard_operator].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "includes the flowing consultation in resolved scope" do
          expect(resolved_scope).to include(flowing_consultation)
        end
      end
    end

    Role::ALL.excluding(%i[administrator nurse secretary physician switchboard_operator]).each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "cannot retrieve any from the database" do
          expect(resolved_scope).to be_empty
        end
      end
    end
  end

  describe "Permitted parameters" do
    PERMITTED_ATTRIBUTES = [
      :id, :called_at, :comment, :concerns_minor,
      :consultation_type_id, :calling_institution_id,
      :assignee_id, :assigned_at,
      :patient_id,
      :cancelled_at, :cancelled_by,
      :validated_at,
      officer_attributes: [:id, :name, :phone, :email, :lambda_user],
      patient_attributes: [:id, :last_name, :first_name, :birthdate, :gender]
    ]

    context "as a visitor" do
      let(:user) { Visitor.new }

      it "permits no attribute" do
        expect(subject.permitted_attributes).to be_empty
      end
    end

    %i[administrator nurse secretary physician switchboard_operator].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "includes the flowing consultation in resolved scope" do
          expect(subject.permitted_attributes).to match_array(PERMITTED_ATTRIBUTES)
        end
      end
    end

    Role::ALL.excluding(%i[administrator nurse secretary physician switchboard_operator]).each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "cannot retrieve any from the database" do
          expect(subject.permitted_attributes).to be_empty
        end
      end
    end
  end
end
