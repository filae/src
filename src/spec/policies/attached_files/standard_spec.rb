# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe AttachedFile::StandardPolicy do
  subject { described_class.new(user, attached_file) }

  let(:attached_file) { create(:attached_file, attachable: concerned_item) }

  context "regarding an appointment" do
    let(:concerned_item) { create(:appointment) }

    context "the user has access to" do
      before { allow_any_instance_of(AppointmentPolicy).to receive(:show?).and_return(true) }

      Role::ALL.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to permit_actions(%i[list download preview create]) }
        end
      end

      context "and is in charge of" do
        let(:concerned_item) { create(:appointment, user_in_charge: user) }

        Role::ALL.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_actions(%i[destroy]) }
          end
        end
      end

      context "and is NOT in charge of" do
        ALLOWED_ROLES = %i[administrator nurse secretary]

        ALLOWED_ROLES.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_actions(%i[destroy]) }
          end
        end

        Role::ALL.excluding(ALLOWED_ROLES).each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_actions(%i[destroy]) }
          end
        end
      end
    end

    context "the user does not have access to" do
      before { allow_any_instance_of(AppointmentPolicy).to receive(:show?).and_return(false) }
      ALLOWED_ROLES = %i[administrator nurse secretary]

      ALLOWED_ROLES.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to permit_actions(%i[list download preview create]) }
        end
      end

      Role::ALL.excluding(ALLOWED_ROLES).each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to forbid_actions(%i[list download preview create]) }
        end
      end
    end
  end

  context "regarding a flowing consultation" do
    let(:concerned_item) { create(:flowing_consultation) }

    ALLOWED_ROLES = %i[administrator nurse secretary physician]

    ALLOWED_ROLES.each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it { is_expected.to permit_actions(%i[list download preview create]) }
      end
    end

    Role::ALL.excluding(ALLOWED_ROLES).each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

          it { is_expected.to forbid_actions(%i[list download preview create]) }
      end
    end

    describe ":destroy?" do
      ALLOWED_ROLES = %i[administrator nurse secretary]

      ALLOWED_ROLES.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to permit_actions(%i[destroy]) }
        end
      end

      context "as a physician" do
        let(:user) { create(:user, :physician) }

        context "who is in charge of" do
          let(:concerned_item) { create(:flowing_consultation, assignee: user, assigned_at: Time.zone.now) }

          it { is_expected.to permit_actions(%i[destroy]) }
        end

        context "who is NOT in charge of" do
          it { is_expected.to forbid_actions(%i[destroy]) }
        end
      end

      Role::ALL.excluding(ALLOWED_ROLES + %i[physician]).each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to forbid_actions(%i[destroy]) }
        end
      end
    end
  end
end
