# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe AttachedFile::ValidatedCertificatePolicy do
  subject { described_class.new(user, attached_file) }

  let(:attached_file) { create(:attached_file, attachable: concerned_item) }

  context "regarding a certificate" do
    context "of an appointment" do
      let(:concerned_item) { create(:certificate, :of_appointment) }

      context "the user has access to" do
        before { allow_any_instance_of(AppointmentPolicy).to receive(:list_certificates?).and_return(true) }
        FORBIDDEN_ROLES = %i[association switchboard_operator]

        Role::ALL.excluding(FORBIDDEN_ROLES).each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_actions(%i[list download preview]) }
          end
        end

        FORBIDDEN_ROLES.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_actions(%i[list download preview]) }
          end
        end
      end

      context "the user does not have access to" do
        before { allow_any_instance_of(AppointmentPolicy).to receive(:list_certificates?).and_return(false) }

        Role::ALL.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_actions(%i[list download preview]) }
          end
        end
      end

      Role::ALL.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to forbid_actions(%i[create destroy]) }
        end
      end
    end

    context "of a flowing_consultation" do
      let(:concerned_item) { create(:certificate, :of_flowing_consultation) }

      context "the user has access to" do
        before { allow_any_instance_of(FlowingConsultationPolicy).to receive(:list_certificates?).and_return(true) }
        FORBIDDEN_ROLES = %i[association switchboard_operator]

        Role::ALL.excluding(FORBIDDEN_ROLES).each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_actions(%i[list download preview]) }
          end
        end

        FORBIDDEN_ROLES.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_actions(%i[list download preview]) }
          end
        end
      end

      context "the user does not have access to" do
        before { allow_any_instance_of(FlowingConsultationPolicy).to receive(:list_certificates?).and_return(false) }

        Role::ALL.each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to forbid_actions(%i[list download preview]) }
          end
        end
      end

      Role::ALL.each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it { is_expected.to forbid_actions(%i[create destroy]) }
        end
      end
    end
  end
end
