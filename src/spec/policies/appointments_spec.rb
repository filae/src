# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

describe AppointmentPolicy do
  ALL_ACTIONS = %i[show new create edit update edit_archived postpone take_charge conclude certificate list_attached_files list_certificates]

  subject { described_class.new(user, appointment) }

  let(:appointment) { create(:appointment) }

  describe "Actions" do
    context "regarding a consultation type she does not has access to" do
      context "as a visitor" do
        let(:user) { Visitor.new }

        it { is_expected.to forbid_all_actions }
      end

      ALLOWED_ACTIONS = {
        administrator: %i[list_certificates list_attached_files],
        association: [],
        nurse: %i[list_certificates list_attached_files],
        physician: %i[list_certificates],
        psychiatrist: %i[list_certificates],
        psychiatrist_expert: %i[list_certificates],
        psychologist: %i[list_certificates],
        psychologist_expert: %i[list_certificates],
        secretary: %i[list_certificates list_attached_files],
        switchboard_operator: []
      }

      ALL_ACTIONS.each do |action|
        describe "##{action}?" do
          Role::ALL.each do |role|
            context "as a(n) #{role}" do
              let(:user) { create(:user, role) }

              if ALLOWED_ACTIONS[role].include?(action)
                it { is_expected.to permit_action(action)}
              else
                it { is_expected.to forbid_action(action)}
              end
            end
          end
        end
      end
    end

    context "regarding consultation type she has access to" do
      let(:consultation_type) { create(:consultation_type, roles: [user.role]) }
      let(:appointment) { create(:appointment, consultation_type: consultation_type) }

      ALLOWED_ACTIONS = {
        administrator: ALL_ACTIONS.excluding(%i[take_charge conclude]),
        association: ALL_ACTIONS.excluding(%i[edit_archived take_charge conclude list_certificates]),
        nurse: ALL_ACTIONS.excluding(%i[take_charge conclude]),
        physician: ALL_ACTIONS.excluding(%i[edit_archived take_charge conclude]),
        psychiatrist: ALL_ACTIONS.excluding(%i[edit_archived take_charge conclude]),
        psychiatrist_expert: %i[list_certificates list_attached_files],
        psychologist: ALL_ACTIONS.excluding(%i[edit_archived take_charge conclude]),
        psychologist_expert: ALL_ACTIONS.excluding(%i[edit_archived take_charge conclude]),
        secretary: ALL_ACTIONS.excluding(%i[take_charge conclude]),
        switchboard_operator: %i[list_attached_files]
      }

      ALL_ACTIONS.each do |action|
        describe "##{action}?" do
          Role::ALL.each do |role|
            context "as a(n) #{role}" do
              let(:user) { create(:user, role) }

              if ALLOWED_ACTIONS[role].include?(action)
                it { is_expected.to permit_action(action)}
              else
                it { is_expected.to forbid_action(action)}
              end
            end
          end
        end
      end

      context "and she can take charge" do
        let(:consultation_type) { create(:consultation_type, roles: [user.role], assigned_roles: [user.role]) }

        Role::ALL.excluding(%i[psychiatrist_expert switchboard_operator]).each do |role|
          context "as a(n) #{role}" do
            let(:user) { create(:user, role) }

            it { is_expected.to permit_actions(%i[take_charge conclude])}
          end
        end
      end
    end
  end

  describe "permitted attributes" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it "does not allow any change" do
        expect(subject.permitted_attributes).to be_empty
      end
    end

    context "users who cannot take charge" do
      Role::ALL.excluding(%i[psychiatrist_expert switchboard_operator]).each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it "allows edition" do
            expected = [
              :id, :duration, :emergency,
              :circumstances, :comment, :official_report,
              :cancelled_at, :cancelled_by,
              :calling_institution_id,
              :consultation_type_id,
              :consultation_subtype_id,
              :consultation_reason_id,
              :officer_id,
              :originating_appointment_id,
              :patient_arrived_at,
              :patient_id,
              :patient_missing,
              :patient_phone,
              :patient_released_at,
              :previous_appointment_id,
              :requisition_received_at,
              :starts_at,
              officer_attributes: [:id, :name, :phone, :email],
              patient_attributes: [:id, :last_name, :first_name, :birthdate, :gender]
            ]
            expect(subject.permitted_attributes).to match_array expected
          end
        end
      end

      %i[psychiatrist_expert switchboard_operator].each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it "does not allow any change" do
            expect(subject.permitted_attributes).to be_empty
          end
        end
      end
    end

    context "users who can take charge" do
      let(:consultation_type) { create(:consultation_type, roles: [user.role], assigned_roles: [user.role]) }
      let(:appointment) { create(:appointment, consultation_type: consultation_type) }

      Role::ALL.excluding(%i[psychiatrist_expert switchboard_operator]).each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it "allows edition and assignment" do
            expected = [
              :id, :duration, :emergency,
              :circumstances, :comment, :official_report,
              :cancelled_at, :cancelled_by,
              :calling_institution_id,
              :consultation_type_id,
              :consultation_subtype_id,
              :consultation_reason_id,
              :officer_id,
              :originating_appointment_id,
              :patient_arrived_at,
              :patient_id,
              :patient_missing,
              :patient_phone,
              :patient_released_at,
              :previous_appointment_id,
              :requisition_received_at,
              :starts_at,
              :user_in_charge_id,
              officer_attributes: [:id, :name, :phone, :email],
              patient_attributes: [:id, :last_name, :first_name, :birthdate, :gender]
            ]
            expect(subject.permitted_attributes).to match_array expected
          end
        end
      end

      %i[psychiatrist_expert switchboard_operator].each do |role|
        context "as a(n) #{role}" do
          let(:user) { create(:user, role) }

          it "does not allow any change" do
            expect(subject.permitted_attributes).to be_empty
          end
        end
      end
    end
  end

  describe "Scope" do
    let(:resolved_scope) { described_class::Scope.new(user, Appointment.all).resolve }
    before { appointment }

    context "as a visitor" do
      let(:user) { Visitor.new }

      it "cannot retrieve any from the database" do
        expect(resolved_scope).to be_empty
      end
    end

    Role::ALL.excluding(%i[psychiatrist_expert switchboard_operator]).each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "includes the appointment in resolved scope" do
          expect(resolved_scope).to include(appointment)
        end
      end
    end

    %i[psychiatrist_expert switchboard_operator].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create(:user, role) }

        it "cannot retrieve any from the database" do
          expect(resolved_scope).to be_empty
        end
      end
    end
  end
end
