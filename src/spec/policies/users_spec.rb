# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a connected user regarding users" do |role|
  context "regarding another user" do
    let(:concerned_user) { create(:user) }

    it { is_expected.to forbid_all_actions }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end
  end

  context "regarding herself" do
    let(:concerned_user) { user }

    it { is_expected.to permit_only_actions(:update) }

    it "permits some attributes" do
      expect(subject.permitted_attributes).to match_array [:password, :password_confirmation]
    end
  end

  it "cannot retrieve any from the database" do
    expect(resolved_scope).to be_empty
  end
end

describe UserPolicy do
  subject { described_class.new(user, concerned_user) }
  let(:resolved_scope) { described_class::Scope.new(user, User.all).resolve }

  context "as a visitor" do
    let(:concerned_user) { create(:user) }
    let(:user) { Visitor.new }

    it { is_expected.to forbid_all_actions }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end

    it "cannot retrieve any from the database" do
      expect(resolved_scope).to be_empty
    end
  end

  context "as an administrator" do
    let(:user) { create(:user, :administrator) }

    context "regarding another user" do
      let(:concerned_user) { create(:user) }

      # Actions
      it { is_expected.to permit_actions(:index, :new, :create, :edit, :update) }

      context "which is inactive" do
        let(:concerned_user) { create(:user, :inactive) }

        it { is_expected.to permit_action(:activate) }
        it { is_expected.to forbid_action(:deactivate) }
      end

      context "which is active" do
        let(:concerned_user) { create(:user) }

        it { is_expected.to permit_action(:deactivate) }
        it { is_expected.to forbid_action(:activate) }
      end

      context "which is scheduled for deactivation" do
        let(:concerned_user) { create(:user, :scheduled_for_deactivation) }

        it { is_expected.to permit_actions(:activate, :deactivate) }
      end

      # Permitted attributes
      it "permits main attributes" do
        expect(subject.permitted_attributes).to match_array [:identifier, :email, :first_name, :last_name, :disabled_at, :role_id]
      end

      # Scope
      it "includes the concerned user in resolved scope" do
        expect(resolved_scope).to include(concerned_user)
      end
    end

    context "regarding herself" do
      let(:concerned_user) { user }

      it { is_expected.to permit_only_actions(:index, :new, :create, :edit, :update) }

      it "permits all attributes" do
        expect(subject.permitted_attributes).to match_array [:identifier, :email, :first_name, :last_name, :disabled_at, :role_id, :password, :password_confirmation]
      end

      it "includes herself in resolved scope" do
        expect(resolved_scope).to include(user)
      end
    end

  end

  Role::ALL.excluding(:administrator).each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user regarding users" do
        let(:user) { create(:user, role) }
      end
    end
  end
end
