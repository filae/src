# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.shared_examples "a connected user regarding master documents for actions" do |role|
  it { is_expected.to permit_actions([:download]) }
  it { is_expected.to forbid_actions([:index, :new, :create, :edit, :update, :destroy]) }
end

describe MasterDocumentPolicy do
  subject { described_class.new(user, certificate_template) }

  let(:certificate_template) { create(:certificate_template) }

  context "regarding actions" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it { is_expected.to forbid_actions([:index, :new, :download, :create, :edit, :update, :destroy]) }
    end

    context "as an admin" do
      let(:user) { create(:user, :administrator) }

      it { is_expected.to permit_actions([:index, :new, :download, :create, :edit, :update, :destroy]) }
    end

    Role::ALL.excluding(:administrator).each do |role|
      context "as a #{role}" do
        let(:user) { create(:user, role) }

        it_behaves_like "a connected user regarding master documents for actions"
      end
    end
  end

end
