# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe CertificateMailer, type: :mailer do

  let(:patient) { create(:patient, first_name: "Camille", last_name: "Dupont")}
  let(:certificatable) { create(:flowing_consultation, patient: patient) }
  let(:certificate) { create(:certificate, certificatable: certificatable) }
  let!(:file) { create(:attached_file, type: "AttachedFile::ValidatedCertificate", attachable: certificate) }

  describe "send_to_officer" do

    subject { CertificateMailer.send_to_officer("moulin@pj.fr", certificate) }

    it "renders the headers" do
      expect(subject.subject).to match("[Filaé - TEST] primum.txt")
      expect(subject.to).to eq(["moulin@pj.fr"])
    end

    it "renders the body" do
      expect(subject.body.encoded).to match("DUPONT Camille")
    end

    describe "regarding flowing consultations" do
      it "uses FROM for stream intervention domains" do
        expect(subject.from).to eq(["stream-mailer@filae.tld"])
      end
    end

    describe "regarding appointments" do
      let(:certificatable) { create(:appointment, patient: patient) }

      it "uses FROM for appointments intervention domains" do
        expect(subject.from).to eq(["appointments-mailer@filae.tld"])
      end
    end
  end
end
