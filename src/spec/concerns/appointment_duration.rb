# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "spec_helper"

shared_examples_for "a model with an appointment duration" do
  let(:described_model) { described_class.name.underscore.to_sym }

  describe "validations" do
    describe "#appointment_duration_in_minutes" do
      it "is invalid when chars are provided" do
        # Given
        model = build(described_model, appointment_duration_in_minutes: "abc")
        # When
        result = model.valid?
        # Then
        expect(result).to be_falsy
        expect(model.errors[:appointment_duration_in_minutes]).to eq ["doit être un nombre entier"]
      end

      it "is invalid when negative integer is provided" do
        # Given
        model = build(described_model, appointment_duration_in_minutes: "-45")
        # When
        result = model.valid?
        # Then
        expect(result).to be_falsy
        expect(model.errors[:appointment_duration_in_minutes]).to eq ["doit être supérieur ou égal à 0"]
      end

      it "is invalid when empty string is provided" do
        # Given
        model = build(described_model, appointment_duration_in_minutes: "")
        # When
        result = model.valid?
        # Then
        expect(result).to be_falsy
        expect(model.errors[:appointment_duration_in_minutes]).to eq ["doit être rempli(e)"]
      end

      it "is valid when positive integer is provided" do
        # Given
        model = build(described_model, appointment_duration_in_minutes: "45")
        # When
        result = model.valid?
        # Then
        expect(result).to be_truthy
      end
    end
  end

  describe "behaviour" do
    describe "#appointment_duration_in_minutes=" do
      it "does nothing when no integer provided" do
        # Given
        duration = ActiveSupport::Duration.build(45)
        model = create(described_model, appointment_duration: duration)
        # When
        model.appointment_duration_in_minutes = "abc"
        # Then
        expect(model.appointment_duration).to eq duration
      end

      it "does nothing when negative integer is provided" do
        # Given
        duration = ActiveSupport::Duration.build(45)
        model = create(described_model, appointment_duration: duration)
        # When
        model.appointment_duration_in_minutes = "-45"
        # Then
        expect(model.appointment_duration).to eq duration
      end

      it "sets appointment_duration to nil when empty string is provided" do
        # Given
        duration = ActiveSupport::Duration.build(45)
        model = create(described_model, appointment_duration: duration)
        # When
        model.appointment_duration_in_minutes = ""
        # Then
        expect(model.appointment_duration).to be_nil
      end

      it "sets appointment_duration as an AcitveSupport::Duration when correct data provided" do
        # Given
        duration = ActiveSupport::Duration.build(45)
        model = create(described_model, appointment_duration: duration)
        # When
        model.appointment_duration_in_minutes = "45"
        # Then
        expect(model.appointment_duration).to eq ActiveSupport::Duration.build(60*45)
      end
    end

    describe "#appointment_duration_in_minutes" do
      it "returns nil when no appointment_duration is set" do
        # Given
        model = create(described_model, appointment_duration: nil)
        # When
        result = model.appointment_duration_in_minutes
        # Then
        expect(result).to be_nil
      end

      it "returns 45 when appointment_duration set to PT45M" do
        # Given
        model = create(described_model, appointment_duration: "PT45M")
        # When
        result = model.appointment_duration_in_minutes
        # Then
        expect(result).to eq 45
      end
    end
  end
end
