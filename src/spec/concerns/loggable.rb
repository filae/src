# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "spec_helper"

shared_examples_for "a loggable model" do
  let(:model) { create(described_class.name.downcase.to_sym) }

  describe "behaviour" do
    it "#logs returns all the action logs" do
      # Given
      user = create(:user)
      update_action = Log::Action.new("updated").to_json
      postpone_action = Log::Action.new("postponed").to_json
      cancel_action = Log::Action.new("cancelled").to_json
      log1 = Log.generate(model, user, Time.zone.now, [update_action, postpone_action])
      log2 = Log.generate(model, user, Time.zone.now, [cancel_action])
      # When
      result = model.logs
      # Then
      expect(result).to eq [log1, log2]
    end
  end
end
