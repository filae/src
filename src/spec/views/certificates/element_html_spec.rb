# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe "json_certificates/element_html", type: :view do

  describe "with simple text" do

    let(:element) {
      {
        "type" => "html",
        "name" => "question-html",
        "html" => "ceci est un texte sans balise"
      }
    }
    let(:expected) { "ceci est un texte sans balise" }

    it "renders it without any modification" do
      # Given
      # When
      render partial: "json_certificates/element_html", locals: { element: element, data: nil}
      # Then
      expect(rendered).to eq expected
    end
  end

  describe "with html text" do

    let(:element) {
      {
        "type" => "html",
        "name" => "question-html",
        "html" => "<p>ceci est un texte <span class=\"bold\">sans</span> balise"
      }
    }
    let(:expected) { "<p>ceci est un texte <span class=\"bold\">sans</span> balise" }

    it "renders it without any modification" do
      # Given
      # When
      render partial: "json_certificates/element_html", locals: { element: element, data: nil}
      # Then
      expect(rendered).to eq expected
    end
  end

  describe "with data interpolation" do

    let(:element) {
      {
        "type" => "html",
        "name" => "question-html",
        "html" => "ce texte est {filae-interpole} {filae-ici} et {filae-la}"
      }
    }
    let(:data) {
      {
        "filae-interpole" => "interpolé",
        "filae-ici" => "ici"
      }
    }
    let(:expected) { 'ce texte est interpolé ici et <span style="color: red">{filae-la}</span>' }

    it "renders it with text substitutions" do
      # Given
      # When
      render partial: "json_certificates/element_html", locals: { element: element, data: data}
      # Then
      expect(rendered).to eq expected
    end
  end
end
