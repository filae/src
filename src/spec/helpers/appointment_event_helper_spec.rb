# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe AppointmentEventHelper, type: :helper do
  describe "#time_slot" do
    it "returns a span with the start and end time" do
      # Given
      appointment = create(:appointment, starts_at: Time.current.change(hour: 10, minute: 0), duration: 90.minutes)
      # When
      result = Nokogiri::HTML(helper.time_slot(appointment))
      # Then
      expect(result.text).to eq("10:00 - 11:30")
    end
  end

  describe "#requisition_received" do
    describe "with a requisition received" do
      it "returns a span with an R with a space before" do
        # Given
        appointment = create(:appointment, requisition_received_at: Time.current)
        # When
        result = Nokogiri::HTML(helper.requisition_received(appointment))
        # Then
        expect(span = result.css("span")).to be_one
        expect(span.text).to eq(" R")
        expect(span.first["title"]).to eq("Réquisition reçue")
      end
    end

    describe "with a requisition yet" do
      it "returns nothing" do
        # Given
        appointment = create(:appointment, requisition_received_at: nil)
        # When
        result = helper.requisition_received(appointment)
        # Then
        expect(result).to be_nil
      end
    end
  end

  describe "#consultation_reason_sign" do
    describe "with a consultation reason" do
      it "returns the code within brackets followed by a space char" do
        # Given
        consultation_reason = create(:consultation_reason, title: "Bonne raison", code: "CR")
        appointment = create(:appointment, consultation_reason:)
        # When
        result = Nokogiri::HTML(helper.consultation_reason_sign(appointment))
        # Then
        expect(span = result.css("span")).to be_one
        expect(span.text).to eq("[CR] ")
        expect(span.first["title"]).to eq("Bonne raison")
      end
    end

    describe "with no consultation reason" do
      it "returns nothing" do
        # Given
        appointment = create(:appointment, consultation_reason: nil)
        # When
        result = helper.consultation_reason_sign(appointment)
        # Then
        expect(result).to be_nil
      end
    end
  end

  describe "#emergency_sign" do
    describe "with an emergency" do
      it "returns a span with an exclamation-circle icon" do
        # Given
        appointment = create(:appointment, emergency: true)
        # When
        result = Nokogiri::HTML(helper.emergency_sign(appointment))
        # Then
        expect(span = result.css("span.text-danger")).to be_one
        expect(icon = span.css("i.fa-exclamation-circle")).to be_one
        expect(icon.first["title"]).to eq("Urgence")
      end
    end

    describe "with a standard appointment" do
      it "returns nothing" do
        # Given
        appointment = create(:appointment, emergency: false)
        # When
        result = helper.emergency_sign(appointment)
        # Then
        expect(result).to be_nil
      end
    end
  end

  describe "#minority_sign" do
    describe "for an appointment concerning a minor patient" do
      it "returns a span with a child icon" do
        # Given
        patient = create(:patient, birthdate: Time.zone.today - 12.years)
        appointment = create(:appointment, patient:)
        # When
        result = Nokogiri::HTML(helper.minority_sign(appointment))
        # Then
        expect(span = result.css("span.patient_#{patient.id}_minority")).to be_one
        expect(icon = span.css("i.fa-child")).to be_one
        expect(icon.first["title"]).to eq("Personne mineure à la date du rendez-vous.")
      end
    end

    describe "for an appointment concerning a major patient" do
      it "returns nothing" do
        # Given
        appointment = create(:appointment, emergency: false)
        # When
        result = helper.minority_sign(appointment)
        # Then
        expect(result).to be_nil
      end
    end
  end

  describe "#patient_fullname" do
    it "returns a span with the patient fullname" do
      # Given
      patient = create(:patient)
      appointment = create(:appointment, patient:)
      # When
      result = Nokogiri::HTML(helper.patient_fullname(appointment))
      # Then
      expect(span = result.css("span.patient_#{patient.id}_fullname")).to be_one
      expect(span.text).to eq(patient.fullname)
    end
  end

  describe "#state_sign" do
    it "returns a span with the appointement state" do
      # Given
      appointment = create(:appointment)
      # When
      result = Nokogiri::HTML(helper.state_sign(appointment))
      # Then
      expect(icon = result.css("i.fa-user-clock")).to be_one
      expect(icon.first["title"]).to eq("Patient attendu")
    end
  end

  describe "#journey_span" do
    let!(:journey) do
      instance_double(Journey, title: "Consultation", abbr_title: "C", state: :patient_not_arrived, valid?: valid)
    end

    describe "with an invalid journey" do
      let(:valid) { false }

      it "renders nothing" do
        # Given
        # When
        result = helper.journey_span(journey)
        # Then
        expect(result).to be_nil
      end
    end

    describe "with a valid journey" do
      let(:valid) { true }

      describe "without an icon name" do
        before { allow(journey).to receive(:icon_name).and_return(nil) }

        it "renders the first letter within a span and a title" do
          # Given
          # When
          result = Nokogiri::HTML(helper.journey_span(journey))
          # Then
          expect(span = result.css(".Patient--unavailable")).to be_one
          expect(span.text).to eq "C"
        end
      end

      describe "with an icon name" do
        before { allow(journey).to receive(:icon_name).and_return("stethoscope") }

        it "renders the icon within a span and a title" do
          # Given
          # When
          result = Nokogiri::HTML(helper.journey_span(journey))
          # Then
          expect(span = result.css(".Patient--unavailable")).to be_one
          expect(span.text).to be_empty
          expect(span.children.css("i.fa-stethoscope")).to be_one
        end
      end
    end
  end

  describe "#full_journey_span" do
    let(:appointment) { create(:appointment) }
    let(:consultation) do
      instance_double(Journey, title: "Consultation", abbr_title: "C",
                               state: :patient_not_arrived, valid?: true, icon_name: nil, children: [])
    end

    before do
      allow(Journey).to receive(:full).with(appointment).and_return(consultation)
    end

    describe "with inactive root journey" do
      it "returns nothing" do
        # Given
        allow(consultation).to receive(:valid?).and_return(false)
        # When
        result = helper.full_journey_span(appointment)
        # Then
        expect(result).to be_nil
      end
    end

    describe "with active root journey" do
      let(:psychologist) do
        instance_double(Journey, title: "Psychologie", abbr_title: "P",
                                 state: :patient_not_arrived, valid?: true, icon_name: nil, children: [])
      end
      let(:jurist) do
        instance_double(Journey, title: "Juriste", abbr_title: "J",
                                 state: :patient_not_arrived, valid?: true, icon_name: nil, children: [])
      end
      let(:expert) do
        instance_double(Journey, title: "Expert", abbr_title: "E",
                                 state: :patient_not_arrived, valid?: true, icon_name: nil, children: [])
      end

      describe "for a stand-alone appointment" do
        it "returns a span with PatientJourney class and 'C' as the text" do
          # Given
          allow(consultation).to receive(:children).and_return([])
          # When
          result = Nokogiri::HTML(helper.full_journey_span(appointment))
          # Then
          expect(span = result.css("span.PatientJourney")).to be_one
          expect(span.text).to eq "C"
        end
      end

      describe "with complementary appointments" do
        it "returns a span with PatientJourney class and '(C » P & J)' as the text" do
          # Given
          allow(consultation).to receive(:children).and_return([psychologist, jurist])
          # When
          result = Nokogiri::HTML(helper.full_journey_span(appointment))
          # Then
          expect(span = result.css("span.PatientJourney")).to be_one
          expect(span.text).to eq "(C » P & J)"
        end
      end

      describe "with nested complementary appointments" do
        it "returns a span with PatientJourney class and '(C » P & (J » E))' as the text" do
          # Given
          allow(consultation).to receive(:children).and_return([psychologist, jurist])
          allow(jurist).to receive(:children).and_return([expert])
          # When
          result = Nokogiri::HTML(helper.full_journey_span(appointment))
          # Then
          expect(span = result.css("span.PatientJourney")).to be_one
          expect(span.text).to eq "(C » P & (J » E))"
        end
      end
    end
  end

  describe "#patient_class" do
    let(:journey) { instance_double(Journey) }

    {
      cancelled: "Patient--unavailable",
      certificate_completed: "Patient--unavailable",
      certificate_delivered: "Patient--out",
      certificate_validated: "Patient--out",
      patient_missing: "Patient--unavailable",
      patient_not_arrived: "Patient--unavailable",
      patient_released: "Patient--out",
      patient_waiting: "Patient--waiting",
      postponed: "Patient--unavailable",
      taken_in_charge: "Patient--taken-in-charge"
    }.each do |state, css_class|
      it "journey with state #{state} returns #{css_class}" do
        # Given
        allow(journey).to receive(:state).and_return(state)
        # When
        result = helper.patient_class(journey)
        # Then
        expect(result).to eq css_class
      end
    end
  end
end
