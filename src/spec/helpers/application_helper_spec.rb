# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "#on_two_lines" do
    it "splits a long line into two roughly length-egal lines" do
      # Given
      text = "Commissariat de police de Garches"
      # When
      result = helper.on_two_lines(text)
      # Then
      expect(result).to eq "Commissariat de <br />police de Garches"
      expect(result).to be_html_safe
    end

    it "splits on the last space before the half" do
      # Given
      text = "Gendarmerie de Saint-Germain-en-Laye"
      # When
      result = helper.on_two_lines(text)
      # Then
      expect(result).to eq "Gendarmerie de <br />Saint-Germain-en-Laye"
      expect(result).to be_html_safe
    end

    it "splits on any non-word character for composed words" do
      # Given
      text = "Saint-Germain-en-Laye"
      # When
      result = helper.on_two_lines(text)
      # Then
      expect(result).to eq "Saint-<br />Germain-en-Laye"
      expect(result).to be_html_safe
    end

    it "returns the whole text if non-word character can’t be found" do
      # Given
      text = "Rambouillet"
      # When
      result = helper.on_two_lines(text)
      # Then
      expect(result).to eq "Rambouillet"
    end

    it "changes nothing on missing or blank text" do
      # Given
      dummy_texts = [nil, "", " "]
      # When
      result = dummy_texts.map { |text| helper.on_two_lines(text) }
      # Then
      expect(result).to eq dummy_texts
    end
  end

  describe "#display_duration" do
    it "displays the translated duration with parenthesis by default" do
      # Given
      duration = ActiveSupport::Duration.build(123456789)
      # When
      result = helper.display_duration(duration)
      # Then
      expect(result).to eq "(3 ans, 10 mois, 4 semaines, 19 heures, 14 minutes, 33 secondes)"
    end

    it "displays the translated duration without parenthesis if asked to" do
      # Given
      duration = ActiveSupport::Duration.build(123456789)
      # When
      result = helper.display_duration(duration, with_parenthesis: false)
      # Then
      expect(result).to eq "3 ans, 10 mois, 4 semaines, 19 heures, 14 minutes, 33 secondes"
    end
  end

  it "#display_log displays a nice log summary" do
    # Given
    log = create(:log)
    # When
    result = helper.display_log(log)
    # Then
    expect(result).to match "fa-clock"
    expect(result).to match "fa-info-circle"
    expect(result).to match "Créé le"
    expect(result.downcase).to match "par #{log.who}".downcase
    expect(result).to match "created"
  end

  describe "#filae_version" do
    it "displays FILAE_VERSION env value if provided" do
      allow(ENV).to receive(:[]).with("FILAE_VERSION").and_return("main+12345")
      expect(helper.filae_version).to eq "main+12345"
    end

    it "displays 'N/A' if FILAE_VERSION not provided" do
      allow(ENV).to receive(:[]).with("FILAE_VERSION").and_return(nil)
      expect(helper.filae_version).to eq "N/A"
    end
  end

  describe "#fa_icon" do
    it "displays a solid icon as default" do
      expect(helper.fa_icon("home")).to eq '<i class="fa-solid fa-home"></i>'
    end

    it "displays a regular icon when requested" do
      expect(helper.fa_icon("home", style: "regular")).to eq '<i class="fa-regular fa-home"></i>'
    end

    it "has no title if not provided" do
      expect(helper.fa_icon("home")).to eq '<i class="fa-solid fa-home"></i>'
    end

    it "has a title if provided" do
      expect(helper.fa_icon("home", title: "Accueil")).to eq '<i class="fa-solid fa-home" title="Accueil"></i>'
    end
  end

  describe "#humanize_with_prefixed_count" do
    it '(User) returns "0 utilisateur"' do
      expect(helper.humanize_with_prefixed_count(User)).to eq "0 utilisateur"
    end

    it '(User, 1) returns "1 utilisateur"' do
      expect(helper.humanize_with_prefixed_count(User, 1)).to eq "1 utilisateur"
    end

    it '(User, 42) returns "42 utilisateurs' do
      expect(helper.humanize_with_prefixed_count(User, 42)).to eq "42 utilisateurs"
    end
  end
end
