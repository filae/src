# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe HomeHelper, type: :helper do
  describe "#greetings" do
    subject { helper.greetings(current_time) }

    {
      6 => "Bonne journée !",
      13 => "Bon après-midi !",
      18 => "Bonne soirée !",
      22 => "Bonne nuit !",
    }.each do |hour, message|
      describe "when at #{hour}h" do
        let(:current_time) { Time.current.change(hour: hour) }

        it { is_expected.to eq(message) }
      end
    end
  end
end
