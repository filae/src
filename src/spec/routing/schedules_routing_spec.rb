# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe SchedulesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/intervention_domains/42/schedules").to route_to("schedules#index", intervention_domain_id: "42")
    end


    it "routes to #show" do
      expect(:get => "/schedules/1").to route_to("schedules#show", id: "1")
    end


    it "routes to #create" do
      expect(:post => "/intervention_domains/42/schedules").to route_to("schedules#create", intervention_domain_id: "42")
    end

    it "routes to #create" do
      expect(:post => "/schedules").to route_to("schedules#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/schedules/1").to route_to("schedules#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/schedules/1").to route_to("schedules#update", id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/schedules/1").to route_to("schedules#destroy", id: "1")
    end

  end
end
