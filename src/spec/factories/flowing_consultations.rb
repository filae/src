# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :flowing_consultation do
    called_at { Time.zone.now - 3.hours }
    comment { nil }
    assigned_at { nil }
    validated_at { nil }
    cancelled_at { nil }
    officer { nil }
    patient { nil }
    intervention_domain
    consultation_type
    calling_institution { nil }
    assignee { nil }
    concerns_minor { false }

    trait :with_officer do
      officer
    end

    trait :archived do
      validated_at { (FlowingConsultation::TIPPING_DELAY + 1.hour).ago }
    end

    trait :just_a_simple_message do
      officer { build(:officer, :lambda_user) }
      consultation_type { nil }
    end

    trait :with_certificate do
      after(:create) do |flowing_consultation, evaluator|
        create_list(:certificate, 1, certificatable: flowing_consultation)
      end
    end
  end
end
