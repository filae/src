# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :certificate_template do
    title { Faker::Lorem.words(number: 3).join(" ") }

    trait :with_master_document do
      after(:build) do |certificate_template|
        certificate_template.master_document = build :master_document
      end
    end

    trait :with_questionnaire do
      questionnaire do
        {
          pages: [
            {
              name: Faker::Lorem.word,
              elements: [
                { type: "comment", name: Faker::Lorem.word }
              ]
            }
          ]
        }
      end
    end
  end
end
