# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :appointment do
    starts_at { Time.zone.now.change({ hour: 10, min: 0, sec: 0}) }
    duration { "PT60M" }
    official_report { "123456" }
    officer { nil }
    patient { nil }
    patient_phone { Faker::PhoneNumber.phone_number }
    circumstances { Faker::Lorem.sentence }
    comment { Faker::Lorem.sentence }
    consultation_type
    consultation_reason { nil }
    calling_institution { nil }
    consultation_subtype { nil }

    trait :cancelled do
      cancelled_at { Time.current }
    end

    trait :with_patient do
      patient
    end

    trait :with_certificate do
      after(:create) do |appointment|
        create(:certificate, certificatable: appointment)
      end
    end

    trait :with_signed_certificate do
      after(:create) do |appointment|
        create(:certificate, :signed, certificatable: appointment)
      end
    end

    trait :with_validated_certificate do
      after(:create) do |appointment|
        create(:certificate, :validated, certificatable: appointment)
      end
    end

    trait :with_sent_certificate do
      after(:create) do |appointment|
        create(:certificate, :sent, certificatable: appointment)
      end
    end
  end
end
