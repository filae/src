# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :certificate do
    questionnaire { nil }
    data { nil }
    delivered_at { "" }
    validated_at { "" }

    of_appointment

    trait :signed do
      after(:create) do |certificate|
        create(:attached_file, :signed, attachable:  certificate)
      end
    end

    trait :validated do
      signed
      validated_at { Time.zone.now }
      association :validator, factory: :user
      checksum { SecureRandom.uuid }
      after(:create) do |certificate|
        create(:attached_file, :validated, attachable:  certificate)
      end
    end

    trait :sent do
      validated
      delivered_at { Time.zone.now }
    end

    trait :of_appointment do
      association :certificatable, factory: :appointment
    end

    trait :of_flowing_consultation do
      association :certificatable, factory: :flowing_consultation
    end
  end
end
