# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe "Flowing consultations", type: :request do
  context "as a switchboard operator" do
    let!(:user) { connected_as_switchboard_operator }
    let!(:intervention_domain) { create(:intervention_domain, :stream, roles: [user.role]) }
    let!(:patient) { create(:patient) }
    let!(:consultation_type) { create(:consultation_type, intervention_domain: intervention_domain) }
    let!(:flowing_consultation) { create(:flowing_consultation, intervention_domain: intervention_domain, consultation_type: consultation_type, id: 42)}

    describe "POST /intervention_domains/:id/flowing_consultations" do
      let(:new_flowing_consultation_path) { "/intervention_domains/#{intervention_domain.id}/flowing_consultations" }
      let(:flowing_consultation_params) {
        {
          intervention_domain_id: intervention_domain.id,
          called_at: Time.zone.now,
          consultation_type_id: consultation_type.id,
        }
      }

      describe "regarding patient" do
        let(:patient_params) {
          {
            gender: nil,
            last_name: nil,
            first_name: nil,
            birth_year: nil
          }
        }

        it "creates a new patient with any patient data" do
          # Given
          patient_params[:birth_year] = 2023
          flowing_consultation_params[:patient_attributes] = patient_params
          expect {
            # When
            post new_flowing_consultation_path, params: { flowing_consultation: flowing_consultation_params }
            # Then
          }.to change(Patient, :count).by(1)
        end

        it "does not create a new patient with empty patient data" do
          # Given
          flowing_consultation_params[:patient_attributes] = patient_params
          expect {
            # When
            post new_flowing_consultation_path, params: { flowing_consultation: flowing_consultation_params }
            # Then
          }.not_to change(Patient, :count)
        end
      end

      describe "regarding officer" do
        let(:officer_params) {
          {
            email: nil,
            name: nil,
            phone: nil
          }
        }

        it "creates a new officer with any officer data" do
          # Given
          officer_params[:phone] = "+33"
          flowing_consultation_params[:officer_attributes] = officer_params
          expect {
            # When
            post new_flowing_consultation_path, params: { flowing_consultation: flowing_consultation_params }
            # Then
          }.to change(Officer, :count).by(1)
        end

        it "does not create a new officer with empty officer data" do
          # Given
          flowing_consultation_params[:officer_attributes] = officer_params
          expect {
            # When
            post new_flowing_consultation_path, params: { flowing_consultation: flowing_consultation_params }
            # Then
          }.not_to change(Officer, :count)
        end
      end
    end

    describe "PATCH /flowing_consultations/42" do
      describe "regarding assignment" do
        let!(:physician) { create(:user, :physician) }

        it "creates an 'assigned' log job on assignement" do
          # Given
          expected_action = Log::Action.new("assigned")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { assignee_id: physician.id, assigned_at: Time.zone.now }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'unassigned' log job on unassignement" do
          # Given
          flowing_consultation.update(assignee: physician, assigned_at: Time.zone.now)
          expected_action = Log::Action.new("unassigned")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { assignee_id: nil }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'reassigned' log job on reassignement" do
          # Given
          flowing_consultation.update(assignee: physician, assigned_at: Time.zone.now)
          new_physician = create(:user, :physician)
          expected_action = Log::Action.new("reassigned")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { assignee_id: new_physician.id }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end

      describe "regarding cancellation" do
        let(:now) { Time.zone.now }

        it "creates a 'cancelled' log job on cancellation" do
          # Given
          expected_action = Log::Action.new({ action: "cancelled", source: "custody-ended"})
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { cancelled_at: now, cancelled_by: "custody-ended" }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'uncancelled' log job on uncancellation" do
          # Given
          flowing_consultation.update(cancelled_at: now)
          expected_action = Log::Action.new("uncancelled")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { cancelled_at: nil }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'recancelled' log job when cancellation datetime changed" do
          # Given
          flowing_consultation.update(cancelled_at: now - 5.minutes)
          expected_action = Log::Action.new("recancelled")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { cancelled_at: now }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end

      describe "regarding validation" do
        let(:now) { Time.zone.now }

        it "creates a 'validated' log job on validation" do
          # Given
          expected_action = Log::Action.new("validated")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { validated_at: now }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates an 'unvalidated' log job on unvalidation" do
          # Given
          flowing_consultation.update(validated_at: now)
          expected_action = Log::Action.new("unvalidated")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { validated_at: nil }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end

        it "creates a 'revalidated' log job when validation datetime changed" do
          # Given
          flowing_consultation.update(validated_at: now - 5.minutes)
          expected_action = Log::Action.new("revalidated")
          expect {
            # When
            patch "/flowing_consultations/42", params: { flowing_consultation: { validated_at: now }}
            # Then
          }.to have_enqueued_job(LogJob)
          compare_log_event_to([expected_action])
        end
      end
    end
  end
end
