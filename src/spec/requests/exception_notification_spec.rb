# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe "Exception notifications", type: :request do
  context "When an error occurs" do
    describe "GET /" do
      it "sends an email with custom headers" do
        # Given
        allow_any_instance_of(HomeController).to receive(:show).and_raise("MyCustomError")
        expect {
          # When
          get root_path
          # Then
        }.to raise_error("MyCustomError").and change { ActionMailer::Base.deliveries.count }.by(1)

        notification_mail = ActionMailer::Base.deliveries.last
        expect(notification_mail.from).to match_array ["errors@filae.tld"]
        expect(notification_mail.to).to match_array ["support@filae.tld", "n1@filae.tld"]
        expect(notification_mail.subject).to match(/\[Filaé - TEST\]/)
        expect(notification_mail.body).to match(/MyCustomError/)
      end
    end
  end
end
