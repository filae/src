# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe "Patients", type: :request do
  describe "GET /index" do
    describe "pagination" do
      before do
        60.times { create(:patient) }
      end

      it "sets the size to default value when not in presets" do
        get "/patients", params: { size: 30 }
        expect(assigns(:size)).to eq 25
      end

      it "sets the size to desired value when wihtin presets" do
        size = Patient::PAGINATIONS.sample
        get "/patients", params: { size: size }
        expect(assigns(:size)).to eq size
      end

      context "with the default size of 25" do
        it "sets the page to the first one when out of range" do
          get "/patients", params: { page: 4 }
          expect(assigns(:page)).to eq 1
        end

        it "sets the page to the desired value when not out of range" do
          [1, 2, 3].each do |page|
            get "/patients", params: { page: page }
            expect(assigns(:page)).to eq page
          end
        end
      end
    end
  end
end
