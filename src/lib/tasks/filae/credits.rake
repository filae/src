# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "bundler"

namespace :filae do
  desc "Create credits file"
  task :credits => :environment do
    File.open("#{Rails.root}/../Credits.md", "w") do |file|
      file.puts header
      file.puts ruby_gems
      file.puts nodejs_modules
      file.puts other_tools
      file.puts footer
    end
  end

  private

  def header
    <<EOS
# Credits / Attributions

Filaé is a free software (AGPLv3) thanks to the many following libraries and tools provided by the community.

EOS
  end

  def ruby_gems
    first_level_gems = [].tap do |gems|
      File.foreach "#{Rails.root}/Gemfile" do |line|
        if match_data = /gem\s*['|"]([\w-]*)['|"](,|)/.match(line)
          gems << match_data[1]
        end
      end
    end.sort
    specs = Bundler.load.specs.select { |s| first_level_gems.include?(s.name) }
    "".tap do |text|
      unless specs.length == 0
        text << "Ruby gems:\n"
        specs.each do |spec|
          text << "- [#{spec.name}](#{spec.homepage || '#'}) : #{spec.summary} (**#{spec.author}**) [#{spec.license}]\n"
        end
      end
    end
  end

  def nodejs_modules
    json_specs = [].tap do |specs|
      `yarn info --manifest --json`.each_line do |line|
        specs << JSON.parse(line)
      end
    end.sort { |a, b| a["value"] <=> b["value"] }
    "".tap do |text|
      unless json_specs.length == 0
        text << "\nNodejs modules:\n"
        json_specs.each do |json_spec|
          node_name = json_spec["value"].split("@npm").first
          node_manifest = json_spec["children"]["Manifest"]
          node_license = node_manifest["License"]&.tr("()", "")
          node_homepage = node_manifest["Homepage"]
          text << "- [#{node_name}](#{ node_homepage || '#'}) [#{node_license}]\n"
        end
      end
    end
  end

  def other_tools
    <<EOS
\nWithout forgetting to mention:
- [ruby](https://www.ruby-lang.org/en/) : open source programming language with a focus on simplicity and productivity (**Yukihiro “Matz” Matsumoto**) [BSD 2-Clause License]
- [nodejs](https://nodejs.org/) : cross-platform, open-source JavaScript runtime environment (**OpenJS Foundation**) [MIT]
- [yarn](https://yarnpkg.com/) : JavaScript package manager [BSD 2-Clause License]
- [postgresql](https://www.postgresql.org/) : open source object-relational database systema (**PostgreSQL Global Development Group**) [PostgreSQL License]
- [LibreOffice writer](https://www.libreoffice.org/) : free and open-source office productivity software suite (**The Document Foundation**) [MPL-2.0]
- [wkhtmltopdf](https://wkhtmltopdf.org/) : open source command line tools to render HTML into PDF [LGPLv3]
- [docker engine](https://docs.docker.com/engine/) : open source containerization technology for building and containerizing applications (**Docker, Inc.**) [Apache License 2.0]
EOS
  end

  def footer
    <<EOS

Many thanks to those people, and many other developers and users beneath the radar, who have helped to make Filaé development possible <3
EOS
  end
end
