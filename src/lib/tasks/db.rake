# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2025 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
namespace :db do

  desc "List databases"
  task :list => :environment do
    puts existing_databases
  end

  desc "Cleans up databases"
  task :cleanup => :environment do
    raise "*** Use only on development environment ***" unless Rails.env.development?
    # Git branches
    current_branches = (`git for-each-ref --format='%(refname)' refs/heads/`).chomp.split(/\n/).map do |branch|
      branch.gsub(".", "_")
    end
    databases_to_keep = []
    current_branches.each do |branch|
      branch.gsub!(/refs\/heads\//, "")
      databases_to_keep << "filae_api_#{branch}"[0, 63]
      databases_to_keep << ("filae_api_#{branch}"[0, 58] + "_test")
    end
    # Filaé databases
    current_databases = existing_databases
    # Drop useless databases
    databases_to_drop = current_databases - databases_to_keep
    unless databases_to_drop.empty?
      puts "Dropping databases:"
      status = true
      databases_to_drop.each do |db_name|
        if status = pgsql_cmd(:dropdb, db_name)
          puts "  -> dropped: #{db_name}"
        else
          puts "  -> Error on dropping: #{db_name}"
        end
      end
      if status
        puts "Databases succesfully cleaned up \\o/"
      else
        puts "Some error(s) occured, please read above /o\\"
      end
    else
      puts "Your databases are clean \\o/"
    end
  end


  private

  def existing_databases
    psql_result =  pgsql_cmd(:psql, "-lt")
    databases = [].tap do |dbs|
      psql_result.each_line do |line|
        db_name = line.split("|")[0].strip
        dbs << db_name if db_name =~ /filae_api_/
      end
    end
    return databases
  end

  def pgsql_cmd(cmd, options)
    `PGPASSWORD=passw0rd #{cmd} --username postgres --host db #{options}`
  end
end
